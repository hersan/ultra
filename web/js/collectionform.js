/**
 * Created by hheredia on 20/01/14.
 */

(function($){

    $.fn.collectionform = function( options ){

        var settings = $.extend( {}, $.fn.collectionform.defaults, options );

        var error;

        $.fn.collectionform.defaults = {
            'addlink': '#add-training',
            'column': 'col-sm-4'
        };

        /*this.find('div.'+settings.column).each(function(){
           if(!$(this).val()){
               error++;
           }
        });*/

        this.find('div.'+settings.column).each(function() {
            addTagFormDeleteLink($(this));
        });

        var $holder = this.data('index', this.find('.'+settings.column ).length);

        $(settings.addlink).on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            //e.stopPropagation();

            // add a new tag form (see next code block)
            addTagForm($holder, settings.column);

            $('.selectpicker').selectpicker('refresh');
            $('.datepicker').datetimepicker({
                pickTime: false,
                language: 'es',
                format: 'YYYY-MM-DD',
                startDate: new Date('1-1-1920'),
                endDate: 'Infinite',
                defaultDate: ''
            });
            /*$('.date').datetimepicker({
                language: 'es',
                pickTime: false
            }).data("DateTimePicker").setDate(new Date("1/1/1920"));*/
        });
    };

    function addTagForm($collectionHolder, column) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = $(prototype.replace(/__name__/g, index));

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<div class="'+ column +'"></div>').append(newForm);
        //var $newFormLi = $('<div class="col-sm-6"></div>').append(newForm.html());
        $collectionHolder.append($newFormLi);
        //$newLinkLi.before($newFormLi);

        // add a delete link to the new form
        addTagFormDeleteLink($newFormLi);
    }

    function addTagFormDeleteLink($tagFormLi) {
        var $removeFormA = $('<a href="#"><span class="glyphicon glyphicon-minus-sign btn-xs"></span> Eliminar</a>');
        $tagFormLi.append($removeFormA);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the tag form
            $tagFormLi.remove();
        });
    }

}(jQuery));