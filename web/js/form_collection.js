(function($){

    $.fn.form_project = function( options ){

        var defaults = {
            addlink: '#add-training',
            column: 'col-sm-2',
            trash: 'Borrar',
            column_trash: 'col-sm-2'
        };

        var settings = $.extend( {}, defaults, options );

        this.find('div.col-sm-12 > div.row').each(function() {
            addTagFormDeleteLink($(this), settings.column_trash,settings.trash);

        });

        var $holder = this.data('index', this.find('div.col-sm-12 > div.row' ).length);

        $(settings.addlink).on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();
            //e.stopPropagation();

            // add a new tag form (see next code block)
            addTagForm($holder, 'col-sm-2', settings.trash);

            $('.selectpicker').selectpicker('refresh');

            e.stopPropagation();
        });
    };

    function addTagForm($collectionHolder, column, message) {
        // Get the data-prototype explained earlier
        var prototype = $collectionHolder.data('prototype');

        // get the new index
        var index = $collectionHolder.data('index');

        // Replace '__name__' in the prototype's HTML to
        // instead be a number based on how many items we have
        var newForm = $(prototype.replace(/__name__/g, index));

        // increase the index with one for the next item
        $collectionHolder.data('index', index + 1);

        // Display the form in the page in an li, before the "Add a tag" link li
        var $newFormLi = $('<div class="row"></div>').append(newForm);
        //var $newFormLi = $('<div class="col-sm-6"></div>').append(newForm.html());
        $collectionHolder.children().append($newFormLi);
        //$newLinkLi.before($newFormLi);

        // add a delete link to the new form
        addTagFormDeleteLink($newFormLi, column, message);
    }

    function addTagFormDeleteLink($tagFormLi, column, message) {
        var $removeFormA = $('<div class="'+column+'"><label>&nbsp;</label><a href="#" class="btn btn-default form-control"><span class="glyphicon glyphicon-trash"></span> '+message+'</a></div>');
        $tagFormLi.append($removeFormA);

        $removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the tag form
            $tagFormLi.remove();
        });
    }

}(jQuery));