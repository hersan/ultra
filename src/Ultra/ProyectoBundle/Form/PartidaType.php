<?php

namespace Ultra\ProyectoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PartidaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clavePartida','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Clave de partida',
                    'autocomplete'=>'on',
                    'maxlength'=>'50',
                    'title'=>'Ingresa la clave de la partida',
                    'autofocus'=>true),
                'label' => 'Clave',
                'required' => false
            ))
            ->add('tituloPartida','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Título de partida',
                    'autocomplete'=>'on',
                    'title'=>'Ingresa el título de la partida'),
                'label' => 'Titulo',
                'required' => false
            ))
            ->add('unidad','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Unidad',
                    'autocomplete'=>'on',
                    'maxlength'=>'50',
                    'title'=>'Ingresa la unidad de medida'),
                'label' => 'Unidad',
                'required' => false
            ))
            ->add('cantidad','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Cantidad',
                    'autocomplete'=>'on',
                    'maxlength'=>'10',
                    'title'=>'Ingresa la cantidad'),
                'label' => 'Cantidad',
                'required' => false
            ))
            ->add('precioUnitario','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Precio Unitario',
                    'autocomplete'=>'on',
                    'maxlength'=>'10',
                    'title'=>'Ingresa el precio unitario'),
                'label' => 'Precio Unitario',
                'required' => false
            ))
            ->add('importeTotal','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Importe total de la partida',
                    'autocomplete'=>'on',
                    'title'=>'Ingresa el importe total'),
                'label' => 'Importe Total',
                'required' => false
            ))
            ->add('disciplinas','entity', array(
                'class' => 'ControlDocumentoBundle:Disciplina',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Disciplina',
                'required' => false,
                'empty_value' => 'Selecciona la disciplina ...'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ProyectoBundle\Entity\Partida'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_proyectobundle_partida';
    }
}
