<?php

namespace Ultra\ProyectoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ProyectoBundle\Entity\Partida;
use Doctrine\ORM\EntityRepository;

class DetalleCfeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('partida','entity',array(
                'class' => 'ProyectoBundle:Partida',
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('par')
                            ->Where('par.nocontrato=3697');
                    },
                'label' => 'Partida',
                'required' => false,
                'multiple' => false,
                'empty_data' => null,
                'empty_value'=>'Seleccione ...',
                'attr' => array(
                    'class' => ' form-control selectpicker',
                    'data-live-search' => true,
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('descripcion','text',array(
                'label' => 'Detalle',
                'attr' => array(
                    'class' => 'form-control input-sm',
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('categoriaseguridad', 'choice', array(
                'label' => 'GC',
                'empty_value' => 'Selecciona...',
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => array('NRS' => 'NRS', 'RS' => 'RS'),
                'div_column' => 'col-sm-2'
            ))
            ->add('volumen','integer',array(
                'label' => 'Volumen',
                'attr' => array(
                    'class' => 'form-control input-sm',
                    'min' => 0
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('fechaprogfin','date', array(
                'label' => 'Fecha',
                'required' => false,
                'format' => 'yyyy-MM-dd',
                'div_column' => 'col-sm-2'
            ))
            //->add('fecharealfin','date')

            /*->add('sots','entity',array(
                    'class' => 'ProyectoBundle:OrdenTrabajo',
                    'label' => 'orden de trabajo',
                    'required' => false,
                    'multiple' => false,
                    'empty_data' => null,
                    'empty_value'=>'Selecione orden de trabajo...'
                ))*/
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_proyectobundle_detalle_cfe';
    }
}
