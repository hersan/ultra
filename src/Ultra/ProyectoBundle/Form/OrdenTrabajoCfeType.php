<?php

namespace Ultra\ProyectoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Null;
use Ultra\ControlDocumentoBundle\Entity\Disciplina;

class OrdenTrabajoCfeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Clave',
                    'autocomplete'=>'on',
                    'maxlength'=>'50',
                    'title'=>'Ingresa la clave de la orden',
                    'autofocus'=>true),
                'label' => 'Clave',
                'required' => false
            ))
            ->add('descripcion','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Descripción de la orden',
                    'autocomplete'=>'on',
                    'title'=>'Ingresa la descripción'
                ),
                'label'=>'Descripción de actividad (Tarea “B”)',
                'required' => false
            ))
            ->add('comentarios','textarea',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Comentarios de la SOT',
                    'autocomplete'=>'on',
                    'title'=>'Ingresa los comentarios'
                ),
                'label' => 'Comentarios',
                'required' => false
            ))
            ->add('unidadesTotales','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'autocomplete'=>'on',
                    'maxlength'=>'4',
                    'title'=>'Ingresa unidades totales',
                    'autofocus'=>true),
                'label' => 'Total de unidades',
                'required' => false
            ))
            ->add('nocartatransmision','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'autocomplete'=>'on',
                    'maxlength'=>'4',
                    'title'=>'Ingresa el # de carta de transmisión',
                    'autofocus'=>true),
                'label' => 'No. de carta de transmision',
                'required' => false
            ))
            ->add('nocd','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'autocomplete'=>'on',
                    'maxlength'=>'4',
                    'title'=>'Ingresa # cd',
                    'autofocus'=>true),
                'label' => '# cd',
                'required' => false
            ))
            ->add('tipotarea', 'choice', array(
                'label' => 'Tipo Tarea',
                'empty_value' => 'Selecciona',
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => array('A' => 'A', 'B' => 'B')
            ))

            ->add('fechaTransmision', 'date', array(
                'label' => 'Fecha de Transmisión',
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('fechaProgInicio', 'date', array(
                'label' => 'Fecha Inicio (programada)',
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('fechaProgFin', 'date', array(
                'label' => 'Fecha Fin (programada)',
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('fechaRealFin', 'date', array(
                'label' => 'Fecha Fin (real)',
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('file','file',array(
                'label' => 'Agregar Pdf',
            ))

            ->add('disciplina','entity',array(
                'class' => 'ControlDocumentoBundle:Disciplina',
                'label' => 'Disciplina',
                'empty_data' => null,
                'empty_value' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker'
                )
            ))
            ->add('estado','choice',array(
                'label' => 'Estado',
                'empty_data' => null,
                'empty_value' => 'Seleciona',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                ),
                'choices' => array('CONCLUIDO'=>'CONCLUIDO','PARCIALIDADES'=>'PARCIALIDADES','CANCELADO'=>'CANCELADO')
            ))
            ->add('codigo','text',array(
                'label' => 'Código de registro',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('planGc','entity',array(
                'class' => 'ProyectoBundle:PlanGc',
                'label' => 'Plan de G.C.',
                'empty_data' => null,
                'empty_value' => 'Seleciona',
                'multiple' => true,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'title' => 'Selecciona',
                )
            ))
            ->add('conteoSot','number', array(
                'label' => 'Conteo de hojas de la SOT',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('conteoEvidencias','number',array(
                'label' => 'Conteo de hojas de evidencia',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('fechaCartaTransmision','date',array(
                'label' => 'Fecha Carta de Transmisión',
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('detalles','collection',array(
                'required'=>false,
                'type' => new DetalleCfeType(),
                'label' => 'Detalle',
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
            ))
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ProyectoBundle\Entity\OrdenTrabajo',
            'required'=>false
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_ordentrabajo3697_type';
    }
}
