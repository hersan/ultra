<?php

namespace Ultra\ProyectoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ultra\ControlDocumentoBundle\Model\UploadFileInterface;
use Ultra\ControlDocumentoBundle\Model\UploadManagerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrdenTrabajo
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ProyectoBundle\Entity\OrdenTrabajoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class OrdenTrabajo implements UploadFileInterface, UploadManagerInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba una clave")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="clave", type="string", length=255)
     */
    private $clave;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="comentarios", type="string", length=255, nullable=true)
     */
    private $comentarios;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoTarea", type="string", length=255)
     */
    private $tipotarea;

    /**
     * @var integer
     * @ORM\Column(name="noCartaTransmision", type="integer", nullable=true)
     */
    private $nocartatransmision;

    /**
     * @var integer
     * @ORM\Column(name="noCd", type="integer", nullable=true)
     */
    private $nocd;

    /**
     * @var \DateTime
     * @Assert\NotNull(message="Seleccione")
     * @ORM\Column(name="fechaTransmision", type="date")
     */
    private $fechaTransmision;

    /**
     * @var integer
     * @ORM\Column(name="unidadesTotales", type="integer", nullable=true)
     */
    private $unidadesTotales;


    /**
     * @var \DateTime
     * @Assert\NotNull(message="Seleccione")
     * @ORM\Column(name="fechaProgInicio", type="date")
     */
    private $fechaProgInicio;

    /**
     * @var \DateTime
     * @ORM\Column(name="fechaProgFin", type="date", nullable=true)
     */
    private $fechaProgFin;

    /**
     * @var \DateTime
     * @ORM\Column(name="fechaRealFin", type="date", nullable=true)
     */
    private $fechaRealFin;

    /**
     * @var \DateTime
     * @ORM\Column(name="fechaTransmisionCd", type="date", nullable=true)
     */
    private $fechaTransmisionCd;

    /**
     * @var
     *
     * @ORM\Column(name="fecha_carta_transmision", type="date", nullable=true)
     */
    private $fechaCartaTransmision;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * @var
     *
     * @ORM\Column(name="estado", type="string", length=20, nullable=true)
     */
    private $estado;

    /**
     * @var
     *
     * @ORM\Column(name="codigo", type="string", length=10, nullable=true)
     */
    private $codigo;

    /**
     * @var
     *
     * @ORM\Column(name="conteo_sot", type="integer", nullable=true)
     */
    private $conteoSot;

    /**
     * @var
     *
     * @ORM\Column(name="conteo_evidencias", type="integer", nullable=true)
     */
    private $conteoEvidencias;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="DetalleOrdenTrabajo", mappedBy="sots", cascade={"persist"})
     */
    private $detalles;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina", inversedBy="proyectos")
     */
    private $disciplina;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Ultra\ProyectoBundle\Entity\PlanGc", inversedBy="ordenes")
     */
    private $planGc;

    /**
     * @var
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    private $file;

    private $temp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->detalles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->planGc = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return OrdenTrabajo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     * @return OrdenTrabajo
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;

        return $this;
    }

    /**
     * Get tipotarea
     *
     * @return string
     */
    public function getTipoTarea()
    {
        return $this->tipotarea;
    }

    /**
     * Set tipotarea
     *
     * @param string $tipoTarea
     * @return OrdenTrabajo
     */
    public function setTipoTarea($tipoTarea)
    {
        $this->tipotarea = $tipoTarea;

        return $this;
    }

    /**
     * Get unidadesTotales
     *
     * @return integer
     */
    public function getUnidadesTotales()
    {
        return $this->unidadesTotales;
    }

    /**
     * Set unidadesTotales
     *
     * @param integer $unidadesTotales
     * @return OrdenTrabajo
     */
    public function setUnidadesTotales($unidadesTotales)
    {
        $this->unidadesTotales = $unidadesTotales;
    
        return $this;
    }

    /**
     * Get noCartaTransmision
     *
     * @return integer
     */
    public function getnocartatransmision()
    {
        return $this->nocartatransmision;
    }

    /**
     * Set noCartaTransmision
     *
     * @param integer $noCartaTransmision
     * @return OrdenTrabajo
     */
    public function setNoCartaTransmision($noCartaTransmision)
    {
        $this->nocartatransmision=$noCartaTransmision;

        return $this;
    }

    /**
     * Get noCd
     *
     * @return integer
     */
    public function getNoCd()
    {
        return $this->nocd;
    }

    /**
     * Set noCd
     *
     * @param integer $noCd
     * @return OrdenTrabajo
     */
    public function setNoCd($noCd)
    {
        $this->nocd =$noCd;

        return $this;
    }

    /**
     * Get fechaTransmision
     *
     * @return \DateTime
     */
    public function getFechaTransmision()
    {
        return $this->fechaTransmision;
    }

    /**
     * Set fechaTransmision
     *
     * @param \DateTime $fechaTransmision
     * @return OrdenTrabajo
     */
    public function setFechaTransmision($fechaTransmision)
    {
        $this->fechaTransmision =$fechaTransmision;

        return $this;
    }

    /**
     * Get fechaTransmisionCd
     *
     * @return \DateTime
     */
    public function getFechaTransmisionCd()
    {
        return $this->fechaTransmisionCd;
    }

    /**
     * Set fechaTransmisionCd
     *
     * @param \DateTime $fechaTransmisionCd
     * @return OrdenTrabajo
     */
    public function setFechaTransmisionCd($fechaTransmisionCd)
    {
        $this->fechaTransmision =$fechaTransmisionCd;

        return $this;
    }

    /**
     * Get fechaProgFin
     *
     * @return \DateTime
     */
    public function getFechaProgFin()
    {
        return $this->fechaProgFin;
    }

    /**
     * Set fechaProgFin
     *
     * @param \DateTime $fechaProgFin
     * @return OrdenTrabajo
     */
    public function setFechaProgFin($fechaProgFin)
    {
        $this->fechaProgFin = $fechaProgFin;

        return $this;
    }

    /**
     * Get fechaRealFin
     *
     * @return \DateTime
     */
    public function getFechaRealFin()
    {
        return $this->fechaRealFin;
    }

    /**
     * Set fechaRealFin
     *
     * @param \DateTime $fechaRealFin
     * @return OrdenTrabajo
     */
    public function setFechaRealFin($fechaRealFin)
    {
        $this->fechaRealFin = $fechaRealFin;

        return $this;
    }

    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if(null !== $this->getFile())
        {
            $this->setPath($this->getClave().'-'.$this->getFechaProgInicio()->format('Y-m-d'));
            $this->setPdf($this->hashFile($this->file->getPathname()) . '.' . $this->file->guessExtension());
        }
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(isset($this->pdfCertificado)) {

            $this->setTemp($this->getAbsolutePath());
            //$this->setPath(null);
            $this->setPdf(null);

        } else {

            $this->setPdf('initial');
        }

        return $this;
    }

    /**
     * @param mixed $temp
     */
    private function setTemp($temp)
    {
        $this->temp = $temp;
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/'.$this->getPdf();
    }

    private function getUploadRootDir(){
        return '/var/sad/uploads/partidas/'. $this->getUploadDir();
    }

    private function getUploadDir(){
        return $this->getPath();
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Get clave
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return OrdenTrabajo
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get fechaProgInicio
     *
     * @return \DateTime
     */
    public function getFechaProgInicio()
    {
        return $this->fechaProgInicio;
    }

    /**
     * Set fechaProgInicio
     *
     * @param \DateTime $fechaProgInicio
     * @return OrdenTrabajo
     */
    public function setFechaProgInicio($fechaProgInicio)
    {
        $this->fechaProgInicio = $fechaProgInicio;

        return $this;
    }

    /*private function getClassName(){
        $class = explode('\\', get_class($this));
        return end($class);
    }*/

    private function hashFile($file){
        return sha1_file($file);
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return OrdenTrabajo
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->getPdf());

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getTemp());
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @return mixed
     */
    private function getTemp()
    {
        return $this->temp;
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Add detalles
     *
     * @param \Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo
     * @return OrdenTrabajo
     */
    public function addDetalle(DetalleOrdenTrabajo $detalles)
    {
        $detalles->setSots($this);
        $this->detalles[] = $detalles;
    
        return $this;
    }

    /**
     * Remove detalles
     *
     * @param DetalleOrdenTrabajo $detalles
     * @internal param $
     */
    public function removeDetalle(DetalleOrdenTrabajo $detalles)
    {
        $this->detalles->removeElement($detalles);
    }

    /**
     * Get detalles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDetalles()
    {
        return $this->detalles;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getConteoEvidencias()
    {
        return $this->conteoEvidencias;
    }

    /**
     * @param mixed $conteoEvidencias
     */
    public function setConteoEvidencias($conteoEvidencias)
    {
        $this->conteoEvidencias = $conteoEvidencias;
    }

    /**
     * @return mixed
     */
    public function getConteoSot()
    {
        return $this->conteoSot;
    }

    /**
     * @param mixed $conteoSot
     */
    public function setConteoSot($conteoSot)
    {
        $this->conteoSot = $conteoSot;
    }

    /**
     * @return mixed
     */
    public function getDisciplina()
    {
        return $this->disciplina;
    }

    /**
     * @param mixed $disciplina
     */
    public function setDisciplina($disciplina)
    {
        $this->disciplina = $disciplina;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getFechaCartaTransmision()
    {
        return $this->fechaCartaTransmision;
    }

    /**
     * @param mixed $fechaCartaTransmision
     */
    public function setFechaCartaTransmision($fechaCartaTransmision)
    {
        $this->fechaCartaTransmision = $fechaCartaTransmision;
    }

    /**
     * @return mixed
     */
    public function getPlanGc()
    {
        return $this->planGc;
    }

    /**
     * Add planGc
     *
     * @param \Ultra\ProyectoBundle\Entity\PlanGc $planGc
     * @return OrdenTrabajo
     */
    public function addPlanGc(\Ultra\ProyectoBundle\Entity\PlanGc $planGc)
    {
        $this->planGc[] = $planGc;
    
        return $this;
    }

    /**
     * Remove planGc
     *
     * @param \Ultra\ProyectoBundle\Entity\PlanGc $planGc
     */
    public function removePlanGc(\Ultra\ProyectoBundle\Entity\PlanGc $planGc)
    {
        $this->planGc->removeElement($planGc);
    }

    public function __toString()
    {
        return $this->getClave();
    }
}