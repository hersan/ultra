<?php

namespace Ultra\ProyectoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RequisicionPersonal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ProyectoBundle\Entity\RequisicionPersonalRepository")
 */
class RequisicionPersonal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="solicitado", type="date")
     */
    private $solicitado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="requerido", type="date")
     */
    private $requerido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="aprobado", type="date")
     */
    private $aprobado;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ProyectoBundle\Entity\Categoria")
     */
    private $categoria;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil")
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set solicitado
     *
     * @param \DateTime $solicitado
     * @return RequisicionPersonal
     */
    public function setSolicitado($solicitado)
    {
        $this->solicitado = $solicitado;
    
        return $this;
    }

    /**
     * Get solicitado
     *
     * @return \DateTime 
     */
    public function getSolicitado()
    {
        return $this->solicitado;
    }

    /**
     * Set requerido
     *
     * @param \DateTime $requerido
     * @return RequisicionPersonal
     */
    public function setRequerido($requerido)
    {
        $this->requerido = $requerido;
    
        return $this;
    }

    /**
     * Get requerido
     *
     * @return \DateTime 
     */
    public function getRequerido()
    {
        return $this->requerido;
    }

    /**
     * Set aprobado
     *
     * @param \DateTime $aprobado
     * @return RequisicionPersonal
     */
    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;
    
        return $this;
    }

    /**
     * Get aprobado
     *
     * @return \DateTime 
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }
}
