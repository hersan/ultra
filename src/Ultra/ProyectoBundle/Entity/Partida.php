<?php

namespace Ultra\ProyectoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Partida
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ProyectoBundle\Entity\PartidaRepository")
 */
class Partida
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba una clave para la partida")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="clavePartida", type="string", length=255)
     */
    private $clavePartida;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba un titulo para la partida")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="tituloPartida", type="text")
     */
    private $tituloPartida;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba la unidad")
     * @ORM\Column(name="unidad", type="string", length=255)
     */
    private $unidad; 

    /**
     * @var integer
     * @Assert\NotBlank(message = "Escriba la cantidad")
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba el precio unitario")
     * @ORM\Column(name="precioUnitario", type="decimal", scale=2 )
     */
    private $precioUnitario;

    /**
     * @var string
     * @Assert\NotBlank(message = "Escriba el total")
     * @ORM\Column(name="importeTotal", type="decimal", scale=2)
     */
    private $importeTotal;

    /**
     * @Assert\NotNull(message="Seleccione")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina")
     */
    private $disciplinas;

    /**
     * @var string
     * @ORM\Column(name="nocontrato", type="string", nullable=true)
     */
    private $nocontrato;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clavePartida
     *
     * @param string $clavePartida
     * @return Partida
     */
    public function setClavePartida($clavePartida)
    {
        $this->clavePartida = $clavePartida;
    
        return $this;
    }

    /**
     * Get clavePartida
     *
     * @return string 
     */
    public function getClavePartida()
    {
        return $this->clavePartida;
    }

    /**
     * Set tituloPartida
     *
     * @param string $tituloPartida
     * @return Partida
     */
    public function setTituloPartida($tituloPartida)
    {
        $this->tituloPartida = $tituloPartida;
    
        return $this;
    }

    /**
     * Get tituloPartida
     *
     * @return string 
     */
    public function getTituloPartida()
    {
        return $this->tituloPartida;
    }

    /**
     * Set unidad
     *
     * @param string $unidad
     * @return Partida
     */
    public function setUnidad($unidad)
    {
        $this->unidad = $unidad;
    
        return $this;
    }

    /**
     * Get unidad
     *
     * @return string 
     */
    public function getUnidad()
    {
        return $this->unidad;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return Partida
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set precioUnitario
     *
     * @param string $precioUnitario
     * @return Partida
     */
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;
    
        return $this;
    }

    /**
     * Get precioUnitario
     *
     * @return string 
     */
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set importeTotal
     *
     * @param string $importeTotal
     * @return Partida
     */
    public function setImporteTotal($importeTotal)
    {
        $this->importeTotal = $importeTotal;
    
        return $this;
    }

    /**
     * Get importeTotal
     *
     * @return string 
     */
    public function getImporteTotal()
    {
        return $this->importeTotal;
    }

    /**
     * Set nocontrato
     *
     * @param string $nocontrato
     * @return Partida
     */
    public function setNocontrato($nocontrato)
    {
        $this->nocontrato = $nocontrato;

        return $this;
    }

    /**
     * Get nocontrato
     *
     * @return string
     */
    public function getNocontrato()
    {
        return $this->nocontrato;
    }

    /**
     * Set disciplinas
     *
     * @param integer $disciplinas
     * @return disciplina
     */
    public function setDisciplinas($disciplinas)
    {
        $this->disciplinas = $disciplinas;

        return $this;
    }


    /**
     * Get disciplinas
     *
     * @return integer
     */
    public function getDisciplinas()
    {
        return $this->disciplinas;
    }

    public function __toString()
    {
        return $this->getClavePartida().' ( CFE-'.$this->getDisciplinas()->getArea()->getContrato()->getNumeroInterno().')';
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->partidas = new \Doctrine\Common\Collections\ArrayCollection();
    }
}