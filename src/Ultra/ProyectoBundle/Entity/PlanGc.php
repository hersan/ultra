<?php

namespace Ultra\ProyectoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanGc
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PlanGc
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Ultra\ProyectoBundle\Entity\OrdenTrabajo", mappedBy="planGc")
     */
    private $ordenes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return PlanGc
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ordenes = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add ordenes
     *
     * @param \Ultra\ProyectoBundle\Entity\OrdenTrabajo $ordenes
     * @return PlanGc
     */
    public function addOrdene(\Ultra\ProyectoBundle\Entity\OrdenTrabajo $ordenes)
    {
        $this->ordenes[] = $ordenes;
    
        return $this;
    }

    /**
     * Remove ordenes
     *
     * @param \Ultra\ProyectoBundle\Entity\OrdenTrabajo $ordenes
     */
    public function removeOrdene(\Ultra\ProyectoBundle\Entity\OrdenTrabajo $ordenes)
    {
        $this->ordenes->removeElement($ordenes);
    }

    /**
     * Get ordenes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrdenes()
    {
        return $this->ordenes;
    }

    public function __toString()
    {
        return $this->nombre;
    }
}