<?php

namespace Ultra\ProyectoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DetalleOrdenTrabajo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajoRepository")
 */
class DetalleOrdenTrabajo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255,nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaprogfin", type="date")
     */
    private $fechaprogfin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecharealfin", type="date", nullable=true)
     */
    private $fecharealfin;

    /**
     * @var integer
     *
     * @ORM\Column(name="volumen", type="integer")
     */
    private $volumen;

    /**
     * @var string
     *
     * @ORM\Column(name="categoriaSeguridad", type="string", length=255)
     */
    private $categoriaseguridad;

    /**
     * var integer
     *
     * Assert\NotBlank(message="Seleccione la partida...")
     * @ORM\ManyToOne(targetEntity="Partida")
     */
    private $partida;

    /**
     * var integer
     *
     * Assert\NotBlank(message="Seleccione la partida...")
     * @ORM\ManyToOne(targetEntity="OrdenTrabajo", inversedBy="detalles")
     */
    private $sots;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return DetalleOrdenTrabajo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaprogfin
     *
     * @param \DateTime $fechaprogfin
     * @return DetalleOrdenTrabajo
     */
    public function setFechaprogfin($fechaprogfin)
    {
        $this->fechaprogfin = $fechaprogfin;
    
        return $this;
    }

    /**
     * Get fechaprogfin
     *
     * @return \DateTime 
     */
    public function getFechaprogfin()
    {
        return $this->fechaprogfin;
    }

    /**
     * Set fecharealfin
     *
     * @param \DateTime $fecharealfin
     * @return DetalleOrdenTrabajo
     */
    public function setFecharealfin($fecharealfin)
    {
        $this->fecharealfin = $fecharealfin;
    
        return $this;
    }

    /**
     * Get fecharealfin
     *
     * @return \DateTime 
     */
    public function getFecharealfin()
    {
        return $this->fecharealfin;
    }

    /**
     * Set volumen
     *
     * @param integer $volumen
     * @return DetalleOrdenTrabajo
     */
    public function setVolumen($volumen)
    {
        $this->volumen = $volumen;
    
        return $this;
    }

    /**
     * Get volumen
     *
     * @return integer 
     */
    public function getVolumen()
    {
        return $this->volumen;
    }

    /**
     * Set categoriaseguridad
     *
     * @param string $categoriaseguridad
     * @return DetalleOrdenTrabajo
     */
    public function setCategoriaSeguridad($categoriaSeguridad)
    {
        $this->categoriaseguridad = $categoriaSeguridad;

        return $this;
    }

    /**
     * Get categoriaseguridad
     *
     * @return string
     */
    public function getCategoriaSeguridad()
    {
        return $this->categoriaseguridad;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->partidadatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param mixed $partida
     */
    public function setPartida($partida)
    {
        $this->partida = $partida;
    }

    /**
     * @return mixed
     */
    public function getPartida()
    {
        return $this->partida;
    }


    /**
     * Set sots
     *
     * @param \Ultra\ProyectoBundle\Entity\OrdenTrabajo $sots
     * @return DetalleOrdenTrabajo
     */
    public function setSots(\Ultra\ProyectoBundle\Entity\OrdenTrabajo $sots = null)
    {
        $this->sots = $sots;
    
        return $this;
    }

    /**
     * Get sots
     *
     * @return \Ultra\ProyectoBundle\Entity\OrdenTrabajo 
     */
    public function getSots()
    {
        return $this->sots;
    }
}