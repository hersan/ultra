<?php

namespace Ultra\ProyectoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo;
use Ultra\ProyectoBundle\Form\DetalleOrdenTrabajoType;

/**
 * DetalleOrdenTrabajo controller.
 *
 * @Route("/detalle_orden_trabajo")
 */
class DetalleOrdenTrabajoController extends Controller
{

    /**
     * Lists all DetalleOrdenTrabajo entities.
     *
     * @Route("/", name="detalle_orden_trabajo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ProyectoBundle:DetalleOrdenTrabajo')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new DetalleOrdenTrabajo entity.
     *
     * @Route("/", name="detalle_orden_trabajo_create")
     * @Method("POST")
     * @Template("ProyectoBundle:DetalleOrdenTrabajo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new DetalleOrdenTrabajo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('detalle_orden_trabajo_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a DetalleOrdenTrabajo entity.
    *
    * @param DetalleOrdenTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(DetalleOrdenTrabajo $entity)
    {
        $form = $this->createForm(new DetalleOrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('detalle_orden_trabajo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new DetalleOrdenTrabajo entity.
     *
     * @Route("/new", name="detalle_orden_trabajo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new DetalleOrdenTrabajo();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a DetalleOrdenTrabajo entity.
     *
     * @Route("/{id}", name="detalle_orden_trabajo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:DetalleOrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DetalleOrdenTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing DetalleOrdenTrabajo entity.
     *
     * @Route("/{id}/edit", name="detalle_orden_trabajo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:DetalleOrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DetalleOrdenTrabajo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a DetalleOrdenTrabajo entity.
    *
    * @param DetalleOrdenTrabajo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(DetalleOrdenTrabajo $entity)
    {
        $form = $this->createForm(new DetalleOrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('detalle_orden_trabajo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing DetalleOrdenTrabajo entity.
     *
     * @Route("/{id}", name="detalle_orden_trabajo_update")
     * @Method("PUT")
     * @Template("ProyectoBundle:DetalleOrdenTrabajo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:DetalleOrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find DetalleOrdenTrabajo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('detalle_orden_trabajo_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a DetalleOrdenTrabajo entity.
     *
     * @Route("/{id}", name="detalle_orden_trabajo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ProyectoBundle:DetalleOrdenTrabajo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find DetalleOrdenTrabajo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('detalle_orden_trabajo'));
    }

    /**
     * Creates a form to delete a DetalleOrdenTrabajo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('detalle_orden_trabajo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
