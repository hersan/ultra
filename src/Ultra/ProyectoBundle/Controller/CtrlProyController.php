<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 8/07/14
 * Time: 11:27 AM
 */


namespace Ultra\ProyectoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ProyectoBundle\Entity\Partida;
use Ultra\ProyectoBundle\Form\OrdenTrabajoCfeType;
use Ultra\ProyectoBundle\Form\CdocPartidaType;
use Doctrine\Common\Collections\ArrayCollection;
use Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo;
use Ultra\ProyectoBundle\Entity\OrdenTrabajo;
use Ultra\ProyectoBundle\Form\OrdenTrabajoType;

/**
 * Ctrlproy controller.
 *
 * @Route("/ctrlproy")
 */
class CtrlProyController extends Controller
{

    /**
     * Reporte general de todos los proyectos.
     *
     * @Route("/", name="repgeneral")
     * @Method("GET")
     * @Template("ProyectoBundle:Repproy:ctrlproy.html.twig")
     */
    public function indexAction()
    {
        //consultar detalles de sot y proyecto

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT o.clave cve, o.tipotarea tt, o.pdf pdf,o.id ido, o.unidadesTotales ut, o.fechaTransmision ft, p.precioUnitario, p.tituloPartida tit, p.clavePartida cp, d.volumen v, d.categoriaseguridad cs
             FROM ProyectoBundle:OrdenTrabajo o
             JOIN o.detalles d JOIN d.partida p JOIN p.disciplinas dis
              JOIN dis.area a JOIN a.contrato con
              WHERE con.abierto=1 and con.numeroInterno='3697'
            ");


        $sots = $query->getResult();
       // ladybug_dump_die($sots);

        return array(
            'sots' => $sots
        );

    return array();
    }

}