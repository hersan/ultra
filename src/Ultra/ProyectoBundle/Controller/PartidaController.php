<?php

namespace Ultra\ProyectoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ProyectoBundle\Entity\Partida;
use Ultra\ProyectoBundle\Form\PartidaType;

/**
 * Partida controller.
 *
 * @Route("/partida")
 */
class PartidaController extends Controller
{

    /**
     * Lists all Partida entities.
     *
     * @Route("/", name="partida")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ProyectoBundle:Partida')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Partida entity.
     *
     * @Route("/", name="partida_create")
     * @Method("POST")
     * @Template("ProyectoBundle:Partida:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Partida();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        //Agrego el campo del numero de contrato, primero consulto la disciplina
        $nointerno=$entity->getDisciplinas()->getArea()->getContrato()->getNumeroInterno();
        $entity->setNocontrato($nointerno);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La partida se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('partida_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La partida no pudo ser agregada.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Partida entity.
    *
    * @param Partida $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Partida $entity)
    {
        $form = $this->createForm(new PartidaType(), $entity, array(
            'action' => $this->generateUrl('partida_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Partida entity.
     *
     * @Route("/new", name="partida_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Partida();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Partida entity.
     *
     * @Route("/{id}", name="partida_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la partida.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Partida entity.
     *
     * @Route("/{id}/edit", name="partida_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se pudo encontrar la partida.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Partida entity.
    *
    * @param Partida $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Partida $entity)
    {
        $form = $this->createForm(new PartidaType(), $entity, array(
            'action' => $this->generateUrl('partida_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }
    /**
     * Edits an existing Partida entity.
     *
     * @Route("/{id}", name="partida_update")
     * @Method("PUT")
     * @Template("ProyectoBundle:Partida:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la partida.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la partida fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('partida_edit', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La partida no pudo ser modificada.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Partida entity.
     *
     * @Route("/{id}", name="partida_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la partida.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'La partida fue eliminada con éxito.'
            );
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'No se puede eliminar la partida pues tiene relación con otros datos.'
            );
        }

        return $this->redirect($this->generateUrl('partida'));
    }

    /**
     * Creates a form to delete a Partida entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('partida_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                'label' => 'Borrar',
                'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm();
    }
}
