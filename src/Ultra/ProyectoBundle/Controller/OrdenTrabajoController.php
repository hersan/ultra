<?php

namespace Ultra\ProyectoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo;
use Ultra\ProyectoBundle\Entity\OrdenTrabajo;
use Ultra\ProyectoBundle\Form\OrdenTrabajoType;

/**
 * OrdenTrabajo controller.
 *
 * @Route("/orden_trabajo")
 */
class OrdenTrabajoController extends Controller {

    /**
     * Lists all OrdenTrabajo entities.
     *
     * @Route("/", name="orden_trabajo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request) {
        //$em = $this->getDoctrine()->getManager();
        //$entities = $em->getRepository('ProyectoBundle:OrdenTrabajo')->findAll();      
        /* $entities = $this
          ->getDoctrine()
          ->getRepository('ProyectoBundle:OrdenTrabajo')
          ->getPosts();

          return $this->render('ProyectoBundle:OrdenTrabajo:index.html.twig', [
          'entities' => $entities
          ]); */ {
            $em = $this->getDoctrine()->getManager();
            $entities = $em->getRepository('ProyectoBundle:OrdenTrabajo')->findByPage(
                    $request->query->getInt('page', 1), 10);

            return $this->render('ProyectoBundle:OrdenTrabajo:index.html.twig', array(
                        'entities' => $entities
            ));
        }
    }

    
    /**
     * Creates a new OrdenTrabajo entity.
     *
     * @Route("/create", name="orden_trabajo_create")
     * @Template("ProyectoBundle:OrdenTrabajo:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new OrdenTrabajo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'La orden de trabajo se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('orden_trabajo_show', array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'La orden de trabajo no pudo ser agregada.'
            );
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a form to create a OrdenTrabajo entity.
     *
     * @param OrdenTrabajo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(OrdenTrabajo $entity) {
        $form = $this->createForm(new OrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('orden_trabajo_create'),
            'method' => 'POST',
        ));
        /*
          $form->add('oculto', 'hidden', array(
          'data' => $p,
          'mapped'=> false
          )); */

        /* $form->add('submit', 'submit', array('label' => 'Create')); */
        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title' => 'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));
        return $form;
    }

    /**
     * Displays a form to create a new OrdenTrabajo entity.
     *
     * @Route("/new", name="orden_trabajo_new")
     * @Template()
     */
    public function newAction() {
        $entity = new OrdenTrabajo();
        $entity->addDetalle(new DetalleOrdenTrabajo());
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a OrdenTrabajo entity.
     *
     * @Route("/{id}/show", name="orden_trabajo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la órden de trabajo.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing OrdenTrabajo entity.
     *
     * @Route("/{id}/edit", name="orden_trabajo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);
        //$entity = new OrdenTrabajo();

        if ($entity->getDetalles()->isEmpty()) {
            $entity->addDetalle(new DetalleOrdenTrabajo());
        }

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la orden de trabajo.');
        }

        $editForm = $this->createEditForm($entity);

        //$deleteForm = $this->createDeleteForm($id);


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
                //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a OrdenTrabajo entity.
     *
     * @param OrdenTrabajo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(OrdenTrabajo $entity) {
        $form = $this->createForm(new OrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('orden_trabajo_update', array('id' => $entity->getId())),
            'method' => 'PUT'
        ));

        /* $form->add('oculto', 'hidden', array(
          'data' => $p,
          'mapped'=> false
          )); */

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn btn-success',
                'title' => 'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));
        return $form;
    }

    /**
     * Edits an existing OrdenTrabajo entity.
     *
     * @Route("/{id}/update", name="orden_trabajo_update")
     * @Method("PUT")
     * @Template("ProyectoBundle:OrdenTrabajo:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        /* $oculto=$request->request->get('ultra_ordentrabajo_type');
          $p=$oculto['oculto']; */
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la orden de trabajo.');
        }

        $current = new ArrayCollection();
        foreach ($entity->getDetalles() as $detalle) {
            $current->add($detalle);
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach ($current as $detalle) {
                if (false === $entity->getDetalles()->contains($detalle)) {
                    $detalle->setSots(null);
                    $em->remove($detalle);
                }
            }

            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'success', 'Los datos de la orden de trabajo fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('orden_trabajo_show', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'La  orden de trabajo no pudo ser modificada.'
            );
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a OrdenTrabajo entity.
     *
     * @Route("/{id}/delete", name="orden_trabajo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la orden de trabajo.');
            }

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Poste entity.');
                }

                $em->remove($entity);
                $em->flush();

                $this->deleteIndex($entity);
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                    'success', 'La orden de trabajo fue eliminada con éxito.'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'No se puede eliminar la orden de trabajo.'
            );
        }

        return $this->redirect($this->generateUrl('orden_trabajo'));
    }

    /**
     * Creates a form to delete a OrdenTrabajo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('orden_trabajo_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'usubmit', array(
                            'label' => 'Borrar',
                            'attr' => array('class' => 'btn btn-warning',
                                'title' => 'Borrar datos'),
                            'glyphicon' => 'glyphicon glyphicon-trash'
                        ))
                        ->getForm();
    }

}
