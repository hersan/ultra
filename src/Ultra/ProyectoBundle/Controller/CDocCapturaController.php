<?php

namespace Ultra\ProyectoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ProyectoBundle\Entity\Partida;
use Ultra\ProyectoBundle\Form\CDocOrdenTrabajoType;
use Ultra\ProyectoBundle\Form\CdocPartidaType;
//use Ultra\ProyectoBundle\Form\PartidaType;
use Doctrine\Common\Collections\ArrayCollection;
use Ultra\ProyectoBundle\Entity\DetalleOrdenTrabajo;
use Ultra\ProyectoBundle\Entity\OrdenTrabajo;
use Ultra\ProyectoBundle\Form\OrdenTrabajoType;

/**
 * CDocCaptura controller.
 *
 * @Route("/cdoccaptura")
 */
class CDocCapturaController extends Controller
{

    /**
     * Lists all Partidas entities.
     *
     * @Route("/", name="cdocpartidas")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:partidas.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //Consulta que jala la disciplina de la partida, de esta el área y de esta el contrato->debe ser terminado

        $query = $em->createQuery(
            "SELECT pa FROM ProyectoBundle:Partida pa
              JOIN pa.disciplinas dis
              JOIN dis.area a JOIN a.contrato con
              WHERE con.abierto=0 and con.numeroInterno='3681'
            ");
        $entities=$query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all OrdenTrabajo entities.
     *
     * @Route("/ordenestrabajo", name="cdocorden_trabajo")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:ordentrabajo.html.twig")
     */
    public function ordentrabajoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ProyectoBundle:OrdenTrabajo')->findAll();

        //Consulta que jala la disciplina de la partida, de esta el área y de esta el contrato->debe ser terminado

        $query = $em->createQuery(
            "SELECT dot, sot.clave,sot.comentarios,sot.id,sot.tipotarea, sot.descripcion, sot.unidadesTotales, sot.fechaProgInicio,sot.fechaProgFin, sot.fechaRealFin,sot.pdf FROM ProyectoBundle:DetalleOrdenTrabajo dot
              JOIN dot.sots sot
              JOIN dot.partida pa
              JOIN pa.disciplinas dis
              JOIN dis.area a JOIN a.contrato con
              WHERE con.abierto=0 and con.numeroInterno='3681'
              GROUP BY sot.id
            ");
        $entities=$query->getResult();

        //ladybug_dump_die($entities);
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new OrdenTrabajo entity.
     *
     * @Route("/createorden", name="cdocorden_trabajo_create")
     * @Template("ProyectoBundle:CDocCaptura:ordentrabajonew.html.twig")
     */
    public function createordenAction(Request $request)
    {
        $entity = new OrdenTrabajo();
        $form = $this->createCreateOrdenForm($entity);

        $form->handleRequest($request);

        $unidadestotales=0;
        $mayor= new \DateTime('2000-01-01');

        $fechafin= null;
        $detallesorden=$entity->getDetalles();

        foreach ($detallesorden as $detalle)
        {
            $unidadestotales=$unidadestotales+$detalle->getVolumen();
            $fechafin=$detalle->getFechaProgFin();
            if ($fechafin>$mayor)
            {
                 $mayor=$fechafin;
            }
        }

        //ladybug_dump_die($mayor);
        $entity->setUnidadesTotales($unidadestotales);
        $entity->setFechaProgFin($mayor);
        $entity->setFechaRealFin($mayor);

        //ladybug_dump_die($form->isValid());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La orden de trabajo se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('cdocorden_trabajo_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La orden de trabajo no pudo ser agregada.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a form to create a OrdenTrabajo entity.
     *
     * @param OrdenTrabajo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateOrdenForm(OrdenTrabajo $entity)
    {
        $form = $this->createForm(new CDocOrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('cdocorden_trabajo_create'),
            'method' => 'POST',
        ));

        /*$form->add('submit', 'submit', array('label' => 'Create'));*/
        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));
        return $form;
    }

    /**
     * Displays a form to create a new OrdenTrabajo entity.
     *
     * @Route("/neworden", name="cdocorden_trabajo_new")
     * @Template()
     */
    public function ordentrabajonewAction()
    {
        $entity = new OrdenTrabajo();
        $entity->addDetalle(new DetalleOrdenTrabajo());
        $form   = $this->createCreateOrdenForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a OrdenTrabajo entity.
     *
     * @Route("/{id}/showorden", name="cdocorden_trabajo_show")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:ordentrabajoshow.html.twig")
     */
    public function showOrdenAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la órden de trabajo.');
        }

        return array(
            'entity'      => $entity,

        );
    }

    /**
     * Displays a form to edit an existing OrdenTrabajo entity.
     *
     * @Route("/{id}/ordenedit", name="cdocorden_trabajo_edit")
     * @Method("GET")
     * @Template()
     */
    public function ordentrabajoeditAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);
        //$entity = new OrdenTrabajo();

        if($entity->getDetalles()->isEmpty()){
            $entity->addDetalle(new DetalleOrdenTrabajo());
        }

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la orden de trabajo.');
        }

        $editForm = $this->createEditOrdenForm($entity);


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );
    }

    /**
     * Creates a form to edit a OrdenTrabajo entity.
     *
     * @param OrdenTrabajo $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditOrdenForm(OrdenTrabajo $entity)
    {
        $form = $this->createForm(new CDocOrdenTrabajoType(), $entity, array(
            'action' => $this->generateUrl('cdocorden_trabajo_update', array('id' => $entity->getId())),
            'method' => 'PUT'
        ));


        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));
        return $form;
    }
    /**
     * Edits an existing OrdenTrabajo entity.
     *
     * @Route("/{id}/ordenupdate", name="cdocorden_trabajo_update")
     * @Method("PUT")
     * @Template("ProyectoBundle:CDocCaptura:ordentrabajoedit.html.twig")
     */
    public function updateordenAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:OrdenTrabajo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la orden de trabajo.');
        }

        $current = new ArrayCollection();
        foreach($entity->getDetalles() as $detalle)
        {
            $current->add($detalle);
        }

        $editForm = $this->createEditOrdenForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach($current as $detalle)
            {
                if(false === $entity->getDetalles()->contains($detalle)){
                    $detalle->setSots(null);
                    $em->remove($detalle);
                }
            }

            //calculo y mando los datos que faltan
            $unidadestotales=0;
            $mayor= new \DateTime('2000-01-01');

            $fechafin= null;
            $detallesorden=$entity->getDetalles();

            foreach ($detallesorden as $detalle)
            {
                $unidadestotales=$unidadestotales+$detalle->getVolumen();
                $fechafin=$detalle->getFechaProgFin();
                if ($fechafin>$mayor)
                {
                    $mayor=$fechafin;
                }
            }

            //ladybug_dump_die($mayor);
            $entity->setUnidadesTotales($unidadestotales);
            $entity->setFechaProgFin($mayor);
            $entity->setFechaRealFin($mayor);

            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la orden de trabajo fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('cdocorden_trabajo_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La  orden de trabajo no pudo ser modificada.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }


//---------------------------------------PARTIDAS--------------------------------------------------------------------
    /**
     * Creates a new Partida entity.
     *
     * @Route("/", name="cdocpartida_create")
     * @Method("POST")
     * @Template("ProyectoBundle:CDocCaptura:partidanew.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Partida();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        //Agrego el campo del numero de contrato
        $entity->setNocontrato(3681);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La partida se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('cdocpartida_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La partida no pudo ser agregada.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Partida entity.
     *
     * @param Partida $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Partida $entity)
    {
        $form = $this->createForm(new CdocPartidaType(), $entity, array(
            'action' => $this->generateUrl('cdocpartida_create'),
            'method' => 'POST',
        ));

        //hago la consulta para traer las disciplinas que necesito
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT dis FROM ControlDocumentoBundle:Disciplina dis
              JOIN dis.area a JOIN a.contrato con
              WHERE con.abierto=0 and con.numeroInterno='3681'
            ");
        $disciplinas=$query->getResult();

        //creo el campo que me hace falta con el arrayCollection que tiene las disciplinas
        $form->add('disciplinas', 'entity',array(
            'class'=>"ControlDocumentoBundle:Disciplina",
            'choices'=>$disciplinas,
            'label' => 'Disciplina',
            'required' => false,
            'empty_value' => 'Selecciona la disciplina ...',
            'attr'=> array('class' => 'selectpicker',
                'data-live-search' => true)
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Partida entity.
     *
     * @Route("/new", name="cdocpartida_new")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:partidanew.html.twig")
     */
    public function newAction()
    {
        $entity = new Partida();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Partida entity.
     *
     * @Route("/{id}/show", name="cdocpartida_show")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:partidasshow.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la partida.');
        }



        return array(
            'entity'      => $entity,
        );
    }

    /**
     * Displays a form to edit an existing Partida entity.
     *
     * @Route("/{id}/cdocedit", name="cdocpartida_edit")
     * @Method("GET")
     * @Template("ProyectoBundle:CDocCaptura:partidasedit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se pudo encontrar la partida.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );
    }

    /**
     * Creates a form to edit a Partida entity.
     *
     * @param Partida $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Partida $entity)
    {
        $form = $this->createForm(new CdocPartidaType(), $entity, array(
            'action' => $this->generateUrl('cdocpartida_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //hago la consulta para traer las disciplinas que necesito
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT dis FROM ControlDocumentoBundle:Disciplina dis
              JOIN dis.area a JOIN a.contrato con
              WHERE con.abierto=0 and con.numeroInterno='3681'
            ");
        $disciplinas=$query->getResult();

        //creo el campo que me hace falta con el arrayCollection que tiene las disciplinas
        $form->add('disciplinas', 'entity',array(
            'class'=>"ControlDocumentoBundle:Disciplina",
            'choices'=>$disciplinas,
            'label' => 'Disciplina',
            'required' => false,
            'empty_value' => 'Selecciona la disciplina ...',
            'attr'=> array('class' => 'selectpicker',
                'data-live-search' => true)
        ));


        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }
    /**
     * Edits an existing Partida entity.
     *
     * @Route("/{id}", name="cdocpartida_update")
     * @Method("PUT")
     * @Template("ProyectoBundle:CDocCaptura:partidasedit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProyectoBundle:Partida')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la partida.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la partida fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('cdocpartida_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La partida no pudo ser modificada.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );
    }

}
