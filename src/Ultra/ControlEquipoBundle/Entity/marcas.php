<?php

namespace Ultra\ControlEquipoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * marcas
 *
 * @ORM\Table(name="AE_marcas")
 * @ORM\Entity(repositoryClass="Ultra\ControlEquipoBundle\Entity\marcasRepository")
 */
class marcas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Asigne una marca comercial...")
     * @ORM\Column(name="marca", type="string", length=255)
     */
    private $marca;


    public function __toString()
        {
            return $this->marca;
        }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return marcas
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    
        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

}