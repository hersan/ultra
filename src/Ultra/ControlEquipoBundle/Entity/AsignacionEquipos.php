<?php

namespace Ultra\ControlEquipoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * AsignacionEquipos
 *
 * @ORM\Table( name="AE_AsignacionEquipos"  )
 * @ORM\Entity(repositoryClass="Ultra\ControlEquipoBundle\Entity\AsignacionEquiposRepository")
 */
class AsignacionEquipos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Elija una persona...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $curriculaId;

    /**
     * @Assert\NotBlank(message="Elija un equipo de la lista de equipos...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlEquipoBundle\Entity\Equipos")
     */
    private $equipoId;

    /**
     * @var string
     * @Assert\NotBlank(message="Describa las áreas autorizadas...")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="areasAutorizadas", type="string", length=255)
     */
    private $areasAutorizadas;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="Seleccione una fecha")
     * @Assert\Type(type="datetime", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="vigencia", type="date")
     */
    private $vigencia;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set equipoId
     *
     * @param integer $equipoId
     * @return AsignacionEquipos
     */
    public function setEquipoId($equipoId)
    {
        $this->equipoId = $equipoId;
    
        return $this;
    }

    /**
     * Get equipoId
     * @return integer
     */
    public function getEquipoId()
    {
        return $this->equipoId;
    }

    /**
     * Set areasAutorizadas
     *
     * @param string $areasAutorizadas
     * @return AsignacionEquipos
     */
    public function setAreasAutorizadas($areasAutorizadas)
    {
        $this->areasAutorizadas = $areasAutorizadas;
    
        return $this;
    }

    /**
     * Get areasAutorizadas
     *
     * @return string 
     */
    public function getAreasAutorizadas()
    {
        return $this->areasAutorizadas;
    }

    /**
     * Set vigencia
     *
     * @param \DateTime $vigencia
     * @return AsignacionEquipos
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;
    
        return $this;
    }

    /**
     * Get vigencia
     *
     * @return \DateTime 
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }


    /**
     * Set curriculaId
     *
     * @param integer $curriculaId
     * @return AsignacionEquipos
     */
    public function setCurriculaId($curriculaId)
    {
        $this->curriculaId = $curriculaId;
    
        return $this;
    }

    /**
     * Get curriculaId
     *
     * @return integer 
     */
    public function getCurriculaId()
    {
        return $this->curriculaId;
    }
}