<?php

namespace Ultra\ControlEquipoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Equipos
 *
 * @ORM\Table( name="AE_Equipos")
 * @ORM\Entity(repositoryClass="Ultra\ControlEquipoBundle\Entity\EquiposRepository")
 */
class Equipos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Elija el tipo de equipo ...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlEquipoBundle\Entity\tiposdeEquipos")
     */
    private $tipoequipoId;

    /**
     * @Assert\NotBlank(message="Elija la marca ...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlEquipoBundle\Entity\marcas")
     */
    private $marcaId;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Digite el modelo...")
     * @ORM\Column(name="modelo", type="string", length=255)
     */
    private $modelo;

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el número de serie...")
     * @ORM\Column(name="serie", type="string", length=255)
     */
    private $serie;

    /**
     * @var string
     * @Assert\NotBlank(message="Elija un estado...")
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var string
     * @ORM\Column(name="servicetag", type="string", length=255, nullable=true)
     */
    private $servicetag;

    /**
     * @var string
     * @ORM\Column(name="comentario", type="string", length=255, nullable=true)
     */
    private $comentario;


    public function __toString()
    {
        return $this->tipoequipoId." ".$this->marcaId." Modelo ".$this->modelo."\n Serie: ". $this->serie;
//        "\n Estado:".$this->getEstado()."\n Comentario: ".$this->comentario;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoequipoId
     *
     * @param integer $tipoequipoId
     * @return Equipos
     */
    public function setTipoequipoId($tipoequipoId)
    {
        $this->tipoequipoId = $tipoequipoId;
    
        return $this;
    }

    /**
     * Get tipoequipoId
     *
     * @return integer 
     */
    public function getTipoequipoId()
    {
        return $this->tipoequipoId;
    }

    /**
     * Set marcaId
     *
     * @param integer $marcaId
     * @return Equipos
     */
    public function setMarcaId($marcaId)
    {
        $this->marcaId = $marcaId;
    
        return $this;
    }

    /**
     * Get marcaId
     *
     * @return integer 
     */
    public function getMarcaId()
    {
        return $this->marcaId;
    }

    /**
     * Set modelo
     *
     * @param string $modelo
     * @return Equipos
     */
    public function setModelo($modelo)
    {
        $this->modelo = $modelo;
    
        return $this;
    }

    /**
     * Get modelo
     *
     * @return string 
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    /**
     * Set serie
     *
     * @param string $serie
     * @return Equipos
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    
        return $this;
    }

    /**
     * Get serie
     *
     * @return string 
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Equipos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set servicetag
     *
     * @param string $servicetag
     * @return Equipos
     */
    public function setServicetag($servicetag)
    {
        $this->servicetag = $servicetag;

        return $this;
    }

    /**
     * Get servicetag
     *
     * @return string
     */
    public function getServicetag()
    {
        return $this->servicetag;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     * @return Equipos
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }



}