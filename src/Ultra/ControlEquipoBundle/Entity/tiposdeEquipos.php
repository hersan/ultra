<?php

namespace Ultra\ControlEquipoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * tiposdeEquipos
 *
 * @ORM\Table(name="AE_tiposdeEquipos")
 * @ORM\Entity(repositoryClass="Ultra\ControlEquipoBundle\Entity\tiposdeEquiposRepository")
 */
class tiposdeEquipos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el tipo de equipo...")
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return tiposdeEquipos
     */
    public function setTipo($tipo)
    {
        $this->tipo= $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    public function __toString()
    {
        return $this->tipo;
    }

}
