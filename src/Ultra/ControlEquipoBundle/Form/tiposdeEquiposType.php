<?php

namespace Ultra\ControlEquipoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class tiposdeEquiposType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipo',null, array("label"=>"Tipo de equipo:", 'attr' => array ( "class"=>"form-control input-sm") ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlEquipoBundle\Entity\tiposdeEquipos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlequipobundle_tiposdeequipos';
    }
}
