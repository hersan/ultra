<?php

namespace Ultra\ControlEquipoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class marcasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marca', null, array( "label"=>"Marca:", 'attr' => array ('class' => 'form-control input-sm')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlEquipoBundle\Entity\marcas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlequipobundle_marcas';
    }
}
