<?php

namespace Ultra\ControlEquipoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EquiposType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipoequipoId','entity',array(
                'required' => false,
                'class' => 'ControlEquipoBundle:tiposdeEquipos',
                'label' => 'Tipo de Equipo',
                'empty_value'=>'Selecciona el tipo de equipo',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('marcaId','entity',array(
                'required' => false,
                'class' => 'ControlEquipoBundle:marcas',
                'label' => 'Marca',
                'empty_value'=>'Selecciona la marca',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('modelo', null, array( "label"=>"Modelo:", 'attr' => array ('class' => 'form-control input-sm')))
            ->add('serie', null, array( "label"=>"Número de serie:", 'attr' => array ('class' => 'form-control input-sm')))
            ->add('estado', 'choice', array(
                'label' => 'Estado',
                'empty_value' => 'Seleccionar...',
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => array(
                'Fuera de operación' => 'Fuera de operación',
                'Disponible'=>'Disponible',
                'En operación'=>'En operación',
                'No localizado'=>'No localizado',
                'Enviado a oficina de Veracruz'=>'Enviado a oficina de Veracruz',
                'En operación por reemplazo de equipo descompuesto'=>'En operación por reemplazo de equipo descompuesto',
                )
            ))
            ->add('servicetag', null, array( "label"=>"Etiqueta de servicio (Service Tag):", 'attr' => array ('class' => 'form-control input-sm')))
            ->add('comentario', null, array( "label"=>"Comentario:", 'attr' => array ('class' => 'form-control input-sm')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlEquipoBundle\Entity\Equipos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlequipobundle_equipos';
    }
}
