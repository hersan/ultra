<?php

namespace Ultra\ControlEquipoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsignacionEquiposType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('equipoId','entity',array(
                'required' => false,
                'class' => 'ControlEquipoBundle:Equipos',
                'label' => 'Equipo',
                'empty_value'=>'Selecciona el equipo',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('CurriculaId','entity', array(
                'class' => 'ControlDocumentoBundle:Curricula',
                'label'=> 'Currícula',
                'empty_value'=>'Selecciona al responsable',
                'required' => false,
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true
                )))
            ->add('areasAutorizadas', null,
                array(
                    "label"=>"Áreas autorizadas",
                    'attr' => array ('class' => 'form-control input-sm')))
            ->add('vigencia','date',
                array(
                    "label"=>"Vigencia",
                    'input' => 'datetime',
                    'label' => 'Fecha de vigencia',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'required' => false
                    ))
             ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlEquipoBundle\Entity\AsignacionEquipos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlequipobundle_asignacionequipos';
    }
}
