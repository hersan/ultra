<?php

namespace Ultra\ControlEquipoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlEquipoBundle\Entity\marcas;
use Ultra\ControlEquipoBundle\Form\marcasType;

/**
 * marcas controller.
 *
 * @Route("/AE/marcas")
 */
class marcasController extends Controller
{

    /**
     * Lists all marcas entities.
     *
     * @Route("/", name="AE_marcas")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlEquipoBundle:marcas')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new marcas entity.
     *
     * @Route("/", name="AE_marcas_create")
     * @Method("POST")
     * @Template("ControlEquipoBundle:marcas:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new marcas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('AE_marcas_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a marcas entity.
    *
    * @param marcas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(marcas $entity)
    {
        $form = $this->createForm(new marcasType(), $entity, array(
            'action' => $this->generateUrl('AE_marcas_create'),
            'method' => 'POST',
        ));

  //        $form->add('submit', 'submit', array('label' => 'Create'));
$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));
        return $form;
    }

    /**
     * Displays a form to create a new marcas entity.
     *
     * @Route("/new", name="AE_marcas_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new marcas();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a marcas entity.
     *
     * @Route("/{id}", name="AE_marcas_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:marcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find marcas entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing marcas entity.
     *
     * @Route("/{id}/edit", name="AE_marcas_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:marcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find marcas entity.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a marcas entity.
    *
    * @param marcas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(marcas $entity)
    {
        $form = $this->createForm(new marcasType(), $entity, array(
            'action' => $this->generateUrl('AE_marcas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing marcas entity.
     *
     * @Route("/{id}", name="AE_marcas_update")
     * @Method("PUT")
     * @Template("ControlEquipoBundle:marcas:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:marcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la marca.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('AE_marcas_edit', array('id' => $id)));
             return $this->redirect($this->generateUrl('AE_marcas_show', array('id' => $entity->getId())));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a marcas entity.
     *
     * @Route("/{id}", name="AE_marcas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlEquipoBundle:marcas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find marcas entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('AE_marcas'));
    }

    /**
     * Creates a form to delete a marcas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('AE_marcas_delete', array('id' => $id)))
            ->setMethod('DELETE')
//            ->add('submit', 'submit', array('label' => 'Delete'))
           ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
