<?php

namespace Ultra\ControlEquipoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlEquipoBundle\Entity\AsignacionEquipos;
use Ultra\ControlEquipoBundle\Form\AsignacionEquiposType;

/**
 * AsignacionEquipos controller.
 *
 * @Route("/AE")
 */
class AsignacionEquiposController extends Controller
{

    /**
     * Lists all AsignacionEquipos entities.
     *
     * @Route("/", name="AE")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlEquipoBundle:AsignacionEquipos')->findAll();

        return array(
            'entities' => $entities
            //'datoscurricula'=>$datos_curricula,
        );
    }
    /**
     * Creates a new AsignacionEquipos entity.
     *
     * @Route("/", name="AE_create")
     * @Method("POST")
     * @Template("ControlEquipoBundle:AsignacionEquipos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AsignacionEquipos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();



      $this->get('session')->getFlashBag()->add(
                'success',
                'Asignación realizada:'
            );



            return $this->redirect($this->generateUrl('AE_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a AsignacionEquipos entity.
    *
    * @param AsignacionEquipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(AsignacionEquipos $entity)
    {

       $form = $this->createForm(new AsignacionEquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create'));
$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;


    }

    /**
     * Displays a form to create a new AsignacionEquipos entity.
     *
     * @Route("/new", name="AE_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AsignacionEquipos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a AsignacionEquipos entity.
     *
     * @Route("/{id}", name="AE_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:AsignacionEquipos')->find($id);

        $datos_curricula = $em->getRepository('ControlDocumentoBundle:Curricula')->find( $entity->getCurriculaId()  );


        if ( ! $datos_curricula ){
            throw $this->createNotFoundException('JF: Imposible encontrar la currícula ..');
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsignacionEquipos entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
          //  'delete_form' => $deleteForm->createView(),
            'datoscurricula' => $datos_curricula,
        );
    }

    /**
     * Displays a form to edit an existing AsignacionEquipos entity.
     *
     * @Route("/{id}/edit", name="AE_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:AsignacionEquipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsignacionEquipos entity.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a AsignacionEquipos entity.
    *
    * @param AsignacionEquipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(AsignacionEquipos $entity)
    {
        $form = $this->createForm(new AsignacionEquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success pull-left',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing AsignacionEquipos entity.
     *
     * @Route("/{id}", name="AE_update")
     * @Method("PUT")
     * @Template("ControlEquipoBundle:AsignacionEquipos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:AsignacionEquipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AsignacionEquipos entity.');
        }

       // $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('AE_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('AE_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a AsignacionEquipos entity.
     *
     * @Route("/{id}", name="AE_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlEquipoBundle:AsignacionEquipos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AsignacionEquipos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('AE'));
    }

    /**
     * Creates a form to delete a AsignacionEquipos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('AE_delete', array('id' => $id)))
            ->setMethod('DELETE')
//            ->add('submit', 'submit', array('label' => 'Delete'))
           ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
