<?php

namespace Ultra\ControlEquipoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlEquipoBundle\Entity\Equipos;
use Ultra\ControlEquipoBundle\Form\EquiposType;

/**
 * Equipos controller.
 *
 * @Route("/AE/equipos")
 */
class EquiposController extends Controller
{

    /**
     * Lists all Equipos entities.
     *
     * @Route("/", name="AE_equipos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlEquipoBundle:Equipos')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Equipos entity.
     *
     * @Route("/", name="AE_equipos_create")
     * @Method("POST")
     * @Template("ControlEquipoBundle:Equipos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Equipos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

       // ladybug_dump_die($entity);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('AE_equipos_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Equipos entity.
    *
    * @param Equipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Equipos $entity)
    {
        $form = $this->createForm(new EquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_equipos_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));
        return $form;
    }

    /**
     * Displays a form to create a new Equipos entity.
     *
     * @Route("/new", name="AE_equipos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Equipos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Equipos entity.
     *
     * @Route("/{id}", name="AE_equipos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:Equipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el equipo.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        return array(
            'entity'      => $entity,
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Equipos entity.
     *
     * @Route("/{id}/edit", name="AE_equipos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:Equipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el equipo.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Equipos entity.
    *
    * @param Equipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Equipos $entity)
    {
        $form = $this->createForm(new EquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_equipos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing Equipos entity.
     *
     * @Route("/{id}", name="AE_equipos_update")
     * @Method("PUT")
     * @Template("ControlEquipoBundle:Equipos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:Equipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el equipo.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('AE_equipos_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('AE_equipos_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Equipos entity.
     *
     * @Route("/{id}", name="AE_equipos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlEquipoBundle:Equipos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar el equipo.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('AE_equipos'));
    }

    /**
     * Creates a form to delete a Equipos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('AE_equipos_delete', array('id' => $id)))
            ->setMethod('DELETE')
           ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
