<?php

namespace Ultra\ControlEquipoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlEquipoBundle\Entity\tiposdeEquipos;
use Ultra\ControlEquipoBundle\Form\tiposdeEquiposType;

/**
 * tiposdeEquipos controller.
 *
 * @Route("/AE/tipos")
 */
class tiposdeEquiposController extends Controller
{

    /**
     * Lists all tiposdeEquipos entities.
     *
     * @Route("/", name="AE_tipos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlEquipoBundle:tiposdeEquipos')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new tiposdeEquipos entity.
     *
     * @Route("/", name="AE_tipos_create")
     * @Method("POST")
     * @Template("ControlEquipoBundle:tiposdeEquipos:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new tiposdeEquipos();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('AE_tipos_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a tiposdeEquipos entity.
    *
    * @param tiposdeEquipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(tiposdeEquipos $entity)
    {
        $form = $this->createForm(new tiposdeEquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_tipos_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create'));
$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));
        return $form;
    }

    /**
     * Displays a form to create a new tiposdeEquipos entity.
     *
     * @Route("/new", name="AE_tipos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new tiposdeEquipos();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a tiposdeEquipos entity.
     *
     * @Route("/{id}", name="AE_tipos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:tiposdeEquipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tiposdeEquipos entity.');
        }

       // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
//            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing tiposdeEquipos entity.
     *
     * @Route("/{id}/edit", name="AE_tipos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:tiposdeEquipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tiposdeEquipos entity.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a tiposdeEquipos entity.
    *
    * @param tiposdeEquipos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(tiposdeEquipos $entity)
    {
        $form = $this->createForm(new tiposdeEquiposType(), $entity, array(
            'action' => $this->generateUrl('AE_tipos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        //$form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing tiposdeEquipos entity.
     *
     * @Route("/{id}", name="AE_tipos_update")
     * @Method("PUT")
     * @Template("ControlEquipoBundle:tiposdeEquipos:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlEquipoBundle:tiposdeEquipos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find tiposdeEquipos entity.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            //return $this->redirect($this->generateUrl('AE_tipos_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('AE_tipos_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a tiposdeEquipos entity.
     *
     * @Route("/{id}", name="AE_tipos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlEquipoBundle:tiposdeEquipos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find tiposdeEquipos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('AE_tipos'));
    }

    /**
     * Creates a form to delete a tiposdeEquipos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('AE_tipos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
