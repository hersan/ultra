<?php

namespace Ultra\ControlCLVBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlCLVBundle\Entity\CatalogoDosimetros;
use Ultra\ControlCLVBundle\Form\CatalogoDosimetrosType;

/**
 * CatalogoDosimetros controller.
 *
 * @Route("/CLV/catalogodosimetros")
 */
class CatalogoDosimetrosController extends Controller
{

    /**
     * Lists all CatalogoDosimetros entities.
     *
     * @Route("/", name="CLV_catalogodosimetros")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlCLVBundle:CatalogoDosimetros')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new CatalogoDosimetros entity.
     *
     * @Route("/", name="CLV_catalogodosimetros_create")
     * @Method("POST")
     * @Template("ControlCLVBundle:CatalogoDosimetros:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CatalogoDosimetros();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('CLV_catalogodosimetros_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CatalogoDosimetros entity.
     *
     * @param CatalogoDosimetros $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CatalogoDosimetros $entity)
    {
        $form = $this->createForm(new CatalogoDosimetrosType(), $entity, array(
            'action' => $this->generateUrl('CLV_catalogodosimetros_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new CatalogoDosimetros entity.
     *
     * @Route("/new", name="CLV_catalogodosimetros_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CatalogoDosimetros();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CatalogoDosimetros entity.
     *
     * @Route("/{id}", name="CLV_catalogodosimetros_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:CatalogoDosimetros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CatalogoDosimetros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CatalogoDosimetros entity.
     *
     * @Route("/{id}/edit", name="CLV_catalogodosimetros_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:CatalogoDosimetros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CatalogoDosimetros entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CatalogoDosimetros entity.
    *
    * @param CatalogoDosimetros $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CatalogoDosimetros $entity)
    {
        $form = $this->createForm(new CatalogoDosimetrosType(), $entity, array(
            'action' => $this->generateUrl('CLV_catalogodosimetros_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));

        return $form;
    }





    /**
     * Edits an existing CatalogoDosimetros entity.
     *
     * @Route("/{id}", name="CLV_catalogodosimetros_update")
     * @Method("PUT")
     * @Template("ControlCLVBundle:CatalogoDosimetros:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:CatalogoDosimetros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CatalogoDosimetros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('CLV_catalogodosimetros_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CatalogoDosimetros entity.
     *
     * @Route("/{id}", name="CLV_catalogodosimetros_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlCLVBundle:CatalogoDosimetros')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CatalogoDosimetros entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('CLV_dosimetro'));
    }

    /**
     * Creates a form to delete a CatalogoDosimetros entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('CLV_catalogodosimetros_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //            ->add('submit', 'submit', array('label' => 'Delete'))
           ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
            ))
            ->getForm()
        ;
    }


}
