<?php

namespace Ultra\ControlCLVBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlCLVBundle\Entity\Conteo;
use Ultra\ControlCLVBundle\Entity\Dosimetro;
use Ultra\ControlCLVBundle\Form\ConteoType;

/**
 * Conteo controller.
 *
 * @Route("/CLV/conteo")
 */
class ConteoController extends Controller
{
    /**
     *
     * Lists all Conteo entities.
     *
     * @Route("/", name="CLV_conteo")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $limite=new \DateTime();

        $limite->modify("+2 months");

        $query = $em->createQuery("
        SELECT c.id,c.fechaConteo,c.fechaVigencia,c.dosis,e.nISD,u.id id1,u.nombre,u.apellidoPaterno, u.apellidoMaterno
        FROM ControlCLVBundle:Conteo c JOIN c.dosimetroId d JOIN d.dosimetroId e JOIN d.curriculaId u
        GROUP BY u.nombre
            ");

        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        return array(
            'result'=>$result
        );
    }

    /**
     * Creates a new Conteo entity.
     *
     * @Route("/create", name="CLV_conteo_create")
     * @Method("POST")
     * @Template("ControlCLVBundle:Conteo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Conteo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El conteo se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('CLV_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Conteo entity.
     *
     * @Route("/{id}/show", name="CLV_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction( $id )
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("
        SELECT c.id,c.fechaConteo,c.fechaVigencia,c.dosis,d.nISD n,u.id id1,u.nombre,u.apellidoPaterno, u.apellidoMaterno FROM ControlCLVBundle:Conteo c JOIN c.dosimetroId d JOIN d.curriculaId u
        WHERE c.id=:id
            ")->setParameter('id',$id);

        try {
            $entity = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        $limite=new \DateTime();
        $limite->modify('+2 month');

        return array(
            'result'      => $entity,
            'id' => $id,
            'fecha' => $limite->format('Y-m-d')
        );
    }

    /**
    * Creates a form to create a Conteo entity.
    *
    * @param Conteo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Conteo $entity )
    {
        $form = $this->createForm(new ConteoType(), $entity, array(
            'action' => $this->generateUrl('CLV_conteo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'));
        return $form;
    }

    /**
     * Displays a form to create a new Conteo entity.
     *
     * @Route("/new", name="CLV_conteo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(  )
    {
        $entity = new Conteo();

        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );

    }


    /**
     * Finds and displays a Conteo entity.
     *
     * @Route("/{id}/showc", name="CLV_showc")
     * @Method("GET")
     * @Template()
     */
    public function showcAction( $id )
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("
        SELECT c.id,c.fechaConteo,c.fechaVigencia,c.dosis,d.nISD,u.id id1,u.nombre,u.apellidoPaterno, u.apellidoMaterno FROM ControlCLVBundle:Conteo c JOIN c.dosimetroId d JOIN d.curriculaId u
        WHERE u.id=:id
            ")->setParameter('id',$id);

        try {
            $entity = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        $limite=new \DateTime();
        $limite->modify('+2 month');

        return array(
            'result'      => $entity,
            'id' => $id,
            'fecha' => $limite->format('Y-m-d')
        );
    }

    /**
     * Displays a form to edit an existing Conteo entity.
     *
     * @Route("/{id}/edit", name="CLV_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:Conteo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos del conteo.');
        }

        $editForm = $this->createEditForm($entity);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Conteo entity.
    *
    * @param Conteo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Conteo $entity)
    {
        $form = $this->createForm(
            new ConteoType(   $this->getDoctrine()->getManager() , $entity   ),
            $entity,
            array(
                'action' => $this->generateUrl(
                        'CLV_conteo_update',
                        array(
                            'id' => $entity->getId()  ,
                        )
                    ),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn btn-success pull-left',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit',
        ));
        return $form;
    }
    /**
     * Edits an existing Conteo entity.
     *
     * @Route("/{id}/update", name="CLV_conteo_update")
     * @Method("PUT")
     * @Template("ControlCLVBundle:Conteo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:Conteo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos del conteo.');
        }

       // $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('CLV_show', array('id' => $id)));
            // return $this->redirect($this->generateUrl('CLV_conteo_show', array('id' => $id,   'ida' => $entity->getDosimetrodesdetabladosimetro()->getId()   )));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        //    'delete_form' => $deleteForm->createView(),
        );
    }

}
