<?php

namespace Ultra\ControlCLVBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlCLVBundle\Entity\Dosimetro;
use Ultra\ControlCLVBundle\Form\DosimetroType;

/**
 * Dosimetro controller.
 *
 * @Route("/CLV/dosimetro")
 */
class DosimetroController extends Controller
{

    /**
     * Lists all Dosimetro entities.
     *
     * @Route("/", name="CLV_dosimetro")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        //Consulta perfil-curricula
        $query = $em->createQuery(
            "SELECT d,c
              FROM ControlCLVBundle:Dosimetro d
              JOIN d.curriculaId c
              " );

        $entities = $query->getResult();


        return array(
            'entities' => $entities
        );
    }
    /**
     * Creates a new Dosimetro entity.
     *
     * @Route("/", name="CLV_dosimetro_create")
     * @Method("POST")
     * @Template("ControlCLVBundle:Dosimetro:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Dosimetro();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El dosímetro se asignó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('CLV_dosimetro_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Dosimetro entity.
    *
    * @param Dosimetro $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Dosimetro $entity)
    {
        $form = $this->createForm(new DosimetroType(), $entity, array(
            'action' => $this->generateUrl('CLV_dosimetro_create'),
            'method' => 'POST',
        ));

$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));
        return $form;
    }

    /**
     * Displays a form to create a new Dosimetro entity.
     *
     * @Route("/new", name="CLV_dosimetro_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Dosimetro();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Dosimetro entity.
     *
     * @Route("/{id}", name="CLV_dosimetro_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:Dosimetro')->find($id);

        $datos_curricula = $em->getRepository('ControlDocumentoBundle:Curricula')->find( $entity->getCurriculaId()  );


        if ( ! $datos_curricula ){
            throw $this->createNotFoundException('JF: Imposible encontrar la currícula ..');
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dosimetro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'datoscurricula' => $datos_curricula,
        );
    }

    /**
     * Displays a form to edit an existing Dosimetro entity.
     *
     * @Route("/{id}/edit", name="CLV_dosimetro_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:Dosimetro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos del conteo.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Dosimetro entity.
    *
    * @param Dosimetro $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Dosimetro $entity)
    {

        $form = $this->createForm(new DosimetroType(), $entity, array(
            'action' => $this->generateUrl('CLV_dosimetro_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }

    /**
     * Edits an existing Dosimetro entity.
     *
     * @Route("/{id}", name="CLV_dosimetro_update")
     * @Method("PUT")
     * @Template("ControlCLVBundle:Dosimetro:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlCLVBundle:Dosimetro')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dosimetro entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('CLV_dosimetro_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('CLV_dosimetro_show', array('id' => $id)));
        }


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Dosimetro entity.
     *
     * @Route("/{id}", name="CLV_dosimetro_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlCLVBundle:Dosimetro')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar los datos del conteo.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('CLV_dosimetro'));
    }

    /**
     * Creates a form to delete a Dosimetro entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('CLV_dosimetro_delete', array('id' => $id)))
            ->setMethod('DELETE')
            //            ->add('submit', 'submit', array('label' => 'Delete'))
           ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
            ))
            ->getForm()
        ;
    }
}
