<?php

namespace Ultra\ControlCLVBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CatalogoDosimetros
 *
 * @ORM\Table( name="CLV_CatalogoDosimetros" )
 * @ORM\Entity(repositoryClass="Ultra\ControlCLVBundle\Entity\CatalogoDosimetrosRepository")
 * @UniqueEntity(
 *               fields={"nISD"},
 *               errorPath="nISD",
 *               message="El número de NISD ya había sido asignado antes..."
 * )
 *
 *
 *
 */




class CatalogoDosimetros
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Type(type="digit", message="Digite sólo números...")
     * @Assert\NotBlank(message="Digite el número de identificación para el dosímetro...")
     * @ORM\Column(name="NISD", type="string", length=12)
     */
    private $nISD;

    public function __toString()
    {
        return $this->getNISD();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nISD
     *
     * @param string $nISD
     * @return CatalogoDosimetros
     */
    public function setNISD($nISD)
    {
        $this->nISD = $nISD;
    
        return $this;
    }

    /**
     * Get nISD
     *
     * @return string 
     */
    public function getNISD()
    {
        return $this->nISD;
    }
}