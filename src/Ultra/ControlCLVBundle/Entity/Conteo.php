<?php

namespace Ultra\ControlCLVBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Conteo
 *
 * @ORM\Table( name="CLV_Conteo" )
 * @ORM\Entity(repositoryClass="Ultra\ControlCLVBundle\Entity\ConteoRepository")
 */
class Conteo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * var integer
     *
     * @Assert\NotBlank(message="Seleccione el número de dosímetro...")
     * ORM\ManyToOne(targetEntity="Dosimetro", inversedBy="conteospordosimetro")
     * @ORM\ManyToOne(targetEntity="Dosimetro")
     */
    private $dosimetroId;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="Asigne una fecha...")
     * @ORM\Column(name="FechaConteo", type="date")
     */
    private $fechaConteo;

    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="Asigne una vigencia...")
     * @ORM\Column(name="FechaVigencia", type="date")
     */
    // Restricción la fecha de vigencia mayor a la del conteo. En la función isVigenciaOK
    private $fechaVigencia;

    /**
     * @var string
     * @Assert\Type(type="numeric", message="Digite sólo números...")
     * @Assert\NotBlank(message="Digite la dosis...")
     * @ORM\Column(name="Dosis", type="integer", length=12)
     */
    private $dosis;



    /**
     * @Assert\True(message = "La fecha de vigencia debe ser posterior a la fecha de conteo")
     */
    public function isVigenciaOK()
    {
        // return true or false
        return ($this->fechaConteo < $this->fechaVigencia);
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set fechaConteo
     *
     * @param \DateTime $fechaConteo
     * @return Conteo
     */
    public function setFechaConteo($fechaConteo)
    {
        $this->fechaConteo = $fechaConteo;
    
        return $this;
    }

    /**
     * Get fechaConteo
     *
     * @return \DateTime 
     */
    public function getFechaConteo()
    {
        return $this->fechaConteo;
    }

    /**
     * Set fechaVigencia
     *
     * @param \DateTime $fechaVigencia
     * @return Conteo
     */
    public function setFechaVigencia($fechaVigencia)
    {
        $this->fechaVigencia = $fechaVigencia;

        return $this;
    }

    /**
     * Get fechaVigencia
     *
     * @return \DateTime
     */
    public function getFechaVigencia()
    {
        return $this->fechaVigencia;
    }

    /**
     * Set dosis
     *
     * @param integer $dosis
     * @return Conteo
     */
    public function setDosis($dosis)
    {
        $this->dosis = $dosis;
    
        return $this;
    }

    /**
     * Get dosis
     *
     * @return integer 
     */
    public function getDosis()
    {
        return $this->dosis;
    }

    /**
     * Set dosimetroId
     *
     * @param integer $dosimetroId
     * @return Conteo
     */
    public function setDosimetroId($dosimetroId)
    {
        $this->dosimetroId = $dosimetroId;
    
        return $this;
    }

    /**
     * Get dosimetroId
     *
     * @return integer 
     */
    public function getDosimetroId()
    {
        return $this->dosimetroId;
    }
}