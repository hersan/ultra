<?php

namespace Ultra\ControlCLVBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Dosimetro
 *
 * @ORM\Table( name="CLV_Dosimetro" )
 * @ORM\Entity(repositoryClass="Ultra\ControlCLVBundle\Entity\DosimetroRepository")
 * @UniqueEntity(
 *               fields={"dosimetroId"},
 *               errorPath="dosimetroId",
 *               message="El número de NISD ya había sido asignado antes..."
 * )
 * @UniqueEntity(
 *               fields={"curriculaId"},
 *               errorPath="curriculaId",
 *               message="La persona ya había sido asignada antes..."
 * )
 *
 *
 */
class Dosimetro
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * var string
     * Assert\Type(type="digit", message="Digite sólo números...")
     * Assert\NotBlank(message="Digite el número de identificación para el dosímetro...")
     * ORM\Column(name="NISD", type="string", length=12, unique=true)
     */
    private $nISD;

     /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlCLVBundle\Entity\CatalogoDosimetros")
     * @Assert\NotBlank(message="Selecciona el dosímetro...")
     */
    private $dosimetroId;


      /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="Asigne una fecha...")
     * @ORM\Column(name="FechaAsignacion", type="date")
     */
    private $fechaAsignacion;


    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     * @Assert\NotBlank(message="Selecciona a la persona correspondiente...")
     */
    private $curriculaId;

    /**
     * quité la arroba para que no interprete esto: ORM\OneToMany(targetEntity="Conteo", mappedBy="dosimetroId")
     */
 //   protected $conteospordosimetro;


  /*  protected $curriculaTexto;

    public function getCurriculaTexto()
    {
    return $this->curriculaTexto;
    }
    public function setCurriculaTexto( $texto )
    {
    $this->curriculaTexto = $texto;
    }
*/

    public function __construct()
{
    $this->fechaAsignacion =   new \DateTime( date('Y') );
}

    public function __toString()
    {
//       return $this->nISD." - ".$this->curriculaId." - ". $this->curriculaTexto. " -  llenar comboboxadicional con el nombre".$this->comboboxadicional[ $this->id  ];
//   $separador = $this->curriculaTexto != '' ? " - " : "";
//      return $this->nISD. $separador . $this->curriculaTexto;

//        $separador = $this->getCurriculaId() != '' ? " - " : "";
//        return $this->nISD. $separador . $this-> getCurriculaId();

        return $this->getDosimetroId()->getNISD();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set curriculaId
     *
     * @param integer $curriculaId
     * @return Dosimetro
     */
    public function setCurriculaId($curriculaId)
    {
        $this->curriculaId = $curriculaId;
    
        return $this;
    }

    /**
     * Get curriculaId
     *
     * @return integer 
     */
    public function getCurriculaId()
    {
        return $this->curriculaId;
    }



    /**
     * Set dosimetroId
     *
     * @param \Ultra\ControlCLVBundle\Entity\CatalogoDosimetros $dosimetroId
     * @return Dosimetro
     */
    public function setDosimetroId(\Ultra\ControlCLVBundle\Entity\CatalogoDosimetros $dosimetroId = null)
    {
        $this->dosimetroId = $dosimetroId;
    
        return $this;
    }

    /**
     * Get dosimetroId
     *
     * @return \Ultra\ControlCLVBundle\Entity\CatalogoDosimetros 
     */
    public function getDosimetroId()
    {
        return $this->dosimetroId;
    }

    /**
     * Set fechaAsignacion
     *
     * @param \DateTime $fechaAsignacion
     * @return Dosimetro
     */
    public function setFechaAsignacion($fechaAsignacion)
    {
        $this->fechaAsignacion = $fechaAsignacion;
    
        return $this;
    }

    /**
     * Get fechaAsignacion
     *
     * @return \DateTime 
     */
    public function getFechaAsignacion()
    {
        return $this->fechaAsignacion;
    }
}