<?php

namespace Ultra\ControlCLVBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogoDosimetrosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        ->add('nISD',null,array(
                   'required' => false,
                   'label'=>'Dosímetro (NISD):',
                   'attr'=>array( 'class' => 'form-control input-sm','autofocus'=>true )
                  )
            )

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlCLVBundle\Entity\CatalogoDosimetros'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlclvbundle_catalogodosimetros';
    }
}
