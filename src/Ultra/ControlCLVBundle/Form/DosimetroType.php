<?php

namespace Ultra\ControlCLVBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
//use Doctrine\ORM\EntityManager;

class DosimetroType extends AbstractType
{

     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('CurriculaId','entity', array(
                'class' => 'ControlDocumentoBundle:Curricula',
                'required' => false,
                'attr' => array('class' => 'selectpicker',
                'data-live-search' => true
                ),
                'label' => 'Currícula',
                'empty_value'=>'Selecciona',
                'empty_data' => null,
            ))
           ->add('dosimetroId','entity', array(
                'class' => 'ControlCLVBundle:CatalogoDosimetros',
                'required' => false,
                'attr' => array('class' => 'selectpicker',
                'data-live-search' => true
                                            ),
                'label' => 'Dosímetro',
                'empty_value'=>'Selecciona',
                'empty_data' => null,
                )
 /*           ->add('nISD',null,array(
                   'required' => false,
                   'label'=>'Dosímetro (NISD):',
                   'attr'=>array( 'class' => 'form-control input-sm','autofocus'=>true )
                  )*/
           );
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlCLVBundle\Entity\Dosimetro'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlclvbundle_dosimetro';
    }
}
