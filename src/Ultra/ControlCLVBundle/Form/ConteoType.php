<?php

namespace Ultra\ControlCLVBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\ControlDocumentoBundle;
use Doctrine\ORM\EntityManager;

class ConteoType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dosimetroId','entity', array(
                'class' => 'ControlCLVBundle:Dosimetro',
                'required' => false,
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true
                ),
                'label' => 'Dosímetro',
                'empty_value'=>'Selecciona',
                'empty_data' => null,
            ))
            ->add('fechaConteo', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha de conteo (año/mes/dia):',
//                'format' => 'dd MM yyyy',
//                'data' => null !== $options['data']->getFechaConteo() ? $options['data']->getFechaConteo() : new \DateTime( date('Y') ),
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false
            ))
            ->add('fechaVigencia', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha de vigencia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false
            ))
            ->add('dosis','text',
                array (
                        'label'=>'Dosis mRem:' ,
                        'attr' => array ('class' => 'form-control input-sm'),
                        'required' => false
                       )
            );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlCLVBundle\Entity\Conteo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controlclvbundle_conteo';
    }



}
