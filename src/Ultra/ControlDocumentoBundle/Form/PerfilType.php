<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\Actividad;
use Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial;
use Ultra\ControlDocumentoBundle\Entity\Escolaridad;
use Ultra\ControlDocumentoBundle\Entity\Nota;
use Ultra\ControlDocumentoBundle\Entity\Habilidad;
use Ultra\ControlDocumentoBundle\Entity\Capacitacion;
use Ultra\ControlDocumentoBundle\Entity\Experiencia;
use Doctrine\ORM\EntityRepository;
use Ultra\ControlDocumentoBundle\Entity\Perfil;

class PerfilType extends AbstractType
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $containerInterface
     */
    public function __construct(ContainerInterface $containerInterface)
    {
        $this->container = $containerInterface;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                                    'placeholder'=> 'Escriba aquí la clave de perfil',
                                    'autocomplete'=>'on',
                                     'maxlength'=>'20',
                                     'autofocus'=>true,
                                     'title'=>'Clave'),
                    'label' => 'Clave',
                    'required' => false
                ))

            ->add('revision','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'Revisión',
                    'required' => false
                ))
            ->add('titulo','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escribe aquí el título largo del perfil',
                        'autocomplete'=>'on',
                        'maxlength'=>'150',
                        'title'=>'Título del perfil'),
                    'label' => 'Título',
                    'required' => false
                ))
            ->add('aprobado', 'date', array(
                'input' => 'datetime',
                'label' => 'Aprobado',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('vigencia', 'date', array(
                'input' => 'datetime',
                'label' => 'Vigencia o Cancelación',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('transmitido', 'date', array(
                'input' => 'datetime',
                'label' => 'Transmitido',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            //->add('doc','file')
            /*->add('tipoDocumento','entity', array(
                    'class' => 'ControlDocumentoBundle:TipoDocumento',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->orderBy('dis.titulo', 'ASC');
                        },
                    'required' => false,
                    'attr' => array('class' => 'selectpicker',
                             'title' => 'Selecciona el tipo...',
                        'data-width'=>'100%',
                        'data-size'=>'7'
                    ),
                    'label' => 'Tipo de Documento',
                ))*/

            ->add('estado','entity', array(
                    'class' => 'ControlDocumentoBundle:Condicion',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->orderBy('dis.id', 'ASC');
                        },
                    'attr' => array('class' => 'selectpicker',
                     'title' => 'Selecciona el estado...',
                        'data-width'=>'100%',
                        'data-size'=>'7',
                    ),
                    'label' => 'Estado',
                    'required' => false
                ))

            ->add('disciplinas', 'entity', array(
                    'class' => 'ControlDocumentoBundle:Disciplina',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->select('dis, area, contrato, cliente')
                                ->join('dis.area','area')
                                ->join('area.contrato','contrato')
                                ->join('contrato.cliente','cliente')
                                ->where('contrato.abierto = 1')
                                ->orderBy('dis.tituloCorto', 'ASC');
                        },
                    'attr' => array(
                        'class' => 'selectpicker',
                        'multiple'=>true,
                        'title'=>'Selecciona las disciplinas correspondientes...',
                        'data-width'=>'100%',
                        'data-size'=>'7',
                        'data-live-search' => true,
                    ),
                    'label' => 'Disciplina (Área:Proyecto)',
                    'required' => true,
                    'multiple' => true,
                    'empty_data' => ' ',
                    'empty_value'=>'Selecciona las disciplinas correspondientes...'
                ))
            ->add('fecha_uad', 'date', array(
                    'input' => 'datetime',
                    'label' => 'UAD Aprobado',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'required' => false,
                    'format' => 'yyyy-MM-dd',
                ))

            ->add('revision_uad','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'UAD Revisión',
                    'required' => false
                ))

            ->add('actividades','collection',array(
                    'required' => false,
                    'type' => new ActividadType(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=>false,
                ))
            ->add('escolaridad','collection',array(
                    'required'=>false,
                    'type' => new EscolaridadType(),
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=>false,
                ))

            ->add('capacitacion','collection',array(
                    'required' => false,
                    'type' => new CapacitacionType($this->container->get('doctrine.orm.entity_manager')),
                    'label' => 'Curso',
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=>false,
                 ))

            ->add('expertise','collection',array(
                    'required' => false,
                    'type' => new ExperienciaType(),
                    'label' => 'Curso',
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                ))
            ->add('habilidades','collection',array(
                    'required'=>false,
                    'type' => new HabilidadType(),
                    'label' => 'Curso',
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                ))
            ->add('conocimientoEspecial','collection',array(
                    'required'=>false,
                    'type' => new ConocimientoEspecialType($this->container->get('doctrine.orm.entity_manager')),
                    'label' => 'Curso',
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                ))
            ->add('notas','collection',array(
                    'required'=>false,
                    'type' => new NotaType(),
                    'label' => 'Nota',
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                ))
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){

                $entity = $event->getData();
                $form = $event->getForm();

                //Determina si el archivo es requerido
                if(!$entity || null === $entity->getId()){
                    $form->add('file','file', array(
                            'label' => '',
                            'required' => true,
                            'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                            'property_path' => 'file',
                            'attr' => array(
                                'class' => '',
                            )
                        ));

                    if ($entity instanceof Perfil) {
                        $entity->addEscolaridad(new Escolaridad());

                        $entity->addActividade(new Actividad());

                        $entity->addExpertise(new Experiencia());

                        $entity->addNota(new Nota());

                        $entity->addCapacitacion(new Capacitacion());

                        $entity->addHabilidade(new Habilidad());

                        $entity->addConocimientoEspecial(new ConocimientoEspecial());
                    }

                }else{

                    $form->add('file','file', array(
                            'label' => '',
                            'required' => false,
                            'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                            'property_path' => 'file',
                            'attr' => array(
                                'class' => '',
                            )
                    ));

                    if($entity instanceof Perfil)
                    {
                        if($entity->getEscolaridad()->isEmpty()){
                            $entity->addEscolaridad(new Escolaridad());
                        }

                        if($entity->getActividades()->isEmpty()){
                            $entity->addActividade(new Actividad());
                        }

                        if($entity->getExpertise()->isEmpty()){
                            $entity->addExpertise(new Experiencia());
                        }

                        if($entity->getNotas()->isEmpty()){
                            $entity->addNota(new Nota());
                        }

                        if($entity->getCapacitacion()->isEmpty()){
                            $entity->addCapacitacion(new Capacitacion());
                        }

                        if($entity->getHabilidades()->isEmpty()){
                            $entity->addHabilidade(new Habilidad());
                        }

                        if($entity->getConocimientoEspecial()->isEmpty()){
                            $entity->addConocimientoEspecial(new ConocimientoEspecial());
                        }
                    }
                }

                //Agrega las seciones del formulario Perfil
                /*if($entity->getEscolaridad()->isEmpty()){
                    $entity->addEscolaridad(new Escolaridad());
                }

                if($entity->getActividades()->isEmpty()){
                    $entity->addActividade(new Actividad());
                }

                if($entity->getExpertise()->isEmpty()){
                    $entity->addExpertise(new Experiencia());
                }

                if($entity->getNotas()->isEmpty()){
                    $entity->addNota(new Nota());
                }

                if($entity->getCapacitacion()->isEmpty()){
                    $entity->addCapacitacion(new Capacitacion());
                }

                if($entity->getHabilidades()->isEmpty()){
                    $entity->addHabilidade(new Habilidad());
                }

                if($entity->getConocimientoEspecial()->isEmpty()){
                    $entity->addConocimientoEspecial(new ConocimientoEspecial());
                }*/

                $event->setData($entity);
        });

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event)
            {

            }
        );



    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Perfil',
            'cascade_validation' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_perfil_type';
    }
}
