<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 25/03/14
 * Time: 05:37 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class EvaluaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('perfiles','entity', array(
                'class' => 'ControlDocumentoBundle:Perfil',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Perfil',
                'required' => false,
                'empty_value' => 'Selecciona el perfil ...'
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {

        return 'ultra_controldocumentobundle_evalua';
    }
}