<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\Competencia;

class CompetenciaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('curso','text', array(
                    'required' => false,
                    'label' => 'Competencia (adicional)',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                ))
            ->add('fechaRealizacion','date',array(
                    'required' => false,
                    'empty_data' => null,
                    //'empty_value'=>'',
                    'label' => 'Fecha de Realización',
                    'widget' => 'choice',
                    'years' => range(date('Y')-40, date('Y')+5),
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'class' => '',
                    )
                ))
            ->add('fecha_vigencia','date',array(
                    'required' => false,
                    'empty_data' => null,
                    //'empty_value' => ' ',
                    'label' => 'Fecha de vigencia',
                    'widget' => 'choice',
                    'years' => range(date('Y')-40, date('Y')+5),
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'class' => ''
                    )
                ))
            //->add('file')
            ->add('tipoAdquisicion','entity',array(
                    'required' => false,
                    'class' => 'ControlDocumentoBundle:TipoAdquisicion',
                    'label' => 'Tipo de Adquisición',
                    'empty_value' => 'Seleccionar...',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                ))
            ->add('file','file',array(
                    'required' => false,
                    'label' => 'Agregar Certificado',
                    'attr' => array(
                        'class' => '',
                    ),

                ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
    }

    public function onPreSetData(FormEvent $event){
        $form = $event->getForm();
        $entity = $event->getData();

        if($entity instanceof Competencia)
        {
            $this->addElements($form,$entity);
        }
    }

    public function addElements(FormInterface $form, Competencia $file)
    {
        $label = 'Agregar Archivo';
        $class = 'class="text-danger';

        if($file->getPath() !== null && $file->getPdfCertificado() !== null)
        {
            $label = 'Archivo disponible (*)';
            $class = '';
        }


        $form->add('file','file',array(
            'required' => false,
            'label' => $label,
            'label_attr' => array(
                'class' => $class,
            ),
            'attr' => array(
                'class' => '',
            ),

        ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Competencia',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_competencia';
    }
}
