<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 8/04/14
 * Time: 04:40 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Form\Type\ActaNacimientoType;
use Ultra\ControlDocumentoBundle\Form\Type\CartillaMilitarType;
use Ultra\ControlDocumentoBundle\Form\Type\CedulaProfesionalType;
use Ultra\ControlDocumentoBundle\Form\Type\CurpType;
use Ultra\ControlDocumentoBundle\Form\Type\CurriculaType;
use Ultra\ControlDocumentoBundle\Form\Type\FotoType;
use Ultra\ControlDocumentoBundle\Form\Type\IfeType;
use Ultra\ControlDocumentoBundle\Form\Type\ImssType;
use Ultra\ControlDocumentoBundle\Form\Type\LicenciaConducirType;
use Ultra\ControlDocumentoBundle\Form\Type\RfcType;
use Ultra\ControlDocumentoBundle\Model\Anexo;

class AnexoType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('ife',new IfeType(),array(
                    'label' => false,
                ))
                ->add('licencia_conducir',new LicenciaConducirType(),array(
                    'label' => false,
                ))
                ->add('cedula_profesional',new CedulaProfesionalType(),array(
                    'label' => false,
                ))
                ->add('rfc',new RfcType(), array(
                    'label' => false,
                ))
                ->add('curp',new CurpType(), array(
                    'label' => false,
                ))
                ->add('imss',new ImssType(), array(
                    'label' => false,
                ))
                ->add('cartilla', new CartillaMilitarType(), array(
                    'label' => false,
                ))
                ->add('acta_nacimiento', new ActaNacimientoType(), array(
                    'label' => false,
                ))
                ->add('curricula', new CurriculaType(), array(
                    'label' => false,
                ))
                ->add('foto', new FotoType(), array(
                    'label' => false,
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\Anexo',
                'empty_data' => function(FormInterface $form){
                        return new Anexo($form->getParent()->getData());
                    }
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_anexo';
    }
}