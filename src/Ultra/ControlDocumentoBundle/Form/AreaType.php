<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AreaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Título del área',
                        'autocomplete'=>'on',
                        'maxlength'=>'45',
                        'title'=>'Ingresa el nombre completo del área',
                        'autofocus'=>true),
                    'label' => 'Título',
                    'required' => false
                ))
            ->add('tituloCorto','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Título Corto',
                        'autocomplete'=>'on',
                        'maxlength'=>'15',
                        'title'=>'Nombre corto del área'),
                    'label' => 'Título Corto',
                    'required' => false
                ))
            ->add('claveArea','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Clave de área',
                        'autocomplete'=>'on',
                        'maxlength'=>'15',
                        'title'=>'Ingresa la clave del área'),
                    'label' => 'Clave de Área',
                    'required' => false
                ))
            ->add('curricula', 'entity', array(
                'empty_value' => 'Seleccionar curricula',
                'class' => 'ControlDocumentoBundle:Curricula',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona curricula'
                ),
                'label' => 'Responsable',
                'required' => false,
            ))

            ->add('contrato','entity', array(
                    'class' => 'ControlDocumentoBundle:Contrato',
                    'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                    'label' => 'Contrato',
                    'required' => false,
                    'empty_value' => 'Selecciona contrato ...'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Area'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_area';
    }
}
