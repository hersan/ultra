<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 19/06/14
 * Time: 10:02 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CursoSoloType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cursos','entity', array(
                'class' => 'ControlDocumentoBundle:Curso',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Cursos',
                'required' => false,
                'empty_value' => 'Selecciona el curso ...'
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {

        return 'ultra_controldocumentobundle_cursosolo';
    }
}