<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class CapacitacionType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->objectManager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $documents = $this->objectManager
                        ->getRepository('ControlDocumentoBundle:Documento')
                        ->createQueryBuilder('d')
                        ->select('d,estado')
                            ->join('d.condicion','estado')
                        ->where('estado.id = 1')
                        ->orWhere('estado.id = 5')
                        ->getQuery()
                        ->useResultCache(true)
                        ->getResult();
        ;

        $builder
            //->add('perfil')
            ->add('capacitacion','entity', array(
                    'required' => true,
                    'empty_data' => null,
                    'empty_value' => 'Seleccione la capacitación',
                    'error_bubbling' => false,
                    //'empty_value' => 'Seleccionar...',
                    'class' => 'ControlDocumentoBundle:Documento',
                    'label' => ' ',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title'=>'Selecciona la capacitación requerida',
                    ),
                    'choices' => $documents,
                    /*'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('d')
                                ->select('d,estado')
                                    ->join('d.condicion','estado')
                                ->where('estado.id = 1')
                                ->orWhere('estado.id = 5')
                                ;
                        }*/

                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Capacitacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_capacitacion';
    }
}
