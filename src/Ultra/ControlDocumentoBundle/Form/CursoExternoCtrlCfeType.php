<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\CursoRepository;

class CursoExternoCtrlCfeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
            ->add('cursoExterno','entity',array(
                'required' => false,
                'class' => 'ControlDocumentoBundle:Curso',
                /*'query_builder' => function(CursoRepository $er){
                        return $er->createQueryBuilder('c')->where('c.id >75 and c.id<83');
                    },*/
                'label' => 'Competencia (Externa)',
                'empty_value'=>'Selecciona un curso',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('fechaRealizacion','date',array(
                'label' => 'Fecha de realización',
                'widget' => 'choice',
                'empty_data' => null,
                'empty_value' => ' ',
                'years' => range(date('Y')-40, date('Y')+5),
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => ''
                )
            ))
            ->add('fechaVigencia','date',array(
                'label' => 'Fecha de vigencia',
                'widget' => 'choice',
                'empty_data' => null,
                'empty_value' => ' ',
                'years' => range(date('Y')-40, date('Y')+5),
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => ''
                )
            ))
            ->add('tipoAdquisicion','entity',array(
                'class' => 'ControlDocumentoBundle:TipoAdquisicion',
                'label' => 'Tipo de Adquisición',
                'empty_value' => 'Seleccionar...',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true
                ),
            ))
            ->add('file','file',array(
                'required' => false,
                'error_bubbling' => false,
                'label' => 'Agregar Certificado',
                //'cascade_validation'=>false,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\CursoExterno',
            'cascade_validation' => true,
            'required' => false,

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_cursoexternoctrl';
    }
}