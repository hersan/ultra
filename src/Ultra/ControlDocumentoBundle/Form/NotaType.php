<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('seccion','choice', array(
                    'required' => false,
                    'label' => 'Sección',
                    'empty_data' => null,
                    'empty_value' => 'Selecciona una sección',
                    'attr' => array(
                        'class' => 'form-control selectpicker input-sm',
                    ),
                    'choices' => array(
                        'actividades' => 'Actividades',
                        'escolaridad' => 'Escolaridad',
                        'capacitacion' => 'Capacitación',
                        'experiencia' => 'Experiencia',
                        'habilidades' => 'Habilidades/Conocimientos Especiales'
                    )
                ))

            ->add('descripcion','text', array(
                    'required' => false,
                    'label' => 'Nota',
                    'attr' => array(
                        'class' => 'form-control input-sm',
                        'maxlength'=>'250'
                    )
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Nota'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_nota';
    }
}
