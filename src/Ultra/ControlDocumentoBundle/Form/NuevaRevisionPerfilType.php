<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 6/06/14
 * Time: 12:50 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class NuevaRevisionPerfilType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escriba aquí la clave de perfil',
                        'autocomplete'=>'on',
                        'maxlength'=>'20',
                        'autofocus'=>true,
                        'title'=>'Clave'),
                    'label' => 'Clave',
                    'required' => false
                ))

            ->add('revision','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'Revisión',
                    'required' => false
                ))
            ->add('titulo','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escribe aquí el título largo del perfil',
                        'autocomplete'=>'on',
                        'maxlength'=>'150',
                        'title'=>'Título del perfil'),
                    'label' => 'Título',
                    'required' => false
                ))
            ->add('aprobado', 'date', array(
                    'input' => 'datetime',
                    'label' => 'Aprobado',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'required' => false,
                    'format' => 'yyyy-MM-dd',
                ))
            ->add('vigencia', 'date', array(
                'input' => 'datetime',
                'label' => 'Vigencia o Cancelación',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('transmitido', 'date', array(
                'input' => 'datetime',
                'label' => 'Transmitido',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            //->add('doc','file')
            /*->add('tipoDocumento','entity', array(
                    'class' => 'ControlDocumentoBundle:TipoDocumento',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->orderBy('dis.titulo', 'ASC');
                        },
                    'required' => false,
                    'attr' => array('class' => 'selectpicker',
                             'title' => 'Selecciona el tipo...',
                        'data-width'=>'100%',
                        'data-size'=>'7'
                    ),
                    'label' => 'Tipo de Documento',
                ))*/

            ->add('estado','entity', array(
                    'class' => 'ControlDocumentoBundle:Condicion',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->orderBy('dis.id', 'ASC');
                        },
                    'attr' => array('class' => 'selectpicker',
                        'title' => 'Selecciona el estado...',
                        'data-width'=>'100%',
                        'data-size'=>'7',
                    ),
                    'label' => 'Estado',
                    'required' => false
                ))

            ->add('disciplinas', 'entity', array(
                    'class' => 'ControlDocumentoBundle:Disciplina',
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('dis')
                                ->orderBy('dis.tituloCorto', 'ASC');
                        },
                    'attr' => array(
                        'class' => 'selectpicker',
                        'multiple'=>true,
                        'title'=>'Selecciona las disciplinas correspondientes...',
                        'data-width'=>'100%',
                        'data-size'=>'7',
                        'data-live-search' => true,
                    ),
                    'label' => 'Disciplina',
                    'required' => true,
                    'multiple' => true,
                    'empty_data' => ' ',
                    'empty_value'=>'Selecciona las disciplinas correspondientes...'
                ))
            ->add('fecha_uad', 'date', array(
                    'input' => 'datetime',
                    'label' => 'UAD Aprobado',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'required' => false,
                    'format' => 'yyyy-MM-dd',
                ))

            ->add('revision_uad','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'UAD Revisión',
                    'required' => false
                ))
            ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){

                $entity = $event->getData();
                $form = $event->getForm();

                //Determina si el archivo es requerido
                if(!$entity || null === $entity->getId()){
                    $form->add('file','file', array(
                            'label' => '',
                            'required' => true,
                            'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                            'property_path' => 'file',
                            'attr' => array(
                                'class' => '',
                            )
                        ));
                }else{

                    $form->add('file','file', array(
                            'label' => '',
                            'required' => false,
                            'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                            'property_path' => 'file',
                            'attr' => array(
                                'class' => '',
                            )
                        ));
                }

                $event->setData($entity);
            });
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Perfil',
                //'cascade_validation' => false,
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_revision_perfil_type';
    }
}