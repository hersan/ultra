<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\PdfCalificacion;
use Ultra\ControlDocumentoBundle\Entity\PdfCertificado;
use Ultra\ControlDocumentoBundle\Entity\PerfilRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PerfilCurriculaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id = $options['disciplina_id'];
        $builder
            ->add('numeroCredencial','text',array(
                    'label' => 'Numero de Credencial',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'No. de Credencial',
                        'autocomplete'=>'on',
                        'maxlength'=>'15',
                        'title'=>'Ingresa el número de credencial')
                ))

            ->add('fechaAlta','date')
            ->add('fechaBaja','date')
            ->add('contrato', 'choice', array(
                'label' => 'Contrato',
                'empty_value' => 'Seleccionar...',
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => array('3697' => 'CFE', '3699' => 'PEMEX','ULTRA'=>'ULTRA')
            ))
            ->add('ubicacion', 'choice', array(
                'label' => 'Ubicación',
                'empty_value' => 'Seleccionar...',
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => array('Laguna Verde' => 'Laguna Verde', 'Dos Bocas' => 'Dos Bocas','Luz Nava'=>'Luz Nava','Paraiso'=>'Paraiso')
            ))


        ->add('perfil','entity',array(
                'class' => 'ControlDocumentoBundle:Perfil',
                    'query_builder' => function(PerfilRepository $er) use ($id){
                            return $er->createQueryBuilder('p')->join('p.disciplinas','dis')->where('dis.id = :id and p.estado=1')->setParameters(array(
                                   'id' => $id
                                ));
                        },
                    'label' => 'Perfil',
                    'empty_value' => 'Seleccionar',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    )
                ))

            ->add('curricula','entity',array(
                    'label' => 'Currícula',
                    'class' => 'ControlDocumentoBundle:Curricula',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                    'empty_value' => 'Seleccionar'
                ))
            ->add('files', new EvaluacionType(), array(
                    'label' => false,
                ))
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){

                $entity = $event->getData();
                $form = $event->getForm();

            });
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\PerfilCurricula',
            'disciplina_id' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_perfilcurricula';
    }
}
