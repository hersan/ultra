<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DisciplinaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Título de la disciplina',
                        'autocomplete'=>'on',
                        'maxlength'=>'50',
                        'title'=>'Ingresa el título completo de la disciplina',
                        'autofocus'=>true),
                    'required' => false
                ))
            ->add('tituloCorto', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Título corto',
                        'autocomplete'=>'on',
                        'maxlength'=>'10',
                        'title'=>'Ingresa el titulo corto de la disciplina'),
                    'required' => false
                ))
            ->add('area','entity', array(
                    'class' => 'ControlDocumentoBundle:Area',
                    'attr' => array('class' => 'selectpicker',
                        'data-live-search' => true),
                    'label' => 'Área',
                    'required' => false,
                    'empty_value' => 'Selecciona área ...'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Disciplina'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_disciplina';
    }
}
