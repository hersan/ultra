<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 31/03/14
 * Time: 10:30 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Model\PdfCertificado;

class PdfCertificadoType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     *
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file','file', array(
                'label' => 'Agrega un Certificado',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\PdfCertificado',
                'empty_data' => function(FormInterface $form){
                        return new PdfCertificado(
                            $form->getParent()->getData()
                        );
                    }
            ));
    }


    /**
     * Returns the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'Ultra_Certificado';
    }


} 