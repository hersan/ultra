<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContratoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','textarea', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escriba aquí en título largo del contrato',
                        'autocomplete'=>'on',
                        'autofocus'=>true,
                        'maxlength'=>'250',
                        'title'=>'Título del contrato',
                         'rows'=>'2'),
                    'required' => false
                ))
            ->add('tituloCorto','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escriba un título corto',
                        'autocomplete'=>'on',
                        'maxlength'=>'25',
                        'title'=>'Título corto'),
                    'required' => false
                ))
            ->add('numeroInterno', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'title'=>'Número interno (Ultra)',
                        'maxlength'=>'25'),
                    'required' => false
                ))
            ->add('numeroExterno','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'title'=>'Número externo (Cliente)',
                        'maxlength'=>'25'),
                    'required' => false
                ))
            ->add('abierto','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'autocomplete'=>'on',
                        'title'=>'Abierto = 1',
                        'maxlength'=>'1'),
                    'required' => false
                ))
            ->add('cliente','entity',array(
                    'class' => 'ControlDocumentoBundle:Cliente',
                    'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                    'required' => false,
                    'empty_value' => 'Selecciona el cliente ...'
                ))
            ->add('etapa','entity',array(
                    'class' => 'Ultra\ControlDocumentoBundle\Entity\EtapaProyecto',
                    'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true,
                    'title'=> 'Etapa del contrato'),
                    'required' => false,
                    'empty_value' => 'Selecciona etapa ...'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Contrato'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_contrato';
    }
}
