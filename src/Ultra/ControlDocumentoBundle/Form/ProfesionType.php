<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfesionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('institucion','entity',array(
                    'class' => 'ControlDocumentoBundle:Institucion',
                    'label' => 'Institución',
                    'required' => false,
                    'empty_data' => null,
                    'empty_value'=> 'Selecciona una universidad',
                    'attr' => array('class' => 'form-control input-sm selectpicker',
                        'placeholder'=>'Selecciona Universidad',
                        'data-live-search' => true,
                        )
                ))
            ->add('nombre','text',array(
                'label' => 'Nombre',
                'required' => false,
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=>'Escriba aquí la profesión',
                    'autocomplete'=>'on',
                    'maxlength'=>'100',
                    'autofocus'=>true,
                    'title'=>'Profesión')
            ))

            ->add('clave','text',array(
            'label' => 'Siglas',
            'required' => false,
            'attr' => array('class' => 'form-control input-sm',
                'placeholder'=>'Ej. IE',
                'autocomplete'=>'on',
                'maxlength'=>'20',
                'title'=>'Siglas')
            ))

            ->add('nivel','choice',array(
            'label' => 'Nivel ',
            'required' => false,
            'choices'=>array('Técnico'=>'Técnico','Licenciatura'=>'Licenciatura','Maestría'=>'Maestría','Doctorado'=>'Doctorado'),
            'attr' => array('class' => 'selectpicker',
            'title'=>'Selecciona ...')
            ));

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Profesion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_profesion';
    }
}
