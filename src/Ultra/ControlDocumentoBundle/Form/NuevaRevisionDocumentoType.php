<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 8/05/14
 * Time: 10:25 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;

class NuevaRevisionDocumentoType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('clave','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Clave del procedimiento',
                    'autocomplete'=>'on',
                    'maxlength'=>'50',
                    'title'=>'Clave del procedimiento',
                    'autofocus'=>true),
                'required' => false,
                'disabled' => true,
            ))

            ->add('revision', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'Revisión',
                    'disabled' => false,
                    'required' => false,
                ))

            ->add('titulo', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el título del procedimiento',
                        'autocomplete'=>'on',
                        'title'=>'Título del procedimiento'),
                    'label' => 'Título',
                    'disabled' => false,
                    'required' => false,
                ))

            ->add('aprobado', 'date',array(
                'input' => 'datetime',
                'label' => 'Aprobado',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
                ))

            ->add('vigencia', 'date', array(
                'input' => 'datetime',
                'label' => 'Vigencia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
                ))

            ->add('verificar','checkbox',array(
                    'label' => '¿Liberado?',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'mapped'=>false,
                    'data' => true,
                    'required' => false,
                ))

            ->add('liberado', 'date', array(
                'input' => 'datetime',
                'label' => 'Liberado (CFE)',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('transmitido', 'date', array(
                'input' => 'datetime',
                'label' => 'Transmitido',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('file', 'file', array(
                    'label' => 'Archivo',
                    'disabled'=>false,
                    'required' => true,
                    //'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                    //'property_path' => 'file'
                ))
            //->add('doc', 'file')
            ->add('disciplina', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:Disciplina',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                    'label' => 'Disciplina',
                    'disabled' => true,
                    'required' => false,
                ))
            ->add('condicion', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:Condicion',
                    'attr' => array('class' => 'form-control selectpicker'),
                    'label' => 'Condición',
                    'disabled' => true,
                    'required' => false,
                ))
            ->add('tipoDocumento', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:TipoDocumento',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                    'label' => 'Tipo de Documento',
                    'disabled'=>true,
                    'required' => false,
                ))
        ;

        /*$builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'preSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));*/
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Documento',
                //'required' => false,
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_revision_documento_type';
    }

    public function preSetData(FormEvent $event)
    {
        if($event->getData() instanceof Documento)
        {
            $entity = $event->getData();
            $form = $event->getForm();

            if(!$entity || !$entity->getId() == null)
            {
                $this->addField($form);
                if($entity->getFormats()->isEmpty())
                {
                    $entity->addFormat(new FormatoAsociado());
                }
            }

            $event->setData($entity);
        }
    }

    public function preSubmit(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        if(!array_key_exists('formats',$entity))
        {
            $config = $form->getConfig();

            if($config->hasOption('type_form'))
            {
                if($config->getOption('type_form') !== 'new')
                {
                    $entity['formats'] = array();
                }
            }

        }
        else
        {
            foreach($entity['formats'] as $format)
            {
                if(empty($format['formatName']) && empty($format['file']))
                {
                    unset($format['formatName']);
                    unset($format['file']);
                    $entity['formats'] = array();
                }
            }
        }

        $event->setData($entity);
    }

    public function addField(FormInterface $formInterface)
    {
        $formInterface->add('formats','collection',array(
            'empty_data' => null,
            'required' => false,
            'type' => new FormatoAsociadoType(),
            'label' => null,
            'allow_add' => true,
            'by_reference' => false,
            'allow_delete' => true,
            'error_bubbling' => false,
            'cascade_validation'=>false,
        ));

        $formInterface->getConfig()->hasOption('form_type');
    }
}