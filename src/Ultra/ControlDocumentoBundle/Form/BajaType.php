<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BajaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
             $builder
                 ->add('fechaBaja','date')
                 ->add('tipoBaja', 'choice', array(
                     'label' => 'Ubicación',
                     'empty_value' => 'Seleccionar...',
                     'attr' => array('class' => 'form-control selectpicker'),
                     'choices' => array('Renuncia Voluntaria' => 'Renuncia Voluntaria', 'Despido Injustificado' => 'Despido Injustificado','Termino de contrato'=>'Termino de contrato')
                 ))
                 ->add('comentbaja','text', array(
                     'label' => 'Actividad',
                     'attr' => array(
                         'class' => 'form-control input-sm',
                         'placeholder'=>'Anotaciones/Comentarios sobre la baja',
                         'maxlength'=>'255',
                     ),
                     'empty_data' => ' ',
                     'required' => true,
                 ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\PerfilCurricula'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_baja';
    }
}