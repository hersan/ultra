<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExperienciaProfesionalType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('puesto','text', array(
                'label' => 'Puesto',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Puesto ocupado'
                )
            ))
            ->add('inicio')
            ->add('termino')
            //->add('comprobante')
            //->add('path')
            //->add('curricula')
            ->add('empresa','entity',array(
                'class' => 'ControlDocumentoBundle:Empresa',
                'label' => 'Empresa',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                )
            ))
            /*->add('add','button',array(
                'label' => '+',
                'attr' => array(
                    'class' => 'btn btn-success',
                    'id' => 'btn-add'
                ),
            ))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_experiencia_profesional';
    }
}
