<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 13/05/14
 * Time: 09:48 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class VigenciaProdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('procedimiento','entity', array(
                'class' => 'ControlDocumentoBundle:Documento',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Procedimientos',
                'required' => false,
                'empty_value' => 'Selecciona el procedimiento ...'
            ))
            ->add('fv', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha límite',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {

        return 'ultra_controldocumentobundle_vigenciaprod';
    }
}