<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TipoDocumentoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Tipo de documento',
                        'autocomplete'=>'on',
                        'maxlength'=>'15',
                        'title'=>'Ingresa el título del tipo de documento',
                        'autofocus'=>true),
                     'required'=>false
                ))
            ->add('vigencia','number',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '(Meses)',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Vigencia del tipo'),
                    'required'=>false
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\TipoDocumento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_tipodocumento';
    }
}
