<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 27/03/14
 * Time: 05:47 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class SeleccionaContratoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('contrato','entity', array(
                'class' => 'ControlDocumentoBundle:Contrato',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Contrato',
                'required' => false,
                'empty_value' => 'Selecciona el contrato ...'
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {

        return 'ultra_controldocumentobundle_seleccionacontrato';
    }
}