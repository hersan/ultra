<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClienteType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Escribe aquí el nombre del cliente',
                        'autocomplete'=>'on',
                        'maxlength'=>'35',
                        'title'=>'Nombre del cliente',
                        'autofocus'=>true),
                    'label' => 'Nombre',
                    'required' => false
            ))

            ->add('siglas','text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Siglas del cliente (Ej. ULTRA)',
                        'autocomplete'=>'on',
                        'maxlength'=>'6',
                        'title'=>'Siglas del cliente'),
                    'label' => 'Siglas',
                    'required' => false
                ))

            ->add('esSocio','checkbox', array(
                    'required' => false,
                    'label' => 'Socio',
                    'attr' => array(
                        'class' => 'checkbox',
                        'title' => 'Marca si el cliente es socio'
                    )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Cliente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_cliente';
    }
}
