<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
/*use Ultra\ControlDocumentoBundle\Entity\Competencia;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Ultra\ControlDocumentoBundle\Entity\CursoInterno;*/
use Symfony\Component\PropertyAccess\PropertyAccess;


class RHumCapturaType extends AbstractType
{
    private $repository;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroControl','text', array(
                'label' => 'Número de Control',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el número de control',
                    'autocomplete'=>'on',
                    'title'=>'Número de control',
                    'maxlength'=>'10',
                    'autofocus'=>true),
                'required' => false
            ))
            /*->add('npcfe','text', array(
                'label' => 'Nip-CFE',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el nip',
                    'autocomplete'=>'on',
                    'title'=>'Nip-CFE',
                    'maxlength'=>'10'),
                'required' => false
            ))
            ->add('rpecfe','text', array(
                'label' => 'RPE-CFE',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el RPE',
                    'autocomplete'=>'on',
                    'title'=>'RPE-CFE',
                    'maxlength'=>'10'),
                'required' => false
            ))*/
            ->add('nombre','text', array(
                'label' => 'Nombre',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el nombre',
                    'maxlength'=>'50',
                    'autocomplete'=>'on',
                    'title'=>'Nombre')
            ))

            ->add('apellidoPaterno','text', array(
                'label' => 'Apellido Paterno',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el apellido paterno',
                    'autocomplete'=>'on',
                    'title'=>'Apellido Paterno',
                    'maxlength'=>'50')
            ))
            ->add('apellidoMaterno','text',array(
                'label' => 'Apellido Materno',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el apellido materno',
                    'autocomplete'=>'on',
                    'title'=>'Apellido materno',
                    'maxlength'=>'50')
            ))
            ->add('genero','choice',array(
                'label' => 'Género',
                'attr' => array('class' => ''),
                'choices' => array('H' => 'Hombre', 'M' => 'Mujer'),
                'expanded' => true
            ))
            /*->add('cedula','text',array(
                    'label' => 'Cédula Profesional',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el número de cédula',
                        'autocomplete'=>'on',
                        'title'=>'Cédula profesional',
                        'maxlength'=>'8')
                ))
            ->add('rfc','text',array(
                    'label' => 'R.F.C.',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el RFC',
                        'autocomplete'=>'on',
                        'title'=>'Registro Federal de Contribuyentes',
                        'maxlength'=>'13')
                ))*/
            /*->add('ife','text',array(
                    'label' => 'Numero de Credencial Ife',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el número de IFE',
                        'autocomplete'=>'on',
                        'title'=>'IFe',
                        'maxlength'=>'13')
                ))*/
            ->add('curp','text',array(
                'label' => 'CURP',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el CURP',
                    'autocomplete'=>'on',
                    'title'=>'CURP',
                    'maxlength'=>'18')
            ))
            ->add('imss','text',array(
                'label' => 'No. IMSS',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el número de IMSS',
                    'autocomplete'=>'on',
                    'title'=>'IMSS',
                    'maxlength'=>'15')
            ))
            ->add('cartilla','text',array(
                'label' => 'Cartilla Militar',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el número de cartilla',
                    'autocomplete'=>'on',
                    'title'=>'Servicio Militar',
                    'maxlength'=>'11')
            ))
            /*->add('lugarNacimiento','text', array(
                    'label' => 'Lugar de Nacimiento',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el lugar de nacimiento',
                        'autocomplete'=>'on',
                        'title'=>'Lugar de nacimiento',
                        'maxlength'=>'100')
                ))*/
            ->add('nacionalidad','country',array(
                'label' => 'Nacionalidad',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search'=> true,
                    'maxlength'=>'50')
            ))
            ->add('fechaNacimiento','birthday',array(
                'input' => 'datetime',
                'label' => 'Fecha de Nacimiento',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            /*->add('domicilio','textarea', array(
                    'label' => 'Domicilio',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el domicilio completo',
                        'autocomplete'=>'on',
                        'title'=>'Domicilio completo',
                        'maxlength'=>'250')
                ))*/
            ->add('telefono','text', array(
                'label' => 'Teléfono',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el teléfono',
                    'autocomplete'=>'on',
                    'title'=>'Teléfono',
                    'maxlength'=>'12')
            ))
            ->add('celular','text', array(
                'label' => 'Celular',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Ingresa el número celular',
                    'autocomplete'=>'on',
                    'title'=>'Número de celular',
                    'maxlength'=>'12')
            ))
            //->add('tipoDocumento')
            //->add('nivelAcademico')
            ->add('estadoCivil','entity',array(
                'required' => false,
                'class' => 'ControlDocumentoBundle:EstadoCivil',
                'empty_data' => null,
                'empty_value'=> 'Selecciona un estado...',
                'attr' => array(
                    'class' => 'selectpicker',
                    'data-live-search' => true,
                )
            ))
            //->add('user')
  /*          ->add('competencias','collection',array(
                'required'=>false,
                'type' => new CompetenciaType(),
                'label' => 'Competencia Adicional',
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'empty_data' => null,
            ))
            ->add('cursosInternos','collection',array(
                'required'=>false,
                'type' => new CursoInternoType(),
                'label' => 'Competencia Interna',
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'empty_data' => null,
            ))
            ->add('cursosExternos','collection',array(
                'required'=>false,
                'type' => new CursoExternoType(),
                'label' => 'Competencia Externo',
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'empty_data' => null,
            ))*/
            /*->add('file',new AnexoType(),array(
                        'label' => false,
                    ))*/
        ;


        /*$eraseNullFields = function(&$entity = null, $property = null){
            if($entity == null || $property == null)
            {
                return;
            }

            $accessor = PropertyAccess::createPropertyAccessor();

            $cursos = $accessor->getValue($entity,'['.$property.']');
            if(is_array($cursos))
            {
                $keys = null;
                /*foreach($cursos as $curso)
                {
                    if(is_array($curso)){
                        $keys = array_keys($curso, '', true);
                    }

                    if(!array_filter($curso) || $keys > 0)
                    {
                        //unset($entity[$property]);
                        $entity[$property] = array();
                    }
                }
            }

        };*/

        /*$builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){
                $entity = $event->getData();
                $form = $event->getForm();

                if($entity->getCompetencias()->isEmpty()){
                    $entity->addCompetencia(new Competencia());
                }

                if($entity->getCursosExternos()->isEmpty()){
                    $entity->addCursosExterno(new CursoExterno());
                }

                if($entity->getCursosInternos()->isEmpty()){
                    $entity->addCursosInterno(new CursoInterno());
                }

                $event->setData($entity);
            }
        );*/

        /*$builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function(FormEvent $event) use ($eraseNullFields){
                $entity = $event->getData();
                $form = $event->getForm();

                $eraseNullFields($entity,'competencias');
                $eraseNullFields($entity,'cursosInternos');
                $eraseNullFields($entity,'cursosExternos');


                //$form->remove('competencias');
                //$form->remove('cursosInternos');
                //$form->remove('cursosExternos');

                $event->setData($entity);
            }
        );*/

       /* $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function(FormEvent $event) use ($eraseNullFields){
                $entity = $event->getData();
                $form = $event->getForm();

                if(!$entity->getCompetencias()->isEmpty()){
                    $keys = $entity->getCompetencias()->getKeys();
                    foreach($keys as $key){
                        $competencia = $entity->getCompetencias()->get($key);
                        if($competencia->getId() === null){
                            if($competencia->getCurso() === null && $competencia->getTipoAdquisicion() === null){
                                $entity->getCompetencias()->removeElement($competencia);
                            }
                        }
                    }
                }

                if(!$entity->getCursosExternos()->isEmpty()){
                    $keys = $entity->getCursosExternos()->getKeys();
                    foreach($keys as $key){
                        $curso = $entity->getCursosExternos()->get($key);
                        if($curso->getId() === null){
                            if($curso->getCursoExterno() === null && $curso->getTipoAdquisicion() === null){
                                $entity->getCompetencias()->removeElement($curso);
                            }
                        }
                    }
                }

                if(!$entity->getCursosInternos()->isEmpty()){
                    $keys = $entity->getCursosInternos()->getKeys();
                    foreach($keys as $key){
                        $curso = $entity->getCursosInternos()->get($key);
                        if($curso->getId() === null){
                            if($curso->getId() === null){
                                if($curso->getCursoInterno() === null && $curso->getTipoAdquisicion() === null){
                                    $entity->getCompetencias()->removeElement($curso);
                                }
                            }
                        }
                    }
                }

                $event->setData($entity);
            }
        );*/

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
            'required' => false,
            //'cascade_validation' => true,
            'validation_groups' => array('rhum')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhum_curricula_type';
    }
}
