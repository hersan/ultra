<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class GeneralDocumentType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title','text', array(
                'label' => 'Título',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('year','text', array(
                'label' => 'Año',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('type','entity', array(
                'class' => 'ControlDocumentoBundle:TipoDocumento',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.id = 11')
                        ->orWhere('t.id = 12')
                        ->orWhere('t.id = 13');
                },
                'label' => 'Tipo',
                'empty_data' => null,
                'empty_value' => 'Selecciona...',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('curriculum', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Generó',
                'empty_data' => null,
                'empty_value' => 'Selecciona...',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('file','file',array(
                'label' => 'Agrega un Archivo'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\GeneralDocument',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_generaldocument';
    }
}
