<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConocimientoEspecialType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->objectManager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $courses = $this->objectManager
                    ->getRepository('ControlDocumentoBundle:Curso')
                    ->createQueryBuilder('c')
                    ->getQuery()
                    ->useResultCache(true)
                    ->getResult();
        ;
        $builder
            ->add('curso','entity',array(
                    'label' => ' ',
                    'empty_data' => null,
                    'empty_value' => 'Seleccionar...',
                    'class' => 'Ultra\ControlDocumentoBundle\Entity\Curso',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    ),
                    'choices' => $courses,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_conocimiento_especial_type';
    }
}
