<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrainerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', 'text', array(
                'label' => 'Nombre',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('paternalSurname', 'text', array(
                'label' => 'A. Paterno',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('maternalSurname', 'text', array(
                'label' => 'A. Materno',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('courses', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curso',
                'label' => 'Cursos',
                'empty_value' => 'Seleccione un Curso',
                'empty_data' => 'null',
                'multiple' => true,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('file', 'file', array(
                'label' => 'Agregar Pdf',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Trainer',
            'type_form' => 'edit',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_trainer';
    }
}
