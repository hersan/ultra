<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 7/04/14
 * Time: 05:11 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;

class PdfCompetenciaType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
                'label' => 'Agrega una evaluación',
                'required' => false,
                'constraints' => array(
                    new File(array(
                        'maxSize' => '5M',
                        'mimeTypes' => array(
                            'application/pdf',
                            'application/x-pdf',
                        ),
                        'mimeTypesMessage' => 'Por favor agregue un pdf valido',
                    ))
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\PdfCompetencia',
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_pdf_competencia';
    }
}