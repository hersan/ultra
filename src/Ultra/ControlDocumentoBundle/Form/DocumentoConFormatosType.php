<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 7/11/14
 * Time: 04:22 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;
use Ultra\ControlDocumentoBundle\Model\DocumentoConFormatos;

class DocumentoConFormatosType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('documento','entity', array(
                    'required' => true,
                    'empty_data' => null,
                    'empty_value' => 'Seleccione el documento',
                    'error_bubbling' => false,
                    //'empty_value' => 'Seleccionar...',
                    'class' => 'ControlDocumentoBundle:Documento',
                    'label' => ' ',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title'=>'Selecciona la capacitación requerida',
                    )
                ))
            ->add('formatos','collection',array(
                    'empty_data' => null,
                    'required' => false,
                    'type' => new FormatoAsociadoType(),
                    'label' => null,
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=>false,
                ))
            ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'preSetData'));
    }

    public function preSetData(FormEvent $event)
    {
        if($event->getData() instanceof DocumentoConFormatos)
        {
            $entity = $event->getData();

            if($entity->getFormatos()->isEmpty())
            {
                $entity->addFormato(new FormatoAsociado());
            }

            $event->setData($entity);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => '\Ultra\ControlDocumentoBundle\Model\DocumentoConFormatos',
                'required' => false,
            ));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'documento_formatos';
    }
}