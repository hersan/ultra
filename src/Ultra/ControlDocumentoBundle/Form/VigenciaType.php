<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 12/05/14
 * Time: 11:25 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class VigenciaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cursos','entity', array(
                'class' => 'ControlDocumentoBundle:Curso',
                'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true),
                'label' => 'Cursos',
                'required' => false,
                'empty_value' => 'Selecciona el curso ...'
            ))
            ->add('fv', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha límite',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
        ;
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {

        return 'ultra_controldocumentobundle_vigencia';
    }
}