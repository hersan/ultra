<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Ultra\ControlDocumentoBundle\Entity\Institucion;
use Ultra\ControlDocumentoBundle\Entity\Profesion;
use Doctrine\ORM\EntityRepository;

class FormacionAcademicaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('certificado')
            ->add('inicio')
            ->add('termino')
            ->add('institucion','entity', array(
                    'class' => 'ControlDocumentoBundle:Institucion',
                    'empty_data' => '',
                    'empty_value' => 'Seleccione una Institución',
                    'label' => 'Institución',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'id' => 'institucion',
                        'data-live-search' => true,
                    ),
                    'query_builder' => function(EntityRepository $er) {
                            return $er->createQueryBuilder('i')->leftJoin('i.profesiones', 'p');
                        }
                ))
            //->add('profesion')
            //->add('curricula')
        ;

        $formModifier = function (FormInterface $form, Institucion $institucion = null) {
            $profesiones = null === $institucion ? array() : $institucion->getProfesiones();

            $form->add('profesion','entity', array(
                    'class' => 'ControlDocumentoBundle:Profesion',
                    'choices' => $profesiones,
                    'empty_data' => '',
                    'empty_value' => 'Selecciona una Profesión',
                    'label' => 'Profesion',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'id' => 'profesion',
                        'data-live-search' => true,
                    ),
                ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier){
                $entity = $event->getData();

                $formModifier($event->getForm(), $entity->getInstitucion());


            }
        );

        $builder->get('institucion')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $institucion = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $institucion);
            }
        );

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\FormacionAcademica',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_formacion_academica_type';
    }
}
