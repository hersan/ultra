<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 7/01/14
 * Time: 04:04 PM
 */

namespace Ultra\ControlDocumentoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\SubmitButtonTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UltraSubmitType extends AbstractType implements SubmitButtonTypeInterface {

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['clicked'] = $form->isClicked();
        $view->vars['glyphicon'] = $options['glyphicon'];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
                'glyphicon' => null
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'button';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'usubmit';
    }
}