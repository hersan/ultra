<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 15/04/14
 * Time: 02:28 PM
 */

namespace Ultra\ControlDocumentoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CedulaProfesionalType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file','file',array(
                'label' => 'Escolaridad',
                'required' => false,
                'empty_data' => null
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Entity\PdfCedulaProfesional'
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_cedula_type';
    }
}