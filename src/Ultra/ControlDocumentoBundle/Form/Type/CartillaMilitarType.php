<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 15/04/14
 * Time: 03:24 PM
 */

namespace Ultra\ControlDocumentoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CartillaMilitarType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file','file',array(
                'label' => 'Cartilla Militar'
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Entity\PdfCartillaMilitar'
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_cartilla_type';
    }
}