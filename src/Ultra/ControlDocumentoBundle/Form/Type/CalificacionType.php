<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/05/14
 * Time: 09:51 AM
 */

namespace Ultra\ControlDocumentoBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CalificacionType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file','file',array(
                'label' => 'Anexo 3. Evaluación',
                'required' => false,
                'empty_data' => null
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Entity\PdfCalificacion'
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_calificacion_type';
    }
}