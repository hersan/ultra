<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/04/14
 * Time: 12:39 PM
 */

namespace Ultra\ControlDocumentoBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Ultra\ControlDocumentoBundle\Model\PdfCursoExterno;

class PdfCursoExternoType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
                'label' => 'Agrega una evaluación',
                'required' => false,
                'error_bubbling' => false,
                'constraints' => array(
                    new File(array(
                        'maxSize' => '5M',
                        'mimeTypes' => array(
                            'application/pdf',
                            'application/x-pdf',
                        ),
                        'mimeTypesMessage' => 'Por favor agregue un pdf válido',
                    ))
                )
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\PdfCursoExterno',
            ));
    }


    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
       return 'ultra_pdf_cursoexterno';
    }
}