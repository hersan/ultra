<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FormatoAsociadoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('formatName','text',array(
                    'label' => 'Nombre del Formato',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'div_column' => 'col-sm-5'
                ))
            ->add('file', 'file', array(
                    'label' => 'Archivo',
                    'attr' => array(

                    ),
                    'div_column' => 'col-sm-5'
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\FormatoAsociado'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_formatoasociado';
    }
}
