<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CursoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            
        $builder
            ->add('clave','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=>'Escriba aquí la clave del curso',
                    'autocomplete'=>'on',
                    'maxlength'=>'250',
                    'autofocus'=>true,
                    'title'=>'Clave'),
                'label' => 'Clave',
                'required' => false
            ))
            ->add('nombre','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=>'Escriba aquí el nombre corto',
                    'autocomplete'=>'on',
                    'maxlength'=>'250',
                    'autofocus'=>true,
                    'title'=>'Nombre corto'),
                    'label' => 'Nombre corto',
                    'required' => false
                ))
            ->add('descripcion','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=>'Escriba aquí una descripción breve del curso',
                    'autocomplete'=>'on',
                    'maxlength'=>'250',
                    'title'=>'Descripción'),
                    'label' => 'Descripción',
                    'required' => false
                ))
            ->add('tipo','entity', array(
                'class' => 'ControlDocumentoBundle:TipoCurso',
                'required' => false,
                'label' => 'Tipo',
                'empty_value' => 'Selecciona el tipo de curso',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'selectpicker',
                    'data-live-search' => true
                )
            ))
            ->add('institucion','entity', array(
                    'class' => 'ControlDocumentoBundle:Institucion',
                    'required' => false,
                    'attr' => array('class' => 'selectpicker',
                    'data-live-search' => true
                    ),
                    'label' => 'Institución',
                    'empty_value'=>'Selecciona la institución...',
                    'empty_data' => null,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Curso'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_curso';
    }

}
