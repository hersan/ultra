<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class InstitucionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre','text',array(
                'label' => 'Nombre',
                'required' => false,
                'attr' => array('class' => 'form-control input-sm',
                'placeholder'=>'Escriba aquí el nombre del Centro de Estudio',
                'autocomplete'=>'on',
                'maxlength'=>'200',
                'autofocus'=>true,
                'title'=>'Institución')
            ))
            ->add('direccion','text',array(
                'label' => 'Dirección',
                'required' => false,
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=>'Escriba aquí la dirección de la institución',
                    'autocomplete'=>'on',
                    'maxlength'=>'200',
                    'autofocus'=>true,
                    'title'=>'Dirección')
            ))
            ->add('tipo','entity', array(
                'class' => 'ControlDocumentoBundle:TipoInstitucion',
                'required' => false,
                'label' => 'Tipo',
                'empty_value' => 'Selecciona',
                'attr' => array(
                    'class' => 'selectpicker',
                    'data-live-search' => true
                )
            ));


    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Institucion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_institucion';
    }
}
