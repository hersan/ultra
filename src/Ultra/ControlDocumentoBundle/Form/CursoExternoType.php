<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;

class CursoExternoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cursoExterno','entity',array(
                    'required' => false,
                    'class' => 'ControlDocumentoBundle:Curso',
                    'label' => 'Competencia (Externa)',
                    'empty_value'=>'Selecciona un curso',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    )
                ))
            ->add('fechaRealizacion','date',array(
                    'label' => 'Fecha de realización',
                    'widget' => 'choice',
                    'empty_data' => null,
                    'empty_value' => ' ',
                    'years' => range(date('Y')-40, date('Y')+5),
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'class' => ''
                    )
                ))
            ->add('fechaVigencia','date',array(
                    'label' => 'Fecha de vigencia',
                    'widget' => 'choice',
                    'empty_data' => null,
                    'empty_value' => ' ',
                    'years' => range(date('Y')-40, date('Y')+5),
                    'format' => 'yyyy-MM-dd',
                    'attr' => array(
                        'class' => ''
                    )
                ))
            ->add('tipoAdquisicion','entity',array(
                    'class' => 'ControlDocumentoBundle:TipoAdquisicion',
                    'label' => 'Tipo de Adquisición',
                    'empty_value' => 'Seleccionar...',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                ))
            ->add('file','file',array(
                    'required' => false,
                    'error_bubbling' => false,
                    'label' => 'Agregar Certificado',
                    //'cascade_validation'=>false,
                ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetData'));
    }

    public function onPreSetData(FormEvent $event){
        $form = $event->getForm();
        $entity = $event->getData();

        if($entity instanceof CursoExterno)
        {
            $this->addElements($form,$entity);
        }
    }

    public function addElements(FormInterface $form, CursoExterno $file)
    {
        $label = 'Agregar Archivo';
        $class = '';

        if($file->getPath() !== null && $file->getPdfCertificado() !== null)
        {
            $label = 'Archivo disponible (*)';
            $class = 'text-danger';
        }


        $form->add('file','file',array(
            'required' => false,
            'label' => $label,
            'label_attr' => array(
                'class' => $class,
            ),

        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\CursoExterno',
            'cascade_validation' => true,
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_cursoexterno';
    }
}
