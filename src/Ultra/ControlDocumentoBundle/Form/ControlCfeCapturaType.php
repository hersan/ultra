<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Symfony\Component\PropertyAccess\PropertyAccess;


class ControlCfeCapturaType extends AbstractType
{
    private $repository;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('cursosExternos','collection',array(
                'required'=>false,
                'type' => new CursoExternoType(),
                'label' => 'Competencia Externo',
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'empty_data' => null,
            ))
        ;


        $eraseNullFields = function(&$entity = null, $property = null){
            if($entity == null || $property == null)
            {
                return;
            }

            $accessor = PropertyAccess::createPropertyAccessor();

            $cursos = $accessor->getValue($entity,'['.$property.']');
            if(is_array($cursos))
            {
                $keys = null;
                /*foreach($cursos as $curso)
                {
                    if(is_array($curso)){
                        $keys = array_keys($curso, '', true);
                    }

                    if(!array_filter($curso) || $keys > 0)
                    {
                        //unset($entity[$property]);
                        $entity[$property] = array();
                    }
                }*/
            }

        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){
                $entity = $event->getData();
                $form = $event->getForm();

               /* if($entity->getCompetencias()->isEmpty()){
                    $entity->addCompetencia(new Competencia());
                }*/

                if($entity->getCursosExternos()->isEmpty()){
                    $entity->addCursosExterno(new CursoExterno());
                }

                /*if($entity->getCursosInternos()->isEmpty()){
                    $entity->addCursosInterno(new CursoInterno());
                }*/

                $event->setData($entity);
            }
        );

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
            'required' => false,
            //'cascade_validation' => true,
            'validation_groups' => array('control')
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_control_cfe_captura_type';
    }
}
