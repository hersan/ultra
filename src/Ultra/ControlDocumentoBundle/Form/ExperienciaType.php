<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExperienciaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tiempo','text', array(
                    'required' => false,
                    'label' => 'Años',
                    'attr' => array(
                        'class' => 'form-control input-sm',
                        'placeholder'=> 'Años de experiencia requeridos',
                        'title'=>'Años',
                        'maxlength'=>'2'
                    )
                ))

            ->add('meses','text', array(
                    'required' => false,
                    'label' => 'Meses',
                    'attr' => array(
                        'class' => 'form-control input-sm',
                        'placeholder'=> 'Meses de experiencia requeridos',
                        'title'=>'Meses',
                        'maxlength'=>'2'
                    )
                ))
            ->add('descripcion','text',array(
                    'required' => false,
                    'label' => 'Descripción',
                    'attr' => array(
                        'class' => 'form-control input-sm',
                        'placeholder'=>'Describe el tipo de experiencia',
                        'autocomplete'=>'on',
                        'maxlength'=>'250',
                        'title'=>'Detalle de la experiencia'
                    )
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Experiencia'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_experiencia';
    }
}
