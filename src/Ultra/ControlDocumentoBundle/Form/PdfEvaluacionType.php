<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 31/03/14
 * Time: 10:23 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Ultra\ControlDocumentoBundle\Model\PdfEvaluacion;

class PdfEvaluacionType extends AbstractType{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
                'label' => 'Agrega una evaluación',
                'required' => false,
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\PdfEvaluacion',
                'empty_data' => function (FormInterface $form) {
                        return new PdfEvaluacion(
                            $form->getParent()->getData()
                        );
                    }
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'ultra_pdf_evaluacion';
    }

} 