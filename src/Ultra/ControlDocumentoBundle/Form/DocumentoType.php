<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\Documento;
use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;

class DocumentoType extends AbstractType
{

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('clave','text',array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Clave del procedimiento',
                        'autocomplete'=>'on',
                        'maxlength'=>'50',
                        'title'=>'Clave del procedimiento',
                        'autofocus'=>true),
                    'required' => true,
                ))
            ->add('emdrac', 'text', array(
                'label' => 'Emdrac',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Numero Emdrac',
                    'autocomplete'=>'on',
                    'maxlength'=>'10',
                    'title'=>'Numero Endrac',
                    'autofocus'=>true),
                'required' => false,
            ))
            ->add('revisionEmdrac', 'number', array(
                'label' => 'Revisión Emdrac',
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Revisión Emdrac',
                    'autocomplete'=>'on',
                    'maxlength'=>'10',
                    'title'=>'Revisión Endrac',
                    'autofocus'=>true),
                'required' => false,
            ))
            ->add('revision', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> '#',
                        'autocomplete'=>'on',
                        'maxlength'=>'2',
                        'title'=>'Revisión'),
                    'label' => 'Revisión'
                ))

            ->add('redistribution',
                    'text',
                    array(
                        'label' => 'Redistribución',
                        'attr' => array(
                            'class' => 'form-control input-sm',
                            'placeholder' => '#',
                            'maxlength' => 2,
                            'title' => 'Redistribución'
                        )
                    ))

            ->add('titulo', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Ingresa el título del procedimiento',
                        'autocomplete'=>'on',
                        'title'=>'Título del procedimiento'),
                    'label' => 'Título'
                ))

            ->add('aprobado', 'date', array(
                'input' => 'datetime',
                'label' => 'Aprobado',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('vigencia', 'date', array(
                'input' => 'datetime',
                'label' => 'Vigencia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('liberado', 'date', array(
                'input' => 'datetime',
                'label' => 'Liberado (CFE)',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('verificar','checkbox',array(
                    'label' => '¿Liberado?',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'empty_data' => null,
                    'mapped'=>false,
                    'data' => true,
                ))

            ->add('transmitido', 'date', array(
                'input' => 'datetime',
                'label' => 'Distribuido',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('file', 'file', array(
                    'label' => 'Archivo',
                    //'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                    //'property_path' => 'file'
                ))
            //->add('doc', 'file')
            ->add('disciplina', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:Disciplina',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                    'label' => 'Disciplina (Área:Proyecto)',
                    'choices' => $this->getDisciplinas(),
                ))
            ->add('condicion', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:Condicion',
                    'attr' => array('class' => 'form-control selectpicker'),
                    'label' => 'Condición'
                ))
            ->add('tipoDocumento', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'ControlDocumentoBundle:TipoDocumento',
                    'query_builder' => function( EntityRepository $er){
                        return $er->createQueryBuilder('t')
                                ->where('t.id = 1')
                                ->orWhere('t.id = 2')
                                ->orWhere('t.id = 5')
                                ->orWhere('t.id = 6');
                    },
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    ),
                    'label' => 'Tipo de Documento'
                ))

            /*->add('formats','collection',array(
                    'empty_data' => null,
                    'required' => false,
                    'type' => new FormatoAsociadoType(),
                    'label' => null,
                    'allow_add' => true,
                    'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=>false,
                ))*/
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'preSetData'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, array($this, 'preSubmit'));
    }

    public function preSetData(FormEvent $event)
    {
        if($event->getData() instanceof Documento)
        {
            $entity = $event->getData();
            $form = $event->getForm();

            if(!$entity || !$entity->getId() == null)
            {
                $this->addField($form);
                if($entity->getFormats()->isEmpty())
                {
                    $entity->addFormat(new FormatoAsociado());
                }
            }

            $event->setData($entity);
        }
    }

    public function preSubmit(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        if(!array_key_exists('formats',$entity))
        {
            $config = $form->getConfig();

            if($config->hasOption('type_form'))
            {
                if($config->getOption('type_form') !== 'new')
                {
                    $entity['formats'] = array();
                }
            }

        }
        else
        {
            foreach($entity['formats'] as $format)
            {
                if(empty($format['formatName']) && empty($format['file']))
                {
                    unset($format['formatName']);
                    unset($format['file']);
                    $entity['formats'] = array();
                }
            }
        }

        $event->setData($entity);
    }

    public function addField(FormInterface $formInterface)
    {
        $formInterface->add('formats','collection',array(
                'empty_data' => null,
                'required' => false,
                'type' => new FormatoAsociadoType(),
                'label' => null,
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'cascade_validation'=>false,
            ));

        $formInterface->getConfig()->hasOption('form_type');
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Documento',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_documento_procedimiento';
    }

    public function getDisciplinas(){
        $query =  $this->entityManager->createQueryBuilder()
                        ->select('d,area,contrato')
                        ->from('ControlDocumentoBundle:Disciplina','d')
                            ->join('d.area','area')
                            ->join('area.contrato','contrato')
                            ->join('contrato.cliente','cliente')
                        ->where('contrato.abierto = 1');

        $result = $query->getQuery()->getResult();

        return $result;
    }
}
