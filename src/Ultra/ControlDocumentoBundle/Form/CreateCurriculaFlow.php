<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 26/02/14
 * Time: 05:19 PM
 */
// src/MyCompany/MyBundle/Form/CreateVehicleFlow.php
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;
use Symfony\Component\Form\FormTypeInterface;

class CreateCurriculaFlow extends FormFlow {

    /**
     * @var FormTypeInterface
     */
    protected $formType;

    public function setFormType(FormTypeInterface $formType) {
        $this->formType = $formType;
    }

    public function getName() {
        return 'createCurricula';
    }

    protected function loadStepsConfig() {
        return array(
            array(
                'label' => 'numeroControl',
                'type' => $this->formType,
            ),
            array(
                'label' => 'nombre',
                'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                        return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->canHaveEngine();
                    },
            ),
            array(
                'label' => 'confirmation',
            ),
        );
    }
}