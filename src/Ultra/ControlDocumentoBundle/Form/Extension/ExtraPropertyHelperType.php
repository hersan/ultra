<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 10/11/14
 * Time: 09:35 AM
 */

namespace Ultra\ControlDocumentoBundle\Form\Extension;


use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExtraPropertyHelperType extends AbstractTypeExtension{

    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return 'form';
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['div_column'] = $options['div_column'];
        $view->vars['user'] = $options['user'];
        $view->vars['type_form'] = $options['type_form'];
        $view->vars['has_file'] = $options['has_file'];
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'div_column' => null,
            'user' => null,
            'type_form' => null,
            'has_file' => false,
        ));
    }

}