<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/05/14
 * Time: 09:57 AM
 */

namespace Ultra\ControlDocumentoBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Form\Type\CalificacionType;
use Ultra\ControlDocumentoBundle\Form\Type\CertificadoType;
use Symfony\Component\Form\FormInterface;
use Ultra\ControlDocumentoBundle\Model\Evaluacion;

class EvaluacionType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('certificado',new CertificadoType(),array(
            'label' => false,
        ))
        ->add('calificacion',new CalificacionType(),array(
            'label' => false,
        ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'Ultra\ControlDocumentoBundle\Model\Evaluacion',
            ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_evaluacion_type';
    }
}