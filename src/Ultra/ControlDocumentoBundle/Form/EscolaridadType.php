<?php

namespace Ultra\ControlDocumentoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EscolaridadType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profesion','entity', array(
                    'required' => false,
                    'class' => 'ControlDocumentoBundle:Profesion',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title'=>'Selecciona una profesión para el perfil',
                    ),
                    'label' => 'Profesión',
                    'empty_value' => 'Selecciona una profesión para el perfil',
                    'empty_data' => null,
                ))
            ->add('titulo','checkbox',array(
                    'label' => 'Título',
                    'attr' => array(
                        'class' => 'checkbox',
                    ),
                    'required' => false,
                ))
            ->add('cedula','checkbox', array(
                    'label' => 'Cedula',
                    'attr' => array(
                        'class' => 'checkbox',
                    ),
                    'required' => false
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\ControlDocumentoBundle\Entity\Escolaridad'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_controldocumentobundle_escolaridad';
    }
}
