<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/05/14
 * Time: 10:13 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Traversable;
use Ultra\ControlDocumentoBundle\Entity\PerfilCurricula;

class Evaluacion implements \IteratorAggregate {

    private $certificado;

    private $calificacion;

    /**
     * @param mixed $calificacion
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    }

    /**
     * @return mixed
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * @param mixed $certificado
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
    }

    /**
     * @return mixed
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator(get_object_vars($this));
    }
}