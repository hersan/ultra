<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 27/02/15
 * Time: 01:15 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface ItemInterface {

    public function setSection(SectionInterface $section);
    public function getSection();

}