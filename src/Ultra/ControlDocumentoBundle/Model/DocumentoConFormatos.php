<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 10/11/14
 * Time: 12:00 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;

class DocumentoConFormatos {

    private $documento;

    private $formatos;

    public function __construct(){
        $this->formatos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param mixed $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    /**
     * @return mixed
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    public function addFormato(FormatoAsociado $formatoAsociado)
    {
        $this->formatos[] = $formatoAsociado;
    }

    public function removeFormato(FormatoAsociado $formatoAsociado)
    {
        $this->formatos->removeElement($formatoAsociado);
    }

    /**
     * @return mixed
     */
    public function getFormatos()
    {
        return $this->formatos;
    }

} 