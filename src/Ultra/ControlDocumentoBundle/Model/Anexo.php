<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 11/04/14
 * Time: 09:45 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Traversable;
use Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile;
use Ultra\ControlDocumentoBundle\Entity\Curricula;

class Anexo implements \IteratorAggregate {

    private $array;

    private $ife;

    private $licenciaConducir;

    private $cedulaProfesional;

    private $actaNacimiento;

    private $rfc;

    private $curp;

    private $imss;

    private $cartilla;

    private $curricula;

    private $foto;

    private $entity;

    function __construct(Curricula $entity)
    {
        $this->array = new \ArrayIterator();

        if($entity === null){
            throw new \Exception('Argumento invalido: '.__CLASS__.' espera una entidad valida');
        }
        $this->entity = $entity;
    }

    /**
     * @param mixed $cartilla
     */
    public function setCartilla(AbstractUltraFile $cartilla = null)
    {
        $this->cartilla = $cartilla;
        if($this->cartilla->getFile() !== null){
            $this->array->append($this->cartilla);
        }

    }

    /**
     * @return mixed
     */
    public function getCartilla()
    {
        return $this->cartilla;
    }

    /**
     * @param $cedulaProfesional
     * @internal param mixed $cedulaProfesional
     */
    public function setCedulaProfesional(AbstractUltraFile $cedulaProfesional = null)
    {
        $this->cedulaProfesional = $cedulaProfesional;
        if($this->cedulaProfesional->getFile() !== null)
        {
            $this->array->append($cedulaProfesional);
        }
    }

    /**
     * @return mixed
     */
    public function getCedulaProfesional()
    {
        return $this->cedulaProfesional;
    }

    /**
     * @param mixed $curp
     */
    public function setCurp(AbstractUltraFile $curp = null)
    {
        $this->curp = $curp;
        if($this->curp->getFile() !== null)
        {
            $this->array->append($this->curp);
        }

    }

    /**
     * @return mixed
     */
    public function getCurp()
    {
        return $this->curp;
    }

    /**
     * @param mixed $curricula
     */
    public function setCurricula(AbstractUltraFile $curricula)
    {
        $this->curricula = $curricula;
        if($this->curricula->getFile() !== null)
        {
            $this->array->append($this->curricula);
        }
    }

    /**
     * @return mixed
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * @param mixed $foto
     */
    public function setFoto(AbstractUltraFile $foto)
    {
        $this->foto = $foto;
        if($this->foto->getFile() !== null)
        {
            $this->array->append($this->foto);
        }
    }

    /**
     * @return mixed
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param mixed $ife
     */
    public function setIfe(AbstractUltraFile $ife = null)
    {
        $this->ife = $ife;
        if($this->ife->getFile() !== null){
            $this->array->append($ife);
        }
    }

    /**
     * @return mixed
     */
    public function getIfe()
    {
        return $this->ife;
    }

    /**
     * @param mixed $imss
     */
    public function setImss(AbstractUltraFile $imss)
    {
        $this->imss = $imss;
        if($this->imss->getFile() !== null){
            $this->array->append($this->imss);
        }
    }

    /**
     * @return mixed
     */
    public function getImss()
    {
        return $this->imss;
    }

    /**
     * @param mixed $licenciaConducir
     */
    public function setLicenciaConducir($licenciaConducir = null)
    {
        $this->licenciaConducir = $licenciaConducir;
        if($this->licenciaConducir->getFile() !== null)
        {
            $this->array->append($licenciaConducir);
        }
    }

    /**
     * @return mixed
     */
    public function getLicenciaConducir()
    {
        return $this->licenciaConducir;
    }

    /**
     * @param mixed $rfc
     */
    public function setRfc(AbstractUltraFile $rfc)
    {
        $this->rfc = $rfc;
        if($this->rfc->getFile() !== null)
        {
            $this->array->append($this->rfc);
        }
    }

    /**
     * @return mixed
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * @param mixed $actaNacimiento
     */
    public function setActaNacimiento($actaNacimiento)
    {
        $this->actaNacimiento = $actaNacimiento;
        if($this->actaNacimiento->getFile() !== null)
        {
            $this->array->append($this->actaNacimiento);
        }
    }

    /**
     * @return mixed
     */
    public function getActaNacimiento()
    {
        return $this->actaNacimiento;
    }


    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return $this->array;
    }
}