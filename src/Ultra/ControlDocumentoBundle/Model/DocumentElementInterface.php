<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/03/15
 * Time: 12:52 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface DocumentElementInterface {

    public function setElement(DocumentContentInterface $content);
    public function getElement();

}