<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 4/04/14
 * Time: 11:18 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploadFileInterface {

    public function setFile(UploadedFile $file = null);
    public function getFile();
    public function getAbsolutePath();

} 