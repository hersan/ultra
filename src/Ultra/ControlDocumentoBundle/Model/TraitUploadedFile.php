<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 7/05/14
 * Time: 06:52 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;

trait TraitUploadedFile {

    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $path;

    /** @var string */
    private $temp;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $pdf;

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Get file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        if (is_file($this->getAbsolutePath())) {

            $this->temp = $this->getAbsolutePath();
            $this->setPdf(null);

        } else {
            $this->pdf = 'initial';
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadRootDir().'/'.$this->getDisciplina()->getArea()->getContrato()->getNumeroInterno().'/'.$this->pdf;
    }

    public function getUploadRootDir()
    {
        return '/var/sad/'.$this->getUploadDir();
    }

    public function getUploadDir()
    {
        return 'uploads/documentos';
    }

    public function getWebPath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadDir().'/'.$this->pdf;
    }

    public function setUploadRootDir($dir){
        $this->rootDir = $dir;
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    public function getTemporaryPath(){
        return $this->path;
    }

} 