<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 6/08/15
 * Time: 06:04 PM
 */

namespace Ultra\ControlDocumentoBundle\Model\Control;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class GeneralDocumentUploadedTrait
 * @package Ultra\ControlDocumentoBundle\Model\Control
 */
trait GeneralDocumentUploadedTrait
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $path;

    /**
     * @var string
     */
    private $temp;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $hash;

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param string $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadDir().'/'.$this->pdf;
    }

    /**
     * @return string
     */
    public function getUploadDir()
    {
        return property_exists($this, 'uploadDirectory') ? $this->uploadDirectory : 'uploads/general';
    }

    /**
     * @param $dir
     */
    public function setUploadRootDir($dir)
    {
        $this->uploadRootDirectory = $dir;
    }

    /**
     * @param $path
     */
    public function temporaryPath($path)
    {
        $this->path = $path;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {

            $this->setHash($this->getFile()->getPathname());
            $this->setPdf($this->namedPdf());

        }

    }

    /**
     * Get file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        if (is_file($this->getAbsolutePath())) {

            $this->temp = $this->getAbsolutePath();
            $this->setPdf(null);

        } else {
            $this->pdf = 'initial';
        }
    }

    /**
     * @return string
     */
    private function namedPdf()
    {
        return sprintf('%s.%s', $this->getHash(), $this->getFile()->guessExtension());
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->formatPath($this->getUploadRootDir(), $this->pdf);
    }

    /**
     * @param $root
     * @param $path
     * @return string
     */
    private function formatPath($root, $path)
    {
        return sprintf('%s%s%s', $root, DIRECTORY_SEPARATOR, $path);
    }

    /**
     * @return string
     */
    public function getUploadRootDir()
    {
        $rootDirectory = property_exists($this, 'uploadRootDirectory') ? $this->uploadRootDirectory : '/var/sad';
        return $this->formatPath($rootDirectory, $this->getUploadDir());
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = sha1_file($hash);
    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {

            $this->setHash($this->getFile()->getPathname());
            $this->setPdf($this->namedPdf());
        }

        if (null != $this->getTemporaryPath()) {

            $f = new Filesystem();

            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),
                true
            );
        }
    }

    /**
     * @return string
     */
    public function getTemporaryPath(){
        return $this->path;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}