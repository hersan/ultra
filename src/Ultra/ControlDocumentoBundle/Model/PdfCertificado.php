<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 31/03/14
 * Time: 10:13 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ultra\ControlDocumentoBundle\Entity\PerfilCurricula;

class PdfCertificado implements PdfFileInterface{

    private $file;

    private $name;

    private $entity;

    public function __construct(PerfilCurricula $entity)
    {
        $this->entity = $entity;
    }


    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(null !== $this->file){
            $this->name = sha1_file($this->file->getPathname()) . '.' . $this->file->guessExtension();
        }else{
            $this->name = null;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function execute(){
        if(null !== $this->name && null !== $this->file){
            $this->entity->setPdfCertificado($this->getName());
            $this->entity->setFiles($this);
        }
    }

}