<?php
/**
 * User: hheredia
 * Date: 26/02/14
 * Time: 06:49 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;

abstract class AbstractDocumentSection implements DocumentSectionInterface {

    protected $id;

    protected $nameSection;

    protected $courses;

    public function setNameSection($name)
    {
        $this->nameSection = $name;

        return $this;
    }

    public function getNameSection()
    {
        return $this->nameSection;
    }

    public function addSection(SectionInterface $section)
    {
        // TODO: Implement addSection() method.
    }

    public function setInstrument(InstrumentInterface $instrument)
    {
        // TODO: Implement setInstrument() method.
    }

    public function addItem(ItemInterface $item)
    {
        // TODO: Implement addItem() method.
    }

    public function getItems()
    {
        // TODO: Implement getItems() method.
    }

    public function getInstrument()
    {
        // TODO: Implement getInstrument() method.
    }

    public function removeSection(SectionInterface $section)
    {
        // TODO: Implement removeSection() method.
    }

    public function getSections()
    {
        // TODO: Implement getSections() method.
    }

    public function removeItem(ItemInterface $item)
    {
        // TODO: Implement removeItem() method.
    }
}