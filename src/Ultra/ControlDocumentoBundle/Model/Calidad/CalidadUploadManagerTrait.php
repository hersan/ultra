<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 6/08/15
 * Time: 12:54 PM
 */

namespace Ultra\ControlDocumentoBundle\Model\Calidad;



trait CalidadUploadManagerTrait
{
    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            //Nombre del archivo con formato Numero de contrato - clave.pdf
            $name = $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension();
            $this->setPdf($name);
        }

    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {
            //Nombre del archivo con formato Numero de contrato - clave.pdf
            $name = $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension();
            $this->setPdf($name);
        }

        if(null != $this->getTemporaryPath()){
            $f = new Filesystem();
            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),true);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir().'/'.$this->getDisciplina()->getArea()->getContrato()->getNumeroInterno(),
            $this->pdf
        );

        $this->file = null;
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}