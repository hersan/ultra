<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 6/08/15
 * Time: 01:03 PM
 */

namespace Ultra\ControlDocumentoBundle\Model\Calidad;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\Mapping as ORM;

trait CalidadUploadableTrait
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $path;

    /**
     * @var string
     */
    private $temp;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $pdf;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $hash;

    /**
     * @var string
     */
    private $uploadDirectory = 'uploads/calidad/Agenda';

    /**
     * @var string
     */
    private $uploadRootDirectory = '/var/sad';


    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param string $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    public function getWebPath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadDir().'/'.$this->pdf;
    }

    public function getUploadDir()
    {
        return $this->uploadDirectory;
    }

    public function setUploadRootDir($dir){
        $this->uploadRootDirectory = $dir;
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {

            $this->setHash($this->getFile()->getPathname());
            $this->setPdf($this->namedPdf());

        }

    }

    /**
     * Get file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        if (is_file($this->getAbsolutePath())) {

            $this->temp = $this->getAbsolutePath();
            $this->setPdf(null);

        } else {
            $this->pdf = 'initial';
        }
    }

    private function namedPdf()
    {
        return sprintf('%s.%s', $this->getHash(), $this->getFile()->guessExtension());
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->formatPath($this->getUploadRootDir(), $this->pdf);
    }

    private function formatPath($root, $path)
    {
        return sprintf('%s%s%s', $root, DIRECTORY_SEPARATOR, $path);
    }

    public function getUploadRootDir()
    {
        return $this->formatPath($this->uploadRootDirectory, $this->getUploadDir());
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = sha1_file($hash);
    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {

            $this->setHash($this->getFile()->getPathname());
            $this->setPdf($this->namedPdf());
        }

        if (null != $this->getTemporaryPath()) {

            $f = new Filesystem();

            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),
                true
            );
        }
    }

    public function getTemporaryPath(){
        return $this->path;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }
}