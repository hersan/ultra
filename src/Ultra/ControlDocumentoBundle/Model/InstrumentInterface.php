<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 27/02/15
 * Time: 01:07 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface InstrumentInterface {

    public function addSection(SectionInterface $section);
    public function removeSection(SectionInterface $section);
    public function getSections();

}