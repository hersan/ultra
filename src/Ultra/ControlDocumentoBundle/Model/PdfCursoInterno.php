<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 7/04/14
 * Time: 02:52 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ultra\ControlDocumentoBundle\Entity\CursoInterno;

class PdfCursoInterno implements CommandInterface{

    private $entity;

    private $file;

    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(null !== $this->file){
            $this->name = sha1_file($this->file->getPathname()) . '.' . $this->file->guessExtension();
        }else{
            $this->name = null;
        }
    }

    public function setReceiver(CursoInterno $entity){
        $this->entity = $entity;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function execute()
    {
        if(null !== $this->getName() && null !== $this->getFile()){
            $this->entity->setPdfCertificado($this->getName());
            $this->entity->setPath($this->entity->getCurricula()->getCurp());
        }
    }

    public function hasFile(){
        if(null !== $this->file){
            return true;
        }

        return false;
    }
} 