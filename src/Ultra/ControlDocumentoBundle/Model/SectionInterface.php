<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 27/02/15
 * Time: 01:08 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface SectionInterface {

    public function addItem(ItemInterface $item);
    public function removeItem(ItemInterface $item);
    public function getItems();
    public function setInstrument(InstrumentInterface $instrument);
    public function getInstrument();

}