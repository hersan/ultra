<?php
/**
 * User: hheredia
 * Date: 26/02/14
 * Time: 06:41 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface DocumentSectionInterface {

    public function setName($name);
    public function getName();
    public function setNumber($number);
    public function getNumber();
} 