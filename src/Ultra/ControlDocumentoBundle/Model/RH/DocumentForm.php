<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 23/03/15
 * Time: 05:39 PM
 */

namespace Ultra\ControlDocumentoBundle\Model\RH;


use Ultra\ControlDocumentoBundle\Model\DocumentInterface;
use Ultra\ControlDocumentoBundle\Model\InstrumentInterface;
use Ultra\ControlDocumentoBundle\Model\SectionInterface;

class DocumentForm implements DocumentInterface, InstrumentInterface{

    private $title;

    private $section= array();

    public function setTitle($title)
    {
        $this->title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function addSection(SectionInterface $section)
    {
        $this->section[] = $section;
     }

    public function removeSection(SectionInterface $section)
    {
        $key = array_search($section, $this->section, true);

        if($key !== false)
        {
            unset($this->section[$key]);
            return true;
        }

        return false;
    }

    public function getSections()
    {
        return $this->section;
    }
}