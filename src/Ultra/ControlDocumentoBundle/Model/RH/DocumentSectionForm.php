<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 23/03/15
 * Time: 06:07 PM
 */

namespace Ultra\ControlDocumentoBundle\Model\RH;


use Ultra\ControlDocumentoBundle\Model\DocumentSectionInterface;
use Ultra\ControlDocumentoBundle\Model\InstrumentInterface;
use Ultra\ControlDocumentoBundle\Model\ItemInterface;
use Ultra\ControlDocumentoBundle\Model\SectionInterface;

class DocumentSectionForm implements DocumentSectionInterface, SectionInterface{

    private $name;

    private $number;

    private $item;

    private $instrument;

    public function setName($name)
    {
        $this->item = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function addItem(ItemInterface $item)
    {
        $this->item[] = $item;

        return $this;
    }

    public function removeItem(ItemInterface $item)
    {
        $key = array_search($item, $this->item, true);

        if($key !== false)
        {
            unset($this->item[$key]);

            return true;
        }

        return false;
    }

    public function getItems()
    {
        return $this->item;
    }

    public function setInstrument(InstrumentInterface $instrument)
    {
        $this->instrument = $instrument;

        return $this;
    }

    public function getInstrument()
    {
        return $this->instrument;
    }
}