<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 24/03/15
 * Time: 10:59 AM
 */

namespace Ultra\ControlDocumentoBundle\Model\RH;


use Ultra\ControlDocumentoBundle\Model\DocumentContentInterface;
use Ultra\ControlDocumentoBundle\Model\DocumentElementInterface;
use Ultra\ControlDocumentoBundle\Model\ItemInterface;
use Ultra\ControlDocumentoBundle\Model\SectionInterface;

class DocumentElementForm implements DocumentElementInterface, ItemInterface{

    private $content;

    private $section;

    public function setElement(DocumentContentInterface $content)
    {
        $this->content = $content;

        return $this;
    }

    public function getElement()
    {
        return $this->content;
    }

    public function setSection(SectionInterface $section)
    {
        $this->section = $section;

        return $this;
    }

    public function getSection()
    {
        return $this->section;
    }
}