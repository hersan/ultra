<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 1/04/14
 * Time: 10:54 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Symfony\Component\HttpFoundation\File\UploadedFile;

interface PdfFileInterface {

    public function getName();

    public function setFile(UploadedFile $file);

    public function getFile();

    public function execute();

} 