<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/04/14
 * Time: 03:56 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface UploadManagerInterface {
    public function preUpload();
    public function upload();
    public function removeUpload();
} 