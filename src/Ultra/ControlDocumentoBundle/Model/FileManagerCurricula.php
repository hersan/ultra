<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 4/04/14
 * Time: 11:20 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;

use Symfony\Component\Filesystem\Filesystem;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;

class FileManagerCurricula implements UploadManagerInterface{
    private $files;

    private $fileSystem;

    private $basePath;

    private $entities;

    function __construct()
    {
        $this->fileSystem = new Filesystem();
        $this->files = array();
        $this->basePath = '/var/sad/uploads/curriculas';
        $this->entities = new \SplObjectStorage();
    }

    public function set(UploadFileInterface $entity){
        $this->entities->attach($entity);
    }

    public function remove()
    {
        if($this->entities->count()){

        }
    }

    public function move()
    {
        if(null !== $this->files && is_array($this->files)){
            foreach($this->files as $file){
                if(is_object($file->getFile())){
                    $file->getFile()->move(
                        $this->getUploadRootDir().'/'.$file->path(),
                        $file->getName()
                    );
                }
            }
        }
    }

    public function add($file = null)
    {
        $this->files[] = $file;
    }

    public function getUploadRootDir(){
        return $this->basePath .'/'. $this->getUploadDir();
    }

    public function getUploadDir(){
        return $this->entity->getCurp();
    }
} 