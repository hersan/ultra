<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 26/03/14
 * Time: 02:17 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class PortableDocumentFormat {

    private $file;

    private $name;

    private $temp;

    private $hash;

    private $path;

    public function __construct(UploadedFile $file = null){
        $this->file = $file;

        if($this->file !== null){
            $this->setName($this->getHash());
        }

    }

    /**
     * @internal param mixed $name
     */
    public function setName($name = null)
    {
        if(null !== $this->file){
            $this->name = $name . '.' . $this->getFile()->guessExtension();
        }else{
            $this->name = null;
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->name
            ? null
            : $this->getUploadRootDir().'/'.$this->name;
    }

    public function getWebPath()
    {
        return null === $this->name
            ? null
            : $this->getUploadDir().'/'.$this->name;
    }

    public  function getUploadRootDir()
    {
        return '/var/sad/'.$this->getUploadDir();
    }

    public function getUploadDir()
    {
        return 'uploads/evaluacion';
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    public function getTemporaryPath(){
        return $this->path;
    }

    public function getHash()
    {
        if(null !== $this->getFile()){
            $this->hash = sha1_file($this->getFile()->getPathname());
            return $this->hash;
        }
        return null;
    }

    public function setPreviousFile($temp = null){
        $this->temp = $temp;
    }

    public function checkForPreviousFile(){

        if (!is_file($this->getAbsolutePath())) {
            return false;
        }
        return true;
    }


} 