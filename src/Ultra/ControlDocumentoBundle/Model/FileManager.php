<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 31/03/14
 * Time: 04:53 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;

use Symfony\Component\Filesystem\Filesystem;
use Ultra\ControlDocumentoBundle\Entity\Curricula;

class FileManager implements UploadManagerInterface {
    private $files;

    private $fileSystem;

    private $basePath;

    private $entity;

    function __construct(Curricula $entity)
    {
        $this->fileSystem = new Filesystem();
        $this->files = array();
        $this->basePath = '/var/sad/uploads/curriculas';
    }

    public function remove()
    {
        ;
    }

    public function move()
    {
        if(is_array($this->files) && null !== $this->files){
            foreach($this->files as $file){
                if(is_object($file)){
                    $this->getFile()->move(

                    );
                }
            }
        }
    }

    public function add($file = null)
    {
        $this->files[] = $file;
    }

    public function getUploadRootDir(){
        return $this->basePath .'/'. $this->getUploadDir();
    }

    public function getUploadDir(){
        return $this->entity->getCurp();
    }
}