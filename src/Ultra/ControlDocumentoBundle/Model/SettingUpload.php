<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 1/04/14
 * Time: 02:10 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;

class SettingUpload {

    private $storage;

    public function __construct($entity = null){
        $this->storage = new ArrayCollection();
    }

    public function add($entity){
        $this->storage->add($entity);
    }

    public function setFiles(){
        if($this->storage->count()){
            foreach($this->storage as $coll){

                foreach($coll as $curso){
                    if(null !== $curso->getFile()){
                        if($curso->getFile()->hasFile())
                        {
                            $curso->getFile()->setReceiver($curso);
                            $curso->getFile()->execute();
                        }
                    }
                }

            }
        }
    }

} 