<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/04/14
 * Time: 06:08 PM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface CommandInterface {

    public function execute();

} 