<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/03/15
 * Time: 10:55 AM
 */

namespace Ultra\ControlDocumentoBundle\Model;


interface DocumentInterface {

    public function setTitle($title);
    public function getTitle();

}