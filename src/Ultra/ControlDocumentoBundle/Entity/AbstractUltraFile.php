<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 24/03/14
 * Time: 04:31 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AbstractUltraFile
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_class", type="string")
 * @ORM\DiscriminatorMap({
 *      "ultra" = "AbstractUltraFile",
 *      "ife" = "PdfIfe",
 *      "licencia" = "PdfLicenciaConducir",
 *      "cedula" = "PdfCedulaProfesional",
 *      "rfc" = "PdfRfc",
 *      "curp" = "PdfCurp",
 *      "imss" = "PdfImss",
 *      "cartilla" = "PdfCartillaMilitar",
 *      "Acta" = "PdfActaNacimiento",
 *      "Curricula" = "PdfCurricula",
 *      "foto" = "ImagenFoto"
 * })
 */
abstract class AbstractUltraFile {


    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string")
     */
    protected $path;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string")
     */
    protected $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_update", type="date")
     */
    protected  $lastUpdate;

    /**
     * @var string
     *
     */
    protected  $temp;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Curricula", inversedBy="anexo")
     */
    private $curriculum;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @internal param mixed $created
     */
    public function setCreated()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = sha1_file($hash);
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @internal param mixed $lastUpdate
     */
    public function setLastUpdate()
    {
        $this->lastUpdate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set curriculum
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curriculum
     * @return AbstractUltraFile
     */
    public function setCurriculum(\Ultra\ControlDocumentoBundle\Entity\Curricula $curriculum = null)
    {
        $this->curriculum = $curriculum;
        if(null !== $curriculum){
            $this->setPath($curriculum->getCurp());
        }
        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * @param string $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * @return string
     */
    public function getTemp()
    {
        return $this->temp;
    }


    abstract public function getAbsolutePath();

    abstract public function getUploadRootDir();

    abstract public function getUploadDir();

    abstract public function preUpload();

    abstract public function upload();

    abstract public function removeUpload();

    abstract public function setFile(UploadedFile $file = null);

    abstract public function getFile();

    abstract public function getTypeName();

}