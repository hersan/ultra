<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/03/14
 * Time: 02:16 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


class DepositoDeClasificacionDeInstituciones {

    public function extraerCompleto(){
        return [
            'EMP' => new ClasificacionDeInstituciones(new Clasificacion('EMP'),'Empresa'),
            'UNI' => new ClasificacionDeInstituciones(new Clasificacion('UNI'),'Univerisad'),
        ];
    }

    public function extraerClasificacion(Clasificacion $clasificacion){
        return $this->extraerCompleto()[(string) $clasificacion];
    }
} 