<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Ultra\ControlDocumentoBundle\Model\Anexo;

/**
 * Curricula
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\CurriculaRepository")
 */
class Curricula
{
    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile
     *
     */
    private $files;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="estado", type="string", length=10, nullable=true)
     */
    private $estado;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     *
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="numero_control", type="string")
     */
    private $numeroControl;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="apellido_paterno", type="string", length=50)
     */
    private $apellidoPaterno;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="apellido_materno", type="string", length=50)
     */
    private $apellidoMaterno;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="genero", type="string", length=50, nullable=true)
     */
    private $genero;

    /**
     * @var string
     * Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="ife", type="string", length=13, nullable=true)
     */
    private $ife;

    /**
     * @var
     * Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="licencia_conducir", type="string", length=255, nullable=true);
     */
    private $licenciaConducir;

    /**
     * @var string
     * Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="cedula", type="string", length=50, nullable=true)
     */
    private $cedula;

    /**
     * @var string
     * Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="rfc", type="string", length=15, nullable=true)
     */
    private $rfc;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="curp", type="string", length=20, nullable=true)
     */
    private $curp;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="imss", type="string", length=50, nullable=true)
     */
    private $imss;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="cartilla", type="string", length=20, nullable=true)
     */
    private $cartilla;

    /**
     * @var string
     * Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="lugar_nacimiento", type="string", length=100, nullable=true)
     */
    private $lugarNacimiento;

    /**
     * @var string
     * @Assert\NotNull(message="Este campo no puede ser nulo")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.", groups={"rhum"})
     * @ORM\Column(name="nacionalidad", type="string", length=50, nullable=true)
     */
    private $nacionalidad;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido", groups={"rhum"})
     * @ORM\Column(name="fecha_nacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="domicilio", type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="telefono", type="string", length=12, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="telefono1", type="string", length=12, nullable=true)
     */
    private $telefono1;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="celular", type="string", length=12, nullable=true)
     */
    private $celular;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="ubicacion", type="string", length=20, nullable=true)
     */
    private $ubicacion;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="extension", type="string", length=5, nullable=true)
     */
    private $extension;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="emailultra", type="string", length=50, nullable=true)
     */
    private $emailultra;

    /**
     * Numero de control interno de CFE
     *
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="np_cfe", type="string", length=100, nullable=true)
     */
    private $npcfe;

    /**
     * Numero de credencial que otorga CFE
     *
     * @var string
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}." , groups={"rhum"})
     * @ORM\Column(name="rpe_cfe", type="string", length=100, nullable=true)
     */
    private $rpecfe;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\CurriculumType
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\CurriculumType")
     */
    private $type;

    /**
     * @Assert\NotBlank(groups={"rhum"})
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\EstadoCivil")
     */
    private $estadoCivil;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\TipoDocumento")
     */
    private $tipoDocumento;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\NivelAcademico")
     */
    private $nivelAcademico;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\UsuarioBundle\Entity\UltraUser", mappedBy="curricula")
     */
    private $user;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\PerfilCurricula", mappedBy="curricula")
     */
    private $curriculas;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Competencia", mappedBy="curricula", cascade={"persist"})
     */
    private $competencias;

    /**
     *
     * @ORM\OneToMany(targetEntity="CursoExterno", mappedBy="curricula", cascade={"persist"})
     * @Assert\NotBlank(groups={"control"})
     * @Assert\NotBlank(groups={"cursos"})
     */
    private $cursosExternos;

    /**
     *
     * @ORM\OneToMany(targetEntity="CursoInterno", mappedBy="curricula", cascade={"persist"})
     * @Assert\NotBlank(groups={"cursos"})
     */
    private $cursosInternos;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="AbstractUltraFile", mappedBy="curriculum", cascade={"persist"})
     */
    private $anexo;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ExperienciaProfesional", mappedBy="curricula", cascade={"persist"})
     */
    private $experiencia;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="FormacionAcademica", mappedBy="curricula", cascade={"persist"})
     */
    private $estudios;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\RHBundle\Entity\Contratacion", mappedBy="curricula")
     */
    private $contratos;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Ultra\CalidadBundle\Entity\AuditoriaVigilancia", mappedBy="originators")
     */
    private $auditorias;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Ultra\RHBundle\Entity\RHAgenda", mappedBy="approvalBy")
     */
    private $agenda;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
        $this->curriculas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->competencias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cursosExternos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cursosInternos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->anexo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->experiencia = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estudios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contratos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->auditorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->agenda = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Curricula
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get genero
     *
     * @return string
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set genero
     *
     * @param string $genero
     * @return Curricula
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get ife
     *
     * @return string
     */
    public function getIfe()
    {
        return $this->ife;
    }

    /**
     * Set ife
     *
     * @param string $ife
     * @return Curricula
     */
    public function setIfe($ife)
    {
        $this->ife = $ife;

        return $this;
    }

    /**
     * Get cedula
     *
     * @return string
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     * @return Curricula
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;

        return $this;
    }

    /**
     * Get rfc
     *
     * @return string
     */
    public function getRfc()
    {
        return $this->rfc;
    }

    /**
     * Set rfc
     *
     * @param string $rfc
     * @return Curricula
     */
    public function setRfc($rfc)
    {
        $this->rfc = $rfc;

        return $this;
    }

    /**
     * Get curp
     *
     * @return string
     */
    public function getCurp()
    {
        return $this->curp;
    }

    /**
     * Set curp
     *
     * @param string $curp
     * @return Curricula
     */
    public function setCurp($curp)
    {
        $this->curp = $curp;

        return $this;
    }

    /**
     * Get imss
     *
     * @return string
     */
    public function getImss()
    {
        return $this->imss;
    }

    /**
     * Set imss
     *
     * @param string $imss
     * @return Curricula
     */
    public function setImss($imss)
    {
        $this->imss = $imss;

        return $this;
    }

    /**
     * Get cartilla
     *
     * @return string
     */
    public function getCartilla()
    {
        return $this->cartilla;
    }

    /**
     * Set cartilla
     *
     * @param string $cartilla
     * @return Curricula
     */
    public function setCartilla($cartilla)
    {
        $this->cartilla = $cartilla;

        return $this;
    }

    /**
     * Get lugarNacimiento
     *
     * @return string
     */
    public function getLugarNacimiento()
    {
        return $this->lugarNacimiento;
    }

    /**
     * Set lugarNacimiento
     *
     * @param string $lugarNacimiento
     * @return Curricula
     */
    public function setLugarNacimiento($lugarNacimiento)
    {
        $this->lugarNacimiento = $lugarNacimiento;

        return $this;
    }

    /**
     * Get nacionalidad
     *
     * @return string
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Set nacionalidad
     *
     * @param string $nacionalidad
     * @return Curricula
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     * @return Curricula
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     * @return Curricula
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Curricula
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono1
     *
     * @return string
     */
    public function getTelefono1()
    {
        return $this->telefono1;
    }

    /**
     * Set telefono1
     *
     * @param string $telefono1
     * @return Curricula
     */
    public function setTelefono1($telefono1)
    {
        $this->telefono1 = $telefono1;

        return $this;
    }

    /**
     * Get celular
     *
     * @return string
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * Set celular
     *
     * @param string $celular
     * @return Curricula
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;

        return $this;
    }

    /**
     * Get ubicacion
     *
     * @return string
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set ubicacion
     *
     * @param string $ubicacion
     * @return Curricula
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Curricula
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get emailultra
     *
     * @return string
     */
    public function getEmailultra()
    {
        return $this->emailultra;
    }

    /**
     * Set emailultra
     *
     * @param string $emailultra
     * @return Curricula
     */
    public function setEmailultra($emailultra)
    {
        $this->email = $emailultra;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoDocumento
     */
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }

    /**
     * Set tipoDocumento
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento
     * @return Curricula
     */
    public function setTipoDocumento(\Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento = null)
    {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get nivelAcademico
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\NivelAcademico
     */
    public function getNivelAcademico()
    {
        return $this->nivelAcademico;
    }

    /**
     * Set nivelAcademico
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\NivelAcademico $nivelAcademico
     * @return Curricula
     */
    public function setNivelAcademico(\Ultra\ControlDocumentoBundle\Entity\NivelAcademico $nivelAcademico = null)
    {
        $this->nivelAcademico = $nivelAcademico;

        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\EstadoCivil
     */
    public function getEstadoCivil()
    {
        return $this->estadoCivil;
    }

    /**
     * Set estadoCivil
     *
     * @param EstadoCivil $estadoCivil
     * @internal param $string
     * @return Curricula
     */
    public function setEstadoCivil(EstadoCivil $estadoCivil = null)
    {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Add user
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraUser $user
     * @return Curricula
     */
    public function addUser(\Ultra\UsuarioBundle\Entity\UltraUser $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraUser $user
     */
    public function removeUser(\Ultra\UsuarioBundle\Entity\UltraUser $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add curriculas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $curriculas
     * @return Curricula
     */
    public function addCurricula(\Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $curriculas)
    {
        $this->curriculas[] = $curriculas;

        return $this;
    }

    /**
     * Remove curriculas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $curriculas
     */
    public function removeCurricula(\Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $curriculas)
    {
        $this->curriculas->removeElement($curriculas);
    }

    /**
     * Get curriculas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurriculas()
    {
        return $this->curriculas;
    }

    /**
     * Add competencias
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Competencia $competencias
     * @return Curricula
     */
    public function addCompetencia(\Ultra\ControlDocumentoBundle\Entity\Competencia $competencias)
    {
        if($competencias){
            $competencias->setCurricula($this);
            $this->competencias[] = $competencias;
        }

        return $this;
    }

    /**
     * Remove competencias
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Competencia $competencias
     */
    public function removeCompetencia(\Ultra\ControlDocumentoBundle\Entity\Competencia $competencias)
    {
        $this->competencias->removeElement($competencias);
    }

    /**
     * Get competencias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetencias()
    {
        return $this->competencias;
    }

    /**
     * Add cursosExternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos
     * @return Curricula
     */
    public function addCursosExterno(\Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos)
    {
        if($cursosExternos){
            $cursosExternos->setCurricula($this);
            $this->cursosExternos[] = $cursosExternos;
        }

        return $this;
    }

    /**
     * Remove cursosExternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos
     */
    public function removeCursosExterno(\Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos)
    {
        $this->cursosExternos->removeElement($cursosExternos);
    }

    /**
     * Get cursosExternos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCursosExternos()
    {
        return $this->cursosExternos;
    }

    /**
     * Add cursosInternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursosInternos
     * @return Curricula
     */
    public function addCursosInterno(\Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursosInternos)
    {
        if($cursosInternos){
            $cursosInternos->setCurricula($this);
            $this->cursosInternos[] = $cursosInternos;
        }

        return $this;
    }

    /**
     * Remove cursosInternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursosInternos
     */
    public function removeCursosInterno(\Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursosInternos)
    {
        $this->cursosInternos->removeElement($cursosInternos);
    }

    /**
     * Get cursosInternos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCursosInternos()
    {
        return $this->cursosInternos;
    }

    public function __toString(){
        return $this->getNombre().' '.$this->getApellidoPaterno().' '.$this->getApellidoMaterno().'('.$this->getNumeroControl().')';
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Curricula
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get apellidoPaterno
     *
     * @return string
     */
    public function getApellidoPaterno()
    {
        return $this->apellidoPaterno;
    }

    /**
     * Set apellidoPaterno
     *
     * @param string $apellidoPaterno
     * @return Curricula
     */
    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->apellidoPaterno = $apellidoPaterno;

        return $this;
    }

    /**
     * Get apellidoMaterno
     *
     * @return string
     */
    public function getApellidoMaterno()
    {
        return $this->apellidoMaterno;
    }

    /**
     * Set apellidoMaterno
     *
     * @param string $apellidoMaterno
     * @return Curricula
     */
    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->apellidoMaterno = $apellidoMaterno;

        return $this;
    }

    /**
     * Get numeroControl
     *
     * @return integer
     */
    public function getNumeroControl()
    {
        return $this->numeroControl;
    }

    /**
     * Set numeroControl
     *
     * @param integer $numeroControl
     * @return Curricula
     */
    public function setNumeroControl($numeroControl)
    {
        $this->numeroControl = $numeroControl;

        return $this;
    }

    /**
     * Get licenciaConducir
     *
     * @return string
     */
    public function getLicenciaConducir()
    {
        return $this->licenciaConducir;
    }

    /**
     * Set licenciaConducir
     *
     * @param string $licenciaConducir
     * @return Curricula
     */
    public function setLicenciaConducir($licenciaConducir)
    {
        $this->licenciaConducir = $licenciaConducir;

        return $this;
    }

    /**
     * Get npcfe
     *
     * @return string
     */
    public function getNpcfe()
    {
        return $this->npcfe;
    }

    /**
     * Set npcfe
     *
     * @param string $npcfe
     * @return Curricula
     */
    public function setNpcfe($npcfe)
    {
        $this->npcfe = $npcfe;

        return $this;
    }

    /**
     * Get rpecfe
     *
     * @return string
     */
    public function getRpecfe()
    {
        return $this->rpecfe;
    }

    /**
     * Set rpecfe
     *
     * @param string $rpecfe
     * @return Curricula
     */
    public function setRpecfe($rpecfe)
    {
        $this->rpecfe = $rpecfe;

        return $this;
    }

    /**
     * Remove anexo
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile $anexo
     */
    public function removeAnexo(\Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile $anexo)
    {
        $this->anexo->removeElement($anexo);
    }

    /**
     * @param mixed $files
     */
    public function setFile(Anexo $files = null)
    {
        $this->files = $files;
        if($this->getAnexo()->isEmpty())
        {
            foreach($this->files as $file)
            {
                if(null !== $file->getFile())
                {
                    $this->addAnexo($file);
                }
            }
            return;
        }

        foreach($this->files as $file)
        {
            if(null !== $file->getFile()){
                foreach($this->getAnexo() as $anexo)
                {
                    if(get_class($file) == get_class($anexo))
                    {
                        $anexo->setFile($file->getFile());
                        //$evaluacion->setName('initial');
                        $file->setFile(null);
                        //$file->getFile();
                    }
                }
            }
        }

        foreach($this->files as $file)
        {
            if(null !== $file->getFile()){
                $this->addAnexo($file);
            }
        }
    }

    /**
     * Get anexo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnexo()
    {
        return $this->anexo;
    }

    /**
     * Add anexo
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile $anexo
     * @return Curricula
     */
    public function addAnexo(\Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile $anexo)
    {
        $anexo->setCurriculum($this);
        $this->anexo[] = $anexo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->files;
    }


    /**
     * Add experiencia
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional $experiencia
     * @return Curricula
     */
    public function addExperiencia(\Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional $experiencia)
    {
        $experiencia->setCurricula($this);
        $this->experiencia[] = $experiencia;
    
        return $this;
    }

    /**
     * Remove experiencia
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional $experiencia
     */
    public function removeExperiencia(\Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional $experiencia)
    {
        $this->experiencia->removeElement($experiencia);
    }

    /**
     * Get experiencia
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Add estudios
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\FormacionAcademica $estudios
     * @return Curricula
     */
    public function addEstudio(\Ultra\ControlDocumentoBundle\Entity\FormacionAcademica $estudios)
    {
        $this->estudios[] = $estudios;
    
        return $this;
    }

    /**
     * Remove estudios
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\FormacionAcademica $estudios
     */
    public function removeEstudio(\Ultra\ControlDocumentoBundle\Entity\FormacionAcademica $estudios)
    {
        $this->estudios->removeElement($estudios);
    }

    /**
     * Get estudios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstudios()
    {
        return $this->estudios;
    }

    /**
     * Add contratos
     *
     * @param \Ultra\RHBundle\Entity\Contratacion $contratos
     * @return Curricula
     */
    public function addContrato(\Ultra\RHBundle\Entity\Contratacion $contratos)
    {
        $this->contratos[] = $contratos;
    
        return $this;
    }

    /**
     * Remove contratos
     *
     * @param \Ultra\RHBundle\Entity\Contratacion $contratos
     */
    public function removeContrato(\Ultra\RHBundle\Entity\Contratacion $contratos)
    {
        $this->contratos->removeElement($contratos);
    }

    /**
     * Get contratos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContratos()
    {
        return $this->contratos;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set extension
     *
     * @param string $extension
     * @return Curricula
     *
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Add auditorias
     *
     * @param \Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditorias
     * @return Curricula
     */
    public function addAuditoria(\Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditorias)
    {
        $this->auditorias[] = $auditorias;
    
        return $this;
    }

    /**
     * Remove auditorias
     *
     * @param \Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditorias
     * @return Curricula
     */
    public function removeAuditoria(\Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditorias)
    {
        $this->auditorias->removeElement($auditorias);

        return $this;
    }

    /**
     * Get auditorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuditorias()
    {
        return $this->auditorias;
    }

    /**
     * Get type
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\CurriculumType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CurriculumType $type
     * @return Curricula
     */
    public function setType(\Ultra\ControlDocumentoBundle\Entity\CurriculumType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Add agenda
     *
     * @param \Ultra\RHBundle\Entity\RHAgenda $agenda
     * @return Curricula
     */
    public function addAgenda(\Ultra\RHBundle\Entity\RHAgenda $agenda)
    {
        $this->agenda[] = $agenda;
    
        return $this;
    }

    /**
     * Remove agenda
     *
     * @param \Ultra\RHBundle\Entity\RHAgenda $agenda
     */
    public function removeAgenda(\Ultra\RHBundle\Entity\RHAgenda $agenda)
    {
        $this->agenda->removeElement($agenda);
    }

    /**
     * Get agenda
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAgenda()
    {
        return $this->agenda;
    }
}