<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Profesion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ProfesionRepository")
 */
class Profesion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="clave", type="string", length=50)
     */
    private $clave;

    /**
     * @var String
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="nivel", type="string", length=20)
     */
    private $nivel;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Institucion", inversedBy="profesiones")
     */
    private $institucion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Profesion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString(){
        return $this->nombre;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Profesion
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set nivel
     *
     * @param string $nivel
     * @return Profesion
     */
    public function setNivel($nivel)
    {
        $this->nivel = $nivel;
    
        return $this;
    }

    /**
     * Get nivel
     *
     * @return string 
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set institucion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Institucion $institucion
     * @return Profesion
     */
    public function setInstitucion(\Ultra\ControlDocumentoBundle\Entity\Institucion $institucion = null)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Institucion 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }
}