<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Ultra\ControlDocumentoBundle\Model\Control\GeneralDocumentUploadedTrait;

/**
 * Trainer
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\TrainerRepository")
 */
class Trainer
{

    use GeneralDocumentUploadedTrait;

    /**
     * @var string
     */
    protected $uploadDirectory = 'uploads/trainers';

    /**
     * @var string
     */
    protected $uploadRootDirectory = '/var/sad';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(min="3")
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(min="3")
     * @ORM\Column(name="paternalSurname", type="string", length=255)
     */
    private $paternalSurname;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Assert\Length(min="3")
     * @ORM\Column(name="maternalSurname", type="string", length=255)
     */
    private $maternalSurname;

    /**
     * @var ArrayCollection
     *
     * @Assert\NotNull()
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curso", inversedBy="trainers")
     */
    private $courses;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Trainer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set paternalSurname
     *
     * @param string $paternalSurname
     * @return Trainer
     */
    public function setPaternalSurname($paternalSurname)
    {
        $this->paternalSurname = $paternalSurname;
    
        return $this;
    }

    /**
     * Get paternalSurname
     *
     * @return string 
     */
    public function getPaternalSurname()
    {
        return $this->paternalSurname;
    }

    /**
     * Set maternalSurname
     *
     * @param string $maternalSurname
     * @return Trainer
     */
    public function setMaternalSurname($maternalSurname)
    {
        $this->maternalSurname = $maternalSurname;
    
        return $this;
    }

    /**
     * Get maternalSurname
     *
     * @return string 
     */
    public function getMaternalSurname()
    {
        return $this->maternalSurname;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->courses = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add courses
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $courses
     * @return Trainer
     */
    public function addCourse(\Ultra\ControlDocumentoBundle\Entity\Curso $courses)
    {
        $this->courses[] = $courses;
    
        return $this;
    }

    /**
     * Remove courses
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $courses
     */
    public function removeCourse(\Ultra\ControlDocumentoBundle\Entity\Curso $courses)
    {
        $this->courses->removeElement($courses);
    }

    /**
     * Get courses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCourses()
    {
        return $this->courses;
    }
}