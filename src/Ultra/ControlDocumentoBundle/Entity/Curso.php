<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Curso
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\CursoRepository")
 */
class Curso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="clave", type="string", length=50)
     */
    protected $clave;

    /**
     * @var String
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @Assert\NotNull(message="Debe de seleccionar una Institución")
     * @ORM\ManyToOne(targetEntity="\Ultra\ControlDocumentoBundle\Entity\Institucion", inversedBy="cursos")
     */
    private $institucion;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\CursoExterno", mappedBy="cursoExterno")
     */
    private $cursosExternos;

    /**
     * @var
     *
     * @Assert\NotNull(message="Debe de seleccionar un tipo de curso")
     * @ORM\ManyToOne(targetEntity="TipoCurso")
     */
    protected $tipo;

    /**
     * @var Trainer[]
     *
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Trainer", mappedBy="courses")
     */
    private $trainers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cursosExternos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->trainers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Curso
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Curso
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Curso
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set institucion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Institucion $institucion
     * @return Curso
     */
    public function setInstitucion(\Ultra\ControlDocumentoBundle\Entity\Institucion $institucion = null)
    {
        $this->institucion = $institucion;
    
        return $this;
    }

    /**
     * Get institucion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Institucion 
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

    /**
     * Add cursosExternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos
     * @return Curso
     */
    public function addCursosExterno(\Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos)
    {
        $this->cursosExternos[] = $cursosExternos;
    
        return $this;
    }

    /**
     * Remove cursosExternos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos
     */
    public function removeCursosExterno(\Ultra\ControlDocumentoBundle\Entity\CursoExterno $cursosExternos)
    {
        $this->cursosExternos->removeElement($cursosExternos);
    }

    /**
     * Get cursosExternos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCursosExternos()
    {
        return $this->cursosExternos;
    }

    /**
     * Set tipo
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoCurso $tipo
     * @return Curso
     */
    public function setTipo(\Ultra\ControlDocumentoBundle\Entity\TipoCurso $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoCurso 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Regresa la clave del curso
     *
     * @return string
     */
    public function __toString(){
        return $this->getNombre();
    }

    /**
     * Add trainers
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Trainer $trainers
     * @return Curso
     */
    public function addTrainer(\Ultra\ControlDocumentoBundle\Entity\Trainer $trainers)
    {
        $this->trainers[] = $trainers;
    
        return $this;
    }

    /**
     * Remove trainers
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Trainer $trainers
     */
    public function removeTrainer(\Ultra\ControlDocumentoBundle\Entity\Trainer $trainers)
    {
        $this->trainers->removeElement($trainers);
    }

    /**
     * Get trainers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTrainers()
    {
        return $this->trainers;
    }
}