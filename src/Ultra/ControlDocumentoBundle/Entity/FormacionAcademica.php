<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FormacionAcademica
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FormacionAcademica
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="certificado", type="string", length=255)
     */
    private $certificado;

    /**
     * @var
     *
     * @ORM\Column(name="inicio", type="date")
     */
    private $inicio;

    /**
     * @var
     *
     * @ORM\Column(name="termino", type="date")
     */
    private $termino;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Profesion")
     */
    private $profesion;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Curricula", inversedBy="estudios")
     */
    private $curricula;

    private $institucion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set certificado
     *
     * @param string $certificado
     * @return FormacionAcademica
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
    
        return $this;
    }

    /**
     * Get certificado
     *
     * @return string 
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return FormacionAcademica
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set termino
     *
     * @param \DateTime $termino
     * @return FormacionAcademica
     */
    public function setTermino($termino)
    {
        $this->termino = $termino;
    
        return $this;
    }

    /**
     * Get termino
     *
     * @return \DateTime 
     */
    public function getTermino()
    {
        return $this->termino;
    }

    /**
     * Set profesion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Profesion $profesion
     * @return FormacionAcademica
     */
    public function setProfesion(\Ultra\ControlDocumentoBundle\Entity\Profesion $profesion = null)
    {
        $this->profesion = $profesion;
    
        return $this;
    }

    /**
     * Get profesion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Profesion 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return FormacionAcademica
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;
    
        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * @param mixed $institucion
     */
    public function setInstitucion($institucion)
    {
        $this->institucion = $institucion;
    }

    /**
     * @return mixed
     */
    public function getInstitucion()
    {
        return $this->institucion;
    }

}