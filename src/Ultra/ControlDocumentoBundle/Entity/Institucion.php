<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Institucion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\InstitucionRepository")
 */
class Institucion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @ORM\OneToMany(targetEntity="\Ultra\ControlDocumentoBundle\Entity\Curso", mappedBy="institucion")
     */
    private $cursos;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Profesion
     *
     * @ORM\OneToMany(targetEntity="Profesion", mappedBy="institucion")
     */
    private $profesiones;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="TipoInstitucion")
     */
    private $tipo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreCentroEstudio
     *
     * @param $nombre
     * @internal param string $nombre
     * @return Institucion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombreCentroEstudio
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cursos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->profesiones = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add cursos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $cursos
     * @return Institucion
     */
    public function addCurso(\Ultra\ControlDocumentoBundle\Entity\Curso $cursos)
    {
        $this->cursos[] = $cursos;
    
        return $this;
    }

    /**
     * Remove cursos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $cursos
     */
    public function removeCurso(\Ultra\ControlDocumentoBundle\Entity\Curso $cursos)
    {
        $this->cursos->removeElement($cursos);
    }

    /**
     * Get cursos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCursos()
    {
        return $this->cursos;
    }

    /**
     * Set tipo
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoInstitucion $tipo
     * @return Institucion
     */
    public function setTipo(\Ultra\ControlDocumentoBundle\Entity\TipoInstitucion $tipo = null)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoInstitucion 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Institucion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add profesiones
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Profesion $profesiones
     * @return Institucion
     */
    public function addProfesione(\Ultra\ControlDocumentoBundle\Entity\Profesion $profesiones)
    {
        $this->profesiones[] = $profesiones;
    
        return $this;
    }

    /**
     * Remove profesiones
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Profesion $profesiones
     */
    public function removeProfesione(\Ultra\ControlDocumentoBundle\Entity\Profesion $profesiones)
    {
        $this->profesiones->removeElement($profesiones);
    }

    /**
     * Get profesiones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProfesiones()
    {
        return $this->profesiones;
    }

    public function __toString() {
        return $this->getNombre();
    }
}