<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExperienciaProfesional
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ExperieciaProfesionalRepository")
 */
class ExperienciaProfesional
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="puesto", type="string", length=255)
     */
    private $puesto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date")
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="termino", type="date")
     */
    private $termino;

    /**
     * @var string
     *
     * @ORM\Column(name="comprobante", type="string", length=255)
     */
    private $comprobante;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Curricula", inversedBy="experiencia")
     */
    private $curricula;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Empresa")
     */
    private $empresa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puesto
     *
     * @param string $puesto
     * @return ExperienciaProfesional
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;
    
        return $this;
    }

    /**
     * Get puesto
     *
     * @return string 
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return ExperienciaProfesional
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set termino
     *
     * @param \DateTime $termino
     * @return ExperienciaProfesional
     */
    public function setTermino($termino)
    {
        $this->termino = $termino;
    
        return $this;
    }

    /**
     * Get termino
     *
     * @return \DateTime 
     */
    public function getTermino()
    {
        return $this->termino;
    }

    /**
     * Set comprobante
     *
     * @param string $comprobante
     * @return ExperienciaProfesional
     */
    public function setComprobante($comprobante)
    {
        $this->comprobante = $comprobante;
    
        return $this;
    }

    /**
     * Get comprobante
     *
     * @return string 
     */
    public function getComprobante()
    {
        return $this->comprobante;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return ExperienciaProfesional
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $curricula
     */
    public function setCurricula($curricula)
    {
        $this->curricula = $curricula;
    }

    /**
     * @return mixed
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa(Empresa $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }


}
