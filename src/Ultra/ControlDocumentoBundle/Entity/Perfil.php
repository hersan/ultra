<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Perfil
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\PerfilRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Perfil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var String
     * @Assert\Regex(
     *     pattern="/^\w[\w\d-]+/",
     *     match=true,
     *     message="La clave debe de comenzar con una letra, seguida de letras, numeros o guion"
     * )
     * @Assert\NotBlank(message = "Escriba una clave")
     * @ORM\Column(name="clave", type="string", length=50)
     */
    private $clave;

    /**
     * @var integer
     * @Assert\NotBlank(message="Proporcione la revisión")
     * @Assert\Type(type="numeric", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="revision", type="integer")
     */
    private $revision;

    /**
     * @var string
     * @Assert\NotBlank(message="Escriba un titulo")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo", type="string", length=150)
     */
    private $titulo;

    /**
     * @var string
     * Assert\NotBlank(message="Este campo es requerido")
     * Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo_corto", type="string", length=45, nullable=true)
     */
    private $tituloCorto;

    /**
     *
     * @Assert\NotBlank(message="Selecione una fecha")
     * @Assert\Date(message="Este valor no es una fecha")
     * @ORM\Column(name="aprobado", type="date")
     */
    private $aprobado;

    /**
     *
     * @Assert\NotBlank(message="Selecione una fecha")
     * @Assert\Date(message="Este valor no es una fecha")
     * @ORM\Column(name="vigencia", type="date")
     */
    private $vigencia;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="liberado", type="date", nullable=true)
     */
    private $liberado;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="transmitido", type="date")
     */
    private $transmitido;

    /**
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     *
     * @Assert\File(maxSize = "10M", mimeTypes = {
     *   "application/pdf",
     *   "application/x-pdf"
     * })
     */
    private $file;

    /**
     *
     * @ORM\Column(name="doc", type="string", length=255, nullable=true)
     */
    private $doc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_uad", type="date", nullable=true, options={"default" = null})
     */
    private $fechaUad;

    /**
     * @var
     *
     * @ORM\Column(name="creado", type="date", nullable=true)
     */
    private $creado;

    /**
     * @var
     *
     * @ORM\Column(name="actualizado", type="date", nullable=true)
     */
    private $actualizado;

    /**
     * @var integer
     *
     * @ORM\Column(name="revision_uad", type="smallint")
     */
    private $revisionUad;

    /**
     * Assert\Type(type="Ultra\ControlDocumentoBundle\Entity\TipoDocumento", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\TipoDocumento")
     */
    private $tipoDocumento;

    /**
     * @Assert\NotNull(message="Seleccione")
     * Assert\Type(type="Ultra\ControlDocumentoBundle\Entity\PerfilCurricula", message="El valor {{value}} no es de tipo {{type}}.")
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\PerfilCurricula", mappedBy="perfil", cascade={"persist"})
     */
    private $perfiles;

    /**
     *
     * @Assert\NotNull(message="Seleccione un estado valido")
     * Assert\Type(type="Ultra\ControlDocumentoBundle\Entity\Condicion",
     *                  message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Condicion")
     */
    private $estado;

    /**
     *
     *
     * @Assert\NotNull(message="Seleccione una disciplina")
     * @Assert\NotBlank(message="Debe seleccionar una disciplina")
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina", inversedBy="perfiles")
     */
    private $disciplinas;

    /**
     * @var
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Actividad", mappedBy="perfil", cascade={"persist"})
     */
    private $actividades;

    /**
     * @var
     *
     * @Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Escolaridad", mappedBy="perfil", cascade={"persist"})
     */
    private $escolaridad;

    /**
     * @var
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Experiencia", mappedBy="perfil", cascade={"persist"})
     */
    private $expertise;

    /**
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="\Ultra\ControlDocumentoBundle\Entity\Capacitacion", mappedBy="perfil", cascade={"persist"})
     */
    private $capacitacion;

    /**
     * @var
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Habilidad", mappedBy="perfil", cascade={"persist"})
     */
    private $habilidades;

    /**
     * @var
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial", mappedBy="perfil", cascade={"persist"})
     */
    private $conocimientoEspecial;

    /**
     * @var
     *
     * Assert\Valid(traverse=true)
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Nota", mappedBy="perfil", cascade={"persist"})
     */
    private $notas;

    /**
     * @var String
     *
     * Varianle para almacenar archivos que se van a borrar del sistema
     */
    private $temp;

    /**
     * @var string
     *
     * Variable que almacena como prefijo una parte de la ruta de un archivo
     * en el sistema.
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=40, nullable=true)
     */
    private $hash = null;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->perfiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->disciplinas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actividades = new \Doctrine\Common\Collections\ArrayCollection();
        $this->escolaridad = new \Doctrine\Common\Collections\ArrayCollection();
        $this->expertise = new \Doctrine\Common\Collections\ArrayCollection();
        $this->capacitacion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->habilidades = new \Doctrine\Common\Collections\ArrayCollection();
        $this->conocimientoEspecial = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Perfil
     */
    public function setClave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    /**
     * Get clave
     *
     * @return string
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set revision
     *
     * @param integer $revision
     * @return Perfil
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * Get revision
     *
     * @return integer
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Perfil
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tituloCorto
     *
     * @param string $tituloCorto
     * @return Perfil
     */
    public function setTituloCorto($tituloCorto)
    {
        $this->tituloCorto = $tituloCorto;

        return $this;
    }

    /**
     * Get tituloCorto
     *
     * @return string
     */
    public function getTituloCorto()
    {
        return $this->tituloCorto;
    }

    /**
     * Set aprobado
     *
     * @param \DateTime $aprobado
     * @return Perfil
     */
    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;

        return $this;
    }

    /**
     * Get aprobado
     *
     * @return \DateTime
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }

    /**
     * Set vigencia
     *
     * @param \DateTime $vigencia
     * @return Perfil
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;

        return $this;
    }

    /**
     * Get vigencia
     *
     * @return \DateTime
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }


    /**
     * Set liberado
     *
     * @param \DateTime $liberado
     * @return Documento
     */
    public function setLiberado($liberado)
    {
        $this->liberado = $liberado;

        return $this;
    }

    /**
     * Get liberado
     *
     * @return \DateTime
     */
    public function getLiberado()
    {
        return $this->liberado;
    }

    /**
     * Set transmitido
     *
     * @param \DateTime $transmitido
     * @return Documento
     */
    public function setTransmitido($transmitido)
    {
        $this->transmitido = $transmitido;

        return $this;
    }

    /**
     * Get transmitido
     *
     * @return \DateTime
     */
    public function getTransmitido()
    {
        return $this->transmitido;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Perfil
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set doc
     *
     * @param string $doc
     * @return Perfil
     */
    public function setDoc($doc)
    {
        $this->doc = $doc;

        return $this;
    }

    /**
     * Get doc
     *
     * @return string
     */
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * Set tipoDocumento
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento
     * @return Perfil
     */
    public function setTipoDocumento(\Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento = null)
    {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoDocumento
     */
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }

    /**
     * Add perfiles
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfiles
     * @return Perfil
     */
    public function addPerfile(\Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfiles)
    {
        $perfiles->setPerfil($this);
        $this->perfiles[] = $perfiles;

        return $this;
    }

    /**
     * Remove perfiles
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfiles
     */
    public function removePerfile(\Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfiles)
    {
        $this->perfiles->removeElement($perfiles);
    }

    /**
     * Get perfiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPerfiles()
    {
        return $this->perfiles;
    }

    /**
     * Set estado
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Condicion $estado
     * @return Perfil
     */
    public function setEstado(\Ultra\ControlDocumentoBundle\Entity\Condicion $estado = null)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Condicion
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Add disciplinas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas
     * @return Perfil
     */
    public function addDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas)
    {
        $this->disciplinas[] = $disciplinas;

        return $this;
    }

    /**
     * Remove disciplinas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas
     */
    public function removeDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas)
    {
        $this->disciplinas->removeElement($disciplinas);
    }

    /**
     * Get disciplinas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDisciplinas()
    {
        return $this->disciplinas;
    }

    /**
     * Add actividades
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Actividad $actividades
     * @return Perfil
     */
    public function addActividade(\Ultra\ControlDocumentoBundle\Entity\Actividad $actividades)
    {
        $actividades->setPerfil($this);
        $this->actividades[] = $actividades;

        return $this;
    }

    /**
     * Remove actividades
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Actividad $actividades
     */
    public function removeActividade(\Ultra\ControlDocumentoBundle\Entity\Actividad $actividades)
    {
        $this->actividades->removeElement($actividades);
    }

    /**
     * Get actividades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividades()
    {
        return $this->actividades;
    }

    /**
     * Add escolaridad
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Escolaridad $escolaridad
     * @return Perfil
     */
    public function addEscolaridad(\Ultra\ControlDocumentoBundle\Entity\Escolaridad $escolaridad)
    {
        $escolaridad->setPerfil($this);
        $this->escolaridad[] = $escolaridad;

        return $this;
    }

    /**
     * Remove escolaridad
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Escolaridad $escolaridad
     */
    public function removeEscolaridad(\Ultra\ControlDocumentoBundle\Entity\Escolaridad $escolaridad)
    {
        $this->escolaridad->removeElement($escolaridad);
    }

    /**
     * Get escolaridad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEscolaridad()
    {
        return $this->escolaridad;
    }

    /**
     * Add experiencia
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Experiencia $experiencia
     * @return Perfil
     */
    public function addExpertise(\Ultra\ControlDocumentoBundle\Entity\Experiencia $experiencia)
    {
        $experiencia->setPerfil($this);
        $this->expertise[] = $experiencia;

        return $this;
    }

    /**
     * Remove experiencia
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Experiencia $experiencia
     */
    public function removeExpertise(\Ultra\ControlDocumentoBundle\Entity\Experiencia $experiencia)
    {
        $this->expertise->removeElement($experiencia);
    }

    /**
     * Get experiencia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExpertise()
    {
        return $this->expertise;
    }

    /**
     * Add capacitacion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Capacitacion $capacitacion
     * @return Perfil
     */
    public function addCapacitacion(\Ultra\ControlDocumentoBundle\Entity\Capacitacion $capacitacion)
    {
        $capacitacion->setPerfil($this);
        $this->capacitacion->add($capacitacion);

        return $this;
    }

    /**
     * Remove capacitacion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Capacitacion $capacitacion
     */
    public function removeCapacitacion(\Ultra\ControlDocumentoBundle\Entity\Capacitacion $capacitacion)
    {
        $this->capacitacion->removeElement($capacitacion);
    }

    /**
     * Get capacitacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCapacitacion()
    {
        return $this->capacitacion;
    }

    /**
     * Add habilidades
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Habilidad $habilidades
     * @return Perfil
     */
    public function addHabilidade(\Ultra\ControlDocumentoBundle\Entity\Habilidad $habilidades)
    {
        $habilidades->setPerfil($this);
        $this->habilidades[] = $habilidades;

        return $this;
    }

    /**
     * Remove habilidades
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Habilidad $habilidades
     */
    public function removeHabilidade(\Ultra\ControlDocumentoBundle\Entity\Habilidad $habilidades)
    {
        $this->habilidades->removeElement($habilidades);
    }

    /**
     * Get habilidades
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHabilidades()
    {
        return $this->habilidades;
    }

    /**
     * Add notas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Nota $notas
     * @return Perfil
     */
    public function addNota(\Ultra\ControlDocumentoBundle\Entity\Nota $notas)
    {
        $notas->setPerfil($this);
        $this->notas[] = $notas;

        return $this;
    }

    /**
     * Remove notas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Nota $notas
     */
    public function removeNota(\Ultra\ControlDocumentoBundle\Entity\Nota $notas)
    {
        $this->notas->removeElement($notas);
    }

    /**
     * Get notas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Perfil
     */
    public function setHash($hash)
    {
        $this->hash = sha1_file($hash);

        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    public function getTemp(){
        return $this->temp;
    }

    public function setTemp($temp){
        $this->temp = $temp;
    }

    public function __toString(){
        return $this->getClave().' '.$this->getTitulo().' Rev.'.$this->getRevision();
    }

    /**
     * Add conocimientoEspecial
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial $conocimientoEspecial
     * @return Perfil
     */
    public function addConocimientoEspecial(\Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial $conocimientoEspecial)
    {
        $conocimientoEspecial->setPerfil($this);
        $this->conocimientoEspecial[] = $conocimientoEspecial;
    
        return $this;
    }

    /**
     * Remove conocimientoEspecial
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial $conocimientoEspecial
     */
    public function removeConocimientoEspecial(\Ultra\ControlDocumentoBundle\Entity\ConocimientoEspecial $conocimientoEspecial)
    {
        $this->conocimientoEspecial->removeElement($conocimientoEspecial);
    }

    /**
     * Get conocimientoEspecial
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConocimientoEspecial()
    {
        return $this->conocimientoEspecial;
    }

    /**
     * Set fechaUad
     *
     * @param \DateTime $fechaUad
     * @return Perfil
     */
    public function setFechaUad($fechaUad)
    {
        $this->fechaUad = $fechaUad;
    
        return $this;
    }

    /**
     * Get fechaUad
     *
     * @return \DateTime 
     */
    public function getFechaUad()
    {
        return $this->fechaUad;
    }

    /**
     * Set revisionUad
     *
     * @param integer $revisionUad
     * @return Perfil
     */
    public function setRevisionUad($revisionUad)
    {
        $this->revisionUad = $revisionUad;
    
        return $this;
    }

    /**
     * Get revisionUad
     *
     * @return integer 
     */
    public function getRevisionUad()
    {
        return $this->revisionUad;
    }

    public function __clone()
    {
        if($this->id)
        {
            $this->id = null;
        }
    }

    /**
     * @param mixed $creado
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;
    }

    /**
     * @return mixed
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * @param mixed $actualizado
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;
    }

    /**
     * @return mixed
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    /**
     *
     * Comienza manejo de archivos
     *
     */

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (is_file($this->getAbsolutePath())) {
            $this->setTemp($this->getAbsolutePath());
            $this->pdf = null;
        } else {
            $this->pdf = sha1(uniqid(mt_rand(), true));
        }
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadRootDir().'/'.$this->pdf;
    }

    public function getWebPath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadDir().'/'.$this->pdf;
    }

    private function getUploadRootDir()
    {
        return '/var/sad/'.$this->getUploadDir();
    }

    private function getUploadDir()
    {
        return 'uploads/perfiles/'.$this->getEstado();
    }

    private function temporaryPath($path){
        $this->path = $path;
    }

    private function getTemporaryPath(){
        return $this->path;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $this->setPdf(
                $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension()
            );
            $this->setHash($this->getFile()->getPathname());
            $this->creado = new \DateTime();
        }

    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {
            //Nombre del archivo con formato Numero de contrato - clave.pdf
            $this->setPdf(
                $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension()
            );
            $this->actualizado = new \DateTime();
        }
    }

    /**
     *
     * @ORM\PostPersist()
     */
    public function upload()
    {

        if(null == $this->getFile())
        {
            return;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->temp = null;
        $this->file = null;
    }

    /**
     *
     * @ORM\PostUpdate()
     */
    public function postUpdate()
    {

        if(null !== $this->getFile())
        {
            if (isset($this->temp)) {
                unlink($this->temp);
            }

            $this->getFile()->move(
                $this->getUploadRootDir(),
                $this->pdf
            );
        }

        $this->temp = null;
        $this->file = null;

    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function move($previousPath, $previousState)
    {
        if($this->getEstado()->getId() !== $previousState->getId())
        {
            $fs = new Filesystem();
            //En este punto el valor de Condicion ya es 2, por lo cual se busca por la ruta anterior
            if($fs->exists($previousPath))
            {
                try{
                    $fs->copy($previousPath, $this->getAbsolutePath(), true);
                    //$fs->remove($previousPath);
                }
                catch (FileNotFoundException $e){
                    return $e->getTrace();
                }
                catch (IOException $e){
                    return $e->getTrace();
                }
            }
            $fs = null;
        }

    }

}