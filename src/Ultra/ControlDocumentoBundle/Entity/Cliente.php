<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cliente
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ClienteRepository")
 */
class Cliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="nombre", type="string", length=35)
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="Escribe las siglas")
     * @Assert\Type(type="string", message="Vacío.")
     * @ORM\Column(name="siglas", type="string", length=6)
     */
    private $siglas;

    /**
     * @var boolean
     *
     * @ORM\Column(name="es_socio", type="boolean")
     */
    private $esSocio = false;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Contrato", mappedBy="cliente")
     */
    private $contratos;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set siglas
     *
     * @param string $siglas
     * @return Cliente
     */
    public function setSiglas($siglas)
    {
        $this->siglas = $siglas;
    
        return $this;
    }

    /**
     * Get siglas
     *
     * @return string 
     */
    public function getSiglas()
    {
        return $this->siglas;
    }

    /**
     * Set esSocio
     *
     * @param boolean $esSocio
     * @return Cliente
     */
    public function setEsSocio($esSocio)
    {
        $this->esSocio = $esSocio;
    
        return $this;
    }

    /**
     * Get esSocio
     *
     * @return boolean 
     */
    public function getEsSocio()
    {
        return $this->esSocio;
    }

    public function __toString()
    {
        return $this->getNombre();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contratos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add contratos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Contrato $contratos
     * @return Cliente
     */
    public function addContrato(\Ultra\ControlDocumentoBundle\Entity\Contrato $contratos)
    {
        $this->contratos[] = $contratos;
    
        return $this;
    }

    /**
     * Remove contratos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Contrato $contratos
     */
    public function removeContrato(\Ultra\ControlDocumentoBundle\Entity\Contrato $contratos)
    {
        $this->contratos->removeElement($contratos);
    }

    /**
     * Get contratos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContratos()
    {
        return $this->contratos;
    }
}