<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 20/05/14
 * Time: 05:49 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PdfCertificado
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\Entity()
 */
class PdfCertificado extends AbstractUploadedFile{

    public function __construct()
    {
        $this->setCreated();
    }
} 