<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/03/14
 * Time: 12:44 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;

/**
 * Class Clasificacion
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 */
class Clasificacion {

    /**
     * @var
     *
     */
    private $codigo;

    function __construct($codigo)
    {
        $this->clave = $codigo;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function __toString(){
        return $this->getCodigo();
    }

} 