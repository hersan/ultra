<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * FormatoAsociado
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\FormatoAsociadoRepository")
 */
class FormatoAsociado extends AbstractUploadManager
{

    /**
     * @var
     *
     * @ORM\Column(name="path", type="string")
     */
    private $path;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Documento", inversedBy="formats")
     */
    private $documento;

    /**
     * @var
     *
     * @ORM\Column(name="format_name", type="string")
     */
    private $formatName;

    private $file;

    public function __construct()
    {
        $this->setCreated();
    }

    /**
     * @param mixed $formatName
     */
    public function setFormatName($formatName)
    {
        $this->formatName = $formatName;
    }

    /**
     * @return mixed
     */
    public function getFormatName()
    {
        return $this->formatName;
    }

    /**
     * Set documento
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Documento $documento
     * @return FormatoAsociado
     */
    public function setDocumento(\Ultra\ControlDocumentoBundle\Entity\Documento $documento = null)
    {
        $this->documento = $documento;

        if($documento !== null)
        {
            $this->setPath($documento->getClave());
        }
    
        return $this;
    }

    /**
     * Get documento
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Documento 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(isset($this->name))
        {
            $this->setTemp($this->getAbsolutePath());
            //$this->setPath(null);
            $this->setName(null);
        }
        else
        {
            $this->setName('initial');
        }

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/'.$this->getName();
    }

    protected function getUploadRootDir()
    {
        return '/var/sad/uploads/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'formato/' . $this->getPath();
    }

    public function __clone()
    {
        if($this->id)
        {
            $this->id = null;
        }
    }
}