<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/03/14
 * Time: 02:11 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


class ClasificacionDeInstituciones {

    private $nombre;

    private $clasificacion;

    function __construct($clasificacion, $nombre)
    {
        $this->clasificacion = $clasificacion;
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getClasificacion()
    {
        return $this->clasificacion;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

}