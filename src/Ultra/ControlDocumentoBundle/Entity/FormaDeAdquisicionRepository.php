<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 24/03/14
 * Time: 12:03 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


class FormaDeAdquisicionRepository {

    public function extraerCompleto(){
        return [
            'curso' => new FormaDeAdquisicion(new Adquisicion('Curso'),'Curso'),
            'adoctrinamiento' => new FormaDeAdquisicion(new Adquisicion('Adoctrinamiento'),'Adoctrinamiento'),
            'lectura' => new FormaDeAdquisicion(new Adquisicion('Curso'),'Curso'),
        ];
    }

    public function extraerEstado(Adquisicion $adquisicion){
        return $this->extraerCompleto()[(string) $adquisicion];
    }

    public function comoArreglo(){
        $array = array();
        foreach($this->extraerCompleto() as $key => $adquisicion){
            $array[$key] = $adquisicion->getNombre();
        }

        return $array;
    }

} 