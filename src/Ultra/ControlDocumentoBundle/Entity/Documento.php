<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Documento
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\DocumentoRepository")
 * @ORM\HasLifecycleCallbacks
 * UniqueEntity({"clave","revision"})
 */
class Documento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacio")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var integer
     * @Assert\NotBlank(message="Este campo no puede estar vacio")
     * @Assert\Type(type="numeric", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="revision", type="integer")
     */
    private $revision;

    /**
     * @var integer
     *
     * @Assert\Type(type="numeric")
     * @@Assert\Range(
     *      min = 1,
     *      max = 4,
     * )
     * @ORM\Column(name="redistribution", type="integer")
     */
    private $redistribution;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="aprobado", type="date")
     */
    private $aprobado;

    /**
     * @var \DateTime fecha de vencimiento
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="vigencia", type="date")
     */
    private $vigencia;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="liberado", type="date", nullable=true)
     */
    private $liberado;

    /**
     * @var \DateTime
     * @Assert\Date(message="El formato de fecha no es valido")
     * @ORM\Column(name="transmitido", type="date", nullable=true)
     */
    private $transmitido;

    /**
     * @var \DateTime
     *
     * Corresponde a la fecha en la cual se agrego el
     * documento por primera vez a la aplicación
     *
     * @ORM\Column(name="creado", type="date", nullable=true)
     */
    private $creado;

    /**
     * @var \DateTime
     *
     * Esta fecha cambia cuando se lleva acabo una actualización
     * en el documento
     *
     * @ORM\Column(name="actualizado", type="date", nullable=true)
     */
    private $actualizado;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^\w+[\w\d-]*\w$/",
     *     match=true,
     *     message="la clave debe de comenzar con una letra, seguida de letras, numeros o guion"
     * )
     * @Assert\NotBlank(message = "La clave tiene que tener un valor")
     * @ORM\Column(name="clave", type="string", length=50)
     */
    private $clave;

    /**
     * @var string
     *
     * @Assert\Regex(
     *     pattern="/^[A-Za-z]{1,2}(-|\s){1}[0-9]{1,5}$/",
     *     match=true,
     *     message="la clave debe de ser dos letras, espacio o guión y cinco números"
     * )
     * @ORM\Column(name="endrac", type="string", nullable=true)
     */
    private $emdrac;

    /**
     * @var string
     *
     * @Assert\Type(
     *  type="scalar",
     *  message="El valor {{ value }} debe de ser {{ type }}."
     * )
     * @ORM\Column(name="revision_endrac", type="string", nullable=true)
     */
    private $revisionEmdrac;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", length=255)
     */
    private $pdf;

    /**
     * @var string
     *
     * @Assert\File(maxSize = "500M", mimeTypes = {
     *   "application/pdf",
     *   "application/x-pdf"
     * })
     */
    private $file;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Condicion")
     */
    private $condicion;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\TipoDocumento")
     */
    private $tipoDocumento;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina", inversedBy="documentos")
     */
    private $disciplina;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\FormatoAsociado", mappedBy="documento", cascade={"persist"})
     */
    private $formats;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\CursoInterno", mappedBy="cursoInterno", cascade={"persist"})
     */
    private $cursos;

    /**
     * Ruta anterior de un archivo
     * @var string
     */
    private $path;

    private $temp;

    private $remove;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Documento
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set revision
     *
     * @param integer $revision
     * @return Documento
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;
    
        return $this;
    }

    /**
     * Get revision
     *
     * @return integer 
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * @return int
     */
    public function getRedistribution()
    {
        return $this->redistribution;
    }

    /**
     * @param int $redistribution
     */
    public function setRedistribution($redistribution)
    {
        $this->redistribution = $redistribution;
    }

    /**
     * Set aprovado
     *
     * @param \DateTime $aprovado
     * @return Documento
     */
    public function setAprobado($aprovado)
    {
        $this->aprobado = $aprovado;
    
        return $this;
    }

    /**
     * Get aprovado
     *
     * @return \DateTime 
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }

    /**
     * Set liberado
     *
     * @param \DateTime $liberado
     * @return Documento
     */
    public function setLiberado($liberado)
    {
        $this->liberado = $liberado;

        return $this;
    }

    /**
     * Get liberado
     *
     * @return \DateTime
     */
    public function getLiberado()
    {
        return $this->liberado;
    }

    /**
     * Set transmitido
     *
     * @param \DateTime $transmitido
     * @return Documento
     */
    public function setTransmitido($transmitido)
    {
        $this->transmitido = $transmitido;

        return $this;
    }

    /**
     * Get transmitido
     *
     * @return \DateTime
     */
    public function getTransmitido()
    {
        return $this->transmitido;
    }

    /**
     * Set vigencia
     *
     * @param \DateTime $vigencia
     * @return Documento
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;
    
        return $this;
    }

    /**
     * Get vigencia
     *
     * @return \DateTime 
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Documento
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Documento
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set condicion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Condicion $condicion
     * @return Documento
     */
    public function setCondicion(\Ultra\ControlDocumentoBundle\Entity\Condicion $condicion = null)
    {
        $this->condicion = $condicion;
    
        return $this;
    }

    /**
     * Get condicion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Condicion 
     */
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set tipoDocumento
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento
     * @return Documento
     */
    public function setTipoDocumento(\Ultra\ControlDocumentoBundle\Entity\TipoDocumento $tipoDocumento = null)
    {
        $this->tipoDocumento = $tipoDocumento;
    
        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoDocumento 
     */
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }

    /**
     * Set disciplina
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplina
     * @return Documento
     */
    public function setDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplina = null)
    {
        $this->disciplina = $disciplina;
    
        return $this;
    }

    /**
     * Get disciplina
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Disciplina 
     */
    public function getDisciplina()
    {
        return $this->disciplina;
    }

    /**
     * regrese el nombre del documento
     *
     * @return string
     */
    public function __toString(){
        return $this->getClave().'-Rev.'.$this->getRevision().'-'.$this->getTitulo();
    }

    //comienza implementacion para guardar documento

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (is_file($this->getAbsolutePath())) {
            $this->temp = $this->getAbsolutePath();
            //$this->path = null;
        } else {
            $this->pdf = 'initial';
        }
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            //Nombre del archivo con formato Numero de contrato - clave.pdf
            $name = $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension();
            $this->setPdf($name);
        }
        $this->creado = new \DateTime();

    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {
            //Nombre del archivo con formato Numero de contrato - clave.pdf
            $name = $this->getClave().'-r'.$this->getRevision().'.'.$this->getFile()->guessExtension();
            $this->setPdf($name);
            $this->actualizado = new \DateTime();
        }

        if(null != $this->getTemporaryPath()){
            $f = new Filesystem();
            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),true);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {

        if(null == $this->getFile())
        {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->temp = null;
        $this->file = null;
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadRootDir().'/'.$this->pdf;
    }

    public function getUploadRootDir()
    {

        if($this->getCondicion()->getId() == 5)
        {
            return '/var/sad/'.$this->getUploadDir().'/1';
        }

        return '/var/sad/'.$this->getUploadDir().'/'.$this->getCondicion()->getId();
    }

    public function getUploadDir()
    {
        return 'uploads/documentos/'.$this->getDisciplina()->getArea()->getContrato()->getNumeroInterno();
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    public function getTemporaryPath(){
        return $this->path;
    }

    public function setTemp($path)
    {
        $this->temp = $path;
    }

    public function getTemp(){
        return $this->temp;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return Documento
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;
    
        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime 
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * Set actualizado
     *
     * @param \DateTime $actualizado
     * @return Documento
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;
    
        return $this;
    }

    /**
     * Get actualizado
     *
     * @return \DateTime 
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    public function __clone()
    {
        if($this->id)
        {
            $this->id = null;
            $this->formats = clone $this->formats;
            $this->cursos = null;
            //$this->cursos = clone $this->cursos;
        }
    }

    public function move($path, Condicion $estado)
    {
        if($this->getCondicion()->getId() !== $estado->getId())
        {
            $fs = new Filesystem();

            if($fs->exists($path))
            {
                try{
                    $fs->copy($path, $this->getAbsolutePath(), true);
                    //$fs->remove($path);
                }
                catch (FileNotFoundException $e){
                    return $e->getTrace();
                }
                catch (IOException $e){
                    return $e->getTrace();
                }
            }
            $fs = null;
        }

    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->cursos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->remove = array();
    }
    
    /**
     * Add formats
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\FormatoAsociado $formats
     * @return Documento
     */
    public function addFormat(\Ultra\ControlDocumentoBundle\Entity\FormatoAsociado $formats)
    {
        $formats->setDocumento($this);

        $this->formats[] = $formats;
    
        return $this;
    }

    /**
     * Remove formats
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\FormatoAsociado $formats
     */
    public function removeFormat(\Ultra\ControlDocumentoBundle\Entity\FormatoAsociado $formats)
    {
        $this->formats->removeElement($formats);
    }

    /**
     * Get formats
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormats()
    {
        return $this->formats;
    }

    public function updateFormats($formats = null)
    {
        foreach($formats as $format)
        {
            if(false == $this->getFormats()->contains($format))
            {
                $format->setDocumento(null);
                $this->removeNullFormats($format);
            }
        }
    }

    public function removeNullFormats(FormatoAsociado $formatoAsociado)
    {
        $this->remove[] = $formatoAsociado;

        return $this;
    }

    public function getRemoveNullFormats()
    {
        return $this->remove;
    }

    /**
     * Add cursos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursos
     * @return Documento
     */
    public function addCurso(\Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursos)
    {
        $cursos->setCursoInterno($this);
        $this->cursos[] = $cursos;
    
        return $this;
    }

    /**
     * Remove cursos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursos
     */
    public function removeCurso(\Ultra\ControlDocumentoBundle\Entity\CursoInterno $cursos)
    {
        $this->cursos->removeElement($cursos);
    }

    /**
     * Get cursos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCursos()
    {
        return $this->cursos;
    }

    /**
     * @return string
     */
    public function getEmdrac()
    {
        return $this->emdrac;
    }

    /**
     * @param string $emdrac
     * @return Documento
     */
    public function setEmdrac($emdrac)
    {
        $this->emdrac = $emdrac;
        return $this;
    }

    /**
     * @return string
     */
    public function getRevisionEmdrac()
    {
        return $this->revisionEmdrac;
    }

    /**
     * @param $revisionEmdrac
     * @return Documento
     */
    public function setRevisionEmdrac($revisionEmdrac)
    {
        $this->revisionEmdrac = $revisionEmdrac;
        return $this;
    }

}