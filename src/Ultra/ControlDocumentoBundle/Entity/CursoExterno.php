<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ultra\ControlDocumentoBundle\Model\UploadFileInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ultra\ControlDocumentoBundle\Model\UploadManagerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CursoExterno
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\CursoExternoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class CursoExterno implements UploadFileInterface, UploadManagerInterface
{
    private $file;

    private $temp;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_realizacion", type="date", nullable=true)
     * @Assert\NotBlank(groups={"control"})
     */
    private $fechaRealizacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_vigencia", type="date", nullable=true)
     * @Assert\NotBlank(groups={"control"})
     */
    private $fechaVigencia;

    /**
     * @var string
     *
     * @ORM\Column(name="pdfCertificado", type="string", length=255, nullable=true)
     */
    private $pdfCertificado;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Curricula", inversedBy="cursosExternos")
     */
    private $curricula;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Curso", inversedBy="cursosExternos")
     * @Assert\NotBlank(groups={"control"})
     */
    private $cursoExterno;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="TipoAdquisicion")
     */
    private $tipoAdquisicion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaRealizacion
     *
     * @param \DateTime $fechaRealizacion
     * @return CursoExterno
     */
    public function setFechaRealizacion($fechaRealizacion)
    {
        $this->fechaRealizacion = $fechaRealizacion;
    
        return $this;
    }

    /**
     * Get fechaRealizacion
     *
     * @return \DateTime 
     */
    public function getFechaRealizacion()
    {
        return $this->fechaRealizacion;
    }

    /**
     * Set fechaVigencia
     *
     * @param \DateTime $fechaVigencia
     * @return CursoExterno
     */
    public function setFechaVigencia($fechaVigencia)
    {
        $this->fechaVigencia = $fechaVigencia;
    
        return $this;
    }

    /**
     * Get fechaVigencia
     *
     * @return \DateTime 
     */
    public function getFechaVigencia()
    {
        return $this->fechaVigencia;
    }

    /**
     * Set pdfCertificado
     *
     * @param string $pdfCertificado
     * @return CursoExterno
     */
    public function setPdfCertificado($pdfCertificado)
    {
        $this->pdfCertificado = $pdfCertificado;
    
        return $this;
    }

    /**
     * Get pdfCertificado
     *
     * @return string 
     */
    public function getPdfCertificado()
    {
        return $this->pdfCertificado;
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return CursoExterno
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;
    
        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * Set cursoExterno
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $cursoExterno
     * @return CursoExterno
     */
    public function setCursoExterno(\Ultra\ControlDocumentoBundle\Entity\Curso $cursoExterno = null)
    {
        $this->cursoExterno = $cursoExterno;
    
        return $this;
    }

    /**
     * Get cursoExterno
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curso 
     */
    public function getCursoExterno()
    {
        return $this->cursoExterno;
    }

    /**
     * Set tipoAdquisicion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoAdquisicion $tipoAdquisicion
     * @return CursoExterno
     */
    public function setTipoAdquisicion(\Ultra\ControlDocumentoBundle\Entity\TipoAdquisicion $tipoAdquisicion = null)
    {
        $this->tipoAdquisicion = $tipoAdquisicion;
    
        return $this;
    }

    /**
     * Get tipoAdquisicion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoAdquisicion 
     */
    public function getTipoAdquisicion()
    {
        return $this->tipoAdquisicion;
    }

    public function estaVigente(){
        if(!$this->fechaVigencia instanceof \DateTime){
            throw new \Exception('Vigencia no es de tipo DateTime');
        }

        if(!$this->fechaRealizacion instanceof \DateTime){
            throw new \Exception('Fecha de realización no es de tipo DateTime');
        }

        $currentDate = new \DateTime();

        if($currentDate < $this->fechaVigencia){
            return true;
        }
        return false;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(isset($this->pdfCertificado))
        {
            $this->setTemp($this->getAbsolutePath());
            //$this->setPath(null);
            $this->setPdfCertificado(null);
        }
        else
        {
            $this->setPdfCertificado('initial');
        }

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $temp
     */
    private function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * @return mixed
     */
    private function getTemp()
    {
        return $this->temp;
    }


    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/'.$this->getPdfCertificado();
    }

    private function getUploadRootDir(){
        return '/var/sad/uploads/curriculas/'. $this->getUploadDir();
    }

    private function getUploadDir(){
        return $this->getPath().'/'.$this->getClassName();
    }

    private function getClassName(){
        $class = explode('\\', get_class($this));
        return end($class);
    }

    private function hashFile($file){
        return sha1_file($file);
    }

    /**
     *
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if(null !== $this->getFile())
        {
            $this->setPath($this->getCurricula()->getCurp());
            $this->setPdfCertificado($this->hashFile($this->file->getPathname()) . '.' . $this->file->guessExtension());
        }
    }

    /**
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->getPdfCertificado());

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getTemp());
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

}