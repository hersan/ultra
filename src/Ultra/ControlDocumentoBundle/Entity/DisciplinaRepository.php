<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * DisciplinaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DisciplinaRepository extends EntityRepository
{
    public function loadAll()
    {

        $em = $this->getEntityManager();

        $query = $em->createQuery("
            SELECT d.id id,d.titulo titulo,d.tituloCorto,
                   a.claveArea area,
                   c.numeroInterno contrato
            FROM ControlDocumentoBundle:Disciplina d
            JOIN d.area a
            JOIN a.contrato c
        ");

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function loadById($id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("
            SELECT d.id id,d.titulo titulo,d.tituloCorto,
                   a.claveArea area,
                   c.numeroInterno contrato
            FROM ControlDocumentoBundle:Disciplina d
              JOIN d.area a
              JOIN a.contrato c
            WHERE d.id = :id
        ")->setParameter('id',$id);

        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
