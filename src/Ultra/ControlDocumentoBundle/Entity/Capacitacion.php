<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Capacitacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\CapacitacionRepository")
 */
class Capacitacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="capacitacion")
     */
    private $perfil;

     /**
     * @var string
     *
     * Assert\NotNull(message="Seleccione un documento")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Documento")
     */
    private $capacitacion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Capacitacion
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set capacitacion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Documento $capacitacion
     * @return Capacitacion
     */
    public function setCapacitacion(\Ultra\ControlDocumentoBundle\Entity\Documento $capacitacion = null)
    {
        $this->capacitacion = $capacitacion;
    
        return $this;
    }

    /**
     * Get capacitacion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Documento 
     */
    public function getCapacitacion()
    {
        return $this->capacitacion;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }
}