<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ultra\ControlDocumentoBundle\Model\Control\GeneralDocumentUploadedTrait;

/**
 * GeneralDocument
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\GeneralDocumentRepository")
 */
class GeneralDocument
{

    use GeneralDocumentUploadedTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=255)
     */
    private $year;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\TipoDocumento
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\TipoDocumento")
     */
    private $type;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $curriculum;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GeneralDocument
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set year
     *
     * @param string $year
     * @return GeneralDocument
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }
    
    /**
     * Get type
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\DocumentType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoDocumento $type
     * @return GeneralDocument
     */
    public function setType(\Ultra\ControlDocumentoBundle\Entity\TipoDocumento $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get curriculum
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getCurriculum()
    {
        return $this->curriculum;
    }

    /**
     * Set curriculum
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curriculum
     * @return GeneralDocument
     */
    public function setCurriculum(\Ultra\ControlDocumentoBundle\Entity\Curricula $curriculum = null)
    {
        $this->curriculum = $curriculum;

        return $this;
    }
}