<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/05/14
 * Time: 08:37 AM
 */

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PdfCalificacion
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\Entity()
 */
class PdfCalificacion extends AbstractUploadedFile{

    public function __construct()
    {
        $this->setCreated();
    }
} 