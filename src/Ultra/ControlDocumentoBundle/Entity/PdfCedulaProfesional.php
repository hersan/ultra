<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 10/04/14
 * Time: 05:20 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PdfIfe
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\Entity()
 */
class PdfCedulaProfesional extends AbstractUltraFile {


    private $file;

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().'/'.$this->getName();
    }

    public function getUploadRootDir()
    {
        return '/var/sad/uploads/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        return 'curriculas/anexo/' . $this->getPath();
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if(isset($this->path) && isset($this->name))
        {
            $this->setTemp($this->getAbsolutePath());
            //$this->setPath(null);
            $this->setName(null);
        }
        else
        {
            $this->setName('initial');
        }

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function preUpload()
    {
        if(!is_object($this->getCreated()) || !($this->getCreated() instanceof \DateTime))
        {
            $this->setCreated();
        }

        $this->setLastUpdate();

        if(null !== $this->getFile())
        {
            $this->setHash($this->file->getPathname());
            $this->setName($this->getHash() . '.' . $this->file->guessExtension());
        }
    }

    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->name);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getTemp());
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getTypeName()
    {
        return 'Cedula Profecional';
    }
    
}