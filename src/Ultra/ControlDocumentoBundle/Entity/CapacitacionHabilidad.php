<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CapacitacionHabilidad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\CapacitacionHabilidadRepository")
 */
class CapacitacionHabilidad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_conocimiento", type="string", length=255)
     */
    private $tipoConocimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="condicion", type="string", length=255)
     */
    private $condicion;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="capacitacionHabilidades")
     */
    private $perfil;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Documento")
     */
    Private $capacitacion;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curso")
     */
    Private $habilidad;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipoConocimiento
     *
     * @param string $tipoConocimiento
     * @return CapacitacionHabilidad
     */
    public function setTipoConocimiento($tipoConocimiento)
    {
        $this->tipoConocimiento = $tipoConocimiento;
    
        return $this;
    }

    /**
     * Get tipoConocimiento
     *
     * @return string 
     */
    public function getTipoConocimiento()
    {
        return $this->tipoConocimiento;
    }

    /**
     * Set condicion
     *
     * @param string $condicion
     * @return CapacitacionHabilidad
     */
    public function setCondicion($condicion)
    {
        $this->condicion = $condicion;
    
        return $this;
    }

    /**
     * Get condicion
     *
     * @return string 
     */
    public function getCondicion()
    {
        return $this->condicion;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return CapacitacionHabilidad
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set capacitacion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Documento $capacitacion
     * @return CapacitacionHabilidad
     */
    public function setCapacitacion(\Ultra\ControlDocumentoBundle\Entity\Documento $capacitacion = null)
    {
        $this->capacitacion = $capacitacion;
    
        return $this;
    }

    /**
     * Get capacitacion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Documento 
     */
    public function getCapacitacion()
    {
        return $this->capacitacion;
    }

    /**
     * Set habilidad
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curso $habilidad
     * @return CapacitacionHabilidad
     */
    public function setHabilidad(\Ultra\ControlDocumentoBundle\Entity\Curso $habilidad = null)
    {
        $this->habilidad = $habilidad;
    
        return $this;
    }

    /**
     * Get habilidad
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curso 
     */
    public function getHabilidad()
    {
        return $this->habilidad;
    }
}