<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Escolaridad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\EscolaridadRepository")
 * @Assert\Callback(methods={"ProfessionalLicense"})
 */
class Escolaridad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * Assert\NotNull(message="Debe de marcar este campo")
     * Assert\True(message="Marca este campo");
     * @ORM\Column(name="titulo", type="boolean")
     */
    private $titulo;

    /**
     * @var boolean
     *
     * Assert\NotNull(message="Debe de marcar este campo")
     * Assert\True(message="Marca este campo");
     * @ORM\Column(name="cedula", type="boolean")
     */
    private $cedula;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="escolaridad")
     */
    private $perfil;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Profesion
     *
     * @Assert\NotNull(message="Selecciona una profesión")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Profesion")
     */
    private $profesion;


    public function __construct()
    {
        $this->titulo = false;
        $this->cedula = false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param boolean $titulo
     * @return Escolaridad
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return boolean 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set cedula
     *
     * @param boolean $cedula
     * @return Escolaridad
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;
    
        return $this;
    }

    /**
     * Get cedula
     *
     * @return boolean 
     */
    public function getCedula()
    {
        return $this->cedula;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Escolaridad
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set profesion
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Profesion $profesion
     * @return Escolaridad
     */
    public function setProfesion(\Ultra\ControlDocumentoBundle\Entity\Profesion $profesion = null)
    {
        $this->profesion = $profesion;
    
        return $this;
    }

    /**
     * Get profesion
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Profesion 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }

    public function ProfessionalLicense(ExecutionContextInterface $context)
    {
        if(true === $this->cedula && false === $this->titulo){
            $context->addViolationAt('cedula','No puede tener cedula sin titulo',array(),null);
        }
    }
}