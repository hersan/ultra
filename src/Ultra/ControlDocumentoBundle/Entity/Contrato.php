<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Contrato
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ContratoRepository")
 */
class Contrato
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo_corto", type="string", length=15)
     */
    private $tituloCorto;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="numero_interno", type="string", length=25)
     */
    private $numeroInterno;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="numero_externo", type="string", length=25)
     */
    private $numeroExterno;

    /**
     * @var boolean
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\NotNull(message="Este campo no puede ser nulo")
     * @ORM\Column(name="abierto", type="boolean")
     */
    private $abierto = true;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\EtapaProyecto")
     */
    private $etapa;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Cliente", inversedBy="contratos")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Area", mappedBy="contrato")
     */
    private $areas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Contrato
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tituloCorto
     *
     * @param string $tituloCorto
     * @return Contrato
     */
    public function setTituloCorto($tituloCorto)
    {
        $this->tituloCorto = $tituloCorto;
    
        return $this;
    }

    /**
     * Get tituloCorto
     *
     * @return string 
     */
    public function getTituloCorto()
    {
        return $this->tituloCorto;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Contrato
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    
        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set abierto
     *
     * @param boolean $abierto
     * @return Contrato
     */
    public function setAbierto($abierto)
    {
        $this->abierto = $abierto;
    
        return $this;
    }

    /**
     * Get abierto
     *
     * @return boolean 
     */
    public function getAbierto()
    {
        return $this->abierto;
    }

    /**
     * Set cliente
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Cliente $cliente
     * @return Contrato
     */
    public function setCliente(\Ultra\ControlDocumentoBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;
    
        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    public function __toString(){
        return $this->numeroInterno;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->areas = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set numeroInterno
     *
     * @param string $numeroInterno
     * @return Contrato
     */
    public function setNumeroInterno($numeroInterno)
    {
        $this->numeroInterno = $numeroInterno;
    
        return $this;
    }

    /**
     * Get numeroInterno
     *
     * @return string 
     */
    public function getNumeroInterno()
    {
        return $this->numeroInterno;
    }

    /**
     * Set numeroExterno
     *
     * @param string $numeroExterno
     * @return Contrato
     */
    public function setNumeroExterno($numeroExterno)
    {
        $this->numeroExterno = $numeroExterno;
    
        return $this;
    }

    /**
     * Get numeroExterno
     *
     * @return string 
     */
    public function getNumeroExterno()
    {
        return $this->numeroExterno;
    }

    /**
     * Add areas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Area $areas
     * @return Contrato
     */
    public function addArea(\Ultra\ControlDocumentoBundle\Entity\Area $areas)
    {
        $this->areas[] = $areas;
    
        return $this;
    }

    /**
     * Remove areas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Area $areas
     */
    public function removeArea(\Ultra\ControlDocumentoBundle\Entity\Area $areas)
    {
        $this->areas->removeElement($areas);
    }

    /**
     * Get areas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAreas()
    {
        return $this->areas;
    }


    /**
     * Set claveArea
     *
     * @param string $claveArea
     * @return Contrato
     */
    public function setClaveArea($claveArea)
    {
        $this->claveArea = $claveArea;
    
        return $this;
    }

    /**
     * Get claveArea
     *
     * @return string 
     */
    public function getClaveArea()
    {
        return $this->claveArea;
    }

    /**
     * Set etapa
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\EtapaProyecto $etapa
     * @return Contrato
     */
    public function setEtapa(\Ultra\ControlDocumentoBundle\Entity\EtapaProyecto $etapa = null)
    {
        $this->etapa = $etapa;
    
        return $this;
    }

    /**
     * Get etapa
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\EtapaProyecto 
     */
    public function getEtapa()
    {
        return $this->etapa;
    }
}