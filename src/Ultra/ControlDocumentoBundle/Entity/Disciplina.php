<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Disciplina
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\DisciplinaRepository")
 */
class Disciplina
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo", type="string", length=50)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo_corto", type="string", length=10)
     */
    private $tituloCorto;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Area", inversedBy="disciplinas")
     */
    private $area;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Documento", mappedBy="disciplina")
     */
    private $documentos;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", mappedBy="disciplinas")
     */
    private $perfiles;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\ProyectoBundle\Entity\OrdenTrabajo", mappedBy="disciplina")
     */
    private $proyectos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documentos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->perfiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proyectos = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Disciplina
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tituloCorto
     *
     * @param string $tituloCorto
     * @return Disciplina
     */
    public function setTituloCorto($tituloCorto)
    {
        $this->tituloCorto = $tituloCorto;
    
        return $this;
    }

    /**
     * Get tituloCorto
     *
     * @return string 
     */
    public function getTituloCorto()
    {
        return $this->tituloCorto;
    }

    /**
     * Set area
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Area $area
     * @return Disciplina
     */
    public function setArea(\Ultra\ControlDocumentoBundle\Entity\Area $area = null)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Add documentos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Documento $documentos
     * @return Disciplina
     */
    public function addDocumento(\Ultra\ControlDocumentoBundle\Entity\Documento $documentos)
    {
        $this->documentos[] = $documentos;
    
        return $this;
    }

    /**
     * Remove documentos
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Documento $documentos
     */
    public function removeDocumento(\Ultra\ControlDocumentoBundle\Entity\Documento $documentos)
    {
        $this->documentos->removeElement($documentos);
    }

    /**
     * Get documentos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocumentos()
    {
        return $this->documentos;
    }

    /**
     * Add perfiles
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfiles
     * @return Disciplina
     */
    public function addPerfile(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfiles)
    {
        $this->perfiles[] = $perfiles;
    
        return $this;
    }

    /**
     * Remove perfiles
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfiles
     */
    public function removePerfile(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfiles)
    {
        $this->perfiles->removeElement($perfiles);
    }

    /**
     * Get perfiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPerfiles()
    {
        return $this->perfiles;
    }

    public function __toString()
    {
        return $this->titulo .' ('. $this->getArea()->getClaveArea().':'.$this->getArea()->getContrato()->getNumeroInterno().')';
    }

    /*public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }*/

    /**
     * Add proyectos
     *
     * @param \Ultra\ProyectoBundle\Entity\OrdenTrabajo $proyectos
     * @return Disciplina
     */
    public function addProyecto(\Ultra\ProyectoBundle\Entity\OrdenTrabajo $proyectos)
    {
        $this->proyectos[] = $proyectos;
    
        return $this;
    }

    /**
     * Remove proyectos
     *
     * @param \Ultra\ProyectoBundle\Entity\OrdenTrabajo $proyectos
     */
    public function removeProyecto(\Ultra\ProyectoBundle\Entity\OrdenTrabajo $proyectos)
    {
        $this->proyectos->removeElement($proyectos);
    }

    /**
     * Get proyectos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProyectos()
    {
        return $this->proyectos;
    }
}