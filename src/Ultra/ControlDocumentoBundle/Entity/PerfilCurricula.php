<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ultra\ControlDocumentoBundle\Model\Evaluacion;

/**
 * PerfilCurricula
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\PerfilCurriculaRepository")
 */
class PerfilCurricula
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="numero_credencial", type="string", length=50)
     */
    private $numeroCredencial;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf_credencial", type="string", length=255, nullable=true)
     */
    private $pdfCredencial;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_alta", type="date")
     */
    private $fechaAlta;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_baja", type="date")
     */
    private $fechaBaja;

    /**
     * @var string
     * @ORM\Column(name="contrato", type="string", length=50)
     */
    private $contrato;

    /**
     * @var string
     *
     * @ORM\Column(name="ubicacion", type="string", length=255, nullable=true)
     */
    private $ubicacion;


    private $files;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="perfiles" )
     */
    private $perfil;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula", inversedBy="curriculas")
     */
    private $curricula;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="AbstractUploadedFile", mappedBy="perfilCurricula", cascade={"persist"})
     */
    private $evaluaciones;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroCredencial
     *
     * @param string $numeroCredencial
     * @return PerfilCurricula
     */
    public function setNumeroCredencial($numeroCredencial)
    {
        $this->numeroCredencial = $numeroCredencial;

        return $this;
    }

    /**
     * Get numeroCredencial
     *
     * @return string 
     */
    public function getNumeroCredencial()
    {
        return $this->numeroCredencial;
    }

    /**
     * Set pdfCredencial
     *
     * @param string $pdfCredencial
     * @return PerfilCurricula
     */
    public function setPdfCredencial($pdfCredencial)
    {
        $this->pdfCredencial = $pdfCredencial;
    
        return $this;
    }

    /**
     * Get pdfCredencial
     *
     * @return string 
     */
    public function getPdfCredencial()
    {
        return $this->pdfCredencial;
    }

    /**
     * Set ubicacion
     *
     * @param string $ubicacion
     * @return PerfilCurricula
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;

        return $this;
    }

    /**
     * Get ubicacion
     *
     * @return string
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return PerfilCurricula
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;
    
        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return PerfilCurricula
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;
    
        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
     * Set contrato
     *
     * @param string $contrato
     * @return PerfilCurricula
     */
    public function setContrato($contrato)
    {
        $this->contrato = $contrato;
    
        return $this;
    }

    /**
     * Get contrato
     *
     * @return string
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * @param mixed $files
     */
    public function setFiles(Evaluacion $files = null)
    {
        $this->files = $files;
        if($this->getEvaluaciones()->isEmpty())
        {
            foreach($this->files as $file)
            {
                if(null !== $file->getFile())
                {
                    $this->addEvaluacione($file);
                }
            }
            return;
        }

        foreach($this->files as $file)
        {
            if(null !== $file->getFile()){
                foreach($this->getEvaluaciones() as $evaluacion)
                {
                    if(get_class($file) == get_class($evaluacion))
                    {
                        $evaluacion->setFile($file->getFile());
                        //$evaluacion->setName('initial');
                        $file->setFile(null);
                        $file->getFile();
                    }
                }
            }
        }

        foreach($this->files as $file)
        {
            if(null !== $file->getFile()){
                $this->addEvaluacione($file);
            }
        }
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return PerfilCurricula
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return PerfilCurricula
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;
    
        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->evaluaciones = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add evaluaciones
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\AbstractUploadedFile $evaluaciones
     * @return PerfilCurricula
     */
    public function addEvaluacione(\Ultra\ControlDocumentoBundle\Entity\AbstractUploadedFile $evaluaciones)
    {
        $evaluaciones->setPerfilCurricula($this);
        $this->evaluaciones[] = $evaluaciones;
    
        return $this;
    }

    /**
     * Remove evaluaciones
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\AbstractUploadedFile $evaluaciones
     */
    public function removeEvaluacione(\Ultra\ControlDocumentoBundle\Entity\AbstractUploadedFile $evaluaciones)
    {
        $this->evaluaciones->removeElement($evaluaciones);
    }

    /**
     * Get evaluaciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEvaluaciones()
    {
        return $this->evaluaciones;
    }

    /**
     * Set tipobaja
     *
     * @param string $tipobaja
     * @return PerfilCurricula
     */
    public function setTipobaja($tipobaja)
    {
        $this->tipobaja = $tipobaja;
    
        return $this;
    }

    /**
     * Get tipobaja
     *
     * @return string 
     */
    public function getTipobaja()
    {
        return $this->tipobaja;
    }

    /**
     * Set comentbaja
     *
     * @param string $comentbaja
     * @return PerfilCurricula
     */
    public function setComentbaja($comentbaja)
    {
        $this->comentbaja = $comentbaja;
    
        return $this;
    }

    /**
     * Get comentbaja
     *
     * @return string 
     */
    public function getComentbaja()
    {
        return $this->comentbaja;
    }

    /**
     * Set baja
     *
     * @param boolean $baja
     * @return PerfilCurricula
     */
    public function setBaja($baja)
    {
        $this->baja = $baja;
    
        return $this;
    }

    /**
     * Get baja
     *
     * @return boolean 
     */
    public function getBaja()
    {
        return $this->baja;
    }
}