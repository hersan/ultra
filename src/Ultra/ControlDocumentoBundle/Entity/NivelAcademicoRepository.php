<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * NivelAcademicoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NivelAcademicoRepository extends EntityRepository
{
}
