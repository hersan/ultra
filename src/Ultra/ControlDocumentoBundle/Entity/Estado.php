<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/03/14
 * Time: 03:55 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


class Estado {

    private $codigo;

    function __construct($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    public function __toString(){
        return $this->getCodigo();
    }

} 