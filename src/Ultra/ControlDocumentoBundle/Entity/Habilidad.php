<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Habilidad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\HabilidadRepository")
 */
class Habilidad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="habilidades")
     */
    private $perfil;

    /**
     * @var
     *
     * Assert\NotNull()
     * Assert\NotBlank()
     * @ORM\Column(name="curso", type="string", length=255, nullable=true)
     */
    private $curso;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Habilidad
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    /**
     * Set curso
     *
     * @param string
     * @return Habilidad
     */
    public function setCurso($curso = null)
    {
        $this->curso = $curso;
    
        return $this;
    }

    /**
     * Get curso
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curso 
     */
    public function getCurso()
    {
        return $this->curso;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }
}