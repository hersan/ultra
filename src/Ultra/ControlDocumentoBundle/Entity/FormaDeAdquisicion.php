<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 24/03/14
 * Time: 11:58 AM
 */

namespace Ultra\ControlDocumentoBundle\Entity;


class FormaDeAdquisicion {

    private $adquisicion;

    private $nombre;

    function __construct(Adquisicion $adquisicion, $nombre)
    {
        $this->adquisicion = $adquisicion;
        $this->nombre = $nombre;
    }

    /**
     * @return \Ultra\ControlDocumentoBundle\Entity\Adquisicion
     */
    public function getAdquisicion()
    {
        return $this->adquisicion;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

} 