<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 15/05/14
 * Time: 10:31 AM
 */

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AbstractUploadManager
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class AbstractUploadManager {

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string")
     */
    protected $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="date")
     */
    protected  $updated;

    /**
     * @var string
     *
     */
    protected  $temp;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    protected function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @internal param mixed $created
     */
    protected  function setCreated()
    {
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @internal param mixed $lastUpdate
     */
    protected function setUpdated()
    {
        $this->updated = new \DateTime();
    }

    /**
     *
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function preUpload(){
        if(null !== $this->getFile()){
            $this->setHash($this->getFile()->getPathname());
            $this->setName($this->getHash().'.'.$this->getFile()->guessExtension());
            $this->setUpdated();
        }
    }

    /**
     * @return UploadedFile
     */
    abstract function getFile();

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    protected function setHash($hash)
    {
        $this->hash = sha1_file($hash);
    }

    /**
     *
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function upload(){
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->name);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getTemp());
            // clear the temp image path
            $this->temp = null;
        }
        $this->setFile(null);
    }

    abstract protected function getUploadRootDir();

    /**
     * @return string
     */
    protected function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param string $temp
     */
    protected function setTemp($temp)
    {
        $this->temp = $temp;
    }

    abstract function setFile(UploadedFile $file = null);

    /**
     *
     * @ORM\PostRemove
     */
    public function removeUpload(){
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    abstract public function getAbsolutePath();

    abstract protected function getUploadDir();

    /**
     * Set file from form
     *
     * @param UploadedFile|null $file
     *
     * @return mixed
     */
    //abstract function setAttachment(UploadedFile $file = null);

    /**
     * Get store file from form
     *
     * @return UploadedFile
     */
    //abstract function getAttachment();
} 