<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 19/05/14
 * Time: 05:57 PM
 */

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class AbstractUltraFile
 * @package Ultra\ControlDocumentoBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type_class", type="string")
 * @ORM\DiscriminatorMap({
 *      "ultra" = "AbstractUploadedFile",
 *      "certificado" = "PdfCertificado",
 *      "calificacion" = "PdfCalificacion"
 * })
 */
class AbstractUploadedFile extends AbstractUploadManager {

    protected $rootDirectory = '/var/sad/uploads/';

    protected $uploadDirectory = 'evaluacion';

    /**
     * @var
     *
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $path;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="PerfilCurricula", inversedBy="evaluaciones")
     */
    protected $perfilCurricula;

    protected $file;

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;

        if(isset($this->name))
        {
            $this->setTemp($this->getAbsolutePath());
            //$this->setPath(null);
            $this->setName(null);
        }
        else
        {
            $this->setName('initial');
        }

        return $this;
    }

    public function getAbsolutePath()
    {
        return $this->getUploadRootDir().DIRECTORY_SEPARATOR.$this->getName();
    }

    protected function getUploadRootDir()
    {
        return sprintf('%s/%S',$this->uploadDirectory, $this->getUploadDir());
    }

    protected function getUploadDir()
    {
        return sprintf('%s/%s',$this->uploadDirectory,$this->getPath());
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Get perfilCurricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula
     */
    public function getPerfilCurricula()
    {
        return $this->perfilCurricula;
    }

    /**
     * Set perfilCurricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfilCurricula
     * @return AbstractUploadedFile
     */
    public function setPerfilCurricula(\Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $perfilCurricula = null)
    {
        $this->perfilCurricula = $perfilCurricula;
        if(null !== $perfilCurricula)
        {
            $this->setPath($perfilCurricula->getId());
        }
        return $this;
    }

    function setAttachment(UploadedFile $file = null)
    {
        ;
    }

    function getAttachment()
    {
        ;
    }

}