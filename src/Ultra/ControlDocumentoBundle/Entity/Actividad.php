<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Actividad
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ActividadRepository")
 */
class Actividad
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * Assert\NotNull()
     * Assert\NotBlank(message="Este campo es necesario")
     * Assert\Type(type="string", message="El valor {{value}} no es de tipo {{type}}.")
     * Assert\Length(
     *      min = "5",
     *      max = "255",
     *      minMessage = "Este campo debe contener al menos {{ limit }} caracteres",
     *      maxMessage = "Este campo no debe de exceder de {{ limit }} caracteres")
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var
     * Assert\Valid(traverse=true)
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="actividades")
     */
    private $perfil;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Actividad
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Actividad
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }
}