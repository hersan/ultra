<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Area
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\AreaRepository")
 */
class Area
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="El título del área no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo", type="string", length=45)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="No puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="titulo_corto", type="string", length=15)
     */
    private $tituloCorto;

    /**
     * @var string
     * @Assert\NotBlank(message="Vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="clave_area", type="string", length=15)
     */
    private $claveArea;

    /**
     * @Assert\NotBlank(message="No puede estar vacío el contrato")
     * @Assert\NotNull(message="No puede ser nulo el contrato")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Contrato", inversedBy="areas")
     */
    private $contrato;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $curricula;

    /**
     *
     * @ORM\OneToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina", mappedBy="area")
     */
    private $disciplinas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Area
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tituloCorto
     *
     * @param string $tituloCorto
     * @return Area
     */
    public function setTituloCorto($tituloCorto)
    {
        $this->tituloCorto = $tituloCorto;
    
        return $this;
    }

    /**
     * Get tituloCorto
     *
     * @return string 
     */
    public function getTituloCorto()
    {
        return $this->tituloCorto;
    }

    public function __toString()
    {
        return $this->claveArea . ' (' . $this->titulo .') ' . $this->getContrato()->getNumeroInterno();
    }

    /**
     * Set contrato
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Contrato $contrato
     * @return Area
     */
    public function setContrato(\Ultra\ControlDocumentoBundle\Entity\Contrato $contrato = null)
    {
        $this->contrato = $contrato;
    
        return $this;
    }

    /**
     * Get contrato
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Contrato 
     */
    public function getContrato()
    {
        return $this->contrato;
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return Area
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;

        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->disciplinas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->perfiles = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add disciplinas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas
     * @return Area
     */
    public function addDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas)
    {
        $this->disciplinas[] = $disciplinas;
    
        return $this;
    }

    /**
     * Remove disciplinas
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas
     */
    public function removeDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplinas)
    {
        $this->disciplinas->removeElement($disciplinas);
    }

    /**
     * Get disciplinas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDisciplinas()
    {
        return $this->disciplinas;
    }


    /**
     * Set claveArea
     *
     * @param string $claveArea
     * @return Area
     */
    public function setClaveArea($claveArea)
    {
        $this->claveArea = $claveArea;
    
        return $this;
    }

    /**
     * Get claveArea
     *
     * @return string 
     */
    public function getClaveArea()
    {
        return $this->claveArea;
    }

}