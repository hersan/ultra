<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * GeneralDocumentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GeneralDocumentRepository extends EntityRepository
{
}
