<?php

namespace Ultra\ControlDocumentoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Experiencia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\ControlDocumentoBundle\Entity\ExperienciaRepository")
 */
class Experiencia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="tiempo", type="integer", nullable=true)
     */
    private $tiempo;

    /**
     * @var integer
     * @ORM\Column(name="meses", type="integer", nullable=true)
     */
    private $meses;

    /**
     * @var string
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil", inversedBy="expertise")
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tiempo
     *
     * @param integer $tiempo
     * @return Experiencia
     */
    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;
    
        return $this;
    }

    /**
     * Get tiempo
     *
     * @return integer
     */
    public function getTiempo()
    {
        return $this->tiempo;
    }

    /**
     * Get meses
     *
     * @return integer 
     */
    public function getMeses()
    {
        return $this->meses;
    }

    /**
     * Set meses
     *
     * @param integer $meses
     * @return Experiencia
     */
    public function setMeses($meses)
    {
        $this->meses = $meses;

        return $this;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Experiencia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Experiencia
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;
    
        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getPerfil()
    {
        return $this->perfil;
    }

    public function __clone()
    {
        if($this->getId())
        {
            $this->id = null;
        }
    }

}