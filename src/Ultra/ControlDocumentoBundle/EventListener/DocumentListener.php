<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 21/02/14
 * Time: 04:14 PM
 */

namespace Ultra\ControlDocumentoBundle\EventListener;


use Doctrine\ORM\Event\PostFlushEventArgs;
use Symfony\Component\HttpFoundation\Request;

class DocumentListener {

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        $this->request;//ya puedes usar request
    }

}