<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 24/04/14
 * Time: 11:51 AM
 */

namespace Ultra\ControlDocumentoBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile;
use Doctrine\ORM\Event\OnFlushEventArgs;

class AbstractUltraFileListener {

    public function prePersist(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        if ($entity instanceof AbstractUltraFile) {
            if($entity->getFile() !== null)
            {
                $entity->preUpload();
            }
        }

    }

    public function preUpdate(PreUpdateEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        $em = $eventArgs->getEntityManager();
        $changes = $eventArgs->getEntityChangeSet();
        if($entity instanceof AbstractUltraFile )
        {
            if($eventArgs->hasChangedField('name'))
            {
                $entity->setLastUpdate();
                $entity->preUpload();
            }

            $uow = $eventArgs->getEntityManager()->getUnitOfWork();
            $uow->recomputeSingleEntityChangeSet(
                $em->getClassMetadata(get_class($entity)),
                $entity
            );
        }
    }

    public function postPersist(LifecycleEventArgs $eventArgs){
        $entity =$eventArgs->getEntity();
        if($entity instanceof AbstractUltraFile){
            if(null == $entity->getFile()){
                return;
            }

            $entity->upload();
        }
    }

    public function onFlush(OnFlushEventArgs $flushEventArgs){
    }

    public function postUpdate(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if ($entity instanceof AbstractUltraFile) {
            $entity->upload();
        }
    }

    public function postRemove(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        return;
    }

} 