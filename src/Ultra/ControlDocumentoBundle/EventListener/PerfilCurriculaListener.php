<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 26/03/14
 * Time: 04:30 PM
 */

namespace Ultra\ControlDocumentoBundle\EventListener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use Ultra\ControlDocumentoBundle\Entity\PerfilCurricula;

class PerfilCurriculaListener {

    private $removeFile;

    private $basePath = '/var/sad/uploads/evaluacion';

    /*public function prePersist(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        if($entity instanceof PerfilCurricula){
            if($entity->getFiles() !== null){

            }
        }
    }*/

    public function preUpdate(PreUpdateEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        if ($entity instanceof PerfilCurricula) {
            //$change = $eventArgs->getEntityChangeSet();
            if($eventArgs->hasChangedField('pdfEvaluacion')){
                $this->removeFile[] = $this->basePath .'/'.$eventArgs->getOldValue('pdfEvaluacion');
            }

            if($eventArgs->hasChangedField('pdfCertificado')){
                $this->removeFile[] = $this->basePath .'/'. $eventArgs->getOldValue('pdfCertificado');
            }

        }
    }

    public function postPersist(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if ($entity instanceof PerfilCurricula) {

            if($entity->getFiles() !== null){
                foreach($entity->getFiles() as $file){
                    $file->getFile()->move(
                        $this->basePath,
                        $file->getFile()->getName()
                    );
                }
            }
        }
    }

    public function onFlush(OnFlushEventArgs $flushEventArgs){
        $em = $flushEventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {

        }
    }

    public function postUpdate(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if ($entity instanceof PerfilCurricula) {
            if(count($this->removeFile) > 0){
                $fs = new Filesystem();
                $fs->remove($this->removeFile);
            }

            if($entity->getFiles() !== null){
                foreach($entity->getFiles() as $file){
                    $file->getFile()->move(
                        $this->basePath,
                        $file->getName()
                    );
                }
            }
        }

    }
} 