<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 18/02/14
 * Time: 12:25 PM
 */

namespace Ultra\ControlDocumentoBundle\EventListener;

use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Ultra\ControlDocumentoBundle\Entity\Perfil;

class PerfilListener {
    /*public function prePersist(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        if ($entity instanceof Perfil) {
            if(null !== $entity->getFile() && null !== $entity->getPdf()){
                $pathName = $entity->getClave().'-r'.$entity->getRevision().'.'.$entity->getFile()->guessExtension();
                $hash = sha1_file($entity->getFile()->getPathname());
                $entity->setPdf($pathName);
                $entity->setHash($hash);
            }
        }

    }

    public function postPersist(LifecycleEventArgs $eventArgs){
        $entity =$eventArgs->getEntity();
        if($entity instanceof Perfil){
            if(null == $entity->getFile()){
                return;
            }

            $entity->getFile()->move(
                $entity->getUploadRootDir(),
                $entity->getPdf()
            );
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        if($eventArgs->getEntity() instanceof Perfil)
        {
            $em = $eventArgs->getEntityManager();
            $uow = $em->getUnitOfWork();

            if($eventArgs->hasChangedField('hash'))
            {
                $entity = $eventArgs->getEntity();
                $pathName = $entity->getClave().'-r'.$entity->getRevision().'.'.$entity->getFile()->guessExtension();
                $entity->setPdf($pathName);
                $hash = sha1_file($entity->getFile()->getPathname());
                $entity->setHash($hash);
                $entity->setUpdated();
                $uow->persist($entity);
                $classMetaData = $em->getClassMetadata(get_class($entity));
                $uow->recomputeSingleEntityChangeSet($classMetaData,$entity);
            }
        }
    }

    public function onFlush(OnFlushEventArgs $flushEventArgs){
        $em = $flushEventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Perfil) {

                if(null !== $entity->getFile()){

                    $originalData = $uow->getOriginalEntityData($entity);

                    $hash = sha1_file($entity->getFile()->getPathname());

                    if($hash !== $originalData['hash']){

                        $pathName = $entity->getClave().'-r'.$entity->getRevision().'.'.$entity->getFile()->guessExtension();
                        $entity->setPdf($pathName);
                        $entity->setHash($hash);
                        $entity->setUpdated();
                        $uow->persist($entity);
                        $classMetaData = $em->getClassMetadata(get_class($entity));
                        $uow->recomputeSingleEntityChangeSet($classMetaData,$entity);
                    }

                }
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if ($entity instanceof Perfil) {
            $tmp = $entity->getTemp();
            if(isset($tmp)){
                unlink($tmp);
            }

            if(null !== $entity->getFile()){
                $entity->getFile()->move(
                    $entity->getUploadRootDir(),
                    $entity->getPdf()
                );
            }
        }
    }*/
} 