<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 25/03/14
 * Time: 04:25 PM
 */
//|| $entity instanceof CursoInterno || $entity instanceof Competencia
namespace Ultra\ControlDocumentoBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Filesystem\Filesystem;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Ultra\ControlDocumentoBundle\Entity\CursoInterno;
use Ultra\ControlDocumentoBundle\Entity\Competencia;


class CursosCurriculaListener {

    private $manager;

    private $basePath;

    private $remove;

    private $move;

    public function __construct(){
        $this->manager = null;
        $this->basePath = '/var/sad/uploads/curriculas';
        $this->move = array();
    }

    public function prePersist(LifecycleEventArgs $eventArgs){

        $entity = $eventArgs->getEntity();
        if( $entity instanceof CursoExterno || $entity instanceof CursoInterno || $entity instanceof Competencia){
            //$entity->setPath($entity->getCurricula()->getCurp());

            if(null !== $entity->getFile()){
                $entity->getFile()->getFile()->move(
                    $this->basePath.'/'.$entity->getPath().'/'.$this->getClassName($entity),
                    $entity->getFile()->getName()
                );
            }
        }

    }

    public function preRemove(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if($entity instanceof CursoExterno || $entity instanceof CursoInterno || $entity instanceof Competencia ){
            if(null !== $entity->getPdfCertificado()){
                $fs = new Filesystem();
                $fs->remove(
                    $this->basePath .'/'. $entity->getpath() .'/'.$this->getClassName($entity).'/'.$entity->getPdfCertificado()
                );
            }

        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs){
        $em = $eventArgs->getEntityManager();
        $entity = $eventArgs->getEntity();

        if($entity instanceof CursoExterno || $entity instanceof CursoInterno || $entity instanceof Competencia){

            if($eventArgs->hasChangedField('pdfCertificado')){
                $this->remove = $this->basePath
                                    .'/'.$entity->getPath()
                                    .'/'.$this->getClassName($entity)
                                    .'/'.$eventArgs->getOldValue('pdfCertificado');
            }
        }
    }

    /*public function preFlush(PreFlushEventArgs $preFlushEventArgs){
        $em = $preFlushEventArgs->getEntityManager();

    }

    public function onFlush(OnFlushEventArgs $onFlushEventArgs){

    }*/

    /*public function postPersist(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();
        if($entity instanceof CursoExterno){
            if(null !== $entity->getFile()->getFile()){
                $entity->getFile()->getFile()->move(
                    $this->basePath.'/'.$entity->getPath().'/'.$this->getClassName($entity),
                    $entity->getFile()->getName()
                );
            }
        }

    }*/

    public function postUpdate(LifecycleEventArgs $eventArgs){
        $entity = $eventArgs->getEntity();

        if($entity instanceof CursoExterno || $entity instanceof CursoInterno || $entity instanceof Competencia ){
            if(null !== $this->remove){
                $fs = new Filesystem();
                $fs->remove($this->remove);
            }

            if(null !== $entity->getFile()->getFile()){
                $entity->getFile()->getFile()->move(
                    $this->basePath.'/'.$entity->getPath().'/'.$this->getClassName($entity),
                    $entity->getFile()->getName()
                );
            }
        }
    }

    /*public function postRemove(LifecycleEventArgs $eventArgs){

    }*/

    protected function getClassName($entity){
        $class = explode('\\', get_class($entity));
        return end($class);
    }

} 