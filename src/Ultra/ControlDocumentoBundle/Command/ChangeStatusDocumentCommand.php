<?php
namespace Ultra\ControlDocumentoBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangeStatusDocumentCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this->setName('ultra:document:expired')
             ->getDescription('Change status document');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        $documents = $em->getRepository('ControlDocumentoBundle:Documento')->findExpired();

        $newDocumentStatus = $em->getRepository('ControlDocumentoBundle:Condicion')->find(5);

        foreach($documents as $document)
        {
            $document->setCondicion($newDocumentStatus);
        }

        $em->flush();

        //$outputInterface->writeln(count($documents));

    }

}