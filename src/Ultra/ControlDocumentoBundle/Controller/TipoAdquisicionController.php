<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\TipoAdquisicion;
use Ultra\ControlDocumentoBundle\Form\TipoAdquisicionType;

/**
 * TipoAdquisicion controller.
 *
 * @Route("/adquisicion")
 */
class TipoAdquisicionController extends Controller
{

    /**
     * Lists all TipoAdquisicion entities.
     *
     * @Route("/", name="adquisicion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:TipoAdquisicion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoAdquisicion entity.
     *
     * @Route("/", name="adquisicion_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:TipoAdquisicion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoAdquisicion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('adquisicion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a TipoAdquisicion entity.
    *
    * @param TipoAdquisicion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoAdquisicion $entity)
    {
        $form = $this->createForm(new TipoAdquisicionType(), $entity, array(
            'action' => $this->generateUrl('adquisicion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoAdquisicion entity.
     *
     * @Route("/new", name="adquisicion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoAdquisicion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoAdquisicion entity.
     *
     * @Route("/{id}", name="adquisicion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoAdquisicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAdquisicion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoAdquisicion entity.
     *
     * @Route("/{id}/edit", name="adquisicion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoAdquisicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAdquisicion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoAdquisicion entity.
    *
    * @param TipoAdquisicion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoAdquisicion $entity)
    {
        $form = $this->createForm(new TipoAdquisicionType(), $entity, array(
            'action' => $this->generateUrl('adquisicion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoAdquisicion entity.
     *
     * @Route("/{id}", name="adquisicion_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:TipoAdquisicion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoAdquisicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAdquisicion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('adquisicion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoAdquisicion entity.
     *
     * @Route("/{id}", name="adquisicion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:TipoAdquisicion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoAdquisicion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('adquisicion'));
    }

    /**
     * Creates a form to delete a TipoAdquisicion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adquisicion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

}
