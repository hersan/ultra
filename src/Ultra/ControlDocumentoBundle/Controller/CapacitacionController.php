<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Capacitacion;
use Ultra\ControlDocumentoBundle\Form\CapacitacionType;

/**
 * Capacitacion controller.
 *
 * @Route("/capacitacion")
 */
class CapacitacionController extends Controller
{

    /**
     * Lists all Capacitacion entities.
     *
     * @Route("/", name="capacitacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Capacitacion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Capacitacion entity.
     *
     * @Route("/", name="capacitacion_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Capacitacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Capacitacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('capacitacion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Capacitacion entity.
    *
    * @param Capacitacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Capacitacion $entity)
    {
        $form = $this->createForm(new CapacitacionType(), $entity, array(
            'action' => $this->generateUrl('capacitacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Capacitacion entity.
     *
     * @Route("/new", name="capacitacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Capacitacion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Capacitacion entity.
     *
     * @Route("/{id}", name="capacitacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Capacitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capacitacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Capacitacion entity.
     *
     * @Route("/{id}/edit", name="capacitacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Capacitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capacitacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Capacitacion entity.
    *
    * @param Capacitacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Capacitacion $entity)
    {
        $form = $this->createForm(new CapacitacionType(), $entity, array(
            'action' => $this->generateUrl('capacitacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Capacitacion entity.
     *
     * @Route("/{id}", name="capacitacion_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Capacitacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Capacitacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Capacitacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('capacitacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Capacitacion entity.
     *
     * @Route("/{id}", name="capacitacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Capacitacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Capacitacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('capacitacion'));
    }

    /**
     * Creates a form to delete a Capacitacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('capacitacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
