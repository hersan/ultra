<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\EtapaProyecto;
use Ultra\ControlDocumentoBundle\Form\EtapaProyectoType;

/**
 * EtapaProyecto controller.
 *
 * @Route("/etapa_proyecto")
 */
class EtapaProyectoController extends Controller
{

    /**
     * Lists all EtapaProyecto entities.
     *
     * @Route("/", name="etapa_proyecto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:EtapaProyecto')->findAll();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        //return $this->render('ControlDocumentoBundle:EtapaProyecto:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new EtapaProyecto entity.
     *
     * @Route("/", name="etapa_proyecto_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:EtapaProyecto:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new EtapaProyecto();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La etapa de proyecto fue agregada con éxito con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('etapa_proyecto_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La etapa de proyecto no pudo ser agregada.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a EtapaProyecto entity.
    *
    * @param EtapaProyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(EtapaProyecto $entity)
    {
        $form = $this->createForm(new EtapaProyectoType(), $entity, array(
            'action' => $this->generateUrl('etapa_proyecto_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new EtapaProyecto entity.
     *
     * @Route("/new", name="etapa_proyecto_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new EtapaProyecto();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a EtapaProyecto entity.
     *
     * @Route("/{id}", name="etapa_proyecto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EtapaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la etapa del proyecto.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EtapaProyecto entity.
     *
     * @Route("/{id}/edit", name="etapa_proyecto_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EtapaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la etapa del proyecto.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a EtapaProyecto entity.
    *
    * @param EtapaProyecto $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EtapaProyecto $entity)
    {
        $form = $this->createForm(new EtapaProyectoType(), $entity, array(
            'action' => $this->generateUrl('etapa_proyecto_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing EtapaProyecto entity.
     *
     * @Route("/{id}", name="etapa_proyecto_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:EtapaProyecto:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EtapaProyecto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la etapa del proyecto.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La etapa del proyecto fue actualizada con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('etapa_proyecto_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La etapa del proyecto no fue actualizada.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a EtapaProyecto entity.
     *
     * @Route("/{id}", name="etapa_proyecto_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:EtapaProyecto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la etapa del Proyecto.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La etapa de proyecto fue eliminada con éxito.'
            );
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La etapa de proyecto no pudo eliminarse.'
            );
        }

        return $this->redirect($this->generateUrl('etapa_proyecto'));
    }

    /**
     * Creates a form to delete a EtapaProyecto entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etapa_proyecto_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                'label' => 'Borrar',
                'attr'=>array('class'=>'btn btn-warning',
                'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
                ))
            ->getForm()
        ;
    }
}
