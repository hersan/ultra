<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {

        /*$this->get('logger')->addAlert('Loging:',array(
            'user' => $this->getUser()->getUsername(),
            'datetime' => (new \DateTime())->format('Y-m-d H:i:s'),
        ));

        if (true === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirect($this->generateUrl('superadmin_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_GERENCIA')) {
            return $this->redirect($this->generateUrl('gerencia_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_JEFE_ADMON')) {
            return $this->redirect($this->generateUrl('jadmin_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('adm_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_JEFE_CALIDAD')) {
            return $this->redirect($this->generateUrl('jcalidad_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_CONTROL_DOC')) {
            return $this->redirect($this->generateUrl('cdoc_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_USER')) {
            return $this->redirect($this->generateUrl('gral_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_RH')) {
            return $this->redirect($this->generateUrl('rhum_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_CAPT')) {
            return $this->redirect($this->generateUrl('captura_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_LICITACION')) {
            return $this->redirect($this->generateUrl('licitacion_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_JEFE_PROY')) {
            return $this->redirect($this->generateUrl('jefe_proy_index'));
        }
        if (true === $this->get('security.context')->isGranted('ROLE_SISTEMAS')) {
            return $this->redirect($this->generateUrl('sistemas_index'));
        }*/


        //ladybug_dump_die($this->getUser());
        return array(

        );
    }
}
