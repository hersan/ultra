<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Documento;
use Ultra\ControlDocumentoBundle\Form\DocumentoType;
use Symfony\Component\HttpFoundation\Response;
use Ultra\ControlDocumentoBundle\Form\NuevaRevisionDocumentoType;

/**
 * Documento controller.
 *
 * @Route("/procedimiento", name="_procedimiento")
 */
class ProcedimientoController extends Controller
{

    /**
     * Lists all Documento entities.
     *
     * @Route("/", name="procedimiento")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Documento')->load();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Documentos entity.
     *
     * @Route("/procedimientospdf", name="procedimientos_pdf")
     * @Method("GET")
     * @Template()
     */
    public function procedimientospdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Documento')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Procedimiento:procedimientospdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Áreas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="listadeprocedimientos.pdf"',
            )
        );

        return $response;

    }

    /**
     * Lists all Documento entities.
     *
     * @Route("/list", name="procedimiento_lista")
     * @Template("ControlDocumentoBundle:Procedimiento:index.html.twig")
     */
    public function listAction(Request $request)
    {

        $clave = $request->request->get('clave');
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('ControlDocumentoBundle:Documento')->findDocsByClave($clave);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);

        return $this->render('ControlDocumentoBundle:Procedimiento:index.html.twig', array('pagination' => $pagination));

    }

    /**
     * Creates a new Documento entity.
     *
     * @Route("/", name="procedimiento_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Procedimiento:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Documento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $fechaaprobado=$entity->getAprobado();
        $fechavigencia=$entity->getVigencia();

        if ($fechaaprobado<$fechavigencia)
        {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Se agregó el procedimiento con los siguientes datos:'
                );

                return $this->redirect($this->generateUrl('procedimiento_show', array('id' => $entity->getId())));
            }
            else
            {
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    'No se pudo agregar el procedimiento.'
                );
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La fecha de aprobación es mayor que la vigencia, no se agregó el procedimiento.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Documento entity.
    *
    * @param Documento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Documento $entity)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $form = $this->createForm(new DocumentoType($em), $entity, array(
            'action' => $this->generateUrl('procedimiento_create'),
            'method' => 'POST',
            'type_form' => 'new',
        ));

         $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
        
    }

    /**
     * Displays a form to create a new Documento entity.
     *
     * @Route("/new", name="procedimiento_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Documento();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Documento entity.
     *
     * @Route("/{id}", name="procedimiento_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Documento')->loadById($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se encuentran documentos.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Documento entity.
     *
     * @Route("/{id}/edit", name="procedimiento_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar el documento.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Documento entity.
    *
    * @param Documento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Documento $entity)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $form = $this->createForm(new DocumentoType($em), $entity, array(
            'action' => $this->generateUrl('procedimiento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'type_form' => 'edit',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr'=>array('class'=>'btn btn-success',
                'title'=>'Editar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        $form->add('move', 'submit', array(
                'disabled' => false,
                'label' => 'Cambiar Disciplina',
                'attr' => array('class' => 'btn btn-default',
                'title'=>'Cambiar Disciplina')
            ));

        return $form;
    }
    /**
     * Edits an existing Documento entity.
     *
     * @Route("/{id}", name="procedimiento_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Procedimiento:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var \Ultra\ControlDocumentoBundle\Entity\Documento $entity
         */
        $entity = $em->getRepository('ControlDocumentoBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Documento entity.');
        }

        $currentPath = $entity->getAbsolutePath();
        $estado = $entity->getCondicion();

        $currentFormats = new \Doctrine\Common\Collections\ArrayCollection();
        foreach($entity->getFormats() as $format)
        {
            $currentFormats->add($format);
        }

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        //ladybug_dump_die($currentPath);

        $fechaaprobado=$entity->getAprobado();
        $fechavigencia=$entity->getVigencia();

        if ($fechaaprobado<$fechavigencia)
        {
            if ($editForm->isValid()) {

               if($editForm->get('move')->isClicked())
               {
                   $entity->temporaryPath($currentPath);
               }

                $entity->move($currentPath, $estado);

                $entity->updateFormats($currentFormats);

                if($entity->getRemoveNullFormats() !== null)
                {
                    foreach($entity->getRemoveNullFormats() as $format)
                    {
                        $em->remove($format);
                    }
                }

                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    'Los datos del procedimientos fueron actualizados.'
                );

                return $this->redirect($this->generateUrl('procedimiento_show', array('id' => $id)));
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La fecha de aprobación es mayor que la vigencia. No se actualizó el procedimiento.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
         //   'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Documento entity.
     *
     * @Route("/{id}", name="procedimiento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Documento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Documento entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Se eliminó el procedimiento.'
            );

            return $this->redirect($this->generateUrl('procedimiento'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'No se pudo eliminar el procedimiento.'
            );
        }

        return $this->redirect($this->generateUrl('procedimiento'));
    }

    /**
     * Creates a form to delete a Documento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('procedimiento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }

    /**
     *
     * @Route("/revision/{id}", name="procedimiento_nueva_revision")
     * @Method("GET")
     * @Template()
     */
    public function revisionAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Documento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar el documento.');
        }

        $editForm = $this->createRevisionForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @Route("/revision/nueva/{id}", name="procedimiento_crear_nueva_revision")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Procedimiento:revision_edit.html.twig")
     */
    public function crearRevisionAction(Request $request, $id)
    {

        $currentPath = null;
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $entity \Ultra\ControlDocumentoBundle\Entity\Documento
         */
        $entity = $em->getRepository('ControlDocumentoBundle:Documento')->find($id);
        $obsoleto = $em->getRepository('ControlDocumentoBundle:Condicion')->find(2);

        $old_entity = clone $entity;

        $old_entity->setCondicion($obsoleto);

        $newCursos = new ArrayCollection();
        if(!$entity->getCursos()->isEmpty())
        {

            foreach($entity->getCursos() as $curso)
            {
                $newCurso = clone $curso;
                $newCursos->add($newCurso);
            }
        }

        if (!$entity) {
            throw $this->createNotFoundException('No existe la entidad solicitada.');
        }

        $currentPath = $entity->getAbsolutePath();

        $editForm = $this->createRevisionForm($entity);

        $editForm->handleRequest($request);

        $estado_actual = $entity->getCondicion()->getId();

        if($entity->getCondicion()->getId() != 1){
            $this->get('session')->getFlashBag()->add(
                'warning',
                'El cambio de revisión es solo para documentos vigentes o expirados.'
            );

            return array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                //'delete_form' => $deleteForm->createView(),
            );
        }

        $revisionActual = $old_entity->getRevision();
        if($entity->getRevision() <= $revisionActual){
            $this->get('session')->getFlashBag()->add(
                'warning',
                'La revisión no puede ser menor a la vigente.'
            );

            return array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                //'delete_form' => $deleteForm->createView(),
            );
        }


        if ($editForm->isValid()) {

            $em->getConnection()->beginTransaction();
            try {

                $old_entity->move($currentPath, $entity->getCondicion());

                foreach($newCursos as $curso)
                {
                    $old_entity->addCurso($curso);
                }

                $em->persist($old_entity);

                foreach ($entity->getFormats() as $format) {
                    $format->setDocumento(null);
                    $em->remove($format);
                }

                foreach ($entity->getCursos() as $curso) {
                    $curso->setCursoInterno(null);
                    $em->remove($curso);
                }

                $em->flush();

                $em->getConnection()->commit();
            } catch (\Exception $e) {
                $em->getConnection()->rollback();
                throw $e;
            }

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos del procedimientos fueron actualizados.'
            );

            return $this->redirect($this->generateUrl('procedimiento_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        );
    }

    private function createRevisionForm(Documento $entity)
    {
        $form = $this->createForm(new NuevaRevisionDocumentoType(), $entity, array(
                'action' => $this->generateUrl('procedimiento_crear_nueva_revision', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Cambiar Revisión',
                'attr' => array('class' => 'btn btn-success',
                    'title'=>'Cambiar revisión'),
                'glyphicon' => 'glyphicon glyphicon-log-in'
            ));

        return $form;
    }
}
