<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\NivelAcademico;
use Ultra\ControlDocumentoBundle\Form\NivelAcademicoType;

/**
 * NivelAcademico controller.
 *
 * @Route("/nivelacademico")
 */
class NivelAcademicoController extends Controller
{

    /**
     * Lists all NivelAcademico entities.
     *
     * @Route("/", name="nivelacademico")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:NivelAcademico')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new NivelAcademico entity.
     *
     * @Route("/", name="nivelacademico_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:NivelAcademico:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new NivelAcademico();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('nivelacademico_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a NivelAcademico entity.
    *
    * @param NivelAcademico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(NivelAcademico $entity)
    {
        $form = $this->createForm(new NivelAcademicoType(), $entity, array(
            'action' => $this->generateUrl('nivelacademico_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }

    /**
     * Displays a form to create a new NivelAcademico entity.
     *
     * @Route("/new", name="nivelacademico_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new NivelAcademico();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a NivelAcademico entity.
     *
     * @Route("/{id}", name="nivelacademico_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:NivelAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NivelAcademico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing NivelAcademico entity.
     *
     * @Route("/{id}/edit", name="nivelacademico_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:NivelAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NivelAcademico entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a NivelAcademico entity.
    *
    * @param NivelAcademico $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(NivelAcademico $entity)
    {
        $form = $this->createForm(new NivelAcademicoType(), $entity, array(
            'action' => $this->generateUrl('nivelacademico_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Update',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }
    /**
     * Edits an existing NivelAcademico entity.
     *
     * @Route("/{id}", name="nivelacademico_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:NivelAcademico:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:NivelAcademico')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NivelAcademico entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('nivelacademico_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a NivelAcademico entity.
     *
     * @Route("/{id}", name="nivelacademico_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:NivelAcademico')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NivelAcademico entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('nivelacademico'));
    }

    /**
     * Creates a form to delete a NivelAcademico entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nivelacademico_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                    'label' => 'Delete',
                    'attr' => array('class' => 'btn btn-default')
                ))
            ->getForm()
        ;
    }
}
