<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ultra\ControlDocumentoBundle\Entity\GeneralDocument;
use Ultra\ControlDocumentoBundle\Form\GeneralDocumentType;

/**
 * GeneralDocument controller.
 *
 * @Route("/documento/general")
 */
class GeneralDocumentController extends Controller
{

    /**
     * Lists all GeneralDocument entities.
     *
     * @Route("/", name="documento_general")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $entities = $em->getRepository('ControlDocumentoBundle:GeneralDocument')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new GeneralDocument entity.
     *
     * @Route("/", name="documento_general_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:GeneralDocument:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new GeneralDocument();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'El documento se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('documento_general_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a GeneralDocument entity.
     *
     * @param GeneralDocument $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GeneralDocument $entity)
    {
        $form = $this->createForm(new GeneralDocumentType(), $entity, array(
            'action' => $this->generateUrl('documento_general_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));


        return $form;
    }

    /**
     * Displays a form to create a new GeneralDocument entity.
     *
     * @Route("/new", name="documento_general_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new GeneralDocument();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a GeneralDocument entity.
     *
     * @Route("/{id}", name="documento_general_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:GeneralDocument')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar documentos.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a GeneralDocument entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('documento_general_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing GeneralDocument entity.
     *
     * @Route("/{id}/edit", name="documento_general_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:GeneralDocument')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar documento.');
        }

        $editForm = $this->createEditForm($entity);


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
    * Creates a form to edit a GeneralDocument entity.
    *
    * @param GeneralDocument $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(GeneralDocument $entity)
    {
        $form = $this->createForm(new GeneralDocumentType(), $entity, array(
            'action' => $this->generateUrl('documento_general_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));
        return $form;
    }

    /**
     * Edits an existing GeneralDocument entity.
     *
     * @Route("/{id}", name="documento_general_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:GeneralDocument:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:GeneralDocument')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GeneralDocument entity.');
        }


        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos del documento fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('documento_general_edit', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El documento no pudo ser modificado.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );
    }

    /**
     * Deletes a GeneralDocument entity.
     *
     * @Route("/{id}", name="documento_general_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:GeneralDocument')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find GeneralDocument entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('documento_general'));
    }
}
