<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Ultra\ControlDocumentoBundle\Entity\Perfil;
use Ultra\ControlDocumentoBundle\Form\NuevaRevisionPerfilType;
use Ultra\ControlDocumentoBundle\Form\PerfilType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;

/**
 * Perfil controller.
 *
 * @Route("/perfil")
 */
class PerfilController extends Controller
{

    /**
     * Lists all Perfil entities.
     *
     * @Route("/index", name="perfil")
     * Cache(expires="")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$entities = $em->getRepository('ControlDocumentoBundle:Perfil')->findByEstado(1);
        $entities = $em->getRepository('ControlDocumentoBundle:Perfil')->loadAll();
       // $paginator  = $this->get('knp_paginator');
       // $pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
       // return $this->render('ControlDocumentoBundle:Perfil:index.html.twig', array('pagination' => $pagination));

       return array(
            'entities' => $entities,
        );

       }

    /**
     * @Route("/perfilespdf/", name="perfiles_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Perfil:perfilespdf.html.twig")
     */
    public function perfilesPdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Perfil:perfilespdf.html.twig',
            array(
                'entities' => $entity));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Perfil',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="perfilespdf.pdf"',
            )
        );

        return $response;
    }


    /**
     * @Route("/perfilpdf/{id}", name="p_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Perfil:perfilpdf.html.twig")
     */
    public function perfilPdfAction( $id )
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $html = $this->renderView('ControlDocumentoBundle:Perfil:perfilpdf.html.twig',
            array(
                'entity' => $entity));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Perfil',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="perfilpdf.pdf"',
            )
        );

        return $response;
    }

    /**
     * Creates a new Perfil entity.
     *
     * @Route("/create", name="perfil_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Perfil:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Perfil();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        //Hago un select para traer el objeto perfil y pasarlo al tipo de documento
        $em = $this->getDoctrine()->getManager();
        $tipo = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find(3);

        $entity->setTipoDocumento($tipo);


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El perfil fue agregado con éxito.'
            );
            return $this->redirect($this->generateUrl('perfil_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El perfil no fue agregado.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Perfil entity.
    *
    * @param Perfil $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Perfil $entity)
    {
        $form = $this->createForm('ultra_perfil_type', $entity, array(
            'action' => $this->generateUrl('perfil_create'),
            'method' => 'POST',
        ));

        
        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Perfil entity.
     *
     * @Route("/new", name="perfil_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Perfil();

        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }


     /**
      * Finds and displays a Perfil entity.
      *
      * @Route("/{id}", name="perfil_show")
      * @Method("GET")
      * @Template()
      */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Perfil entity.
     *
     * @Route("/{id}/edit", name="perfil_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        //$entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);

        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->findByAllRelationship($id);


        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Perfil entity.
    *
    * @param Perfil $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Perfil $entity)
    {
        $form = $this->createForm('ultra_perfil_type', $entity, array(
            'action' => $this->generateUrl('perfil_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));



        return $form;
    }

    /**
     * Edits an existing Perfil entity.
     *
     * @Route("/{id}", name="perfil_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Perfil:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        /**
         * @var $em \Doctrine\Common\Persistence\ObjectManager
         */
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $entity \Ultra\ControlDocumentoBundle\Entity\Perfil
         */
        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->findByAllRelationship($id);

        //Hago un select para traer el objeto perfil y pasarlo al tipo de documento
        //$em = $this->getDoctrine()->getManager();
        $tipo = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find(3);

        $entity->setTipoDocumento($tipo);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $currentTraining = new ArrayCollection();
        foreach($entity->getCapacitacion() as $training){
            $currentTraining->add($training);
        }

        $currentSkills = new ArrayCollection();
        foreach($entity->getHabilidades() as $skill){
            $currentSkills->add($skill);
        }

        $currentNotes = new ArrayCollection();
        foreach($entity->getNotas() as $note){
            $currentNotes->add($note);
        }

        $currentExpertise = new ArrayCollection();
        foreach($entity->getExpertise() as $expertise){
            $currentExpertise->add($expertise);
        }

        $currentProfession = new ArrayCollection();
        foreach($entity->getEscolaridad() as $profession){
            $currentProfession->add($profession);
        }

        $currentTasks = new ArrayCollection();
        foreach($entity->getActividades() as $task){
            $currentTasks->add($task);
        }

        $currentKnowledge = new ArrayCollection();
        foreach($entity->getConocimientoEspecial() as $knowledge){
            $currentKnowledge->add($knowledge);
        }

        //ladybug_dump($currentSkills);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            foreach($currentTraining as $training){
                if(false === $entity->getCapacitacion()->contains($training)){
                    $training->setPerfil(null);
                    //$em->persist($training);
                    $em->remove($training);
                }
            }
            //ladybug_dump_die($currentTraining);
            foreach($currentSkills as $skill){
                if(false === $entity->getHabilidades()->contains($skill)){
                    $skill->setPerfil(null);
                    $em->remove($skill);
                }
            }

            foreach($currentKnowledge as $knowledge){
                if(false === $entity->getConocimientoEspecial()->contains($knowledge)){
                    $knowledge->setPerfil(null);
                    $em->remove($knowledge);
                }
            }

            foreach($currentTasks as $task){
                if(false === $entity->getActividades()->contains($task)){
                    $task->setPerfil(null);
                    //$em->persist($training);
                    $em->remove($task);
                }
            }

            foreach($currentProfession as $profession){
                if(false === $entity->getEscolaridad()->contains($profession)){
                    $profession->setPerfil(null);
                    //$em->persist($training);
                    $em->remove($profession);
                }
            }

            foreach($currentExpertise as $expertise){
                if(false === $entity->getExpertise()->contains($expertise)){
                    $expertise->setPerfil(null);
                    //$em->persist($training);
                    $em->remove($expertise);
                }
            }

            foreach($currentNotes as $note){
                //ladybug_dump($entity->getNotas());
                if(false === $entity->getNotas()->contains($note)){
                    $note->setPerfil(null);
                    //$em->persist($training);
                    $em->remove($note);
                }
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El perfil fue actualizado con éxito.'
            );

            return $this->redirect($this->generateUrl('perfil_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El perfil no pudo ser actualizado.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Perfil entity.
     *
     * @Route("/{id}", name="perfil_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Perfil entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El perfil fue eliminado con éxito.'
            );

        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El perfil no pudo ser eliminado.'
            );
        }

        return $this->redirect($this->generateUrl('perfil'));
    }

    /**
     * Creates a form to delete a Perfil entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('perfil_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array('label' => 'Borrar',
                'attr' => array('class' => 'btn btn-warning',
                'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
                 ))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing Perfil entity.
     *
     * @Route("/revision/{id}/new", name="perfil_nueva_revision")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Perfil:edit_revision.html.twig")
     */
    public function newRevisionAction($id){
        $em = $this->getDoctrine()->getManager();
        //$entity = $em->getRepository('ControlDocumentoBundle:Perfil')->findByAllRelationship($id);
        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $editForm = $this->createRevisionForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @internal param $id
     *
     * @Route("/revision/{id}", name="perfil_crear_revision")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Perfil:edit_revision.html.twig")
     */
    public function createRevisionAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Perfil')->findByAllRelationship($id);
        //$entity = $em->getRepository('ControlDocumentoBundle:Perfil')->find($id);
        $newEntity = new Perfil();

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }

        $cloneEntity = clone $entity;

        //$em->detach($entity);

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createRevisionForm($entity);
        $editForm->handleRequest($request);

        $newEntity->setRevisionUad($entity->getRevisionUad());
        $newEntity->setFechaUad($entity->getFechaUad());
        $newEntity->setClave($entity->getClave());
        $newEntity->setRevision($entity->getRevision());
        $newEntity->setTitulo($entity->getTitulo());
        $newEntity->setTituloCorto($entity->getTituloCorto());
        $newEntity->setAprobado($entity->getAprobado());
        $newEntity->setVigencia($entity->getVigencia());
        $newEntity->setLiberado($entity->getLiberado());
        $newEntity->setTransmitido($entity->getTransmitido());
        //$newEntity->setPdf($entity->getPdf());
        //$newEntity->setEstado($entity->getEstado());
        $newEntity->setEstado(
            $em->getRepository('ControlDocumentoBundle:Condicion')->find(1)
        );
        //$newEntity->setPdf($cloneEntity->getPdf());
        $newEntity->setTipoDocumento($entity->getTipoDocumento());
        $newEntity->setFile($entity->getFile());

        /*foreach($entity->getPerfiles() as $perfil){
            $newEntity->addPerfile(clone $perfil);
        }*/

        foreach($entity->getDisciplinas() as $disciplina){
            $newEntity->addDisciplina($disciplina);
        }

        foreach($entity->getActividades() as $actividad){
            $newEntity->addActividade(clone $actividad);
        }

        foreach($entity->getEscolaridad() as $actividad){
            $newEntity->addEscolaridad(clone $actividad);
        }

        foreach($entity->getExpertise() as $actividad){
            $newEntity->addExpertise(clone $actividad);
        }

        foreach($entity->getCapacitacion() as $actividad){
            $newEntity->addCapacitacion(clone $actividad);
        }

        foreach($entity->getHabilidades() as $actividad){
            $newEntity->addHabilidade(clone $actividad);
        }

        foreach($entity->getConocimientoEspecial() as $actividad){
            $newEntity->addConocimientoEspecial(clone $actividad);
        }

        foreach($entity->getNotas() as $actividad){
            $newEntity->addNota(clone $actividad);
        }


        if ($editForm->isValid()) {

            $em->persist($newEntity);

            $entity->setFechaUad($cloneEntity->getFechaUad());
            $entity->setRevisionUad($cloneEntity->getRevisionUad());
            $entity->setTitulo($cloneEntity->getTitulo());
            $entity->setTituloCorto($cloneEntity->getTituloCorto());
            $entity->setRevision($cloneEntity->getRevision());
            $entity->setAprobado($cloneEntity->getAprobado());
            $entity->setVigencia($cloneEntity->getVigencia());
            $entity->setLiberado($cloneEntity->getLiberado());
            $entity->setTransmitido($cloneEntity->getTransmitido());

            $entity->setFile();
            $entity->setPdf($cloneEntity->getPdf());
            $entity->setEstado(
                $em->getRepository('ControlDocumentoBundle:Condicion')->find(2)
            );
            $entity->move($cloneEntity->getAbsolutePath(), $cloneEntity->getEstado());

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El perfil fue actualizado con éxito.
                 Edite el nuevo perfil con los cambios requeridos'
            );

            return $this->redirect($this->generateUrl('perfil_show', array('id' => $newEntity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El perfil no pudo ser actualizado.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Perfil entity.
     *
     * @param Perfil $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createRevisionForm(Perfil $entity)
    {
        $form = $this->createForm(new NuevaRevisionPerfilType(), $entity, array(
                'action' => $this->generateUrl('perfil_crear_revision', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                    'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));



        return $form;
    }

}