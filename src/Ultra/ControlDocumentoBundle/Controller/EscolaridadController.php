<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Escolaridad;
use Ultra\ControlDocumentoBundle\Form\EscolaridadType;

/**
 * Escolaridad controller.
 *
 * @Route("/escolaridad")
 */
class EscolaridadController extends Controller
{

    /**
     * Lists all Escolaridad entities.
     *
     * @Route("/", name="escolaridad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Escolaridad')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Escolaridad entity.
     *
     * @Route("/", name="escolaridad_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Escolaridad:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Escolaridad();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('escolaridad_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Escolaridad entity.
    *
    * @param Escolaridad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Escolaridad $entity)
    {
        $form = $this->createForm(new EscolaridadType(), $entity, array(
            'action' => $this->generateUrl('escolaridad_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Escolaridad entity.
     *
     * @Route("/new", name="escolaridad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Escolaridad();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Escolaridad entity.
     *
     * @Route("/{id}", name="escolaridad_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Escolaridad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escolaridad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Escolaridad entity.
     *
     * @Route("/{id}/edit", name="escolaridad_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Escolaridad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escolaridad entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Escolaridad entity.
    *
    * @param Escolaridad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Escolaridad $entity)
    {
        $form = $this->createForm(new EscolaridadType(), $entity, array(
            'action' => $this->generateUrl('escolaridad_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Escolaridad entity.
     *
     * @Route("/{id}", name="escolaridad_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Escolaridad:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Escolaridad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escolaridad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('escolaridad_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Escolaridad entity.
     *
     * @Route("/{id}", name="escolaridad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Escolaridad')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Escolaridad entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('escolaridad'));
    }

    /**
     * Creates a form to delete a Escolaridad entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('escolaridad_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
