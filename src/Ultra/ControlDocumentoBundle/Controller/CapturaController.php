<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class CapturaController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/Captura", name="captura")
 */
class CapturaController extends Controller
{
    /**
     * @Route("/index", name="captura_index")
     * @Template("ControlDocumentoBundle:Captura:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH c.id = 1 AND d.vigencia< :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        return array(
            'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite
        );
    }
}
