<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Trainer;
use Ultra\ControlDocumentoBundle\Form\TrainerType;

/**
 * Trainer controller.
 *
 * @Route("/trainers")
 */
class TrainerController extends Controller
{

    /**
     * Lists all Trainer entities.
     *
     * @Route("/", name="trainers")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Trainer')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Trainer entity.
     *
     * @Route("/", name="trainers_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Trainer:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Trainer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El instructor fue creado'
            );

            return $this->redirect($this->generateUrl('trainers_show', array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El instructor no fue creado.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Trainer entity.
     *
     * @param Trainer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Trainer $entity)
    {
        $form = $this->createForm(new TrainerType(), $entity, array(
            'action' => $this->generateUrl('trainers_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Trainer entity.
     *
     * @Route("/new", name="trainers_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Trainer();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Trainer entity.
     *
     * @Route("/{id}", name="trainers_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Trainer entity.
     *
     * @Route("/{id}/edit", name="trainers_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Trainer entity.
    *
    * @param Trainer $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Trainer $entity)
    {
        $form = $this->createForm(new TrainerType(), $entity, array(
            'action' => $this->generateUrl('trainers_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Editar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Editar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit',
        ));

        return $form;
    }
    /**
     * Edits an existing Trainer entity.
     *
     * @Route("/{id}", name="trainers_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Trainer:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El instructor fue editado.'
            );

            return $this->redirect($this->generateUrl('trainers_edit', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El instructor no fue editado.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Trainer entity.
     *
     * @Route("/{id}", name="trainers_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Trainer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Trainer entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El Instructor fue borrado.'
            );
        }

        return $this->redirect($this->generateUrl('trainers'));
    }

    /**
     * Creates a form to delete a Trainer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trainers_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array('label' => 'Borrar',
                'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }
}
