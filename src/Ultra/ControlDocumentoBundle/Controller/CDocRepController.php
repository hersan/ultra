<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 12/05/14
 * Time: 09:50 AM
 */

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Form\CursoSoloType;
use Ultra\ControlDocumentoBundle\Form\VigenciaProdType;
use Ultra\ControlDocumentoBundle\Form\VigenciaType;


/**
 * Curricula ReportesCDoc controller.
 *
 * @Route("/cdocrep")
 */
class CDocRepController extends Controller
{

    /**
     * @Route("/{pdf}/concertificado", name="con_certificado")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:concertificado.html.twig")
     */
    public function concertificadoAction($pdf){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        if ($pdf==1)
        {
        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,p.clave clave, p.titulo tit, p.revision rev,c.id cid,c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl, ev.id idpdf
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc JOIN pc.evaluaciones ev JOIN pc.perfil p
              WHERE pc.fechaBaja >:hoy AND ev INSTANCE OF ControlDocumentoBundle:PdfCertificado"
        )->setParameter('hoy', $hoy->format('Y-m-d') );

        }
        else
        {
            $query = $em->createQuery(
                "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,p.clave clave, p.titulo tit, p.revision rev,c.id cid,c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl, ev.id idpdf
                  FROM ControlDocumentoBundle:Curricula c
                  JOIN c.curriculas pc JOIN pc.evaluaciones ev JOIN pc.perfil p
                  WHERE pc.fechaBaja >:hoy AND ev INSTANCE OF ControlDocumentoBundle:PdfCalificacion"
            )->setParameter('hoy', $hoy->format('Y-m-d') );
        }

        $entities = $query->getResult();

        //ladybug_dump_die($entities);
        //ladybug_dump_die($entities);
        return array(
            'entities' => $entities,
            'pdf'=>$pdf
        );
    }


    /**
     * @Route("/calificado", name="cdoc_calificado")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:calificado.html.twig")
     */
    public function calificadoAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        //Consulto todas las relaciones Perfil-curricula vigentes
        $qtodos = $em->createQuery(
            "SELECT pc.id,p.id pid,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc
             FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
             WHERE pc.fechaBaja>:hoy
              ")->setParameter('hoy', $hoy->format('Y-m-d') );

        $todos = $qtodos->getResult();

        //Para guardar los datos de las personas
        $personas = Array ();

        foreach ($todos as $pc)
        {

            $cid=$pc['cid'];
            $pid=$pc['pid'];
            $nom=$pc['nom'];
            $app=$pc['app'];
            $apm=$pc['apm'];
            $nc=$pc['nc'];

            //Consulta para saber si le faltan cursos externos
            $query = $em->createQuery(
                "SELECT count(cur.id) faltan
                 FROM ControlDocumentoBundle:Perfil pe JOIN pe.conocimientoEspecial cesp JOIN cesp.curso cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosExternos ce JOIN ce.cursoExterno cu
                 WHERE c.id=:cid AND ce.fechaVigencia>:hoy)
                 ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'cid'=>$cid,'pid'=>$pid));

            //Consulta para saber si le faltan cursos internos
            $query1 = $em->createQuery(
                "SELECT count(cur.id) faltan FROM ControlDocumentoBundle:Perfil pe JOIN pe.capacitacion cap JOIN cap.capacitacion cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosInternos ci JOIN ci.cursoInterno cu
                 WHERE c.id=:cid)
                 ")->setParameters(array('cid'=>$cid,'pid'=>$pid));

            $entities = $query->getResult();
            $entities1 = $query1->getResult();
            $faltanext=$entities[0]['faltan'];
            $faltanint=$entities1[0]['faltan'];

            if ($faltanext>0 OR $faltanint>0)
            {
               //No califica y se guarda en el arreglo

            }
            else
            {
               //Califica
                $personas[]=array($cid,$nc,$nom,$app,$apm,$pid);
            }

        }

        //ladybug_dump_die($personas);

        return array(
            'personas'=>$personas

        );
    }

    /**
     * @Route("/doccomp", name="cdoc_doccomp")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:doc_completa.html.twig")
     */
    public function doccompAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        //Consulto todas las relaciones Perfil-curricula vigentes
        $qtodos = $em->createQuery(
            "SELECT pc.id,p.id pid,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc
             FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
             WHERE pc.fechaBaja>:hoy
              ")->setParameter('hoy', $hoy->format('Y-m-d') );

        $todos = $qtodos->getResult();

        //Para guardar los datos de las personas
        $personas = Array ();

        foreach ($todos as $pc)
        {

            $cid=$pc['cid'];
            $pid=$pc['pid'];
            $nom=$pc['nom'];
            $app=$pc['app'];
            $apm=$pc['apm'];
            $nc=$pc['nc'];

            //Consulta para saber si le faltan cursos externos
            $query = $em->createQuery(
                "SELECT count(cur.id) faltan
                 FROM ControlDocumentoBundle:Perfil pe JOIN pe.conocimientoEspecial cesp JOIN cesp.curso cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosExternos ce JOIN ce.cursoExterno cu
                 WHERE c.id=:cid)
                 ")->setParameters(array('cid'=>$cid,'pid'=>$pid));

            //Consulta para saber si le faltan cursos internos
            $query1 = $em->createQuery(
                "SELECT count(cur.id) faltan FROM ControlDocumentoBundle:Perfil pe JOIN pe.capacitacion cap JOIN cap.capacitacion cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosInternos ci JOIN ci.cursoInterno cu
                 WHERE c.id=:cid)
                 ")->setParameters(array('cid'=>$cid,'pid'=>$pid));

            $entities = $query->getResult();
            $entities1 = $query1->getResult();
            $faltanext=$entities[0]['faltan'];
            $faltanint=$entities1[0]['faltan'];

            if ($faltanext>0 OR $faltanint>0)
            {
                //No califica y se guarda en el arreglo

            }
            else
            {
                //Califica
                $personas[]=array($cid,$nc,$nom,$app,$apm,$pid);
            }

        }

        //ladybug_dump_die($personas);

        return array(
            'personas'=>$personas

        );
    }



    /**
     * @Route("/nocalificado", name="cdoc_nocalificado")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:nocalificado.html.twig")
     */
    public function nocalificadoAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();
        //Consulto todas las relaciones Perfil-curricula vigentes
        $qtodos = $em->createQuery(
            "SELECT pc.id,p.id pid,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc
             FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
             WHERE pc.fechaBaja>:hoy
              ")->setParameter('hoy', $hoy->format('Y-m-d') );

        $todos = $qtodos->getResult();
        //Para guardar los datos de las personas
        $personas = Array ();

        foreach ($todos as $pc)
        {
            //ladybug_dump_die($pc['nc']);
            $cid=$pc['cid'];
            $pid=$pc['pid'];
            $nom=$pc['nom'];
            $app=$pc['app'];
            $apm=$pc['apm'];
            $nc=$pc['nc'];


            //Consulta para saber si le faltan cursos externos
            $query = $em->createQuery(
                "SELECT count(cur.id) faltan
                 FROM ControlDocumentoBundle:Perfil pe JOIN pe.conocimientoEspecial cesp JOIN cesp.curso cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosExternos ce JOIN ce.cursoExterno cu
                 WHERE c.id=:cid AND ce.fechaVigencia>:hoy)
                 ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'cid'=>$cid,'pid'=>$pid));

            //Consulta para saber si le faltan cursos internos
            $query1 = $em->createQuery(
                "SELECT count(cur.id) faltan FROM ControlDocumentoBundle:Perfil pe JOIN pe.capacitacion cap JOIN cap.capacitacion cur
                 WHERE pe.id=:pid AND cur.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosInternos ci JOIN ci.cursoInterno cu
                 WHERE c.id=:cid AND ci.fechaVigencia>:hoy)
                 ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'cid'=>$cid,'pid'=>$pid));

            $entities = $query->getResult();
            $entities1 = $query1->getResult();
            $faltanext=$entities[0]['faltan'];
            $faltanint=$entities1[0]['faltan'];

            if ($faltanext>0 OR $faltanint>0)
            {
                //No califica y se guarda en el arreglo
                $personas[]=array($cid,$nc,$nom,$app,$apm,$pid);
            }
            else
            {
                //Califica

            }

        }

        //ladybug_dump_die($personas);

        return array(
            'personas'=>$personas

        );
    }

    /**
     * @Route("/concertificadopdf", name="con_certificadopdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:concertificadopdf.html.twig")
     */
    public function concertificadopdfAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        //Consulta perfil-curricula
        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,p.clave clave, p.titulo tit, p.revision rev,c.id cid,c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl, ev.id idpdf
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc JOIN pc.evaluaciones ev JOIN pc.perfil p
              WHERE pc.fechaBaja >:hoy AND ev INSTANCE OF ControlDocumentoBundle:PdfCertificado"
        )->setParameter('hoy', $hoy->format('Y-m-d') );

        $entities = $query->getResult();

       // ladybug_dump_die($entities);
        $html = $this->renderView('ControlDocumentoBundle:CDocRep:concertificadopdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal con Certificado',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="concertificado.pdf"',
            )
        );

        return $response;
    }

    /**
     * @Route("/sincertificado", name="sin_certificado")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:sincertificado.html.twig")
     */
    public function sincertificadoAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        //Consulta perfil-curricula

        $query = $em->createQuery(
            "SELECT pc.id pid,c.id cid, c.numeroControl nc, c.apellidoPaterno ap, c.apellidoMaterno am, c.nombre nom, pc.fechaBaja fb, pc.fechaAlta fa, p.clave clave, p.titulo tit, p.revision rev
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc JOIN pc.perfil p
              WHERE pc.fechaBaja >:hoy AND pc.id NOT IN
              (SELECT pcc.id
              FROM ControlDocumentoBundle:PerfilCurricula pcc
              JOIN pcc.evaluaciones ev
              WHERE ev INSTANCE OF ControlDocumentoBundle:PdfCertificado)"
        )->setParameter('hoy', $hoy->format('Y-m-d') );


        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * @Route("/sincertificadopdf", name="sin_certificadopdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:sincertificadopdf.html.twig")
     */
    public function sincertificadopdfAction(){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        //Consulta perfil-curricula
        $query = $em->createQuery(
            "SELECT pc.id pid,c.numeroControl nc, c.apellidoPaterno ap, c.apellidoMaterno am, c.nombre nom, pc.fechaBaja fb, pc.fechaAlta fa, p.clave clave, p.titulo tit, p.revision rev
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc JOIN pc.perfil p
              WHERE pc.fechaBaja >:hoy AND pc.id NOT IN
              (SELECT pcc.id
              FROM ControlDocumentoBundle:PerfilCurricula pcc
              JOIN pcc.evaluaciones ev
              WHERE ev INSTANCE OF ControlDocumentoBundle:PdfCertificado)"
        )->setParameter('hoy', $hoy->format('Y-m-d') );
        $entities = $query->getResult();

        $html = $this->renderView('ControlDocumentoBundle:CDocRep:sincertificadopdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal sin certificado',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="sincertificado.pdf"',
            )
        );

        return $response;

    }

    /**
     * Edits an existing CDocRep entity.
     *
     * @Route("/cdocvigencia", name="cdoc_vigencia")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CDocRep:cursosvigentes.html.twig")
     */
    public function vigenciaAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_vigencia');

        $fvigencia=$formulario['fv'];
        $curso=$formulario['cursos'];
        //ladybug_dump_die($curso);
        //consulta a las personas que tienen el curso con fecha de vigencia menor a la solicitada

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id=:curso and ce.fechaVigencia< :hoy"
        )->setParameters(
                array (
                'hoy'=> $fvigencia,
                'curso'=>$curso
                )
            );

        $entities = $query->getResult();
        //ladybug_dump_die($entities);

        return array(
            'personas' => $entities,
        );

    }

    /**
     * @Route("/vigencias", name="cdoc_vigencias")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:vigencias.html.twig")
     */
    public function vigenciasAction(){
        //ladybug_dump_die($entity);
        $form = $this->createVigenciaForm();


        return array(
           'vform'=>$form->createView()
        );
    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CDocRep $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createVigenciaForm()
    {
            $form = $this->createForm(new VigenciaType(), null , array(
            'action' => $this->generateUrl('cdoc_vigencia'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Consultar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Consultar'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }


    /**
     * Edits an existing CDocRep entity.
     *
     * @Route("/cdocvigenciaprod", name="cdoc_vigenciaprod")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CDocRep:cursosvigentesprod.html.twig")
     */
    public function vigenciaprodAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_vigenciaprod');

        $fvigencia=$formulario['fv'];
        $procedimiento=$formulario['procedimiento'];
        //ladybug_dump_die($curso);
        //consulta a las personas que tienen el curso con fecha de vigencia menor a la solicitada

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.titulo tit, c.clave ccve, c.revision crev
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosInternos ce JOIN ce.cursoInterno c WHERE c.id=:procedimiento and ce.fechaVigencia< :hoy"
        )->setParameters(
                array (
                    'hoy'=> $fvigencia,
                    'procedimiento'=>$procedimiento
                )
            );

        $entities = $query->getResult();
        //ladybug_dump_die($entities);

        return array(
            'personas' => $entities,
        );

    }

    /**
     * @Route("/vigenciasprod", name="cdoc_vigenciasprod")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:vigenciasprod.html.twig")
     */
    public function vigenciasprodAction(){
        //ladybug_dump_die($entity);
        $form = $this->createVigenciaprodForm();

        return array(
            'vform'=>$form->createView()
        );
    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CDocRep $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createVigenciaprodForm()
    {
        $form = $this->createForm(new VigenciaProdType(), null , array(
            'action' => $this->generateUrl('cdoc_vigenciaprod'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Consultar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Consultar'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }

/*Pruebas para reporte de faltantes de comprobantes de curso*/


    /**
     * Edits an existing CDocRep entity.
     *
     * @Route("/faltacomprobante", name="cdoc_faltacomprobante")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CDocRep:faltacomprobante.html.twig")
     */
    public function comprobanteAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_cursosolo');

        $curso=$formulario['cursos'];
        $hoy=new \DateTime();

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT DISTINCT p.id pid
             FROM ControlDocumentoBundle:PerfilCurricula pc
             JOIN pc.perfil p JOIN p.conocimientoEspecial ce JOIN ce.curso c
             WHERE c.id=:curso and pc.fechaBaja>:hoy"
        )->setParameters(
                array (
                    'curso'=>$curso,
                    'hoy'=>$hoy,
                )
            );
        $entities = $query->getResult();


        //ladybug_dump_die($entities);

        $curriculas = new ArrayCollection();

        //Se recuperan los cursos relacionados a la curricula
        foreach($entities as $r){
           // ladybug_dump_die($r['pid']);
           //hago la consulta para guardar las personas que tienen la curricula
            $query1 = $em->createQuery(
                "SELECT cu.id id,cu.nombre nom,cu.apellidoPaterno apa, cu.apellidoMaterno ama, cu.numeroControl nc, c.nombre cur
                 FROM ControlDocumentoBundle:PerfilCurricula pc
                 JOIN pc.curricula cu JOIN cu.cursosExternos ce JOIN ce.cursoExterno c
                 WHERE c.id=:curso
                 AND ce.pdfCertificado IS NULL
                 AND pc.perfil=:perfilid"
            )->setParameters(
                    array (
                        'curso'=>$curso,
                        'perfilid'=>$r['pid']
                    )
                );
            $entities1 = $query1->getResult();
           // ladybug_dump_die($entities1);
            $curriculas[]=$entities1;

        }
        //ladybug_dump_die($curso);

        return array(
            'personas' => $curriculas, 'curso'=>$curso
        );

    }

    /**
     * @Route("/comprobantes", name="cdoc_comprobantes")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:comprobantes.html.twig")
     */
    public function comprobantesAction(){
        //ladybug_dump_die($entity);
        $form = $this->createComprobantesForm();


        return array(
            'cform'=>$form->createView()
        );
    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CDocRep $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createComprobantesForm()
    {
        $form = $this->createForm(new CursoSoloType(), null , array(
            'action' => $this->generateUrl('cdoc_faltacomprobante'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Consultar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Consultar'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }

    /**
     * @Route("/{curso}/comprobantespdf",name="comprobantespdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:faltacomprobantepdf.html.twig")
     */
    public function comprobantespdfAction($curso){


        $hoy=new \DateTime();

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT DISTINCT p.id pid
             FROM ControlDocumentoBundle:PerfilCurricula pc
             JOIN pc.perfil p JOIN p.conocimientoEspecial ce JOIN ce.curso c
             WHERE c.id=:curso and pc.fechaBaja>:hoy"
        )->setParameters(
                array (
                    'curso'=>$curso,
                    'hoy'=>$hoy,
                )
            );
        $entities = $query->getResult();


        //ladybug_dump_die($entities);

        $curriculas = new ArrayCollection();

        //Se recuperan los cursos relacionados a la curricula
        foreach($entities as $r){
            // ladybug_dump_die($r['pid']);
            //hago la consulta para guardar las personas que tienen la curricula
            $query1 = $em->createQuery(
                "SELECT cu.id id,cu.nombre nom,cu.apellidoPaterno apa, cu.apellidoMaterno ama, cu.numeroControl nc, c.nombre cur
                 FROM ControlDocumentoBundle:PerfilCurricula pc
                 JOIN pc.curricula cu JOIN cu.cursosExternos ce JOIN ce.cursoExterno c
                 WHERE c.id=:curso
                 AND ce.pdfCertificado IS NULL
                 AND pc.perfil=:perfilid"
            )->setParameters(
                    array (
                        'curso'=>$curso,
                        'perfilid'=>$r['pid']
                    )
                );
            $entities1 = $query1->getResult();
            // ladybug_dump_die($entities1);
            $curriculas[]=$entities1;

        }

        $html = $this->renderView('ControlDocumentoBundle:CDocRep:faltacomprobantepdf.html.twig',
            array(
                'personas' => $curriculas, 'curso'=>$curso));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal sin comprobantes',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="faltancomprobantepdf.pdf"',
            )
        );

        return $response;

    }

    /**
     * Edits an existing CDocRep entity.
     *
     * @Route("/faltacurso", name="cdoc_faltacurso")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CDocRep:faltacurso.html.twig")
     */
    public function faltacursoAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_cursosolo');
        $curso=$formulario['cursos'];

        $em = $this->getDoctrine()->getManager();

        $datoscurso = $em->getRepository('ControlDocumentoBundle:Curso')->findById($curso);

        //ladybug_dump_die($datoscurso);

        $hoy=new \DateTime();


        $query = $em->createQuery(
            "SELECT DISTINCT p.id pid
             FROM ControlDocumentoBundle:PerfilCurricula pc
             JOIN pc.perfil p JOIN p.conocimientoEspecial ce JOIN ce.curso c
             WHERE c.id=:curso and pc.fechaBaja>:hoy"
        )->setParameters(
                array (
                    'curso'=>$curso,
                    'hoy'=>$hoy,
                )
            );
        $entities = $query->getResult();


        //ladybug_dump_die($entities);

        $curriculas = new ArrayCollection();

        //Se recuperan los cursos relacionados a la curricula
        foreach($entities as $r){
            // ladybug_dump_die($r['pid']);
            //hago la consulta para guardar las personas que tienen la curricula
            $query1 = $em->createQuery(
                "SELECT DISTINCT curr.id, curr.numeroControl nc, curr.nombre nom, curr.apellidoPaterno apa, curr.apellidoMaterno ama FROM ControlDocumentoBundle:PerfilCurricula pcu
                 JOIN pcu.curricula curr
                 JOIN curr.cursosExternos cex
                 JOIN cex.cursoExterno cc
                 WHERE pcu.perfil=:perfilid
                 AND curr.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:PerfilCurricula pc
                 JOIN pc.curricula cu
                 JOIN cu.cursosExternos ce
                 JOIN ce.cursoExterno c
                 WHERE pc.perfil=:perfilid AND c.id=:curso
                 )"
            )->setParameters(
                    array (
                        'curso'=>$curso,
                        'perfilid'=>$r['pid']
                    )
                );


            $entities1 = $query1->getResult();

            $curriculas[]=$entities1;

        }

       // ladybug_dump_die($curriculas);

        return array(
            'personas' => $curriculas, 'curso'=>$curso,'dato'=>$datoscurso
        );

    }

    /**
     * @Route("/fcursos", name="cdoc_fcursos")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:fcursos.html.twig")
     */
    public function fcursosAction(){
        //ladybug_dump_die($entity);
        $form = $this->createFcursoForm();


        return array(
            'cform'=>$form->createView()
        );
    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CDocRep $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createFcursoForm()
    {
        $form = $this->createForm(new CursoSoloType(), null , array(
            'action' => $this->generateUrl('cdoc_faltacurso'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Consultar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Consultar'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }

    /**
     * @Route("/{curso}/faltacursopdf",name="faltacursopdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:faltacursopdf.html.twig")
     */
    public function faltacursopdfAction($curso){


        $hoy=new \DateTime();

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT DISTINCT p.id pid
             FROM ControlDocumentoBundle:PerfilCurricula pc
             JOIN pc.perfil p JOIN p.conocimientoEspecial ce JOIN ce.curso c
             WHERE c.id=:curso and pc.fechaBaja>:hoy"
        )->setParameters(
                array (
                    'curso'=>$curso,
                    'hoy'=>$hoy,
                )
            );
        $entities = $query->getResult();

        $datoscurso = $em->getRepository('ControlDocumentoBundle:Curso')->findById($curso);
        //ladybug_dump_die($entities);

        $curriculas = new ArrayCollection();

        //Se recuperan los cursos relacionados a la curricula
        foreach($entities as $r){
            // ladybug_dump_die($r['pid']);
            //hago la consulta para guardar las personas que tienen la curricula
            $query1 = $em->createQuery(
                "SELECT DISTINCT curr.id, curr.numeroControl nc, curr.nombre nom, curr.apellidoPaterno apa, curr.apellidoMaterno ama FROM ControlDocumentoBundle:PerfilCurricula pcu
                 JOIN pcu.curricula curr
                 JOIN curr.cursosExternos cex
                 JOIN cex.cursoExterno cc
                 WHERE pcu.perfil=:perfilid
                 AND curr.id NOT IN
                 (SELECT cu.id FROM ControlDocumentoBundle:PerfilCurricula pc
                 JOIN pc.curricula cu
                 JOIN cu.cursosExternos ce
                 JOIN ce.cursoExterno c
                 WHERE pc.perfil=:perfilid AND c.id=:curso
                 )"
            )->setParameters(
                    array (
                        'curso'=>$curso,
                        'perfilid'=>$r['pid']
                    )
                );
            $entities1 = $query1->getResult();
            // ladybug_dump_die($entities1);
            $curriculas[]=$entities1;

        }

        $html = $this->renderView('ControlDocumentoBundle:CDocRep:faltacursopdf.html.twig',
            array(
                'personas' => $curriculas, 'curso'=>$curso,'dato'=>$datoscurso));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal sin comprobantes',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="faltacursopdf.pdf"',
            )
        );

        return $response;

    }

    /**
     * @Route("/alertaspdf",name="alertaspdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDocRep:alertaspdf.html.twig")
     */
    public function altertaspdfAction(){


        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();



        $html = $this->renderView('ControlDocumentoBundle:CDocRep:alertaspdf.html.twig',
            array(
                'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite,));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Alertas de vencimiento',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="alertasdevencimiento.pdf"',
            )
        );

        return $response;

    }
}