<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\PerfilCurricula;
use Ultra\ControlDocumentoBundle\Form\CurriculaType;
use Ultra\ControlDocumentoBundle\Form\EvaluaType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Response;


/**
 * Curricula controller.
 *
 * @Route("/curricula")
 */
class CurriculaController extends Controller
{
    /**
     * Lists all Curricula entities.
     *
     * @Route("/", name="curricula")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();



        $query = $em->createQuery(
            "SELECT c FROM ControlDocumentoBundle:Curricula c
              JOIN c.type t
              WHERE t.id =1
            ");

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all Curricula entities Alfa.
     *
     * @Route("/alfa/", name="curricula_alfa")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Curricula:alfa.html.twig")
     */
    public function alfaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT c FROM ControlDocumentoBundle:Curricula c
              JOIN c.type t
              WHERE t.id =2
            ");

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     *
     * @Route("/reportes", name="rep_curricula")
     * @Template("ControlDocumentoBundle:Curricula:reportes.html.twig")
     * @return Response
     */
    public function reportesAction()
    {
        $this->_datatable();                                                        // call the datatable config initializer
        return $this->render('ControlDocumentoBundle:Curricula:reportes.html.twig');                 // replace "XXXMyBundle:Module:index.html.twig" by yours
    }

    /**
     * Creates a new Curricula entity.
     *
     * @Route("/", name="curricula_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Curricula:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Curricula();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La currícula se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('curricula_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La currícula no pudo ser guardada'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );

    }

    /**
    * Creates a form to create a Curricula entity.
    *
    * @param Curricula $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Curricula $entity)
    {
        $form = $this->createForm(new CurriculaType(), $entity, array(
            'action' => $this->generateUrl('curricula_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Curricula entity.
     *
     * @Route("/new", name="curricula_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Curricula();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Curricula entity.
     *
     * @Route("/{id}", name="curricula_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la Curricula.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Curricula entity.
     *
     * @Route("/{id}/edit", name="curricula_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Curricula entity.
    *
    * @param Curricula $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Curricula $entity)
    {
        $form = $this->createForm(new CurriculaType(), $entity, array(
            'action' => $this->generateUrl('curricula_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        /*$form->remove('npcfe');
        $form->remove('rpecfe');*/

       $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEvaluaForm(Curricula $entity)
    {
        $form = $this->createForm(new EvaluaType(), null , array(
            'action' => $this->generateUrl('curr_evaluax', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Evaluar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Evalua'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }

    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}", name="curricula_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Curricula:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);
        //$entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAnexoByCurricula($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $currentTraining = new ArrayCollection();
        foreach($entity->getCompetencias() as $training){
            $currentTraining->add($training);
        }

        $cursosExternos = new ArrayCollection();
        foreach($entity->getCursosExternos() as $cursos){
            $cursosExternos->add($cursos);
        }

        $cursosInternos = new ArrayCollection();
        foreach($entity->getCursosInternos() as $cursos){
            $cursosInternos->add($cursos);
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {


            foreach($currentTraining as $training){
                if(false === $entity->getCompetencias()->contains($training)){
                    $training->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($training);
                }
            }

            foreach($cursosInternos as $cursos){
                if(false === $entity->getCursosInternos()->contains($cursos)){
                    $cursos->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($cursos);
                }
            }

            foreach($cursosExternos as $cursos){
                if(false === $entity->getCursosExternos()->contains($cursos)){
                    $cursos->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($cursos);
                }
            }

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la curricula fueron modificados.'
            );

            return $this->redirect($this->generateUrl('curricula_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La curricula no fue modificada.'
            );
        }


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Curricula entity.
     *
     * @Route("/{id}", name="curricula_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la curricula.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La currícula fue eliminada'
            );
            return $this->redirect($this->generateUrl('curricula'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La currícula no pudo ser eliminada.'
            );
        }

        return $this->redirect($this->generateUrl('curricula'));
    }

    /**
     * Creates a form to delete a Curricula entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('curricula_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array('label' => 'Borrar',
              'attr' => array('class' => 'btn btn-warning',
              'title'=>'Borrar datos'),
              'glyphicon' => 'glyphicon glyphicon-trash'
              ))
            ->getForm()
        ;
    }


    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}/{p}/evalua", name="curr_evalua")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Curricula:evalua.html.twig")
     */
    public function evaluaAction(Request $request,$id,$p){


        $result = null;
        $em = $this->getDoctrine()->getManager();

        $cv = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$cv) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $query = $em->createQuery("
            SELECT c, pc, p FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
            WHERE c.id = :id AND p.id= :pe
        ")->setParameters( array('id'=>$id,'pe'=>$p));

        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        $cursosInternos = new ArrayCollection();
        $cursosExternos = new ArrayCollection();
        $conocimientosEspeciales = new ArrayCollection();
        $capacitacionEspecifica = new ArrayCollection();
        $cursosExternosNoVigentes = array();
        $cursosInternosNoVigentes = array();
        $perfiles = new ArrayCollection();
        $difInterno = array();
        $difExterno = array();

        foreach($result as $r){
            foreach($r->getCursosInternos() as $c){
                $cursosInternos[] = $c->getCursoInterno();
            }
        }

        //Se recuperan cursos externos
        foreach($result as $r){
            foreach($r->getCursosExternos() as $c){
                $cursosExternos[] = $c->getCursoExterno();
            }
        }

        //Recuperamos los perfiles relacionados a la curricula
        foreach($result as $r){
            foreach($r->getCurriculas() as $pc){
                $perfiles[] = $pc->getPerfil();
            }
        }


        //Recuperamos la capacitacion especifica del perfil
        foreach($perfiles as $perfil){
            foreach($perfil->getCapacitacion() as $c){
                $capacitacionEspecifica[] = $c->getCapacitacion();
            }
        }

        foreach($perfiles as $perfil){
            foreach($perfil->getConocimientoEspecial() as $c){
                $conocimientosEspeciales[] = $c->getCurso();
            }
        }

        //Se determinan las diferencias
        foreach($capacitacionEspecifica as $c){
            if(!$cursosInternos->contains($c)){
                $difInterno[] = $c;
            }
        }

        foreach($conocimientosEspeciales as $c){
            if(!$cursosExternos->contains($c)){
                $difExterno[] = $c;
            }
        }


        foreach($result as $r){
            foreach($r->getCursosExternos() as $c){
                if(!$c->estaVigente()){
                    //$c->getCursoExterno();
                    $cursosExternosNoVigentes[] = $c;
            }
        }
        }
        foreach($result as $r){
        foreach($r->getCursosInternos() as $c){
            if(!$c->estaVigente()){
                $cursosInternosNoVigentes[] = $c;
            }
        }
        }

        //ladybug_dump_die($cursosExternosNoVigentes);
        return array(
            'capacitacion' => $capacitacionEspecifica,
            'conocimientos' => $conocimientosEspeciales,
            'cursos' => $cursosInternos,
            'cursose'=> $cursosExternos,
            'perfiles' => $perfiles,
            'cursos_internos' => $difInterno,
            'cursos_externos' => $difExterno,
            'cursos_externos_no_vigentes' => $cursosExternosNoVigentes,
            'cursos_internos_no_vigentes' => $cursosInternosNoVigentes,
            'cv'=>$cv,
            'perfil_id'=>$p
        );
    }

    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}/evaluax", name="curr_evaluax")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Curricula:evaluaxper.html.twig")
     */
    public function evaluaxAction(Request $request,$id){

        $formulario=$request->request->get('ultra_controldocumentobundle_evalua');

        //ladybug_dump_die($formulario['perfiles']);
        $p=$formulario['perfiles'];

        $em = $this->getDoctrine()->getManager();

        $curricula = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$curricula) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $perfil = $em->getRepository('ControlDocumentoBundle:Perfil')->find($p);

        if (!$perfil) {
            throw $this->createNotFoundException('Imposible encontrar perfil.');
        }

        $cursosInternos = new ArrayCollection();
        $cursosExternos = new ArrayCollection();
        $conocimientosEspeciales = new ArrayCollection();
        $capacitacionEspecifica = new ArrayCollection();
        $cursosExternosNoVigentes = array();
        $cursosInternosNoVigentes = array();
        $perfiles = new ArrayCollection();
        $difInterno = array();
        $difExterno = array();

            foreach($curricula->getCursosInternos() as $c){
                $cursosInternos[] = $c->getCursoInterno();
            }



        //Se recuperan cursos externos

            foreach($curricula->getCursosExternos() as $c){
                $cursosExternos[] = $c->getCursoExterno();
            }


        //Recuperamos la capacitacion especifica del perfil

            foreach($perfil->getCapacitacion() as $c){
                $capacitacionEspecifica[] = $c->getCapacitacion();
            }

//ladybug_dump_die($perfil->getCapacitacion());
            foreach($perfil->getConocimientoEspecial() as $c){
                $conocimientosEspeciales[] = $c->getCurso();
            }

        //Se determinan las diferencias
        foreach($capacitacionEspecifica as $c){
            if(!$cursosInternos->contains($c)){
                $difInterno[] = $c;
            }
        }


        foreach($conocimientosEspeciales as $c){
            if(!$cursosExternos->contains($c)){
                $difExterno[] = $c;
            }
        }


        foreach($curricula->getCursosExternos() as $c){
                if(!$c->estaVigente()){
                $cursosExternosNoVigentes[] = $c;
                }
            }

        foreach($curricula->getCursosInternos() as $c){
            if(!$c->estaVigente()){
                //ladybug_dump_die($c->estaVigente());
                $cursosInternosNoVigentes[] = $c;
            }
        }

        return array(
            'capacitacion' => $capacitacionEspecifica,
            'conocimientos' => $conocimientosEspeciales,
            'cursos' => $cursosInternos,
            'cursose'=> $cursosExternos,
            'perfil' => $perfil,
            'cursos_internos' => $difInterno,
            'cursos_externos' => $difExterno,
            'cursos_externos_no_vigentes' => $cursosExternosNoVigentes,
            'cursos_internos_no_vigentes' => $cursosInternosNoVigentes,
            'cv'=>$curricula
        );
    }

    /**
     * @Route("/{id}/historial", name="curr_historial")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Curricula:historial.html.twig")
     */
    public function historialAction($id){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        //Consulta perfil-curricula
        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,p.id pe, p.clave,p.revision,p.titulo
              FROM ControlDocumentoBundle:Perfil p
              JOIN p.perfiles pc
              WHERE pc.curricula = :id"
        )->setParameters(array(
                'id' => $id,
            ));

        $entities = $query->getResult();

        //ladybug_dump_die($entity);
        $form = $this->createEvaluaForm($entity);


        return array(
            'entities' => $entities, 'entity'=>$entity,'eform'=>$form->createView()
        );
    }

    /**
     * @Route("/{id}/historialpdf",name="curr_historialpdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Curricula:historialpdf.html.twig")
     */
    public function historiapdfAction($id){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        //Consulta perfil-curricula
        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,p.id pe, p.clave,p.titulo
              FROM ControlDocumentoBundle:Perfil p
              JOIN p.perfiles pc
              WHERE pc.curricula = :id"
        )->setParameters(array(
                'id' => $id,
            ));

        $entities = $query->getResult();

        $html = $this->renderView('ControlDocumentoBundle:Curricula:historialpdf.html.twig',
            array(
                'entity' => $entity,'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="historialpdf.pdf"',
            )
        );

        return $response;

    }





}
