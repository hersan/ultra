<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Form\RHumCapturaType;

/**
 * Class RHumController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/RHum", name="rhum")
 */
class RHumController extends Controller
{
     /**
     * @Route("/index", name="rhum_index")
     * @Template("ControlDocumentoBundle:RHum:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $hoy=new \DateTime();
        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales

        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (t.titulo = 'general' and d.year='2015')
            ");

        $documentos = $q->getResult();


        return array(
            'proyectos' => $proyectos,
            'documentos'=>$procedimientos,
            'lim'=>$limite,
            'lm'=>$listamaestra,'cumple'=>$cumple,
            'doc'=>$documentos
        );
    }

    /**
     * Displays a form to edit an existing ControlCfe entity.
     *
     * @Route("/{id}/edit", name="rhum_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $editForm = $this->createEditForm($entity);


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ControlCfe entity.
     *
     * @param Curricula $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Curricula $entity)
    {
        $form = $this->createForm(new RHumCapturaType(), $entity, array(
            'action' => $this->generateUrl('rhum_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('rhum')
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }


    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}", name="rhum_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:RHum:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }


        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {


            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la curricula fueron modificados.'
            );

            return $this->redirect($this->generateUrl('rhum_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La curricula no fue modificada.'
            );
        }


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );

    }

    /**
     * Finds and displays a Curricula entity.
     *
     * @Route("/{id}", name="rhum_show")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:RHum:show.html.twig")
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la Curricula.');
        }

        return array(
            'entity'      => $entity,
        );
    }

}