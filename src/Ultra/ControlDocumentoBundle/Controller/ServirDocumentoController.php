<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Ultra\CalidadBundle\Entity\Agenda;
use Ultra\CalidadBundle\Entity\AuditoriaVigilancia;
use Ultra\CalidadBundle\Entity\Recomendaciones;
use Ultra\ControlDocumentoBundle\Entity\AbstractUltraFile;
use Ultra\ControlDocumentoBundle\Entity\AbstractUploadedFile;
use Ultra\ControlDocumentoBundle\Entity\Documento;
use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;
use Ultra\ControlDocumentoBundle\Entity\GeneralDocument;
use Ultra\ControlDocumentoBundle\Entity\Perfil;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\CursoInterno;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Ultra\ControlDocumentoBundle\Entity\Competencia;
use Ultra\ControlDocumentoBundle\Entity\Trainer;
use Ultra\ProyectoBundle\Entity\OrdenTrabajo;
use Ultra\RHBundle\Entity\RHAgenda;

class ServirDocumentoController extends Controller
{
    /**
     * @Route("/{id}/servir", name="servir")
     * @Template()
     * @ParamConverter("documento", class="ControlDocumentoBundle:Documento")
     */
    public function servirAction(Documento $documento)
    {
        if (!file_exists($documento->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($documento->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $documento->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $documento->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/{id}/display/pdf", name="display_pdf")
     * @ParamConverter("Perfil", class="ControlDocumentoBundle:Perfil")
     */
    public function displayPdfAction(Perfil $perfil)
    {
        /*$path = $this->container->getParameter('ultra.directory.perfil');
        $filePath = $path .'/'. $perfil->getPdf();*/

        if (!file_exists($perfil->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($perfil->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $perfil->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $perfil->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/{id}/display/curricula", name="display_curricula")
     * @ParamConverter("Perfil", class="ControlDocumentoBundle:Perfil")
     */
    public function displayCurriculaAction(Curricula $curricula)
    {
        //$path = $this->container->getParameter('ultra.directory.perfil');
        //$filePath = $path .'/'. $curricula->getPdf();
        $filePath = $curricula->getAbsolutePath();

        if (!file_exists($filePath)) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($filePath);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $curricula->getPdfCurricula(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $curricula->getPdfCurricula())
        );
        return $response;
    }

    /**
     * @Route("/servir/cinterno/{id}", name="servir_cinterno")
     * @Template()
     * @ParamConverter("interno", class="ControlDocumentoBundle:CursoInterno")
     */
    public function servircinternoAction(CursoInterno $interno)
    {
        if (!file_exists($interno->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($interno->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $interno->getPdfCertificado(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $interno->getPdfCertificado())
        );
        return $response;
    }

    /**
     * @Route("/servir/cexterno/{id}", name="servir_cexterno")
     * @Template()
     * @ParamConverter("externo", class="ControlDocumentoBundle:CursoExterno")
     */
    public function servircexternoAction(CursoExterno $externo)
    {
        if (!file_exists($externo->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($externo->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $externo->getPdfCertificado(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $externo->getPdfCertificado())
        );
        return $response;
    }

    /**
     * @Route("/servir/ordentrabajo/{id}", name="servir_orden_trabajo")
     * @Template()
     * @ParamConverter("orden", class="ProyectoBundle:OrdenTrabajo")
     */
    public function servirordentrabajoAction(OrdenTrabajo $orden)
    {
        if (!file_exists($orden->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($orden->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $orden->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $orden->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/certificado/puesto/{id}", name="servir_certificado_puesto")
     * @Template()
     * @ParamConverter("certificado", class="ControlDocumentoBundle:AbstractUploadedFile")
     */
    public function servirCertificadoAction(AbstractUploadedFile $certificado)
    {
        if (!file_exists($certificado->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($certificado->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $certificado->getName(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $certificado->getName())
        );
        return $response;
    }

    /**
     * @Route("/servir/anexos/{id}", name="servir_anexos")
     * @Template()
     * @ParamConverter("anexo", class="ControlDocumentoBundle:AbstractUltraFile")
     */
    public function serviranexosAction(AbstractUltraFile $anexo)
    {   //ladybug_dump_die($anexo->getAbsolutePath());
        if (!file_exists($anexo->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($anexo->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $anexo->getName(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $anexo->getName())
        );
        return $response;
    }

    /**
     * @Route("/servir/competencia/{id}", name="servir_competencia")
     * @Template()
     * @ParamConverter("competencia", class="ControlDocumentoBundle:Competencia")
     */
    public function servircompetenciaAction(Competencia $competencia)
    {
        if (!file_exists($competencia->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($competencia->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $competencia->getPdfCertificado(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $competencia->getPdfCertificado())
        );
        return $response;
    }

    /**
     * @Route("/servir/formato/asociado/{id}", name="servir_formato_asociado")
     * @Template()
     * @ParamConverter("formato_asociado", class="ControlDocumentoBundle:FormatoAsociado")
     */
    public function servirFormatoAsociadoAction(FormatoAsociado $formatoAsociado)
    {
        if (!file_exists($formatoAsociado->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($formatoAsociado->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $formatoAsociado->getName(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $formatoAsociado->getName())
        );
        return $response;
    }

    /**
     * @Route("/servir/auditoria/{id}", name="servir_auditoria")
     * @Template()
     * @ParamConverter("auditoria", class="CalidadBundle:AuditoriaVigilancia")
     */
    public function servirAuditoriaAction(AuditoriaVigilancia $formatoAsociado)
    {
        if (!file_exists($formatoAsociado->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($formatoAsociado->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $formatoAsociado->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $formatoAsociado->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/recomendacion/{id}", name="servir_recomendacion")
     * @Template()
     * @ParamConverter("recomendacion", class="CalidadBundle:Recomendaciones")
     */
    public function servirRecomendacionAction(Recomendaciones $formatoAsociado)
    {
        if (!file_exists($formatoAsociado->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($formatoAsociado->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $formatoAsociado->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $formatoAsociado->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/agenda/{id}", name="servir_agenda")
     * @Template()
     * @ParamConverter("agenda", class="CalidadBundle:Agenda")
     */
    public function servirAgendaAction(Agenda $file)
    {
        if (!file_exists($file->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($file->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $file->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $file->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/documento/general/{id}", name="servir_documento_general")
     * @Template()
     * @ParamConverter("general_document", class="ControlDocumentoBundle:GeneralDocument")
     */
    public function servirGeneralDocumentAction(GeneralDocument $file)
    {
        if (!file_exists($file->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($file->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $file->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $file->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/rh/agenda/{id}", name="servir_rh_agenda")
     * @Template()
     * @ParamConverter("agenda", class="RHBundle:RHAgenda")
     */
    public function servirRhAgendaAction(RHAgenda $file)
    {
        if (!file_exists($file->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($file->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $file->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $file->getPdf())
        );
        return $response;
    }

    /**
     * @Route("/servir/trainers/{id}", name="servir_trainers")
     * @ParamConverter("trainer", class="ControlDocumentoBundle:Trainer")
     */
    public function servirTrainersAction(Trainer $file)
    {
        if (!file_exists($file->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($file->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $file->getPdf(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $file->getPdf())
        );
        return $response;
    }

//COMENTYARIO
}
