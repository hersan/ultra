<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Disciplina;
use Ultra\ControlDocumentoBundle\Form\DisciplinaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Disciplina controller.
 *
 * @Route("/disciplina")
 */
class DisciplinaController extends Controller
{

    /**
     * Lists all Disciplina entities.
     *
     * @Route("/", name="disciplina")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Disciplina')->loadAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Disciplinas entity.
     *
     * @Route("/disciplinaspdf", name="disciplinas_pdf")
     * @Method("GET")
     * @Template()
     */
    public function disciplinaspdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Disciplina')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Disciplina:disciplinaspdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Áreas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadedisciplinas.pdf"',
            )
        );

        return $response;

    }

    /**
     * Creates a new Disciplina entity.
     *
     * @Route("/", name="disciplina_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Disciplina:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Disciplina();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La disciplina se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('disciplina_show', array('id' => $entity->getId())));
        }

        $this->get('session')->getFlashBag()->add(
            'danger',
            'La disciplina no pudo ser agregada.'
        );

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Disciplina entity.
    *
    * @param Disciplina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Disciplina $entity)
    {
        $form = $this->createForm(new DisciplinaType(), $entity, array(
            'action' => $this->generateUrl('disciplina_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Disciplina entity.
     *
     * @Route("/new", name="disciplina_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Disciplina();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Disciplina entity.
     *
     * @Route("/{id}", name="disciplina_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Disciplina')->loadById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la disciplina.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Disciplina entity.
     *
     * @Route("/{id}/edit", name="disciplina_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la disciplina.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Disciplina entity.
    *
    * @param Disciplina $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Disciplina $entity)
    {
        $form = $this->createForm(new DisciplinaType(), $entity, array(
            'action' => $this->generateUrl('disciplina_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing Disciplina entity.
     *
     * @Route("/{id}", name="disciplina_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Disciplina:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la disciplina.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La disciplina quedo actualizada con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('disciplina_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'Los datos de la disciplina no pudieron ser actualizados.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Disciplina entity.
     *
     * @Route("/{id}", name="disciplina_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la disciplina');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La disciplina fue eliminada.'
            );
            return $this->redirect($this->generateUrl('disciplina'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La disciplina no se pudo eliminar.'
            );
        }

        return $this->redirect($this->generateUrl('disciplina'));
    }

    /**
     * Creates a form to delete a Disciplina entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('disciplina_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }
}
