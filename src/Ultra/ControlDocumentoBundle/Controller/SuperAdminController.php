<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SuperAdminController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/superadmin", name="superadmin")
 */
class SuperAdminController extends Controller
{
    /**
     * @Route("/index", name="superadmin_index")
     * @Template("ControlDocumentoBundle:SuperAdmin:index.html.twig")
     */
    public function indexAction()
    {

        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);
        //$this->get('ladybug')->log($proyectos);

        $limite=new \DateTime();

        $hoy=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );
        $procedimientos = $query->getResult();


        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $hoy=new \DateTime();
        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales

        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (t.titulo = 'general' and d.year='2015')
            ");

        $documentos = $q->getResult();

        return array(
            'proyectos' => $proyectos,
            'documentos'=>$procedimientos,
            'lim'=>$limite,
            'lm'=>$listamaestra,
            'cumple'=>$cumple,
            'doc'=>$documentos
        );
    }
    /**
     * @Route("/reporteador", name="reporteador")
     * @Template("ControlDocumentoBundle:SuperAdmin:reporteador.html.twig")
     */
    public function reporteadorAction()
    {
        return array ();
    }


}
