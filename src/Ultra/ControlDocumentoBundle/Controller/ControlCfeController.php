<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Ultra\ControlDocumentoBundle\Form\ControlCfeCapturaType;

/**
 * Class ControlCfeController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/ControlCfe", name="controlcfe")
 */
class ControlCfeController extends Controller
{
    /**
     * @Route("/index", name="controlcfe_index")
     * @Template("ControlDocumentoBundle:ControlCfe:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH c.id = 1 AND d.vigencia< :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();

        //Consulto también los conteos por vencer

        $q = $em->createQuery("
        SELECT c.id,c.fechaConteo,c.fechaVigencia,c.dosis,d.nISD,u.nombre,u.apellidoPaterno, u.apellidoMaterno FROM ControlCLVBundle:Conteo c JOIN c.dosimetroId d JOIN d.curriculaId u
        WHERE c.fechaVigencia< :limite ORDER BY c.fechaVigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );

        try {
            $result = $q->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        return array(
            'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite,'result'=>$result
        );
    }

    /**
     *
     * @Route("/captura", name="ctrlcfe_captura")
     * @Template("ControlDocumentoBundle:ControlCfe:captura.html.twig")
     */
    public function ctrlcfe_capturaAction()
    {

        $c=3697;
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT c.id,c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl,c.npcfe,c.rpecfe
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.contrato= :c"
        )->setParameter('c', $c);


        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Displays a form to edit an existing ControlCfe entity.
     *
     * @Route("/{id}/edit", name="ctrlcfe_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        if($entity->getCursosExternos()->isEmpty()){
            $entity->addCursosExterno(new CursoExterno());
        }

        $editForm = $this->createEditForm($entity);


//        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ControlCfe entity.
     *
     * @param Curricula $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Curricula $entity)
    {
        $form = $this->createForm(new ControlCfeCapturaType(), $entity, array(
            'action' => $this->generateUrl('ctrlext_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('control')
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }


    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}", name="ctrlext_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:ControlCfe:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }


        $cursosExternos = new ArrayCollection();
        foreach($entity->getCursosExternos() as $cursos){
            $cursosExternos->add($cursos);
        }


        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach($cursosExternos as $cursos){
                if(false === $entity->getCursosExternos()->contains($cursos)){
                    $cursos->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($cursos);
                }
            }

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la curricula fueron modificados.'
            );

            return $this->redirect($this->generateUrl('ctrlcfe_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La curricula no fue modificada.'
            );
        }


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        );

    }

    /**
     * Finds and displays a Curricula entity.
     *
     * @Route("/{id}", name="ctrlcfe_show")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:ControlCfe:show.html.twig")
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la Curricula.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Creates a form to delete a ControlCfe entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('curricula_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array('label' => 'Borrar',
                'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
            ;
    }


}