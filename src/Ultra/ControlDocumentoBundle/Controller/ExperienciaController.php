<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Experiencia;
use Ultra\ControlDocumentoBundle\Form\ExperienciaType;

/**
 * Experiencia controller.
 *
 * @Route("/experiencia")
 */
class ExperienciaController extends Controller
{

    /**
     * Lists all Experiencia entities.
     *
     * @Route("/", name="experiencia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Experiencia')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Experiencia entity.
     *
     * @Route("/", name="experiencia_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Experiencia:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Experiencia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('experiencia_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Experiencia entity.
    *
    * @param Experiencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Experiencia $entity)
    {
        $form = $this->createForm(new ExperienciaType(), $entity, array(
            'action' => $this->generateUrl('experiencia_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Experiencia entity.
     *
     * @Route("/new", name="experiencia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Experiencia();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Experiencia entity.
     *
     * @Route("/{id}", name="experiencia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Experiencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experiencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Experiencia entity.
     *
     * @Route("/{id}/edit", name="experiencia_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Experiencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experiencia entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Experiencia entity.
    *
    * @param Experiencia $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Experiencia $entity)
    {
        $form = $this->createForm(new ExperienciaType(), $entity, array(
            'action' => $this->generateUrl('experiencia_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Experiencia entity.
     *
     * @Route("/{id}", name="experiencia_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Experiencia:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Experiencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Experiencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('experiencia_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Experiencia entity.
     *
     * @Route("/{id}", name="experiencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Experiencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Experiencia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('experiencia'));
    }

    /**
     * Creates a form to delete a Experiencia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('experiencia_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
