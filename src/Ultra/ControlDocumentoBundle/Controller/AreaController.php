<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Area;
use Ultra\ControlDocumentoBundle\Form\AreaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Area controller.
 *
 * @Route("/area")
 */
class AreaController extends Controller
{

    /**
     * Lists all Area entities.
     *
     * @Route("/", name="area")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Area')->findAll();

        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar el área.');
        }

        /*$query = $em->createQuery("
            SELECT a.claveArea clave,a.id id,a.titulo, a.tituloCorto,
                   c.numeroInterno contrato, u.nombre nombre, u.apellidoPaterno ap,u.apellidoMaterno am
            FROM ControlDocumentoBundle:Disciplina d
            JOIN d.area a
            JOIN a.contrato c JOIN a.curricula u
            GROUP BY contrato
        ");*/

        /*try {
            $entities = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }*/

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Area entity.
     *
     * @Route("/areaspdf", name="areas_pdf")
     * @Method("GET")
     * @Template()
     */
    public function areaspdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Area')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Area:areaspdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
            array('lowquality' => false,
                'print-media-type' => true,
                'encoding' => 'utf-8',
                'page-size' => 'Letter',
                'outline-depth' => 8,
                'orientation' => 'Portrait',
                'title'=> 'Áreas',
                'user-style-sheet'=> 'css/bootstrap.css',
                'header-right'=>'Pag. [page] de [toPage]',
                'header-font-size'=>7,
            )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadeareas.pdf"',
            )
           );

        return $response;

    }
    
    /**
     * Creates a new Area entity.
     *
     * @Route("/pdfshow", name="area_pdf")
     * @Method("GET")
     * @Template()
     */
    public function pdfshowAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Area')->findAll();
        
        /* 
          *
        $this->get('knp_snappy.pdf')->generateFromHtml(
        $this->renderView(
            'ControlDocumentoBundle:Area:pdfshow.html.twig',
            array(
               'entities' => $entities
                )            
        ),
        '/home/aloyo/public_html/Ultra/web/pdf/file.pdf'
        );
        return array('entities' => $entities);   */ 
        
         $html = $this->renderView('ControlDocumentoBundle:Area:pdfshow.html.twig',
            array(
               'entities' => $entities,
                ));

         $response = new Response (
         $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
          200,
          array(
         'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
         'Content-Disposition'   => 'attachment; filename="file.pdf"', 
         'Encoding' => 'UTF-8',
          )
          );
        
         return $response;
        
    }
    
    /**
     * Creates a new Area entity.
     *
     * @Route("/", name="area_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Area:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Area();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El área se agregó con los siguientes datos:'
            );

        return $this->redirect($this->generateUrl('area_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Area entity.
    *
    * @param Area $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Area $entity)
    {
        $form = $this->createForm(new AreaType(), $entity, array(
            'action' => $this->generateUrl('area_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Area entity.
     *
     * @Route("/new", name="area_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Area();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Area entity.
     *
     * @Route("/{id}", name="area_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Area')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el área.');
        }

        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
          //  'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Area entity.
     *
     * @Route("/{id}/edit", name="area_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Area')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el área.');
        }

        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
           // 'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Area entity.
    *
    * @param Area $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Area $entity)
    {
        $form = $this->createForm(new AreaType(), $entity, array(
            'action' => $this->generateUrl('area_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing Area entity.
     *
     * @Route("/{id}", name="area_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Area:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Area')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el área.');
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos del área fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('area_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El área no pudo ser modificada.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Area entity.
     *
     * @Route("/{id}", name="area_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Area')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar el área');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El área fue eliminada con éxito.'
            );
            return $this->redirect($this->generateUrl('area'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'No se puede eliminar el área pues tiene relación con otros datos.'
            );
        }

        return $this->redirect($this->generateUrl('area'));
    }

    /**
     * Creates a form to delete a Area entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('area_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }
}
