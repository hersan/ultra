<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\ExperienciaProfesional;
use Ultra\ControlDocumentoBundle\Form\ExperienciaProfesionalType;

/**
 * ExperienciaProfesional controller.
 *
 * @Route("/experiencia_profesional")
 */
class ExperienciaProfesionalController extends Controller
{

    /**
     * Lists all ExperienciaProfesional entities.
     *
     * @Route("/", name="experiencia_profesional")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:ExperienciaProfesional')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ExperienciaProfesional entity.
     *
     * @Route("/", name="experiencia_profesional_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:ExperienciaProfesional:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ExperienciaProfesional();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('experiencia_profesional_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a ExperienciaProfesional entity.
    *
    * @param ExperienciaProfesional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(ExperienciaProfesional $entity)
    {
        $form = $this->createForm(new ExperienciaProfesionalType(), $entity, array(
            'action' => $this->generateUrl('experiencia_profesional_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ExperienciaProfesional entity.
     *
     * @Route("/new", name="experiencia_profesional_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ExperienciaProfesional();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ExperienciaProfesional entity.
     *
     * @Route("/{id}", name="experiencia_profesional_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:ExperienciaProfesional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExperienciaProfesional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ExperienciaProfesional entity.
     *
     * @Route("/{id}/edit", name="experiencia_profesional_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:ExperienciaProfesional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExperienciaProfesional entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ExperienciaProfesional entity.
    *
    * @param ExperienciaProfesional $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ExperienciaProfesional $entity)
    {
        $form = $this->createForm(new ExperienciaProfesionalType(), $entity, array(
            'action' => $this->generateUrl('experiencia_profesional_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ExperienciaProfesional entity.
     *
     * @Route("/{id}", name="experiencia_profesional_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:ExperienciaProfesional:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:ExperienciaProfesional')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ExperienciaProfesional entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('experiencia_profesional_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ExperienciaProfesional entity.
     *
     * @Route("/{id}", name="experiencia_profesional_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:ExperienciaProfesional')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ExperienciaProfesional entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('experiencia_profesional'));
    }

    /**
     * Creates a form to delete a ExperienciaProfesional entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('experiencia_profesional_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
