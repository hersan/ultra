<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\TipoInstitucion;
use Ultra\ControlDocumentoBundle\Form\TipoInstitucionType;

/**
 * TipoInstitucion controller.
 *
 * @Route("/tipo_institucion")
 */
class TipoInstitucionController extends Controller
{

    /**
     * Lists all TipoInstitucion entities.
     *
     * @Route("/", name="tipo_institucion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:TipoInstitucion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoInstitucion entity.
     *
     * @Route("/", name="tipo_institucion_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:TipoInstitucion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoInstitucion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipo_institucion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a TipoInstitucion entity.
    *
    * @param TipoInstitucion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoInstitucion $entity)
    {
        $form = $this->createForm(new TipoInstitucionType(), $entity, array(
            'action' => $this->generateUrl('tipo_institucion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoInstitucion entity.
     *
     * @Route("/new", name="tipo_institucion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoInstitucion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoInstitucion entity.
     *
     * @Route("/{id}", name="tipo_institucion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoInstitucion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoInstitucion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoInstitucion entity.
     *
     * @Route("/{id}/edit", name="tipo_institucion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoInstitucion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoInstitucion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoInstitucion entity.
    *
    * @param TipoInstitucion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoInstitucion $entity)
    {
        $form = $this->createForm(new TipoInstitucionType(), $entity, array(
            'action' => $this->generateUrl('tipo_institucion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TipoInstitucion entity.
     *
     * @Route("/{id}", name="tipo_institucion_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:TipoInstitucion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoInstitucion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoInstitucion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipo_institucion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoInstitucion entity.
     *
     * @Route("/{id}", name="tipo_institucion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:TipoInstitucion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoInstitucion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipo_institucion'));
    }

    /**
     * Creates a form to delete a TipoInstitucion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipo_institucion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
