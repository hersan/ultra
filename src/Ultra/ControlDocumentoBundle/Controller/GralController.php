<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Class GralController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/Gral", name="gral")
 */
class GralController extends Controller
{
    /**
     * @Route("/index", name="gral_index")
     * @Template("ControlDocumentoBundle:Gral:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();

        $limite=new \DateTime();
        $hoy=new \DateTime();
        $em = $this->getDoctrine()->getManager();

      /*  $limite->modify("+2 months");



        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );

        $procedimientos = $query->getResult();*/

        //CURSOS FALTANTES
        $user = $this->getUser();
        $username=$user->getUsername();

        $faltanext =  Array();
        $faltanint =  Array();
        $bandera=0;

        if ($user->getcurricula()!==null)
        {
            $id=$user->getcurricula()->getId();

            //  Consultar perfil curricula con el id de curricula para recuperar el perfil actual

            //Consulto todas las relaciones Perfil-curricula vigentes
            $qtodos = $em->createQuery(
                "SELECT pc.id,p.id pid,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc
                 FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
                 WHERE pc.fechaBaja>:hoy and c.id= :id
                  ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'id'=>$id));

            $todos = $qtodos->getResult();
            if (empty($todos))
            {
            //NO HAY RELACION PERFIL-CURRICULA VIGENTE
                $bandera=1;
            }
            else
            {
            //recupero el id del perfil actual de la persona
            $pid=$todos[0]['pid'];

                //Consulta para saber si le faltan cursos externos
                $query = $em->createQuery(
                    "SELECT cur.nombre nom
                     FROM ControlDocumentoBundle:Perfil pe JOIN pe.conocimientoEspecial cesp JOIN cesp.curso cur
                     WHERE pe.id=:pid AND cur.id NOT IN
                     (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosExternos ce JOIN ce.cursoExterno cu
                     WHERE c.id=:cid AND ce.fechaVigencia>:hoy)
                     ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'cid'=>$id,'pid'=>$pid));

                //Consulta para saber si le faltan cursos internos
                $query1 = $em->createQuery(
                    "SELECT cur.titulo tit,cur.revision rev FROM ControlDocumentoBundle:Perfil pe JOIN pe.capacitacion cap JOIN cap.capacitacion cur
                     WHERE pe.id=:pid AND cur.id NOT IN
                     (SELECT cu.id FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p JOIN c.cursosInternos ci JOIN ci.cursoInterno cu
                     WHERE c.id=:cid AND ci.fechaVigencia>:hoy)
                     ")->setParameters(array('hoy'=> $hoy->format('Y-m-d'),'cid'=>$id,'pid'=>$pid));

                $entities = $query->getResult();
                $entities1 = $query1->getResult();

                //ladybug_dump_die($entities1);

                foreach ($entities as $ext)
                {
                    $faltanext[]=$ext;
                }
                foreach ($entities1 as $int)
                {
                    $faltanint[]=$int;
                }

                $bandera=2;
               }
        }

        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales
        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (t.titulo = 'general' and d.year='2015')
            ");

        $documentos = $q->getResult();

        //ladybug_dump_die($cumple);

        return array(
            'proyectos' => $proyectos,
            //'documentos'=>$procedimientos,
            'lim'=>$limite,
            'faltanext'=>$faltanext,
            'faltanint'=>$faltanint,
            'user'=>$user,
            'b'=>$bandera,
            'lm'=>$listamaestra,
            'cumple'=>$cumple,
            'doc'=>$documentos
        );
    }

}
