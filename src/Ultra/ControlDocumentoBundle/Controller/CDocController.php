<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\CursoExterno;
use Ultra\ControlDocumentoBundle\Form\CursosCdocType;

/**
 * Class CDocController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/CDoc", name="cdoc")
 */
class CDocController extends Controller
{
    /**
     * @Route("/index", name="cdoc_index")
     * @Template("ControlDocumentoBundle:CDoc:index.html.twig")
     */
    public function indexAction()
    {
        $hoy=new \DateTime();
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales

        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (t.titulo = 'general' and d.year='2015')
            ");

        $documentos = $q->getResult();

        //Consulto los documentos generales

        $q1 = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (d.title='Manual SAD - Control de Documentos')
            ");

        $ayuda = $q1->getResult();


        return array(
            'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite,
            'lm'=>$listamaestra,
            'cumple'=>$cumple,
            'doc'=>$documentos,
            'help'=>$ayuda
        );
    }

    /**
     * @Route("/{type}/lista", name="cdoc_todos")
     * @Template("ControlDocumentoBundle:CDoc:lista.html.twig")
     */
    public function todosAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByCliente($cliente->getId(),$type);
        }

        $tipo=$type;

        return array(
            'procedimientos' => $documentos,
            'tipo'=>$tipo
        );
    }

    /**
     *
     * @Route("/list", name="cdoc_list")
     */
    public function listAction(){
        $query = $this->getDoctrine()
            ->getManager()
            ->createQuery('
                SELECT d.id, d.clave FROM ControlDocumentoBundle:Documento d
                JOIN d.disciplina dis JOIN dis.area a JOIN a.contrato
            ');

        $documentos = $query->getResult();
        $response = new JsonResponse();
        $response->setData($documentos);
        return $response;
    }

    /**
     *
     * @Route("/proy", name="cdoc_proy")
     */
    public function proyectoAction(){
        $query = $this->getDoctrine()
            ->getManager()
            ->createQuery('
                SELECT c.id, c.numeroInterno FROM ControlDocumentoBundle:Contrato c
            ');

        $proyectos = $query->getResult();
        $response = new JsonResponse();
        $response->setData($proyectos);
        return $response;
    }

    /**
     *
     * @Route("/{id}/area", name="cdoc_area")
     */
    public function areaAction($id){
        $query = $this->getDoctrine()
            ->getManager()
            ->createQuery('
                SELECT a.id, a.claveArea FROM ControlDocumentoBundle:Area a
                JOIN a.contrato c
                WITH c.id = :id
            ');
        $query->setParameter('id',$id);
        $areas = $query->getResult();
        $response = new JsonResponse();
        $response->setData($areas);
        return $response;
    }

    /**
     *
     * @Route("/{id}/disc", name="cdoc_disc")
     */
    public function disciplinaAction($id){
        $query = $this->getDoctrine()
            ->getManager()
            ->createQuery('
                SELECT d.id, d.tituloCorto FROM ControlDocumentoBundle:Disciplina d
                JOIN d.area a JOIN a.contrato c
                WITH a.id = :id
            ');
        $query->setParameter('id',$id);
        $disciplina = $query->getResult();
        $response = new JsonResponse();
        $response->setData($disciplina);
        return $response;
    }

    /**
     *
     * @Route("/{id}/docs", name="cdoc_docs")
     */
    public function documentosAction($id){
        $query = $this->getDoctrine()
            ->getManager()
            ->createQuery('
                SELECT p.id, p.titulo FROM ControlDocumentoBundle:Perfil p
                JOIN p.disciplinas disc
                WITH disc.id = :id
            ');
        $query->setParameter('id',$id);
        $documentos = $query->getResult();
        $response = new JsonResponse();
        $response->setData($documentos);
        return $response;
    }



    /**
     * @Route("/{type}/listac", name="cdoc_todos_c")
     * @Template("ControlDocumentoBundle:CDoc:lista_c.html.twig")
     */
    public function listacAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByClienteC($cliente->getId(),$type);
        }

        $tipo=$type;
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos,
            'tipo'=>$tipo
        );
    }

    /**
     * @Route("/{type}/listacm", name="cdoc_todos_cm")
     * @Template("ControlDocumentoBundle:CDoc:lista_cm.html.twig")
     */
    public function listacmAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByClienteCm($cliente->getId(),$type);
        }
        $tipo=$type;
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos,
            'tipo'=>$tipo
        );
    }

    /**
     * @Route("/{type}/lista_o", name="cdoc_todos_o")
     * @Template("ControlDocumentoBundle:CDoc:lista_o.html.twig")
     */
    public function listaoAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByClienteo($cliente->getId(),$type);
        }
        $tipo=$type;
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos,
            'tipo'=>$tipo
        );
    }


    /**
     * @Route("/perfiles", name="cdoc_perfiles")
     * @Template("ControlDocumentoBundle:CDoc:perfiles.html.twig")
     */
    public function perfilAction()
    {
        $documentos = null;
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($cliente->getId());
        }
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }

    /**
     * @Route("/perfiles_c", name="cdoc_perfiles_c")
     * @Template("ControlDocumentoBundle:CDoc:perfiles_c.html.twig")
     */
    public function perfilcAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClientC($cliente->getId());
        }
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }

    /**
     * @Route("/perfiles_cm", name="cdoc_perfiles_cm")
     * @Template("ControlDocumentoBundle:CDoc:perfiles_cm.html.twig")
     */
    public function perfilcmAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClientCm($cliente->getId());
        }
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }

    /**
     * @Route("/perfiles_o", name="cdoc_perfiles_o")
     * @Template("ControlDocumentoBundle:CDoc:perfiles_o.html.twig")
     */
    public function perfiloAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClientO($cliente->getId());
        }
        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }

    /**
     * @Route("/inicio", name="cdoc_inicio")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:Curricula:inicio.html.twig")
     */
    public function inicioAction(){
return array ();
    }



    /**
     * @Route("/showmdoc", name="cdoc_m_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDoc:showmaestratmp.html.twig")
     */
    public function showmdAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();

        foreach( $clientes as $cliente){
            $perfiles[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($cliente->getId());
        }

        foreach( $clientes as $cliente){
            $manuales[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByCliente($cliente->getId(),2);
        }
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByCliente($cliente->getId(),6);
        }

        foreach( $clientes as $cliente){
            $procedimientos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByCliente($cliente->getId(),1);
        }

        foreach( $clientes as $cliente){
            $reglamentos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByCliente($cliente->getId(),5);
        }

        $html = $this->renderView('ControlDocumentoBundle:CDoc:showmaestratmp.html.twig',
            array(
                'perfiles' => $perfiles,
                'manuales' => $manuales,
                'documentos' => $documentos,
                'procedimientos' => $procedimientos,
                'reglamentos' => $reglamentos,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Landscape',
                    'title'=> 'listamaestra',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-left'=>'Hoja [page] de [toPage]',
                    'header-font-size'=>8,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listramaestra.pdf"',
            )
        );

        return $response;

    }


    /**
     * @Route("/showmdocex", name="cdoc_excel")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDoc:showmaestraexcel.html.twig")
     */
    public function showmexcelAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        foreach( $clientes as $cliente){
            $perfiles[$cliente->getSiglas()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClientToExcel($cliente->getId());
        }

        //Consulta para la hoja2

        foreach( $clientes as $cliente){
            $procedimientos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByClienteToExcel($cliente->getId(),1);
        }
        //Consulta para hoja3
        foreach( $clientes as $cliente){
            $manuales[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByClienteToExcel($cliente->getId(),2);
        }

        //Consulta para hoja4
        foreach( $clientes as $cliente){
            $documentos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByClienteToExcel($cliente->getId(),6);
        }

        //Consulta para hoja5
        foreach( $clientes as $cliente){
            $reglamentos[$cliente->getSiglas()] = $this->getDoctrine()
                ->getRepository('ControlDocumentoBundle:Documento')
                ->findByClienteToExcel($cliente->getId(),5);
        }


        //______________________________________________________________EXCEL _______________________________________

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("DSAD-System")
            ->setLastModifiedBy("DSAD-System")
            ->setSubject("lista Maestra")
            ->setDescription("Lista maestra de documentos controlados")
            ->setKeywords("Lista maestra");
        $phpExcelObject->getDefaultStyle()->getFont()->setSize(8);
        //$phpExcelObject->getStyle('A3:E3')->getFont()->setBold(true)->setSize(12);
        $objSheet = $phpExcelObject->getActiveSheet();
        // rename the sheet
        $objSheet->setTitle('Perfiles');
        $objSheet->getStyle('A1:E3')->getFont()->setBold(true)->setSize(10);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->setCellValue('C1', 'LISTA MAESTRA');
        $phpExcelObject->getActiveSheet()->setCellValue('C2', 'PERFILES');
        $phpExcelObject->getActiveSheet()->setCellValue('A3', 'CLAVE');
        $phpExcelObject->getActiveSheet()->setCellValue('B3', 'REV.');
        $phpExcelObject->getActiveSheet()->setCellValue('C3', 'TITULO');
        $phpExcelObject->getActiveSheet()->setCellValue('D3', 'APROBADO');
        $phpExcelObject->getActiveSheet()->setCellValue('E3', 'LIBERADO');
        $phpExcelObject->getActiveSheet()->setCellValue('F3', 'DISTRIBUIDO');
        $phpExcelObject->getActiveSheet()->setCellValue('G3', 'ESTADO');

        $arreglo=array_merge($perfiles['ULTRA'],$perfiles['CFE'],$perfiles['PEMEX']);

        $phpExcelObject->getActiveSheet()
            ->fromArray(
                $arreglo,  // The data to set
                NULL,        // Array values with this value will not be set
                'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );

        //HOJA 2
        $phpExcelObject->createSheet();

// Add some data to the second sheet, resembling some different data types
        $phpExcelObject->setActiveSheetIndex(1);
        $objSheet = $phpExcelObject->getActiveSheet();
        // rename the sheet
        $objSheet->setTitle('Procedimientos');
        $objSheet->getStyle('A1:E3')->getFont()->setBold(true)->setSize(10);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->setCellValue('C1', 'LISTA MAESTRA');
        $phpExcelObject->getActiveSheet()->setCellValue('C2', 'PROCEDIMIENTOS');
        $phpExcelObject->getActiveSheet()->setCellValue('A3', 'CLAVE');
        $phpExcelObject->getActiveSheet()->setCellValue('B3', 'REV.');
        $phpExcelObject->getActiveSheet()->setCellValue('C3', 'TITULO');
        $phpExcelObject->getActiveSheet()->setCellValue('D3', 'APROBADO');
        $phpExcelObject->getActiveSheet()->setCellValue('E3', 'LIBERADO');
        $phpExcelObject->getActiveSheet()->setCellValue('F3', 'DISTRIBUIDO');
        $phpExcelObject->getActiveSheet()->setCellValue('G3', 'ESTADO');


        $arreglop=array_merge($procedimientos['ULTRA'],$procedimientos['CFE'],$procedimientos['PEMEX']);

        $phpExcelObject->getActiveSheet()
            ->fromArray(
                $arreglop,  // The data to set
                NULL,        // Array values with this value will not be set
                'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );

        //HOJA 3
        $phpExcelObject->createSheet();

// Add some data to the second sheet, resembling some different data types
        $phpExcelObject->setActiveSheetIndex(2);
        $objSheet = $phpExcelObject->getActiveSheet();
        // rename the sheet
        $objSheet->setTitle('Manuales');
        $objSheet->getStyle('A1:E3')->getFont()->setBold(true)->setSize(10);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->setCellValue('C1', 'LISTA MAESTRA');
        $phpExcelObject->getActiveSheet()->setCellValue('C2', 'MANUALES');
        $phpExcelObject->getActiveSheet()->setCellValue('A3', 'CLAVE');
        $phpExcelObject->getActiveSheet()->setCellValue('B3', 'REV.');
        $phpExcelObject->getActiveSheet()->setCellValue('C3', 'TITULO');
        $phpExcelObject->getActiveSheet()->setCellValue('D3', 'APROBADO');
        $phpExcelObject->getActiveSheet()->setCellValue('E3', 'LIBERADO');
        $phpExcelObject->getActiveSheet()->setCellValue('F3', 'DISTRIBUIDO');
        $phpExcelObject->getActiveSheet()->setCellValue('G3', 'ESTADO');


        $arreglom=array_merge($manuales['ULTRA'],$manuales['CFE'],$manuales['PEMEX']);

        $phpExcelObject->getActiveSheet()
            ->fromArray(
                $arreglom,  // The data to set
                NULL,        // Array values with this value will not be set
                'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );

        //HOJA 4
        $phpExcelObject->createSheet();

// Add some data to the second sheet, resembling some different data types
        $phpExcelObject->setActiveSheetIndex(3);
        $objSheet = $phpExcelObject->getActiveSheet();
        // rename the sheet
        $objSheet->setTitle('Documentos');
        $objSheet->getStyle('A1:E3')->getFont()->setBold(true)->setSize(10);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->setCellValue('C1', 'LISTA MAESTRA');
        $phpExcelObject->getActiveSheet()->setCellValue('C2', 'DOCUMENTOS');
        $phpExcelObject->getActiveSheet()->setCellValue('A3', 'CLAVE');
        $phpExcelObject->getActiveSheet()->setCellValue('B3', 'REV.');
        $phpExcelObject->getActiveSheet()->setCellValue('C3', 'TITULO');
        $phpExcelObject->getActiveSheet()->setCellValue('D3', 'APROBADO');
        $phpExcelObject->getActiveSheet()->setCellValue('E3', 'LIBERADO');
        $phpExcelObject->getActiveSheet()->setCellValue('F3', 'DISTRIBUIDO');
        $phpExcelObject->getActiveSheet()->setCellValue('G3', 'ESTADO');


        $arreglod=array_merge($documentos['ULTRA'],$documentos['CFE'],$documentos['PEMEX']);

        $phpExcelObject->getActiveSheet()
            ->fromArray(
                $arreglod,  // The data to set
                NULL,        // Array values with this value will not be set
                'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );

        //HOJA 5
        $phpExcelObject->createSheet();

// Add some data to the second sheet, resembling some different data types
        $phpExcelObject->setActiveSheetIndex(4);
        $objSheet = $phpExcelObject->getActiveSheet();
        // rename the sheet
        $objSheet->setTitle('Reglamentos');
        $objSheet->getStyle('A1:E3')->getFont()->setBold(true)->setSize(10);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setAutoSize(true);
        $objSheet->getColumnDimension('C')->setAutoSize(true);
        $objSheet->getColumnDimension('D')->setAutoSize(true);
        $objSheet->getColumnDimension('E')->setAutoSize(true);

        $phpExcelObject->getActiveSheet()->setCellValue('C1', 'LISTA MAESTRA');
        $phpExcelObject->getActiveSheet()->setCellValue('C2', 'REGLAMENTOS');
        $phpExcelObject->getActiveSheet()->setCellValue('A3', 'CLAVE');
        $phpExcelObject->getActiveSheet()->setCellValue('B3', 'REV.');
        $phpExcelObject->getActiveSheet()->setCellValue('C3', 'TITULO');
        $phpExcelObject->getActiveSheet()->setCellValue('D3', 'APROBADO');
        $phpExcelObject->getActiveSheet()->setCellValue('E3', 'LIBERADO');
        $phpExcelObject->getActiveSheet()->setCellValue('F3', 'DISTRIBUIDO');
        $phpExcelObject->getActiveSheet()->setCellValue('G3', 'ESTADO');


        $arreglor=array_merge($reglamentos['ULTRA'],$reglamentos['CFE'],$reglamentos['PEMEX']);

        $phpExcelObject->getActiveSheet()
            ->fromArray(
                $arreglor,  // The data to set
                NULL,        // Array values with this value will not be set
                'A4'         // Top left coordinate of the worksheet range where
            //    we want to set these values (default is A1)
            );
        $phpExcelObject->setActiveSheetIndex();
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=listaMaestra.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;

    }



    /**
     * @Route("/showmdocexxx", name="cdoc_excelxx")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDoc:showmaestraexcel.html.twig")
     */
    public function showmexcelxxAction()
    {
        //consulta que devuelve las claves de los documentos a marcar y los guarda en marcas

        $limite=new \DateTime();
        $marcas[0]='X';

        $limite->modify("-1 months");
        // ladybug_dump_die($limite);


        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d.clave clave FROM ControlDocumentoBundle:Documento d
             WHERE d.actualizado> :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );

        $doc_modificados = $query->getResult();

        $query = $em->createQuery(
            "SELECT p.clave clave FROM ControlDocumentoBundle:Perfil p
             WHERE p.actualizado> :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );

        $per_modificados = $query->getResult();



        foreach ($doc_modificados as $dm)
        {
            $marcas[]=$dm['clave'];
           }

        foreach ($per_modificados as $pm)
        {
            $marcas[]=$pm['clave'];
        }

        //ladybug_dump_die($marcas);

        //$marcas=array ("UPP-P-1.d","UPP-P-1.c)","UPP-P-1.b","UPP-P-10","UPP-P-10.a)","UPP-P-10.c)","UPP-P-10.d)","UPP-P-10.b)","UPP-4.7");


        $contratos = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Contrato')->findAll();

   //  $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findBy(array(),array('nombre'=>"ASC"));
   //  $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->findAll();



//Obtener esta información de la Base de datos:
/*	Estado
 *	1. Vigente
 *	2. Obsoleto
 *	3. Cancelado
*/

      $doc_consulta=array(
                        "documentos" => array ( "id"=>6,"titulo"=>"DOCUMENTOS","estado"=>1) ,
                        "manuales" => array ( "id"=>2,"titulo"=>"MANUALES","estado"=>1),
                        "procedimientos"=> array ( "id"=>1,"titulo"=>"PROCEDIMIENTOS","estado"=>1),
                        "procedimientos1"=> array ( "id"=>1,"titulo"=>"PROCEDIMIENTOS","estado"=>5),
                        "reglamentos" => array( "id"=>5,"titulo"=>"REGLAMENTOS","estado"=>1),
                        "perfiles"=> array("id"=>3,"titulo"=>"PERFILES","estado"=>1),
                        "separador_cancelados" => array ("id"=>"separador", "titulo"=>" ============ CANCELADOS ============="),
                        "documentos_cancelados" => array ( "id"=>6,"titulo"=>"DOCUMENTOS CANCELADOS","estado"=>3) ,
                        "manuales_cancelados" => array ( "id"=>2,"titulo"=>"MANUALES CANCELADOS","estado"=>3),
                        "procedimientos_cancelados"=> array ( "id"=>1,"titulo"=>"PROCEDIMIENTOS CANCELADOS","estado"=>3),
                        "reglamentos_cancelados" => array( "id"=>5,"titulo"=>"REGLAMENTOS CANCELADOS","estado"=>3),
                        "perfiles_cancelados"=> array("id"=>3,"titulo"=>"PERFILES CANCELADOS","estado"=>3),
                        );


      $tiposdoc_orden_hoja['Lista Maestra']=$doc_consulta;

      $tiposdoc_orden_hoja['Documentos']=array(
                                            "documentos" => $doc_consulta['documentos'],
                                            "separador_cancelados" => $doc_consulta['separador_cancelados'],
                                            "documentos_cancelados" =>$doc_consulta['documentos_cancelados']
                            );

      $tiposdoc_orden_hoja['Manuales']=array(
                            "manuales" => $doc_consulta['manuales'],
                             "separador_cancelados" => $doc_consulta['separador_cancelados'],
                            "manuales_cancelados" => $doc_consulta['manuales_cancelados']
                            );

      $tiposdoc_orden_hoja['Procedimientos']= array(
                            "procedimientos"=> $doc_consulta['procedimientos'],
                            "procedimientos1"=> $doc_consulta['procedimientos1'],
                            "separador_cancelados" => $doc_consulta['separador_cancelados'],
                            "procedimientos_cancelados" =>$doc_consulta['procedimientos_cancelados']
                            );


      $tiposdoc_orden_hoja['Reglamentos']= array(
                            "reglamentos"=> $doc_consulta['reglamentos'],
                            "separador_cancelados" => $doc_consulta['separador_cancelados'],
                            "reglamentos_cancelados"=> $doc_consulta['reglamentos_cancelados']
                            );

      $tiposdoc_orden_hoja['Perfiles']=array(
                            "perfiles"=> $doc_consulta['perfiles'],
                            "separador_cancelados" => $doc_consulta['separador_cancelados'],
                            "perfiles_cancelados"=> $doc_consulta['perfiles_cancelados']
                            );

//      $tiposdoc_orden_hoja['Lista Maestra']=$doc_consulta;



foreach ( $tiposdoc_orden_hoja as $tiposdoc_orden )
{
        foreach (  $tiposdoc_orden as $vartipo => $arr_info )
        {
            switch ( $arr_info['id'] ) {
            case "separador":
                ${$vartipo}[0][0] = array( $arr_info['titulo'] );
                break;
            case 3:
                foreach( $contratos as $contrato)
                ${$vartipo}[$contrato->getID()] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByContratoToExcel($contrato->getID(),$arr_info['estado']);
                break;
            default:
                foreach( $contratos as $contrato)
                ${$vartipo}[ $contrato->getID() ] = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByContratoToExcel($contrato->getID(), $arr_info['id'], $arr_info['estado']  );
                break;
                               }
               //  if (  count ( ${$vartipo}[ $contrato->getID() ])  == 0    ) unset (   ${$vartipo}[ $contrato->getID()] );
        }

    //ladybug_dump_die(${'documentos'});
        foreach ($tiposdoc_orden as $vartipo => $arr_info )
        {

            if ( $arr_info['id'] == "separador")
                $arreglo_numeros[$vartipo][]="1";
            if ( $arr_info['id'] != "separador")
                foreach( $contratos as $contrato)
                {
                    $valor01=0;
                    if ( isset ( $arreglo_numeros[$vartipo][$contrato->getID()]  ) )    unset (  $arreglo_numeros[$vartipo][$contrato->getID()]  );

                        foreach ( array_keys ( ${$vartipo}[$contrato->getID()] )  as $valor )
                        {
                            $valor01=$valor+1;
                            $arreglo_numeros[$vartipo][$contrato->getID()][] = array( (string) $valor01 );

                        }
                }
        }


        foreach ($tiposdoc_orden as $vartipo => $arr_info )
            {

		    if ( isset ( $arreglo_numeros[$vartipo] ) )
                ${$vartipo}=array_replace_recursive($arreglo_numeros[$vartipo], ${$vartipo});

            }



           foreach ($tiposdoc_orden as $vartipo => $arr_info )
       {
           foreach( $contratos as $contrato)

		    if ( $arr_info['id'] == 3 )
		    {
			    $total_disciplinas=0;
			    $disciplina_anterior = "";
			    $row_x[$vartipo]=1;

			   // if (   isset (  ${$vartipo}[$contrato->getID()] )   )
            foreach ( ${$vartipo}[$contrato->getID()]  as $arr_renglon_perfil )
            {
                if ( isset ($arr_renglon_perfil['Disciplina'] ) AND $arr_renglon_perfil['Disciplina'] == $disciplina_anterior )
                        {
                            unset ($arr_renglon_perfil['Disciplina']);
                            $total_disciplinas++;
                        }


                if ( isset($arr_renglon_perfil['Disciplina'] ))
                {
                $perfiles_temp[$contrato->getID()][]  = array ( $arr_renglon_perfil['Disciplina'] );
                $disciplina_anterior = $arr_renglon_perfil['Disciplina'] ;
                }

                unset ($arr_renglon_perfil['Disciplina']);
                $perfiles_temp[$contrato->getID()][]  = $arr_renglon_perfil ;

                $row_x[$vartipo]++;
            }

            if ( isset ($perfiles_temp))  ${$vartipo}[$contrato->getID()]=$perfiles_temp[$contrato->getID()];
            unset ( $perfiles_temp );


		    }
       }

}
        //______________________________________________________________EXCEL _______________________________________

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("SAD")
            ->setLastModifiedBy("SAD")
            ->setTitle("Lista Maestra de Documentos Controlados")
            ->setSubject("Lista maestra de documentos controlados generada por SAD")
            ->setDescription("Lista maestra de documentos controlados (Reporte)")
            ->setKeywords("Lista maestra")
            ->setCompany("Ultra Ingeniería S.A. de C.V.")
            ->setCategory("Export");
         $phpExcelObject->getDefaultStyle()->getFont()->setName('Arial');
        $phpExcelObject->getDefaultStyle()->getFont()->setSize(8);

        $lineatitulos[]=array("No"=>"No.","clave"=>'CLAVE',"revision"=>'REV.',"titulo"=>'TÍTULO',"Fecha"=>'FECHA',"liberado"=>'LIBERADO',"transmitido"=>'DISTRIBUIDO',"descripcion"=>'EDO.');


       $HOJAACTIVA = 0;
        //ladybug_dump($tiposdoc_orden_hoja);
        foreach ( $tiposdoc_orden_hoja as $clave => $tiposdoc_orden )
        {

            //inicializacion de variables por cada pestaña de excel
          $arreglo_ListaMaestra=array();
          $style_overlay_encabezado=array();
          $mergecells_jf=array();
          $renglones_disciplina=array();
          unset ( $separador_anterior );
            //ladybug_dump_die($arreglo_ListaMaestra);

          //if ( isset (  $arreglo_ListaMaestra )  )  $arreglo_ListaMaestra=array();
         if ( $HOJAACTIVA >0 )
         {
                $phpExcelObject->createSheet();
                $phpExcelObject->setActiveSheetIndex($HOJAACTIVA);
         }
        $HOJAACTIVA++;
        $objSheet = $phpExcelObject->getActiveSheet();

        $objSheet->setTitle($clave);
        $objSheet->getColumnDimension('A')->setAutoSize(true);
        $objSheet->getColumnDimension('B')->setWidth(13);
        $objSheet->getColumnDimension('C')->setWidth(5);
        $objSheet->getColumnDimension('D')->setWidth(107);
        $objSheet->getColumnDimension('E')->setWidth(10);
        $objSheet->getColumnDimension('F')->setWidth(13);
        $objSheet->getColumnDimension('G')->setWidth(14);
        $objSheet->getColumnDimension('H')->setWidth(10);
	    $objSheet->getColumnDimension('I')->setWidth(1);

        $row_LM=1;

        $style_overlay_encabezado_general['fill']['type'] = \PHPExcel_Style_Fill::FILL_SOLID;
        $style_overlay_encabezado_general['alignment']['vertical'] = "center";
        $style_overlay_encabezado_general['alignment']['horizontal'] = "center";
//no furula le gana el wrap true de abajo        $style_overlay_encabezado_general['alignment']['wrap']='false';
//        $style_overlay_encabezado_general['fill']['color']['rgb'] = "c6d9f1";

        foreach ($tiposdoc_orden as $tipo => $arr_info )
             {


         if ( $arr_info['id'] == "separador" )    $separador_anterior=array(array($arr_info['titulo']));
            else
         if ( isset ( $arreglo_numeros[$tipo]) )    // entonces si hay datos en el arreglo
         {
//  Estilos y formato   separaadores Encabezados     [título].
                 if ( isset ( $separador_anterior) )
                 {
                 $cellRange_LM = "A".$row_LM.':'."H".$row_LM;
                 $style_overlay_encabezado[$clave][$cellRange_LM]['fill']['color']['rgb'] = "8c8c00";
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['bold'] = true;
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['size'] = 14;
                 $style_overlay_encabezado[$clave][$cellRange_LM]=array_merge_recursive($style_overlay_encabezado[$clave][$cellRange_LM],$style_overlay_encabezado_general);
                 $mergecells_jf[$clave][$row_LM]=true;

                     $arreglo_ListaMaestra=array_merge ( $arreglo_ListaMaestra,  $separador_anterior);
                    // ladybug_dump_die($arreglo_ListaMaestra);
                     unset ( $separador_anterior );
                     $row_LM=$row_LM+1;

                 }
                 $cellRange_LM = "A".$row_LM.':'."H".$row_LM;
                 $style_overlay_encabezado[$clave][$cellRange_LM]['fill']['color']['rgb'] = "c6d9f1";
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['bold'] = true;
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['size'] = 14;
                 $style_overlay_encabezado[$clave][$cellRange_LM]=array_merge_recursive($style_overlay_encabezado[$clave][$cellRange_LM],$style_overlay_encabezado_general);
                 $mergecells_jf[$clave][$row_LM]=true;

              $arreglo_ListaMaestra=array_merge ( $arreglo_ListaMaestra, array(array($arr_info['titulo'])));
              $row_LM=$row_LM+1;
             //ladybug_dump_die($arreglo_ListaMaestra);

// Estilos y formato  titulos columnas
                 $cellRange_LM = "A".($row_LM).':'."H".($row_LM );
                 $style_overlay_encabezado[$clave][$cellRange_LM]['fill']['color']['rgb'] = "c6d9f1";
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['bold'] = false;
                 $style_overlay_encabezado[$clave][$cellRange_LM]['font']['size'] = 8;
                 $style_overlay_encabezado[$clave][$cellRange_LM]=array_merge_recursive($style_overlay_encabezado[$clave][$cellRange_LM],$style_overlay_encabezado_general);
                 $altorenglon[$clave][$row_LM]=10;

                 $row_LM=$row_LM+1;

                $arreglo_ListaMaestra=array_merge ( $arreglo_ListaMaestra,$lineatitulos );


                  foreach( $contratos as $contrato)
                  {
                  if ( isset (   ${$tipo}[$contrato->getID()]   )   AND     count(  ${$tipo}[$contrato->getID()]      )   >   0    )
                    {
                        //ladybug_dump_die($tipo);
      //           Estilos  y formato Separadores de clientes
                      $cellRange_LM = "A".$row_LM.':'."H".$row_LM;
                      $style_overlay_encabezado[$clave][$cellRange_LM]['font']['bold'] = false;
                      $style_overlay_encabezado[$clave][$cellRange_LM]['font']['size'] = 8;
                      $style_overlay_encabezado[$clave][$cellRange_LM]['fill']['color']['rgb'] = "bbb7b5";
                      $style_overlay_encabezado[$clave][$cellRange_LM]=array_merge_recursive($style_overlay_encabezado[$clave][$cellRange_LM],$style_overlay_encabezado_general);
                      $mergecells_jf[$clave][$row_LM]=true;


                 $arreglo_ListaMaestra = array_merge ( $arreglo_ListaMaestra , array(array($contrato->getCliente()->getNombre()." - CONTRATO No. ".$contrato->getNumeroExterno()." ( P ".$contrato->getNumeroInterno()." )" )), ${$tipo}[$contrato->getID()] ) ;

                 $row_LM = $row_LM + count ( ${$tipo}[$contrato->getID()] ) + 1;
                    }

                  }


         }    //fin si hay datos en el arreglo
             }


//ListaMaestra

            $objSheet->getStyle('A1:A'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objSheet->getStyle( 'A1:'."H".($row_LM-1) )->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objSheet->getStyle('A1:A'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objSheet->getStyle('B1:B'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getStyle('C1:C'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getStyle('E1:E'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getStyle('F1:H'.$row_LM)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objSheet->getStyle('A1:H'.$row_LM)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objSheet->getStyle('A1:H'.$row_LM)->getAlignment()->setWrapText(true);


        $renglon = 1;
//
        foreach ( $arreglo_ListaMaestra as $arreglo_renglon )
        {

            if ( count ($arreglo_renglon) < 2  AND ! isset ( $mergecells_jf[$clave][$renglon])  )
            {
                $mergecells_jf[$clave][$renglon]=true;
                $renglones_disciplina[]=$renglon;

            }


                foreach ( $marcas as $marca)
                    if ( isset ( $arreglo_renglon['clave'] ) AND $marca == $arreglo_renglon['clave'] AND  $arreglo_renglon['descripcion'] == 'Vigente' )
                          {
                      $style_overlay_encabezado[$clave]["I".$renglon]['borders']['right']['style'] = \PHPExcel_Style_Border::BORDER_THIN;
                      }

                    $renglon++;


        }

	foreach ( $style_overlay_encabezado[$clave] as $rango => $arreglo_estilo )
		{
		$objSheet->getStyle($rango)->applyFromArray($arreglo_estilo);
		}   

	foreach ( $altorenglon[$clave]  as $renglon => $altura )
	          $objSheet->getRowDimension($renglon)->setRowHeight($altura);



	if (isset ($renglones_disciplina))
        foreach ( $renglones_disciplina as $renglon )
            {
                $objSheet->mergeCells("A".$renglon.":"."H".$renglon);
                $objSheet->getStyle('A'.$renglon)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->getStyle('A'.$renglon)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objSheet->getStyle('A'.$renglon)->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID);
                $objSheet->getStyle('A'.$renglon)->getFill()->getStartColor()->setRGB('F0F0F0');
                $objSheet->getStyle('A'.$renglon)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objSheet->getStyle('A'.$renglon)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $objSheet->getStyle('A'.$renglon)->getFont()->setBold(true);
            }

         foreach ( $mergecells_jf[$clave] as $renglon=> $activado )
                 $objSheet->mergeCells("A".$renglon.":"."H".$renglon);
            //ladybug_dump_die($arreglo_ListaMaestra);

            $objSheet->fromArray(
                            $arreglo_ListaMaestra  // The data to set
                          , NULL            // Array values with this value will not be set
                          , 'A1'         // Top left coordinate of the worksheet range where we want to set these values (default is A1)
                          , true                     //  El true es para  mixed strictNullComparison para que el "0" no sea NULO
                        );


//Agregando texto al final del documento de Excel.
        $jf_col="A";
        $jf_row=$row_LM+4;
        $objSheet->mergeCells("A".$jf_row.":H".$jf_row );
        $objSheet->setCellValue($jf_col.$jf_row, "                                                              ELABORÓ                                                                                                                                                 APROBÓ");
        $jf_row=$jf_row+4;
        $objSheet->mergeCells("A".$jf_row.":H".$jf_row );

        $objRichText = new \PHPExcel_RichText();

        $objRichText->createText('                               ');

        $objSubrayado = $objRichText->createTextRun('       TEC. Leandro Rivera Gómez       ');
        $objSubrayado->getFont()->setUnderline(true);
        $objSubrayado->getFont()->setSize(11);
        $objSubrayado->getFont()->setName('Arial');

        $objRichText->createText('                                                                             ');

        $objSubrayado_01 = $objRichText->createTextRun('       L.C.P. Daniel Galindo García      ');
        $objSubrayado_01->getFont()->setUnderline(true);
        $objSubrayado_01->getFont()->setSize(11);
        $objSubrayado_01->getFont()->setName('Arial');

        $objSheet->getCell($jf_col.$jf_row)->setValue($objRichText);

    //    $objSheet->getStyle($jf_col.$jf_row)->getFont()->setName('Arial');
    //     $objSheet->getStyle($jf_col.$jf_row)->getFont()->setSize(8);

        $jf_row=$jf_row+1;
        $objSheet->mergeCells("A".$jf_row.":H".$jf_row );
        $objSheet->setCellValue($jf_col.$jf_row, "                                        Encargado de Control de Documentos                                                                                                                Jefe de Administración ");


        //jorgefrancisco orientacion de hoja para impresion en duro
        $objSheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        setlocale(LC_ALL,"es_MX");
        $fechadehoy=strftime("%d/%B/%Y");
        $fechadehoygbajo=strtoupper( strftime("%d_%B_%Y")   );

        $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

        $manana= new \DateTime();
        $manana->modify('+1 day');

       // $fechadehoy_encabezado=$manana->format('d/m/Y');

        $fechadehoy_encabezado=" ".$manana->format('d')."/".$meses[$manana->format('m')-1]. "/".$manana->format('Y');


//strtoupper(

        $objDrawing = new \PHPExcel_Worksheet_HeaderFooterDrawing();
        $objDrawing->setPath('./img/logo_ultra_ListaMaestra.png');
        //$objDrawing->setHeight(72);
        //$objDrawing->setWidth(200);
        $objSheet->getHeaderFooter()->addImage($objDrawing, \PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);

        //$objPHPExcel->getActiveSheet()->getPageMargins()->setHeader( 10 );
        $objSheet->getPageMargins()->setTop( 1.2 ) ;    //  la medida es en pulgadas.
        $objSheet->getHeaderFooter()->setOddHeader('&L&G &C CONTROL DE DOCUMENTOS Y REGISTROS '."\n\n\n".'&BAnexo 2. Lista Maestra de Documentos Controlados. &R UCT-01 REV. 18'."\n".' Página 10 de 17'."\n".' 18/FEB/13'."\n\n".'HOJA &P DE &N'."\n".'FECHA: '.$fechadehoy_encabezado);


}

                $phpExcelObject->setActiveSheetIndex(0);
                // create the writer
                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
                // create the response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                // adding headers
                $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                $response->headers->set('Content-Disposition', 'attachment;filename=ListaMaestra_'.$fechadehoygbajo.'.xlsx');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'max-age=0');

                return $response;

    }

    /**
     * Displays a form to editcursos an existing CDoc entity.
     *
     * @Route("/{id}/editcursos", name="cdoc_editcursos")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }
        if($entity->getCursosExternos()->isEmpty()){
            $entity->addCursosExterno(new CursoExterno());
        }
//ladybug_dump_die($entity);
        $editForm = $this->createEditForm($entity);
        //$deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //  'delete_form' => $deleteForm->createView(),
        );

    }

    /**
     * Creates a form to edit a CDoc entity.
     *
     * @param Curricula $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Curricula $entity)
    {
        $form = $this->createForm(new CursosCdocType(), $entity, array(
            'action' => $this->generateUrl('cdoc_cursos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('cursos')
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }


    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}/", name="cdoc_cursos_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CDoc:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }


        $cursosExternos = new ArrayCollection();
        foreach($entity->getCursosExternos() as $cursos){
            $cursosExternos->add($cursos);
        }

        $cursosInternos = new ArrayCollection();
        foreach($entity->getCursosInternos() as $cursos){
            $cursosInternos->add($cursos);
        }

        //$deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            foreach($cursosInternos as $cursos){
                if(false === $entity->getCursosInternos()->contains($cursos)){
                    $cursos->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($cursos);
                }
            }

            foreach($cursosExternos as $cursos){
                if(false === $entity->getCursosExternos()->contains($cursos)){
                    $cursos->setCurricula(null);
                    //$em->persist($training);
                    $em->remove($cursos);
                }
            }

            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la curricula fueron modificados.'
            );

            return $this->redirect($this->generateUrl('cdoc_cursos_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La curricula no fue modificada.'
            );
        }


        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        );

    }

    /**
     * Finds and displays a Curricula entity.
     *
     * @Route("/{id}/cdocshow", name="cdoc_cursos_show")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDoc:show.html.twig")
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la Curricula.');
        }
       // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
         //   'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Lists all Curricula entities.
     *
     * @Route("/actCursos", name="actualiza_Cursos")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CDoc:cursos.html.twig")
     */
    public function actualizaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();


        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        //return $this->render('ControlDocumentoBundle:Curricula:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }


}
