<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\TipoProcedimiento;
use Ultra\ControlDocumentoBundle\Form\TipoProcedimientoType;

/**
 * TipoProcedimiento controller.
 *
 * @Route("/tipoprocedimiento")
 */
class TipoProcedimientoController extends Controller
{

    /**
     * Lists all TipoProcedimiento entities.
     *
     * @Route("/", name="tipoprocedimiento")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:TipoProcedimiento')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoProcedimiento entity.
     *
     * @Route("/", name="tipoprocedimiento_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:TipoProcedimiento:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoProcedimiento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoprocedimiento_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a TipoProcedimiento entity.
    *
    * @param TipoProcedimiento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoProcedimiento $entity)
    {
        $form = $this->createForm(new TipoProcedimientoType(), $entity, array(
            'action' => $this->generateUrl('tipoprocedimiento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }

    /**
     * Displays a form to create a new TipoProcedimiento entity.
     *
     * @Route("/new", name="tipoprocedimiento_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoProcedimiento();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoProcedimiento entity.
     *
     * @Route("/{id}", name="tipoprocedimiento_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoProcedimiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProcedimiento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoProcedimiento entity.
     *
     * @Route("/{id}/edit", name="tipoprocedimiento_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoProcedimiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProcedimiento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoProcedimiento entity.
    *
    * @param TipoProcedimiento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoProcedimiento $entity)
    {
        $form = $this->createForm(new TipoProcedimientoType(), $entity, array(
            'action' => $this->generateUrl('tipoprocedimiento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Update',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }
    /**
     * Edits an existing TipoProcedimiento entity.
     *
     * @Route("/{id}", name="tipoprocedimiento_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:TipoProcedimiento:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoProcedimiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoProcedimiento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipoprocedimiento_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoProcedimiento entity.
     *
     * @Route("/{id}", name="tipoprocedimiento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:TipoProcedimiento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoProcedimiento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoprocedimiento'));
    }

    /**
     * Creates a form to delete a TipoProcedimiento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipoprocedimiento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                    'label' => 'Delete',
                    'attr' => array('class' => 'btn btn-default')
                ))
            ->getForm()
        ;
    }
}
