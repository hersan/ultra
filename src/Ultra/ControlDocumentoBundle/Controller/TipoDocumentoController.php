<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\TipoDocumento;
use Ultra\ControlDocumentoBundle\Form\TipoDocumentoType;

/**
 * TipoDocumento controller.
 *
 * @Route("/tipodocumento")
 */
class TipoDocumentoController extends Controller
{

    /**
     * Lists all TipoDocumento entities.
     *
     * @Route("/", name="tipodocumento")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->findAll();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        //return $this->render('ControlDocumentoBundle:TipoDocumento:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TipoDocumento entity.
     *
     * @Route("/", name="tipodocumento_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:TipoDocumento:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TipoDocumento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El tipo de documento fue agregado con éxito.'
            );
            return $this->redirect($this->generateUrl('tipodocumento_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El tipo de documento no fue agregado.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a TipoDocumento entity.
    *
    * @param TipoDocumento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(TipoDocumento $entity)
    {
        $form = $this->createForm(new TipoDocumentoType(), $entity, array(
            'action' => $this->generateUrl('tipodocumento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new TipoDocumento entity.
     *
     * @Route("/new", name="tipodocumento_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new TipoDocumento();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TipoDocumento entity.
     *
     * @Route("/{id}", name="tipodocumento_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoDocumento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing TipoDocumento entity.
     *
     * @Route("/{id}/edit", name="tipodocumento_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoDocumento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TipoDocumento entity.
    *
    * @param TipoDocumento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoDocumento $entity)
    {
        $form = $this->createForm(new TipoDocumentoType(), $entity, array(
            'action' => $this->generateUrl('tipodocumento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing TipoDocumento entity.
     *
     * @Route("/{id}", name="tipodocumento_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:TipoDocumento:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoDocumento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El tipo de documento fue actualizado con éxito.'
            );
            return $this->redirect($this->generateUrl('tipodocumento_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'success',
                'El tipo de documento no fue actualizado.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TipoDocumento entity.
     *
     * @Route("/{id}", name="tipodocumento_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:TipoDocumento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoDocumento entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El tipo de documento fue eliminado con éxito.'
            );
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El tipo de documento no fue eliminado.'
            );
        }

        return $this->redirect($this->generateUrl('tipodocumento'));
    }

    /**
     * Creates a form to delete a TipoDocumento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipodocumento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
                ))
            ->getForm()
        ;
    }
}
