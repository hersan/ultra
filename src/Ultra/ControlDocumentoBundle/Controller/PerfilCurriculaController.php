<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\PdfCertificado;
use Ultra\ControlDocumentoBundle\Entity\PerfilCurricula;
use Ultra\ControlDocumentoBundle\Form\PerfilCurriculaType;
use Ultra\ControlDocumentoBundle\Entity\Contrato;
use Ultra\ControlDocumentoBundle\Model\SettingUpload;
/**
 * PerfilCurricula controller.
 *
 * @Route("/perfil_curricula")
 */
class PerfilCurriculaController extends Controller
{

    /**
     * Lists all PerfilCurricula entities.
     *
     * @Route("/", name="perfil_curricula")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Contrato')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all PerfilCurricula entities.
     *
     * @Route("/list/{id}/discipline", name="perfil_curricula_list_discipline")
     * @Method("GET")
     * @Template()
     */
    public function listDisciplineAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository('ControlDocumentoBundle:Contrato')->findOneBy(array('id'=>$id));
        //ladybug_dump_die($contrato);
        if(!$contrato){
            throw $this->createNotFoundException('El contrato no existe');
        }
        $query = $em->createQuery(
            "SELECT dis.titulo, dis.id FROM ControlDocumentoBundle:Contrato c
             JOIN c.areas a JOIN a.disciplinas dis WITH c.id = :id"
             )->setParameters(array(
                    'id' => $id,
                ));

        $pagination = $query->getResult();
             
 
        return array('pagination' => $pagination, 'contrato' => $contrato);
        
    }

    /**
     * Lists all PerfilCurricula entities.
     *
     * @Route("/list/{id}/{con}/job_profile", name="perfil_curricula_list_job_profile")
     * @Method("GET")
     * @Template()
     */
    public function listJobProfileAction($id, $con){
        $em = $this->getDoctrine()->getManager();

        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($id);

        if(!$disciplina){
            $this->createNotFoundException('No existe la disciplina');
        }

        $query = $em->createQuery(
            "SELECT pf.clave,pf.titulo,pf.revision, pcc.nombre,pcc.apellidoPaterno,pcc.apellidoMaterno, pc.id FROM ControlDocumentoBundle:Disciplina dis
             JOIN dis.perfiles pf JOIN pf.perfiles pc JOIN pc.curricula pcc WITH dis.id = :id AND pc.contrato= :con"
        )->setParameters(array(
                    'id' => $id,
                    'con'=> $con
                ));
        $pagination = $query->getResult();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query->getResult(),$this->get('request')->query->get('page', 1),10);
             
 
        return array('pagination' => $pagination, 'disciplina' => $disciplina);
    }

    /**
     * Creates a new PerfilCurricula entity.
     *
     * @Route("/", name="perfil_curricula_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:PerfilCurricula:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new PerfilCurricula();
        $post = $request->request->get('ultra_controldocumentobundle_perfilcurricula');
        $id = $post['disciplina_id'];
        //ladybug_dump_die($id);
        $form = $this->createCreateForm($entity, $id);
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La curricula quedo relacionada'
            );
            return $this->redirect($this->generateUrl('perfil_curricula_list_job_profile', array('id' => $id,'con'=>3697)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La curricula no se relacionó.'
            );
        }

        return array(
            'id' => $id,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a PerfilCurricula entity.
     *
     * @param PerfilCurricula $entity The entity
     * @param $id
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PerfilCurricula $entity, $id)
    {
        $form = $this->createForm(new PerfilCurriculaType(), $entity, array(
            'action' => $this->generateUrl('perfil_curricula_create'),
            'method' => 'POST',
            'disciplina_id' => $id
        ));

        $form->add('disciplina_id', 'hidden',array(
                'data'=>$id,
                'required' => false,
                'mapped' => false,
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos')));

        return $form;
    }

    /**
     * Displays a form to create a new PerfilCurricula entity.
     *
     * @Route("/{id}/new", name="perfil_curricula_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($id)
    {
        $entity = new PerfilCurricula();
        $em = $this->getDoctrine()->getManager();
        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($id);
        $form = $this->createCreateForm($entity, $disciplina->getId());
        return array(
            'disciplina' => $disciplina,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a PerfilCurricula entity.
     *
     * @Route("/{id}/{dis}", name="perfil_curricula_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id,$dis)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:PerfilCurricula')->find($id);
        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($dis);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la relación.');
        }

        $deleteForm = $this->createDeleteForm($entity, $disciplina->getId());

        return array(
            'disciplina' => $disciplina,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing PerfilCurricula entity.
     *
     * @Route("/{id}/{dis}/edit", name="perfil_curricula_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $dis)
    {
        $em = $this->getDoctrine()->getManager();

        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($dis);
        $entity = $em->getRepository('ControlDocumentoBundle:PerfilCurricula')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la relación Perfil-Curricula.');
        }

        $editForm = $this->createEditForm($entity, $disciplina->getId());
       // $deleteForm = $this->createDeleteForm($entity, $disciplina->getId());

        return array(
            'disciplina' => $disciplina,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
         //   'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a PerfilCurricula entity.
     *
     * @param PerfilCurricula $entity The entity
     * @param $id
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(PerfilCurricula $entity, $id)
    {
        $form = $this->createForm(new PerfilCurriculaType(), $entity, array(
            'action' => $this->generateUrl('perfil_curricula_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'disciplina_id'=>$id
        ));

        $form->add('disciplina_id', 'hidden',array(
                'data'=>$id,
                'required' => false,
                'mapped' => false,
            ));
        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));

        return $form;
    }

    /**
     * Edits an existing PerfilCurricula entity.
     *
     * @Route("/{id}", name="perfil_curricula_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:PerfilCurricula:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $post = $request->request->get('ultra_controldocumentobundle_perfilcurricula');
        $dis = $post['disciplina_id'];
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:PerfilCurricula')->find($id);
        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($dis);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la relación Perfil-curricula.');
        }

        /*if(!$entity->getEvaluaciones()->isEmpty())
        {
            $entity->getEvaluaciones()->clear();
        }*/
        //$deleteForm = $this->createDeleteForm($entity, $disciplina->getId());
        $editForm = $this->createEditForm($entity, $disciplina->getId());
        $editForm->handleRequest($request);

        //valido las fechas
        $fechaalta=$entity->getFechaAlta();
        $fechabaja=$entity->getFechaBaja();

        if ($fechaalta<$fechabaja)
        {
            if ($editForm->isValid()) {

                /*$data = $editForm->getData();
                if(!$entity->getEvaluaciones()->isEmpty())
                {
                    foreach($entity->getEvaluaciones() as $evaluacion)
                    {
                        if($evaluacion instanceof PdfCertificado){
                            $evaluacion->setFile($data->getEvaluaciones()->getCertificado()->getFile());
                        }
                    }
                }*/

                $em->flush();

                $this->get('session')->getFlashBag()->add('success','La relación se actualizó');
                return $this->redirect($this->generateUrl('perfil_curricula_show', array('dis' => $disciplina->getId(),'id'=>$id)));
            }
            else
            {
                $this->get('session')->getFlashBag()->add(
                    'danger',
                    'La relación no pudo ser actualizada.'
                );
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La fecha de alta es mayor que la baja, no se agregó la curricula.'
            );
        }

        return array(
            'disciplina' => $disciplina,
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
          //  'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a PerfilCurricula entity.
     *
     * @Route("/{id}", name="perfil_curricula_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $post = $request->request->get('ultra_controldocumentobundle_perfilcurricula');
        $dis = $post['disciplina_id'];
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:PerfilCurricula')->find($id);
        $disciplina = $em->getRepository('ControlDocumentoBundle:Disciplina')->find($dis);

        $form = $this->createDeleteForm($entity, $disciplina->getId());
        $form->handleRequest($request);

        if ($form->isValid()) {

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PerfilCurricula entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('perfil_curricula'));
    }

    /**
     * Creates a form to delete a PerfilCurricula entity by id.
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\PerfilCurricula $entity
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PerfilCurricula $entity, $id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('perfil_curricula_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('disciplina_id', 'hidden',array(
                    'data'=>$id,
                    'required' => false,
                    'mapped' => false,
                ))
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar' ,
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
