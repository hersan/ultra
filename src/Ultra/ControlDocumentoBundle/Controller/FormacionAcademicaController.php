<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\FormacionAcademica;
use Ultra\ControlDocumentoBundle\Form\FormacionAcademicaType;

/**
 * FormacionAcademica controller.
 *
 * @Route("/formacion/academica")
 */
class FormacionAcademicaController extends Controller
{

    /**
     * Lists all FormacionAcademica entities.
     *
     * @Route("/", name="formacion_academica")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:FormacionAcademica')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new FormacionAcademica entity.
     *
     * @Route("/", name="formacion_academica_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:FormacionAcademica:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new FormacionAcademica();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //$em = $this->getDoctrine()->getManager();
            //$em->persist($entity);
            //$em->flush();

            //return $this->redirect($this->generateUrl('formacion_academica_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a FormacionAcademica entity.
    *
    * @param FormacionAcademica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(FormacionAcademica $entity)
    {
        $form = $this->createForm(new FormacionAcademicaType(), $entity, array(
            'action' => $this->generateUrl('formacion_academica_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FormacionAcademica entity.
     *
     * @Route("/new", name="formacion_academica_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new FormacionAcademica();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a FormacionAcademica entity.
     *
     * @Route("/{id}", name="formacion_academica_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormacionAcademica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing FormacionAcademica entity.
     *
     * @Route("/{id}/edit", name="formacion_academica_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormacionAcademica entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a FormacionAcademica entity.
    *
    * @param FormacionAcademica $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FormacionAcademica $entity)
    {
        $form = $this->createForm(new FormacionAcademicaType(), $entity, array(
            'action' => $this->generateUrl('formacion_academica_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FormacionAcademica entity.
     *
     * @Route("/{id}", name="formacion_academica_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:FormacionAcademica:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormacionAcademica')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormacionAcademica entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('formacion_academica_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a FormacionAcademica entity.
     *
     * @Route("/{id}", name="formacion_academica_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:FormacionAcademica')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FormacionAcademica entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('formacion_academica'));
    }

    /**
     * Creates a form to delete a FormacionAcademica entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('formacion_academica_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
