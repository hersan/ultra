<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Profesion;
use Ultra\ControlDocumentoBundle\Form\ProfesionType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Profesion controller.
 *
 * @Route("/profesion")
 */
class ProfesionController extends Controller
{

    /**
     * Lists all Profesion entities.
     *
     * @Route("/", name="profesion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Profesion')->findAll();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        // return $this->render('ControlDocumentoBundle:Profesion:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Profesion entity.
     *
     * @Route("/profesionespdf", name="profesiones_pdf")
     * @Method("GET")
     * @Template()
     */
    public function profesionespdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Profesion')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Profesion:profesionespdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Áreas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadeprofesiones.pdf"',
            )
        );

        return $response;

    }

    /**
     * Creates a new Profesion entity.
     *
     * @Route("/", name="profesion_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Profesion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Profesion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La profesión fue agregada con éxito.'
            );
            return $this->redirect($this->generateUrl('profesion_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La profesión no se agregó.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Profesion entity.
    *
    * @param Profesion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Profesion $entity)
    {
        $form = $this->createForm(new ProfesionType(), $entity, array(
            'action' => $this->generateUrl('profesion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
            'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Profesion entity.
     *
     * @Route("/new", name="profesion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Profesion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Profesion entity.
     *
     * @Route("/{id}", name="profesion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Profesion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la profesión.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Profesion entity.
     *
     * @Route("/{id}/edit", name="profesion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Profesion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la profesión.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Profesion entity.
    *
    * @param Profesion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Profesion $entity)
    {
        $form = $this->createForm(new ProfesionType(), $entity, array(
            'action' => $this->generateUrl('profesion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
            'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }
    /**
     * Edits an existing Profesion entity.
     *
     * @Route("/{id}", name="profesion_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Profesion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Profesion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Profesion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La profesión fue actualizada con éxito.'
            );

            return $this->redirect($this->generateUrl('profesion_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La profesión fue actualizada con éxito.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Profesion entity.
     *
     * @Route("/{id}", name="profesion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Profesion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Profesion entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La profesión fue eliminada con éxito.'
            );
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La profesión no fue eliminada.'
            );
        }

        return $this->redirect($this->generateUrl('profesion'));
    }

    /**
     * Creates a form to delete a Profesion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('profesion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                'label' => 'Borrar',
                'attr' => array('class'=>'btn btn-warning',
                'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
            ;
    }
}
