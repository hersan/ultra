<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Condicion;
use Ultra\ControlDocumentoBundle\Form\CondicionType;

/**
 * Condicion controller.
 *
 * @Route("/condicion")
 */
class CondicionController extends Controller
{

    /**
     * Lists all Condicion entities.
     *
     * @Route("/", name="condicion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
       $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Condicion')->findAll();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        //return $this->render('ControlDocumentoBundle:Condicion:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Condicion entity.
     *
     * @Route("/", name="condicion_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Condicion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Condicion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La condición fue agregada con éxito.'
            );
            return $this->redirect($this->generateUrl('condicion_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La condición fue agregada con éxito.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Condicion entity.
    *
    * @param Condicion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Condicion $entity)
    {
        $form = $this->createForm(new CondicionType(), $entity, array(
            'action' => $this->generateUrl('condicion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Condicion entity.
     *
     * @Route("/new", name="condicion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Condicion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Condicion entity.
     *
     * @Route("/{id}", name="condicion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Condicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la condición.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Condicion entity.
     *
     * @Route("/{id}/edit", name="condicion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Condicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la condición.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Condicion entity.
    *
    * @param Condicion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Condicion $entity)
    {
        $form = $this->createForm(new CondicionType(), $entity, array(
            'action' => $this->generateUrl('condicion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

         $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing Condicion entity.
     *
     * @Route("/{id}", name="condicion_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Condicion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Condicion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la condición.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La condición fue actualizada con éxito.'
            );
            return $this->redirect($this->generateUrl('condicion_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La condición no se pudo agregar.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Condicion entity.
     *
     * @Route("/{id}", name="condicion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Condicion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar la condición.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La condición fue eliminada con éxito.'
            );
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'La condición fue eliminada con éxito.'
            );
        }

        return $this->redirect($this->generateUrl('condicion'));
    }

    /**
     * Creates a form to delete a Condicion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('condicion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }
}
