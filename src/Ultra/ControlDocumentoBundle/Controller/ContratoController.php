<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Contrato;
use Ultra\ControlDocumentoBundle\Form\ContratoType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contrato controller.
 *
 * @Route("/contrato")
 */
class ContratoController extends Controller
{

    /**
     * Lists all Contrato entities.
     *
     * @Route("/", name="contrato")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Contrato')->findAll();
      //  $paginator  = $this->get('knp_paginator');
      //  $pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),5);
        
      //  return $this->render('ControlDocumentoBundle:Contrato:index.html.twig', array('pagination' => $pagination));
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new contrato entity.
     *
     * @Route("/contratospdf", name="contratos_pdf")
     * @Method("GET")
     * @Template()
     */
    public function contratospdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Contrato')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Contrato:contratospdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Contrato',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadecontratos.pdf"',
            )
        );

        return $response;

    }

    /**
     * Creates a new Contrato entity.
     *
     * @Route("/", name="contrato_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Contrato:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Contrato();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El contrato fue agregado con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('contrato_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El contrato no fue agregado.'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Contrato entity.
    *
    * @param Contrato $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Contrato $entity)
    {
        $form = $this->createForm(new ContratoType(), $entity, array(
            'action' => $this->generateUrl('contrato_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => ' Guardar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
                'glyphicon' => 'glyphicon glyphicon-floppy-saved'
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Contrato entity.
     *
     * @Route("/new", name="contrato_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Contrato();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Contrato entity.
     *
     * @Route("/{id}", name="contrato_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Contrato')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el contrato.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Contrato entity.
     *
     * @Route("/{id}/edit", name="contrato_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Contrato')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el contrato.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Contrato entity.
    *
    * @param Contrato $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Contrato $entity)
    {
        $form = $this->createForm(new ContratoType(), $entity, array(
            'action' => $this->generateUrl('contrato_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit'
            ));

        return $form;
    }
    /**
     * Edits an existing Contrato entity.
     *
     * @Route("/{id}", name="contrato_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Contrato:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Contrato')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el contrato.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos del contrato fueron actualizados.'
            );
            return $this->redirect($this->generateUrl('contrato_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'Los datos del contrato no fueron actuzalizados.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Contrato entity.
     *
     * @Route("/{id}", name="contrato_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Contrato')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar el contrato.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El contrato fue eliminado.'
            );
            return $this->redirect($this->generateUrl('contrato'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El contrato no pudo ser eliminado.'
            );
        }

        return $this->redirect($this->generateUrl('contrato'));
    }

    /**
     * Creates a form to delete a Contrato entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contrato_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class'=>'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash'
            ))
            ->getForm()
        ;
    }
}
