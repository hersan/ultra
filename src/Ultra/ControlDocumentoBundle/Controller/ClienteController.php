<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Cliente;
use Ultra\ControlDocumentoBundle\Form\ClienteType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Cliente controller.
 *
 * @Route("/cliente")
 */
class ClienteController extends Controller {

    /**
     * Lists all Cliente entities.
     *
     * @Route("/", name="cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Cliente')->findAll();
        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($query,$this->get('request')->query->get('page', 1),10);
        // return $this->render('ControlDocumentoBundle:Cliente:index.html.twig', array('pagination' => $pagination));        
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Cliente entity.
     *
     * @Route("/clientespdf", name="clientes_pdf")
     * @Method("GET")
     * @Template()
     */
    public function clientespdfAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Cliente')->findAll();

        $html = $this->renderView('ControlDocumentoBundle:Cliente:clientespdf.html.twig', array(
            'entities' => $entities,
        ));

        $response = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title' => 'Perfil',
                    'user-style-sheet' => 'css/bootstrap.css',
                    'header-right' => 'Pag. [page] de [toPage]',
                    'header-font-size' => 7,
                )), 200, array(
            'Content-Type' => '/home/aloyo/public_html/Ultra/web/pdf',
            'Content-Disposition' => 'attachment; filename="listadeclientes.pdf"',
                )
        );

        return $response;
    }

    /**
     * Creates a new Cliente entity.
     *
     * @Route("/", name="cliente_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Cliente:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Cliente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'El cliente fue agregado con los siguientes datos:');

            return $this->redirect($this->generateUrl('cliente'));
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'Falta uno o mas campos.'
            );
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Cliente entity.
     *
     * @param Cliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cliente $entity) {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('cliente_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title' => 'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));
        return $form;
    }

    /**
     * Displays a form to create a new Cliente entity.
     *
     * @Route("/new", name="cliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Cliente();
        $form = $this->createCreateForm($entity);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'El cliente fue agregado con los siguientes datos:');

            return $this->redirect($this->generateUrl('cliente'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Cliente entity.
     *
     * @Route("/{id}", name="cliente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar al cliente.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Cliente entity.
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar al cliente.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Cliente entity.
     *
     * @param Cliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Cliente $entity) {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('cliente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn btn-success',
                'title' => 'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }

    /**
     * Edits an existing Cliente entity.
     *
     * @Route("/{id}", name="cliente_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Cliente:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar al cliente.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'Los datos del cliente fueron modificados.'
            );
            return $this->redirect($this->generateUrl('cliente_show', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'Los datos del cliente no se pudiaron actualizar.'
            );
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Cliente entity.
     *
     * @Route("/{id}", name="cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Cliente')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar al cliente.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'success', 'El cliente fue eliminado.'
            );
            return $this->redirect($this->generateUrl('cliente'));
        } else {
            $this->get('session')->getFlashBag()->add(
                    'danger', 'El cliente no pudo ser eliminado.'
            );
        }

        return $this->redirect($this->generateUrl('cliente'));
    }

    /**
     * Creates a form to delete a Cliente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('cliente_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'usubmit', array(
                            'label' => 'Borrar',
                            'attr' => array('class' => 'btn btn-warning',
                                'title' => 'Borrar datos'),
                            'glyphicon' => 'glyphicon glyphicon-trash'
                        ))
                        ->getForm();
    }

}
