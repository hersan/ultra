<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\ORM\NoResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ultra\ControlDocumentoBundle\Entity\FormatoAsociado;
use Ultra\ControlDocumentoBundle\Form\FormatoAsociadoType;

/**
 * FormatoAsociado controller.
 *
 * @Route("/formato/asociado")
 */
class FormatoAsociadoController extends Controller
{

    /**
     * Lists all FormatoAsociado entities.
     *
     * @Route("/", name="formato_asociado")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        /*$em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:FormatoAsociado')->findAll();*/

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("
            SELECT d, f
            FROM ControlDocumentoBundle:FormatoAsociado f
              JOIN f.documento d
              JOIN d.condicion c
            WHERE c.id = 1
        ");

        try {
            $entities = $query->getResult();
            //ladybug_dump($entities);
        } catch (NoResultException $e) {
            $this->createNotFoundException('No se encontro coincidencia');
        }

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new FormatoAsociado entity.
     *
     * @Route("/", name="formato_asociado_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:FormatoAsociado:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new FormatoAsociado();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('formato_asociado_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a FormatoAsociado entity.
     *
     * @param FormatoAsociado $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FormatoAsociado $entity)
    {
        $form = $this->createForm(new FormatoAsociadoType(), $entity, array(
            'action' => $this->generateUrl('formato_asociado_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new FormatoAsociado entity.
     *
     * @Route("/new", name="formato_asociado_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new FormatoAsociado();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a FormatoAsociado entity.
     *
     * @Route("/{id}", name="formato_asociado_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormatoAsociado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormatoAsociado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing FormatoAsociado entity.
     *
     * @Route("/{id}/edit", name="formato_asociado_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormatoAsociado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormatoAsociado entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a FormatoAsociado entity.
    *
    * @param FormatoAsociado $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FormatoAsociado $entity)
    {
        $form = $this->createForm(new FormatoAsociadoType(), $entity, array(
            'action' => $this->generateUrl('formato_asociado_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FormatoAsociado entity.
     *
     * @Route("/{id}", name="formato_asociado_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:FormatoAsociado:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:FormatoAsociado')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FormatoAsociado entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('formato_asociado_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a FormatoAsociado entity.
     *
     * @Route("/{id}", name="formato_asociado_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:FormatoAsociado')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FormatoAsociado entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('formato_asociado'));
    }

    /**
     * Creates a form to delete a FormatoAsociado entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('formato_asociado_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
