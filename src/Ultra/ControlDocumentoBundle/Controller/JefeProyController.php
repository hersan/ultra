<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Ultra\ControlDocumentoBundle\Form\VencimientoType;

/**
 * Class JefeProyController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/JefeProy", name="jefe_proy")
 */
class JefeProyController extends Controller
{
    /**
     * @Route("/index", name="jefe_proy_index")
     * @Template("ControlDocumentoBundle:JefeProy:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $hoy=new \DateTime();
        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales

        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH (t.titulo = 'general' and d.year='2015')
            ");

        $documentos = $q->getResult();

        return array(
            'proyectos' => $proyectos,
            'documentos'=>$procedimientos,
            'lim'=>$limite,
            'lm'=>$listamaestra,
            'cumple'=>$cumple,
            'doc'=>$documentos
        );
    }

    /**
     * @Route("/vencimientos", name="jefe_proy_vencimientos")
     * @Template("ControlDocumentoBundle:JefeProy:vencimientos.html.twig")
     */
    public function vencimientos()
    {

        $form = $this->createVencimientoForm();

        return array(

            'vform'=>$form->createView()

        );
    }

    /**
     *
     * @Route("/resultvenc", name="result_vencimientos")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:JefeProy:vencresult.html.twig")
     */
    public function vencimientoAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_vencimiento');

        $fvigencia=$formulario['fv'];
//ladybug_dump_die($fvigencia);

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $fvigencia );
        $procedimientos = $query->getResult();
        //ladybug_dump_die($entities);

        return array(
            'documentos' => $procedimientos,
            'lim'=> $fvigencia
        );

    }

    /**
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\CDocRep $entity
     * @return \Symfony\Component\Form\Form The form
     */
    private function createVencimientoForm()
    {
        $form = $this->createForm(new VencimientoType(), null , array(
            'action' => $this->generateUrl('result_vencimientos'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Consultar',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Consultar'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }



}
