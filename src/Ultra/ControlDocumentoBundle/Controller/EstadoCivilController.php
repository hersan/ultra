<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\EstadoCivil;
use Ultra\ControlDocumentoBundle\Form\EstadoCivilType;

/**
 * EstadoCivil controller.
 *
 * @Route("/estadocivil")
 */
class EstadoCivilController extends Controller
{

    /**
     * Lists all EstadoCivil entities.
     *
     * @Route("/", name="estadocivil")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:EstadoCivil')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new EstadoCivil entity.
     *
     * @Route("/", name="estadocivil_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:EstadoCivil:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new EstadoCivil();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('estadocivil_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a EstadoCivil entity.
    *
    * @param EstadoCivil $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(EstadoCivil $entity)
    {
        $form = $this->createForm(new EstadoCivilType(), $entity, array(
            'action' => $this->generateUrl('estadocivil_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Create',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }

    /**
     * Displays a form to create a new EstadoCivil entity.
     *
     * @Route("/new", name="estadocivil_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new EstadoCivil();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a EstadoCivil entity.
     *
     * @Route("/{id}", name="estadocivil_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EstadoCivil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EstadoCivil entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EstadoCivil entity.
     *
     * @Route("/{id}/edit", name="estadocivil_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EstadoCivil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EstadoCivil entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a EstadoCivil entity.
    *
    * @param EstadoCivil $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EstadoCivil $entity)
    {
        $form = $this->createForm(new EstadoCivilType(), $entity, array(
            'action' => $this->generateUrl('estadocivil_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
                'label' => 'Update',
                'attr' => array('class' => 'btn btn-default')
            ));

        return $form;
    }
    /**
     * Edits an existing EstadoCivil entity.
     *
     * @Route("/{id}", name="estadocivil_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:EstadoCivil:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:EstadoCivil')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EstadoCivil entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('estadocivil_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a EstadoCivil entity.
     *
     * @Route("/{id}", name="estadocivil_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:EstadoCivil')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EstadoCivil entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('estadocivil'));
    }

    /**
     * Creates a form to delete a EstadoCivil entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estadocivil_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                    'label' => 'Delete',
                    'attr' => array('class' => 'btn btn-default')
                ))
            ->getForm()
        ;
    }
}
