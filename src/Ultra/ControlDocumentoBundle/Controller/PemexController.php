<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class PemexController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/Pemex", name="pemex")
 */
class PemexController extends Controller
{
    /**
     * @Route("/index", name="pemex_index")
     * @Template("ControlDocumentoBundle:Pemex:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH c.id = 1 AND d.vigencia< :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        return array(
            'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite
        );
    }

    /**
     * @Route("/{type}/manual", name="pemex_manual")
     * @Template("ControlDocumentoBundle:Pemex:manual.html.twig")
     */
    public function manualAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($documentos);

        return array(
            'procedimientos' => $documentos
        );
        //return array();
    }

    /**
     * @Route("/perfiles", name="pemex_perfiles")
     * @Template("ControlDocumentoBundle:Pemex:perfil.html.twig")
     */
    public function perfilAction()
    {
        $cliente = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);

        $documentos = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($cliente->getId());

        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }


    /**
     * @Route("/{type}/docum", name="pemex_docum")
     * @Template("ControlDocumentoBundle:Pemex:docs.html.twig")
     */
    public function documAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/{type}/proc", name="pemex_proc")
     * @Template("ControlDocumentoBundle:Pemex:proc.html.twig")
     */
    public function procAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/{type}/regl", name="pemex_regl")
     * @Template("ControlDocumentoBundle:Pemex:reglamentos.html.twig")
     */
    public function reglAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/showmpemex", name="pemex_m_pdf")
     * @Template("ControlDocumentoBundle:Pemex:showmaestrapemex.html.twig")
     */
    public function showmpemexAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(2);

        $perfiles = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($clientes->getId());
        $manuales= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),2);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),6);
        $procedimientos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),1);
        $reglamentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),5);


        $html = $this->renderView('ControlDocumentoBundle:Pemex:showmaestrapemex.html.twig',
            array(
                'perfiles' => $perfiles,
                'manuales' => $manuales,
                'documentos' => $documentos,
                'procedimientos' => $procedimientos,
                'reglamentos' => $reglamentos,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Landscape',
                    'title'=> 'listamaestra',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-left'=>'Hoja [page] de [toPage]',
                    'header-font-size'=>8,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listramaestrapemex.pdf"',
            )
        );

        return $response;

    }
}