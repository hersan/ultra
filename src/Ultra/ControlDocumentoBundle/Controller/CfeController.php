<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class CfeController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/Cfe", name="cfe")
 */
class CfeController extends Controller
{
    /**
     * @Route("/index", name="cfe_index")
     * @Template("ControlDocumentoBundle:Cfe:index.html.twig")
     */
    public function indexAction()
    {
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH c.id = 1 AND d.vigencia< :limite
            ")->setParameter('limite', $limite->format('Y-m-d') );


        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);

        return array(
            'proyectos' => $proyectos,'documentos'=>$procedimientos,'lim'=>$limite
        );
    }

    /**
     * @Route("/{type}/manual", name="cfe_manual")
     * @Template("ControlDocumentoBundle:Cfe:manual.html.twig")
     */
    public function manualAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($documentos);

        return array(
            'procedimientos' => $documentos
        );
        //return array();
    }

    /**
     * @Route("/perfiles", name="cfe_perfiles")
     * @Template("ControlDocumentoBundle:Cfe:perfil.html.twig")
     */
    public function perfilAction()
    {
        $cliente = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);

        $documentos = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($cliente->getId());

        //ladybug_dump($documentos);
        return array(
            'procedimientos' => $documentos
        );

    }


    /**
     * @Route("/{type}/docum", name="cfe_docum")
     * @Template("ControlDocumentoBundle:Cfe:docs.html.twig")
     */
    public function documAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/{type}/proc", name="cfe_proc")
     * @Template("ControlDocumentoBundle:Cfe:proc.html.twig")
     */
    public function procAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/{type}/regl", name="cfe_regl")
     * @Template("ControlDocumentoBundle:Cfe:reglamentos.html.twig")
     */
    public function reglAction($type)
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);
        //ladybug_dump_die($clientes);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),$type);
        //ladybug_dump_die($docu);
        return array(
            'procedimientos' => $documentos,
        );
        //return array();
    }

    /**
     * @Route("/showmcfe", name="cfe_m_pdf")
     * @Template("ControlDocumentoBundle:Cfe:showmaestracfe.html.twig")
     */
    public function showmcfeAction()
    {
        $clientes = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Cliente')->find(1);

        $perfiles = $this->getDoctrine()->getRepository('ControlDocumentoBundle:Perfil')->findByClient($clientes->getId());
        $manuales= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),2);
        $documentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),6);
        $procedimientos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),1);
        $reglamentos= $this->getDoctrine()->getRepository('ControlDocumentoBundle:Documento')->findByCliente($clientes->getId(),5);


        $html = $this->renderView('ControlDocumentoBundle:Cfe:showmaestracfe.html.twig',
            array(
                'perfiles' => $perfiles,
                'manuales' => $manuales,
                'documentos' => $documentos,
                'procedimientos' => $procedimientos,
                'reglamentos' => $reglamentos,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Landscape',
                    'title'=> 'listamaestra',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-left'=>'Hoja [page] de [toPage]',
                    'header-font-size'=>8,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listramaestracfe.pdf"',
            )
        );

        return $response;

    }
}