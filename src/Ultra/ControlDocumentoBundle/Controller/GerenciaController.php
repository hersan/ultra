<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GerenciaController
 * @package Ultra\ControlDocumentoBundle\Controller
 * @Route("/Gerencia", name="gerencia")
 */
class GerenciaController extends Controller
{
    /**
     * @Route("/index", name="gerencia_index")
     * @Template("ControlDocumentoBundle:Gerencia:index.html.twig")
     */
    public function indexAction()
    {
        $hoy=new \DateTime();
        $proyectos = $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Contrato')->findAll();
        //$procedimientos= $this->getDoctrine()->getManager()->getRepository('ControlDocumentoBundle:Documento')->findByCliente(1,1);

        $limite=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WHERE (c.id = 1 or c.id = 5) AND d.vigencia< :limite
              ORDER BY d.vigencia
            ")->setParameter('limite', $limite->format('Y-m-d') );

        //Consulta la lista maestra actual
        $q = $em->createQuery(
            "SELECT d.id id FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1 AND d.clave='LM'
            ");

        $listamaestra = $q->getResult();

        $procedimientos = $query->getResult();
        //ladybug_dump_die($procedimientos);
        $querycumple = $em->createQuery(
            "SELECT  c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.fechaNacimiento fn
              FROM ControlDocumentoBundle:Curricula c
              WHERE MONTH (c.fechaNacimiento)= :hoy
              ORDER BY c.apellidoPaterno ASC"
        )->setParameter('hoy', $hoy->format('m') );

        $cumple = $querycumple->getResult();

        //Consulto los documentos generales

        $q = $em->createQuery(
            "SELECT d FROM ControlDocumentoBundle:GeneralDocument d
              JOIN d.type t
              WITH ((t.titulo = 'general' OR t.titulo = 'gerencial' )  and d.year='2015')
            ");

        $documentos = $q->getResult();

        return array(
            'proyectos' => $proyectos,
            'documentos'=>$procedimientos,
            'lim'=>$limite,'lm'=>$listamaestra,
            'cumple'=>$cumple,
            'doc'=>$documentos
        );
    }

    /**
     * @Route("/grafica", name="gerencia_grafica")
     * @Template("ControlDocumentoBundle:Gerencia:grafica.html.twig")
     */
    public function graficaAction()
    {

        $limite=new \DateTime();
        $hoy=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH (c.id = 1 OR c.id = 5) AND d.vigencia< :limite AND d.vigencia>:hoy
            ")->setParameters( array ('limite'=> $limite->format('Y-m-d'), 'hoy'=>$hoy->format('Y-m-d')));

        $query1 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH (c.id = 1 OR c.id = 5) AND d.vigencia> :hoy
            ")->setParameter('hoy', $hoy->format('Y-m-d') );

        $query2 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c JOIN d.disciplina dis
              JOIN dis.area a JOIN a.contrato con JOIN con.cliente cli
              WITH (c.id = 1 OR c.id = 5) AND d.vigencia< :hoy
            ")->setParameter('hoy', $hoy->format('Y-m-d') );


        $xvencer = $query->getSingleResult();
        $actualizados = $query1->getSingleResult();
        $vencidos = $query2->getSingleResult();

        //ladybug_dump_die($vencidos);

        return array(
            'docxvencer'=>$xvencer,'docact'=>$actualizados,'docven'=>$vencidos,'lim'=>$limite
        );
    }

    /**
     * @Route("/dsad", name="gerencia_dsad")
     * @Template("ControlDocumentoBundle:Gerencia:datossad.html.twig")
     */
    public function datossadAction()
    {
        $limite=new \DateTime();
        $hoy=new \DateTime();

        $limite->modify("+2 months");

        $em = $this->getDoctrine()->getManager();

        //Vigentes
        $query = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
              WITH c.id = 1
            ");

        $vigentes = $query->getSingleResult();

        //Todos
        $query1 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Documento d
              JOIN d.tipoDocumento t JOIN d.condicion c
            ");

        //Auditorias abiertas
        $query2 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM CalidadBundle:AuditoriaVigilancia d
              JOIN d.estado c
              WITH c.id = 1
            ");

        //Auditorias cerradas
        $query3 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM CalidadBundle:AuditoriaVigilancia d
              JOIN d.estado c
              WITH c.id = 4
            ");

        //Perfiles vigentes
        $query4 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Perfil d
              JOIN d.estado c
              WITH c.id = 1
            ");

        //Curricula
        $query5 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ControlDocumentoBundle:Curricula d
            ");

        //Partidas del 3697
        $query6 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ProyectoBundle:Partida d
            WHERE d.nocontrato = '3697'
            ");
        //Ordenes de trabajo 3697
        $query7 = $em->createQuery(
            "SELECT COUNT(o.id) AS num FROM ProyectoBundle:OrdenTrabajo o
             JOIN o.detalles dor JOIN dor.partida p
             WHERE p.nocontrato = '3697'
            ");

        //Partidas del 3681
        $query8 = $em->createQuery(
            "SELECT COUNT(d.id) AS num FROM ProyectoBundle:Partida d
            WHERE d.nocontrato = '3681'
            ");
        //Ordenes de trabajo 3681
        $query9 = $em->createQuery(
            "SELECT COUNT(o.id) AS num FROM ProyectoBundle:OrdenTrabajo o
             JOIN o.detalles dor JOIN dor.partida p
             WHERE p.nocontrato = '3681'
            ");

        $vigentes = $query->getSingleResult();
        $todos = $query1->getSingleResult();
        $auditorias = $query2->getSingleResult();
        $auditoriasc = $query3->getSingleResult();
        $perfiles = $query4->getSingleResult();
        $curriculas= $query5->getSingleResult();
        $partidas97= $query6->getSingleResult();
        $ordentrabajo97= $query7->getSingleResult();
        $partidas81= $query8->getSingleResult();
        $ordentrabajo81= $query9->getSingleResult();

        return array(
            'vigentes'=>$vigentes,
            'todos'=>$todos,
            'auditorias'=>$auditorias,
            'auditoriasc'=>$auditoriasc,
            'perfiles'=>$perfiles,
            'curriculas'=>$curriculas,
            'partidas97'=>$partidas97,
            'ordentrabajo97'=>$ordentrabajo97,
            'partidas81'=>$partidas81,
            'ordentrabajo81'=>$ordentrabajo81,
        );
    }

}
