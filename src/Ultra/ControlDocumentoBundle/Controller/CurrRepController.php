<?php
/**
 * Created by PhpStorm.
 * User: aloyo
 * Date: 1/04/14
 * Time: 10:54 AM
 */

namespace Ultra\ControlDocumentoBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ultra\ControlDocumentoBundle\Entity\Curricula;
use Ultra\ControlDocumentoBundle\Entity\Perfil;
use Ultra\ControlDocumentoBundle\Form\EvaluaType;


/**
 * Curricula Reportes controller.
 *
 * @Route("/currrep")
 */
class CurrRepController extends Controller
{
    /**
     *
     * @Route("/pcdisciplinapdf", name="pxdisciplina_pdf")
     * @Method("GET")
     * @Template()
     */
    public function pxdisciplinapdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository('ControlDocumentoBundle:Contrato')->find(2);

        if(!$contrato){
            throw $this->createNotFoundException('El contrato no existe');
        }
        $query = $em->createQuery(
            "SELECT dis.titulo, dis.id, a.claveArea area FROM ControlDocumentoBundle:Contrato c
             JOIN c.areas a JOIN a.disciplinas dis  WHERE c.id = :id"
        )->setParameters(array(
                'id' => 2,
            ));

        $entities = $query->getResult();

        $html = $this->renderView('ControlDocumentoBundle:CurrRep:pxdisciplinapdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Disciplinas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadedisciplinas.pdf"',
            )
        );

        return $response;

    }

    /**
     * @Route("/{p}/perporperpdf",name="curr_perporperpdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personasenperfilpdf.html.twig")
     */
    public function perporperpdfAction($p){

        $em = $this->getDoctrine()->getManager();

        $perfil = $em->getRepository('ControlDocumentoBundle:Perfil')->find($p);

        if (!$perfil) {
            throw $this->createNotFoundException('Imposible encontrar perfil.');
        }

        $hoy=new \DateTime();

        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.perfil = :per AND pc.contrato=3697 AND pc.fechaBaja >:hoy"
        )->setParameters(array(
                'per' => $p,
                'hoy' => $hoy->format('Y-m-d')
            ));

        $entities = $query->getResult();



        $html = $this->renderView('ControlDocumentoBundle:CurrRep:personasenperfilpdf.html.twig',
            array(
                'perfil' => $perfil,'personas'=>$entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="personasporperpdf.pdf"',
            )
        );

        return $response;

    }


    /**
     * @Route("/empleadospdf", name="curricula_emp_pdf")
     * @Method("GET")
     * @Template()
     */
    public function empleadosPdfAction()
    {
        $em = $this->getDoctrine()->getManager();

//        $c=3697;
//        $query = $em->createQuery(
//            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel, c.celular cel
//              FROM ControlDocumentoBundle:Curricula c
//              JOIN c.curriculas pc
//              JOIN C.type t
//              WHERE (pc.contrato= :c AND t.id=1)"
//        )->setParameter('c', $c);
//
//        $entities = $query->getResult();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel, c.celular cel
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.type t
              WHERE t.id =1
            ");

        $entities = $query->getResult();

       /* $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();*/


        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar personas.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:personalpdf.html.twig',
            array(
                'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="empleadospdf.pdf"',
            )
        );

        return $response;
    }

    /**
     * @Route("/alfapdf/", name="alfa_pdf")
     * @Method("GET")
     * @Template()
     */
    public function alfaPdfAction()
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel, c.celular cel
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.type t
              WHERE t.id =2
            ");

        $entities = $query->getResult();


        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar personas.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:alfapdf.html.twig',
            array(
                'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="alfa.pdf"',
            )
        );

        return $response;
    }

    /**
     * @Route("/{c}/personalinacpdf", name="curr_personalinacpdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personalinacyactpdf.html.twig")
     */
    public function personalinacpdfAction($c){

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findByCotratoNoVigente($c);
        $i='inactivo';

        $html = $this->renderView('ControlDocumentoBundle:CurrRep:personalinacyactpdf.html.twig',
            array(
                'entities' => $entities,'b'=>$i
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="personalinactivopdf.pdf"',
            )
        );

        return $response;

    }

    /**
     * @Route("/{c}/personalactpdf", name="curr_personalactpdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personalinacyactpdf.html.twig")
     */
    public function personalactpdfAction($c){

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findByCotratoVigente($c);
        $i='activo';

        $html = $this->renderView('ControlDocumentoBundle:CurrRep:personalinacyactpdf.html.twig',
            array(
                'entities' => $entities,'b'=>$i
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="personalactivopdf.pdf"',
            )
        );

        return $response;

    }

    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}/{p}/evaluapdf", name="curr_evaluapdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:evaluapdf.html.twig")
     */
    public function evaluapdfAction(Request $request,$id,$p){


        $result = null;
        $em = $this->getDoctrine()->getManager();

        $cv = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$cv) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $query = $em->createQuery("
            SELECT c, pc, p FROM ControlDocumentoBundle:Curricula c JOIN c.curriculas pc JOIN pc.perfil p
            WHERE c.id = :id AND p.id= :pe
        ")->setParameters( array('id'=>$id,'pe'=>$p));

        try {
            $result = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $this->createNotFoundException('Ahora si lo ejecuta');
        }

        $cursosInternos = new ArrayCollection();
        $cursosExternos = new ArrayCollection();
        $conocimientosEspeciales = new ArrayCollection();
        $capacitacionEspecifica = new ArrayCollection();
        $cursosInternosNoVigentes = array();
        $cursosExternosNoVigentes = array();
        $perfiles = new ArrayCollection();
        $difInterno = array();
        $difExterno = array();
        //ladybug_dump_die($result);
        //Se recuperan los cursos relacionados a la curricula
        foreach($result as $r){
            foreach($r->getCursosInternos() as $c){
                $cursosInternos[] = $c->getCursoInterno();
            }

        }

        //Se recuperan cursos externos
        foreach($result as $r){
            foreach($r->getCursosExternos() as $c){
                $cursosExternos[] = $c->getCursoExterno();
            }

        }

        //Recuperamos los perfiles relacionados a la curricula
        foreach($result as $r){
            foreach($r->getCurriculas() as $pc){
                $perfiles[] = $pc->getPerfil();
            }
        }


        //Recuperamos la capacitacion especifica del perfil
        foreach($perfiles as $perfil){
            foreach($perfil->getCapacitacion() as $c){
                $capacitacionEspecifica[] = $c->getCapacitacion();
            }
        }

        foreach($perfiles as $perfil){
            foreach($perfil->getConocimientoEspecial() as $c){
                $conocimientosEspeciales[] = $c->getCurso();
            }
        }

        //Se determinan las diferencias
        foreach($capacitacionEspecifica as $c){
            if(!$cursosInternos->contains($c)){
                $difInterno[] = $c;
            }
        }

        foreach($conocimientosEspeciales as $c){
            if(!$cursosExternos->contains($c)){
                $difExterno[] = $c;
            }
        }


        foreach($result as $r){
            foreach($r->getCursosExternos() as $c){
                if(!$c->estaVigente()){
                    //$c->getCursoExterno();
                    $cursosExternosNoVigentes[] = $c;
                }
            }

        }
        foreach($result as $r){
            foreach($r->getCursosInternos() as $c){
                if(!$c->estaVigente()){
                    $cursosInternosNoVigentes[] = $c;
                }
            }
        }
        //ladybug_dump_die($perfiles);

        $html = $this->renderView('ControlDocumentoBundle:CurrRep:evaluapdf.html.twig',
            array(
                'capacitacion' => $capacitacionEspecifica,
                'conocimientos' => $conocimientosEspeciales,
                'cursos' => $cursosInternos,
                'cursose'=> $cursosExternos,
                'perfiles' => $perfiles,
                'cursos_internos' => $difInterno,
                'cursos_externos' => $difExterno,
                'cursos_internos_no_vigentes' => $cursosInternosNoVigentes,
                'cursos_externos_no_vigentes' => $cursosExternosNoVigentes,
                'cv'=>$cv
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="evaluapdf.pdf"',
            )
        );

        return $response;

    }

    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/{id}/{p}/evaluapdfx", name="curr_evaluapdfx")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:evaluapdfx.html.twig")
     */
    public function evaluapdfxAction(Request $request,$id,$p){


        $em = $this->getDoctrine()->getManager();

        $curricula = $em->getRepository('ControlDocumentoBundle:Curricula')->find($id);

        if (!$curricula) {
            throw $this->createNotFoundException('Imposible encontrar la curricula.');
        }

        $perfil = $em->getRepository('ControlDocumentoBundle:Perfil')->find($p);

        if (!$perfil) {
            throw $this->createNotFoundException('Imposible encontrar perfil.');
        }

        $cursosInternos = new ArrayCollection();
        $cursosExternos = new ArrayCollection();
        $conocimientosEspeciales = new ArrayCollection();
        $capacitacionEspecifica = new ArrayCollection();
        $cursosExternosNoVigentes = array();
        $perfiles = new ArrayCollection();
        $difInterno = array();
        $difExterno = array();

        foreach($curricula->getCursosInternos() as $c){
            $cursosInternos[] = $c->getCursoInterno();
        }



        //Se recuperan cursos externos

        foreach($curricula->getCursosExternos() as $c){
            $cursosExternos[] = $c->getCursoExterno();
        }


        //Recuperamos la capacitacion especifica del perfil

        foreach($perfil->getCapacitacion() as $c){
            $capacitacionEspecifica[] = $c->getCapacitacion();
        }

//ladybug_dump_die($perfil->getCapacitacion());
        foreach($perfil->getConocimientoEspecial() as $c){
            $conocimientosEspeciales[] = $c->getCurso();
        }


        //Se determinan las diferencias
        foreach($capacitacionEspecifica as $c){
            if(!$cursosInternos->contains($c)){
                $difInterno[] = $c;
            }
        }

        foreach($conocimientosEspeciales as $c){
            if(!$cursosExternos->contains($c)){
                $difExterno[] = $c;
            }
        }



        foreach($curricula->getCursosExternos() as $c){
            if(!$c->estaVigente()){
                //$c->getCursoExterno();
                $cursosExternosNoVigentes[] = $c;
            }
        }
//ladybug_dump_die($cursosExternos);

        $html = $this->renderView('ControlDocumentoBundle:CurrRep:evaluapdfx.html.twig',
            array(
                'capacitacion' => $capacitacionEspecifica,
                'conocimientos' => $conocimientosEspeciales,
                'cursos' => $cursosInternos,
                'cursose'=> $cursosExternos,
                'perfil' => $perfil,
                'cursos_internos' => $difInterno,
                'cursos_externos' => $difExterno,
                'cursos_externos_no_vigentes' => $cursosExternosNoVigentes,
                'cv'=>$curricula
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="evaluapdfx.pdf"',
            )
        );

        return $response;

    }

    /**
     * @Route("/{c}/personalpdf", name="personal_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personalpdf.html.twig")
     */
    public function personalPdfAction($c)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel, c.celular cel
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.contrato= :c"
        )->setParameter('c', $c);

        $entities = $query->getResult();

        /*$em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findByCotrato($c);*/

        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:personalpdf.html.twig',
            array(
                'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="personalpdf.pdf"',
            )
        );

        return $response;
    }




    /**
     * @Route("/{c}/puestospdf", name="puestos_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:puestospdf.html.twig")
     */
    public function puestosPdfAction($c)
    {
        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();
        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, p.clave cve, p.titulo tit, p.revision rev, d.titulo dis
            FROM ControlDocumentoBundle:Curricula c
            JOIN c.curriculas pc JOIN pc.perfil p JOIN p.disciplinas d JOIN d.area a
            WHERE pc.fechaBaja >:hoy AND pc.contrato=3697 AND a.contrato=2"
        )->setParameter('hoy', $hoy->format('Y-m-d') );


        $entities = $query->getResult();

        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:puestospdf.html.twig',
            array(
                'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="puestospdf.pdf"',
            )
        );

        return $response;
    }

    /**
     * @Route("/{c}/puestosordenadospdf", name="puestos__ordenados_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:puestosordenadospdf.html.twig")
     */
    public function puestosordenadosPdfAction($c)
    {
        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();
        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, p.clave cve, p.titulo tit, p.revision rev, d.titulo dis
            FROM ControlDocumentoBundle:Curricula c
            JOIN c.curriculas pc JOIN pc.perfil p JOIN p.disciplinas d JOIN d.area a
            WHERE pc.fechaBaja >:hoy AND pc.contrato=3697 AND a.contrato=2 ORDER BY nc"
        )->setParameter('hoy', $hoy->format('Y-m-d') );


        $entities = $query->getResult();

        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:puestospdf.html.twig',
            array(
                'entities' => $entities));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="puestospdf.pdf"',
            )
        );

        return $response;
    }


    /**
     * @Route("/{c}/agendapdf", name="agenda_pdf")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:agendapdf.html.twig")
     */
    public function agendaPdfAction($c)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT DISTINCT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel,c.telefono1 tel1,c.email mail,c.emailultra mailu,
              c.celular cel,c.extension ext
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.contrato= :c"
        )->setParameter('c', $c);

        $entities = $query->getResult();


        if (!$entities) {
            throw $this->createNotFoundException('Imposible encontrar el perfil.');
        }


        $html = $this->renderView('ControlDocumentoBundle:CurrRep:agendapdf.html.twig',
            array(
                'entities' => $entities,
                'contrato'=> $c));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Personal',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    //'header-left'=>'<img src="{{ app.request.scheme ~'://' ~ app.request.httpHost ~ asset('img/logo_5.png') }}" width="100" height="40">',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="agendapdf.pdf"',
            )
        );

        return $response;
    }

    /**
     * @Route("/{c}/POExvencer", requirements={"c"="\d+"}, name="curr_POExvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function poexvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='76' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=0;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/PNOExvencer", requirements={"c"="\d+"}, name="curr_PNOExvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function pnoexvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='80' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=1;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/PNOEE3xvencer", requirements={"c"="\d+"}, name="curr_PNOEE3xvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function pnoee3xvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='82' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=2;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/PNOEEMxvencer", requirements={"c"="\d+"}, name="curr_PNOEEMxvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function pnoeemxvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='81' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=3;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/POEEExvencer", requirements={"c"="\d+"}, name="curr_POEEExvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function poeeexvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='78' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=4;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/POEE3xvencer", requirements={"c"="\d+"}, name="curr_POEE3xvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function poee3xvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='79' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=5;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/POEEMxvencer", requirements={"c"="\d+"}, name="curr_POEEMxvencer")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:poexvencer.html.twig")
     */
    public function poeemxvencerAction($c){

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ControlDocumentoBundle:Curricula')->findAll();

        $hoy=new \DateTime();

        $hoy->modify("+2 months");

        $query = $em->createQuery(
            "SELECT cu.numeroControl nco,cu.nombre nom,cu.apellidoPaterno apa,cu.apellidoMaterno ama,ce.id ceid,ce.fechaVigencia fv,c.id cid,c.descripcion des, c.clave ccve
             FROM ControlDocumentoBundle:Curricula cu
             JOIN cu.cursosExternos ce JOIN ce.cursoExterno c WHERE c.id='77' and ce.fechaVigencia< :hoy"
        )->setParameter(
                'hoy', $hoy->format('Y-m-d')
            );

        $b=6;

        $entities = $query->getResult();

        return array(
            'b'=>$b,
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/personal", requirements={"c"="\d+"}, name="curricula_personal")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personal.html.twig")
     */
    public function personalAction($c){

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel, c.celular cel
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.contrato= :c"
        )->setParameter('c', $c);

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }



    /**
     * @Route("/{c}/personalinac", name="curr_personalinac")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personalinac.html.twig")
     */
    public function personalinacAction($c){

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findByCotratoNoVigente($c);
        $i='inactivo';

        return array(
            'entities' => $entities,'b'=>$i
        );

    }

    /**
     * @Route("/{c}/personalact", name="curr_personalact")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:personalinac.html.twig")
     */
    public function personalactAction($c){

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('ControlDocumentoBundle:Curricula')->findByCotratoVigente($c);
        $i='activo';

        return array(
            'entities' => $entities,'b'=>$i
        );

    }


    /**
     * @Route("/{c}/puestos", requirements={"c"="\d+"}, name="curricula_puestos")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:puestos.html.twig")
     */
    public function puestosAction($c){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, p.clave cve, p.titulo tit, p.revision rev, d.titulo dis
            FROM ControlDocumentoBundle:Curricula c
            JOIN c.curriculas pc JOIN pc.perfil p JOIN p.disciplinas d JOIN d.area a
            WHERE pc.fechaBaja >:hoy AND pc.contrato=3697 AND a.contrato=2"
        )->setParameter('hoy', $hoy->format('Y-m-d') );


        $entities = $query->getResult();
        return array(
            'entities' => $entities,
        );

    }

    /**
     * @Route("/{c}/puestosxperfil", requirements={"c"="\d+"}, name="curricula_puestosxperfil")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:puestosxperfil.html.twig")
     */
    public function puestosxperfilAction($c){

        $em = $this->getDoctrine()->getManager();

        $hoy=new \DateTime();

        $query = $em->createQuery(
            "SELECT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, p.clave cve, p.titulo tit, p.revision rev, d.titulo dis
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc JOIN pc.perfil p JOIN p.disciplinas d
              WHERE pc.fechaBaja >:hoy AND pc.contrato=3697 GROUP BY c.id"
        )->setParameter('hoy', $hoy->format('Y-m-d') );


        $entities = $query->getResult();

        $form = $this->createPuestosxPerfilForm();


        return array(
            'entities' => $entities,'eform'=>$form->createView()
        );

    }

    /**
     * @Route("/{c}/agenda", requirements={"c"="\d+"}, name="curricula_agenda")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:agenda.html.twig")
     */
    public function agendaAction($c){
        if ($c=='1')
        {
            $c='ULTRA';
        }

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery(
            "SELECT DISTINCT c.nombre nom,c.apellidoPaterno app,c.apellidoMaterno apm,c.numeroControl nc, c.telefono tel,c.telefono1 tel1,c.email mail,c.emailultra mailu,
              c.celular cel,c.extension ext
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.contrato= :c"
        )->setParameter('c', $c);

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
            'contrato' => $c
        );
    }

    /**
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPuestosxPerfilForm()
    {
        $form = $this->createForm(new EvaluaType(), null , array(
            'action' => $this->generateUrl('curr_perporper'),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Ver',
            'attr' => array('class'=>'btn btn-primary btn-xs',
                'title'=>'Ver'),
            'glyphicon' => 'glyphicon glyphicon-ok-circle'
        ));

        return $form;
    }

    /**
     * Edits an existing Curricula entity.
     *
     * @Route("/perporper", name="curr_perporper")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:CurrRep:personasenperfil.html.twig")
     */
    public function perporperAction(Request $request){

        $formulario=$request->request->get('ultra_controldocumentobundle_evalua');

        //ladybug_dump_die($formulario['perfiles']);
        $p=$formulario['perfiles'];

        $em = $this->getDoctrine()->getManager();

        $perfil = $em->getRepository('ControlDocumentoBundle:Perfil')->find($p);

        if (!$perfil) {
            throw $this->createNotFoundException('Imposible encontrar perfil.');
        }

        $hoy=new \DateTime();

        $query = $em->createQuery(
            "SELECT pc.id,pc.fechaAlta fa,pc.fechaBaja fb,c.id cid, c.nombre,c.apellidoPaterno,c.apellidoMaterno,c.numeroControl
              FROM ControlDocumentoBundle:Curricula c
              JOIN c.curriculas pc
              WHERE pc.perfil = :per AND pc.contrato=3697 AND pc.fechaBaja >:hoy"
        )->setParameters(array(
                'per' => $p,
                'hoy' => $hoy->format('Y-m-d')
            ));

        $entities = $query->getResult();


        return array(
            'perfil' => $perfil,'personas'=>$entities
        );

    }

     /**
     * @Route("/{id}/pxdisciplina", name="curr_pxdisciplina")
     * @Method("GET")
     * @Template("ControlDocumentoBundle:CurrRep:pxdisciplina.html.twig")
     */
    public function pxdiscAction($id){

        $em = $this->getDoctrine()->getManager();
        $contrato = $em->getRepository('ControlDocumentoBundle:Contrato')->findOneBy(array('id'=>$id));
        //ladybug_dump_die($contrato);
        if(!$contrato){
            throw $this->createNotFoundException('El contrato no existe');
        }
        $query = $em->createQuery(
            "SELECT dis.titulo, dis.id, a.claveArea area FROM ControlDocumentoBundle:Contrato c
             JOIN c.areas a JOIN a.disciplinas dis  WHERE c.id = :id"
        )->setParameters(array(
                'id' => $id,
            ));

        $entities = $query->getResult();

        return array( 'r' => $entities);
    }


}