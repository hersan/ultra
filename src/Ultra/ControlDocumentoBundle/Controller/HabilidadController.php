<?php

namespace Ultra\ControlDocumentoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\ControlDocumentoBundle\Entity\Habilidad;
use Ultra\ControlDocumentoBundle\Form\HabilidadType;

/**
 * Habilidad controller.
 *
 * @Route("/habilidad")
 */
class HabilidadController extends Controller
{

    /**
     * Lists all Habilidad entities.
     *
     * @Route("/", name="habilidad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ControlDocumentoBundle:Habilidad')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Habilidad entity.
     *
     * @Route("/", name="habilidad_create")
     * @Method("POST")
     * @Template("ControlDocumentoBundle:Habilidad:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Habilidad();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('habilidad_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Habilidad entity.
    *
    * @param Habilidad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Habilidad $entity)
    {
        $form = $this->createForm(new HabilidadType(), $entity, array(
            'action' => $this->generateUrl('habilidad_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Habilidad entity.
     *
     * @Route("/new", name="habilidad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Habilidad();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Habilidad entity.
     *
     * @Route("/{id}", name="habilidad_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Habilidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habilidad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Habilidad entity.
     *
     * @Route("/{id}/edit", name="habilidad_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Habilidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habilidad entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Habilidad entity.
    *
    * @param Habilidad $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Habilidad $entity)
    {
        $form = $this->createForm(new HabilidadType(), $entity, array(
            'action' => $this->generateUrl('habilidad_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Habilidad entity.
     *
     * @Route("/{id}", name="habilidad_update")
     * @Method("PUT")
     * @Template("ControlDocumentoBundle:Habilidad:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ControlDocumentoBundle:Habilidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Habilidad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('habilidad_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Habilidad entity.
     *
     * @Route("/{id}", name="habilidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ControlDocumentoBundle:Habilidad')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Habilidad entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('habilidad'));
    }

    /**
     * Creates a form to delete a Habilidad entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('habilidad_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
