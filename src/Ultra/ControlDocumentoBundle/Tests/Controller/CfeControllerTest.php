<?php

namespace Ultra\ControlDocumentoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CfeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testProc()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/proc');
    }

    public function testManual()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/manual');
    }

    public function testPerfil()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/perfil');
    }

}
