<?php

namespace Ultra\ControlDocumentoBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GralControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testProcedimiento()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/proc');
    }

    public function testManual()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/man');
    }

    public function testPerfil()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/perfil');
    }

    public function testDocumento()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/documento');
    }

}
