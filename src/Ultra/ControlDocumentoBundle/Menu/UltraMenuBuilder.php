<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 8/01/14
 * Time: 03:37 PM
 */

namespace Ultra\ControlDocumentoBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
class UltraMenuBuilder extends ContainerAware{

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'breadcrumb'));
        $menu->addChild('Cliente', array('route' => 'cliente'));
        $menu->addChild('Contrato', array('route' => 'contrato'));
        $menu->addChild('Area', array('route' => 'area'));
        $menu->addChild('Disciplina', array('route' => 'disciplina'));
        $menu->addChild('Documentos', array('route' => 'procedimiento'));
        $menu->addChild('About Me', array(
                'route' => 'ultra_controldocumento_default_index',
                'routeParameters' => array('id' => 42)
            ));
        // ... add more children

        return $menu;
    }
} 