<?php

namespace Ultra\PrestamoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SolicitudType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('expediente', 'entity', array(
                'empty_value' => 'Seleccionar el expediente',
                'class' => 'ControlDocumentoBundle:Curricula',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona expediente'
                ),
                'label' => 'Expediente',
            ))

            ->add('nuHojas','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'3',
                    'title'=>'#'),
                'label' => 'Num. de hojas',
                'required' => false
            ))
            ->add('especificaciones','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'200',
                    'title'=>'Especificaciones'),
                'label' => 'Especificaciones',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\PrestamoBundle\Entity\Archivo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_prestamobundle_solicitud';
    }
}