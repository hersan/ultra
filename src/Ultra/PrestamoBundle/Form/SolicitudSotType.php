<?php

namespace Ultra\PrestamoBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\Disciplina;

class SolicitudSotType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('disciplina','entity',array(
                'class' => 'ControlDocumentoBundle:Disciplina',
                'label' => 'Seleccione una Disciplina',
                'empty_value' => 'Seleeciona...',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                ),
                'mapped' => false,
                'group_by' => 'area',
                'query_builder' => function(EntityRepository $repository){
                    return $repository->createQueryBuilder('d')
                            ->join('d.area','area')
                            ->join('area.contrato','contrato')
                            ->where('contrato.id <> 4')->andWhere('contrato.abierto = 1')
                        ;
                },
            ))

            ->add('nuHojas','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'3',
                    'title'=>'#'),
                'label' => 'Num. de hojas',
                'required' => false
            ))
            ->add('especificaciones','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'200',
                    'title'=>'Especificaciones'),
                'label' => 'Especificaciones',
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this,'preSetData'));
        $builder->get('disciplina')->addEventListener(FormEvents::POST_SUBMIT, array($this,'postSubmit'));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\PrestamoBundle\Entity\Sot'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_prestamobundle_solicitud_sot';
    }

    public function preSetData(FormEvent $event){
        $data = $event->getData();
        $form = $event->getForm();
        $this->addSot($form, $form->get('disciplina')->getData());

    }

    public function postSubmit(FormEvent $event){
        $data = $event->getForm()->getData();
        /*if($data instanceof Contrato)
            $this->addDisciplina($event->getForm()->getParent(), $data);*/
        if($data instanceof Disciplina)
            $this->addSot($event->getForm()->getParent(), $data);
    }

    public function addSot(FormInterface $form, Disciplina $disciplina = null){

        $sots = array();
        if($disciplina instanceof Disciplina)
            $sots = null === $disciplina ? array() : $disciplina->getProyectos();

        $form->add('ot', 'entity', array(
            'empty_value' => 'Seleccionar la sot',
            'class' => 'ProyectoBundle:OrdenTrabajo',
            'attr' => array(
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'title'=>'Selecciona SOT'
            ),
            'label' => 'Orden de Trabajo',
            'choices' => $sots,
            'required' => false,
        ));
    }
}