<?php

namespace Ultra\PrestamoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PrestaPerfilType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('perfil', 'entity', array(
                'empty_value' => 'Selecciona el perfil',
                'class' => 'ControlDocumentoBundle:Perfil',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona el perfil'
                ),
                'label' => 'Perfil',
            ))

            ->add('nuHojas','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'3',
                    'title'=>'#'),
                'label' => 'Num. de hojas',
                'required' => false
            ))
            ->add('especificaciones','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'200',
                    'title'=>'Especificaciones'),
                'label' => 'Especificaciones',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\PrestamoBundle\Entity\PrestaPerfil'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_prestamobundle_prestaperfil';
    }
}
