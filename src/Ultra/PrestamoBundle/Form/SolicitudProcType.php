<?php

namespace Ultra\PrestamoBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\ControlDocumentoBundle\Entity\Documento;

class SolicitudProcType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('procedimiento', 'entity', array(
                'empty_value' => 'Selecciona el procedimiento',
                'class' => 'ControlDocumentoBundle:Documento',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona el procedimiento'
                ),
                'label' => 'Procedimiento',
            ))

            /*->add('ot', 'entity', array(
                'empty_value' => 'Seleccionar la sot',
                'class' => 'ProyectoBundle:OrdenTrabajo',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona SOT'
                ),
                'label' => 'Orden de Trabajo',
            ))*/
            ->add('nuHojas','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'3',
                    'title'=>'#'),
                'label' => 'Num. de hojas',
                'required' => false
            ))
            ->add('especificaciones','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'200',
                    'title'=>'Especificaciones'),
                'label' => 'Especificaciones',
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\PrestamoBundle\Entity\Procedimiento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_prestamobundle_solicitud_procedimiento';
    }


}