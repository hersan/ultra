<?php

namespace Ultra\PrestamoBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PrestaRecomType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('recomendacion', 'entity', array(
                'empty_value' => 'Selecciona la RAP/RAC',
                'class' => 'CalidadBundle:Recomendaciones',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona la RAP/RAC'
                ),
                'label' => 'RAP/RAC',
                'query_builder' => function(EntityRepository $repository){
                        return $repository->createQueryBuilder('a')
                            ->join('a.estado','condicion')
                            ->where('condicion.id = 4')
                            ;
                    },
            ))

            ->add('nuHojas','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'3',
                    'title'=>'#'),
                'label' => 'Num. de hojas',
                'required' => false
            ))
            ->add('especificaciones','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> '#',
                    'maxlength'=>'200',
                    'title'=>'Especificaciones'),
                'label' => 'Especificaciones',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\PrestamoBundle\Entity\PrestaRecom'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_prestamobundle_prestarecom';
    }
}
