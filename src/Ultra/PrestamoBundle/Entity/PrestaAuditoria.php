<?php

namespace Ultra\PrestamoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PrestaAuditoria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\PrestamoBundle\Entity\PrestaAuditoriaRepository")
 */
class PrestaAuditoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="nuHojas", type="integer")
     */
    private $nuHojas;

    /**
     * @var string
     *
     * @ORM\Column(name="especificaciones", type="string", length=255)
     */
    private $especificaciones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitud", type="date")
     */
    private $fechaSolicitud;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRetorno", type="date", nullable=true)
     */
    private $fechaRetorno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRetornoReal", type="date", nullable=true)
     */
    private $fechaRetornoReal;

    /**
     * @Assert\NotBlank(message="Elija una persona...",groups={"autoriza"} )
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $solicitante;

    /**
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $autoriza;

    /**
     * @Assert\NotBlank(message="Elija la auditoria...")
     * @ORM\ManyToOne(targetEntity="Ultra\CalidadBundle\Entity\AuditoriaVigilancia")
     */
    private $auditoria;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return PrestaAuditoria
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set nuHojas
     *
     * @param integer $nuHojas
     * @return PrestaAuditoria
     */
    public function setNuHojas($nuHojas)
    {
        $this->nuHojas = $nuHojas;

        return $this;
    }

    /**
     * Get nuHojas
     *
     * @return integer
     */
    public function getNuHojas()
    {
        return $this->nuHojas;
    }

    /**
     * Set especificaciones
     *
     * @param string $especificaciones
     * @return PrestaAuditoria
     */
    public function setEspecificaciones($especificaciones)
    {
        $this->especificaciones = $especificaciones;

        return $this;
    }

    /**
     * Get especificaciones
     *
     * @return string
     */
    public function getEspecificaciones()
    {
        return $this->especificaciones;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     * @return PrestaAuditoria
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set fechaRetorno
     *
     * @param \DateTime $fechaRetorno
     * @return PrestaAuditoria
     */
    public function setFechaRetorno($fechaRetorno)
    {
        $this->fechaRetorno = $fechaRetorno;

        return $this;
    }

    /**
     * Get fechaRetorno
     *
     * @return \DateTime
     */
    public function getFechaRetorno()
    {
        return $this->fechaRetorno;
    }

    /**
     * Set fechaRetornoReal
     *
     * @param \DateTime $fechaRetornoReal
     * @return PrestaAuditoria
     */
    public function setFechaRetornoReal($fechaRetornoReal)
    {
        $this->fechaRetornoReal = $fechaRetornoReal;

        return $this;
    }

    /**
     * Get fechaRetornoReal
     *
     * @return \DateTime
     */
    public function getFechaRetornoReal()
    {
        return $this->fechaRetornoReal;
    }

    /**
     * Set solicitante
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $solicitante
     * @return PrestaAuditoria
     */
    public function setSolicitante(\Ultra\ControlDocumentoBundle\Entity\Curricula $solicitante = null)
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    /**
     * Get solicitante
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * Set autoriza
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $autoriza
     * @return PrestaAuditoria
     */
    public function setAutoriza(\Ultra\ControlDocumentoBundle\Entity\Curricula $autoriza = null)
    {
        $this->autoriza = $autoriza;

        return $this;
    }

    /**
     * Get autoriza
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getAutoriza()
    {
        return $this->autoriza;
    }

    /**
     * Set auditoria
     *
     * @param \Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditoria
     * @return Auditoria
     */
    public function setAuditoria(\Ultra\CalidadBundle\Entity\AuditoriaVigilancia $auditoria = null)
    {
        $this->auditoria = $auditoria;

        return $this;
    }

    /**
     * Get auditoria
     *
     * @return \Ultra\CalidadBundle\Entity\AuditoriaVigilancia
     */
    public function getAuditoria()
    {
        return $this->auditoria;
    }
}