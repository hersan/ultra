<?php

namespace Ultra\PrestamoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Archivo
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\PrestamoBundle\Entity\ArchivoRepository")
 */
class Archivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="nuHojas", type="integer", nullable=true)
     */
    private $nuHojas;

    /**
     * @var string
     *
     * @ORM\Column(name="especificaciones", type="string", length=255)
     */
    private $especificaciones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitud", type="date")
     */
    private $fechaSolicitud;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\Column(name="fechaRetorno", type="date", nullable=true)
     */
    private $fechaRetorno;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"devolver"})
     * @ORM\Column(name="fechaRetornoReal", type="date", nullable=true)
     */
    private $fechaRetornoReal;

    /**
     * @Assert\NotBlank(message="Elija una persona...",groups={"autoriza"} )
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $solicitante;

    /**
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $autoriza;

    /**
     * @Assert\NotBlank(message="Elija el expediente...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $expediente;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return Archivo
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return integer 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param integer $tipoSolicitud
     * @return Archivo
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;
    
        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return integer 
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set nuHojas
     *
     * @param integer $nuHojas
     * @return Archivo
     */
    public function setNuHojas($nuHojas)
    {
        $this->nuHojas = $nuHojas;
    
        return $this;
    }

    /**
     * Get nuHojas
     *
     * @return integer 
     */
    public function getNuHojas()
    {
        return $this->nuHojas;
    }

    /**
     * Set especificaciones
     *
     * @param string $especificaciones
     * @return Archivo
     */
    public function setEspecificaciones($especificaciones)
    {
        $this->especificaciones = $especificaciones;
    
        return $this;
    }

    /**
     * Get especificaciones
     *
     * @return string 
     */
    public function getEspecificaciones()
    {
        return $this->especificaciones;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     * @return Archivo
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;
    
        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime 
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set fechaRetorno
     *
     * @param \DateTime $fechaRetorno
     * @return Archivo
     */
    public function setFechaRetorno($fechaRetorno)
    {
        $this->fechaRetorno = $fechaRetorno;
    
        return $this;
    }

    /**
     * Get fechaRetorno
     *
     * @return \DateTime 
     */
    public function getFechaRetorno()
    {
        return $this->fechaRetorno;
    }

    /**
     * Set fechaRetornoReal
     *
     * @param \DateTime $fechaRetornoReal
     * @return Archivo
     */
    public function setFechaRetornoReal($fechaRetornoReal)
    {
        $this->fechaRetornoReal = $fechaRetornoReal;
    
        return $this;
    }

    /**
     * Get fechaRetornoReal
     *
     * @return \DateTime 
     */
    public function getFechaRetornoReal()
    {
        return $this->fechaRetornoReal;
    }

    /**
     * Set Solicitante
     *
     * @param integer $solicitante
     * @return Archivo
     */
    public function setSolicitante($solicitante)
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    /**
     * Get solicitante
     *
     * @return integer
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * Set Autoriza
     *
     * @param integer $autoriza
     * @return Archivo
     */
    public function setAutoriza($autoriza)
    {
        $this->autoriza = $autoriza;

        return $this;
    }

    /**
     * Get Autoriza
     *
     * @return integer
     */
    public function getAutoriza()
    {
        return $this->autoriza;
    }

    /**
     * Set Expediente
     *
     * @param integer $expediente
     * @return Archivo
     */
    public function setExpediente($expediente)
    {
        $this->expediente = $expediente;

        return $this;
    }

    /**
     * Get expediente
     *
     * @return integer
     */
    public function getExpediente()
    {
        return $this->expediente;
    }
}
