<?php

namespace Ultra\PrestamoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Sot
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\PrestamoBundle\Entity\SotRepository")
 */
class Sot
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="nuHojas", type="integer", nullable=true)
     */
    private $nuHojas;

    /**
     * @var string
     *
     * @ORM\Column(name="especificaciones", type="string", length=255)
     */
    private $especificaciones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitud", type="date")
     */
    private $fechaSolicitud;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\Column(name="fechaRetorno", type="date", nullable=true)
     */
    private $fechaRetorno;

    /**
     * @var \DateTime
     * @Assert\NotBlank(groups={"devolver"})
     * @ORM\Column(name="fechaRetornoReal", type="date", nullable=true)
     */
    private $fechaRetornoReal;

    /**
     * @Assert\NotBlank(message="Elija una persona...",groups={"autoriza"} )
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $solicitante;

    /**
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $autoriza;

    /**
     * Assert\NotBlank(message="Elija la orden de trabajo...")
     * @ORM\ManyToOne(targetEntity="Ultra\ProyectoBundle\Entity\OrdenTrabajo")
     */
    private $ot;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return Sot
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoSolicitud
     *
     * @param integer $tipoSolicitud
     * @return Sot
     */
    public function setTipoSolicitud($tipoSolicitud)
    {
        $this->tipoSolicitud = $tipoSolicitud;

        return $this;
    }

    /**
     * Get tipoSolicitud
     *
     * @return integer
     */
    public function getTipoSolicitud()
    {
        return $this->tipoSolicitud;
    }

    /**
     * Set nuHojas
     *
     * @param integer $nuHojas
     * @return Sot
     */
    public function setNuHojas($nuHojas)
    {
        $this->nuHojas = $nuHojas;

        return $this;
    }

    /**
     * Get nuHojas
     *
     * @return integer
     */
    public function getNuHojas()
    {
        return $this->nuHojas;
    }

    /**
     * Set especificaciones
     *
     * @param string $especificaciones
     * @return Sot
     */
    public function setEspecificaciones($especificaciones)
    {
        $this->especificaciones = $especificaciones;

        return $this;
    }

    /**
     * Get especificaciones
     *
     * @return string
     */
    public function getEspecificaciones()
    {
        return $this->especificaciones;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     * @return Sot
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set fechaRetorno
     *
     * @param \DateTime $fechaRetorno
     * @return Sot
     */
    public function setFechaRetorno($fechaRetorno)
    {
        $this->fechaRetorno = $fechaRetorno;

        return $this;
    }

    /**
     * Get fechaRetorno
     *
     * @return \DateTime
     */
    public function getFechaRetorno()
    {
        return $this->fechaRetorno;
    }

    /**
     * Set fechaRetornoReal
     *
     * @param \DateTime $fechaRetornoReal
     * @return Sot
     */
    public function setFechaRetornoReal($fechaRetornoReal)
    {
        $this->fechaRetornoReal = $fechaRetornoReal;

        return $this;
    }

    /**
     * Get fechaRetornoReal
     *
     * @return \DateTime
     */
    public function getFechaRetornoReal()
    {
        return $this->fechaRetornoReal;
    }

    /**
     * Set Solicitante
     *
     * @param integer $solicitante
     * @return Sot
     */
    public function setSolicitante($solicitante)
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    /**
     * Get solicitante
     *
     * @return integer
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * Set Autoriza
     *
     * @param integer $autoriza
     * @return Sot
     */
    public function setAutoriza($autoriza)
    {
        $this->autoriza = $autoriza;

        return $this;
    }

    /**
     * Get Autoriza
     *
     * @return integer
     */
    public function getAutoriza()
    {
        return $this->autoriza;
    }

    /**
     * Set Ot
     *
     * @param integer $Ot
     * @return Sot
     */
    public function setOt($Ot)
    {
        $this->ot = $Ot;

        return $this;
    }

    /**
     * Get Ot
     *
     * @return integer
     */
    public function getOt()
    {
        return $this->ot;
    }

}
