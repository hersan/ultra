<?php

namespace Ultra\PrestamoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PrestaPerfil
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\PrestamoBundle\Entity\PrestaPerfilRepository")
 */
class PrestaPerfil
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    /**
     * @var integer
     *
     * @ORM\Column(name="nuHojas", type="integer")
     */
    private $nuHojas;

    /**
     * @var string
     *
     * @ORM\Column(name="especificaciones", type="string", length=255)
     */
    private $especificaciones;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaSolicitud", type="date")
     */
    private $fechaSolicitud;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRetorno", type="date", nullable=true)
     */
    private $fechaRetorno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaRetornoReal", type="date", nullable=true)
     */
    private $fechaRetornoReal;

    /**
     * @Assert\NotBlank(message="Elija una persona...",groups={"autoriza"} )
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $solicitante;

    /**
     * @Assert\NotBlank(groups={"autoriza"})
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $autoriza;

    /**
     * @Assert\NotBlank(message="Elija el perfil...")
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil")
     */
    private $perfil;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estado
     *
     * @param integer $estado
     * @return PrestaPerfil
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return integer
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set nuHojas
     *
     * @param integer $nuHojas
     * @return PrestaPerfil
     */
    public function setNuHojas($nuHojas)
    {
        $this->nuHojas = $nuHojas;

        return $this;
    }

    /**
     * Get nuHojas
     *
     * @return integer
     */
    public function getNuHojas()
    {
        return $this->nuHojas;
    }

    /**
     * Set especificaciones
     *
     * @param string $especificaciones
     * @return PrestaPerfil
     */
    public function setEspecificaciones($especificaciones)
    {
        $this->especificaciones = $especificaciones;

        return $this;
    }

    /**
     * Get especificaciones
     *
     * @return string
     */
    public function getEspecificaciones()
    {
        return $this->especificaciones;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     * @return PrestaPerfil
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set fechaRetorno
     *
     * @param \DateTime $fechaRetorno
     * @return PrestaPerfil
     */
    public function setFechaRetorno($fechaRetorno)
    {
        $this->fechaRetorno = $fechaRetorno;

        return $this;
    }

    /**
     * Get fechaRetorno
     *
     * @return \DateTime
     */
    public function getFechaRetorno()
    {
        return $this->fechaRetorno;
    }

    /**
     * Set fechaRetornoReal
     *
     * @param \DateTime $fechaRetornoReal
     * @return PrestaPerfil
     */
    public function setFechaRetornoReal($fechaRetornoReal)
    {
        $this->fechaRetornoReal = $fechaRetornoReal;

        return $this;
    }

    /**
     * Get fechaRetornoReal
     *
     * @return \DateTime
     */
    public function getFechaRetornoReal()
    {
        return $this->fechaRetornoReal;
    }

    /**
     * Set solicitante
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $solicitante
     * @return PrestaPerfil
     */
    public function setSolicitante(\Ultra\ControlDocumentoBundle\Entity\Curricula $solicitante = null)
    {
        $this->solicitante = $solicitante;

        return $this;
    }

    /**
     * Get solicitante
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getSolicitante()
    {
        return $this->solicitante;
    }

    /**
     * Set autoriza
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $autoriza
     * @return PrestaPerfil
     */
    public function setAutoriza(\Ultra\ControlDocumentoBundle\Entity\Curricula $autoriza = null)
    {
        $this->autoriza = $autoriza;

        return $this;
    }

    /**
     * Get autoriza
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getAutoriza()
    {
        return $this->autoriza;
    }

    /**
     * Set perfil
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $perfil
     * @return Perfil
     */
    public function setPerfil(\Ultra\ControlDocumentoBundle\Entity\Perfil $perfil = null)
    {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil
     */
    public function getPerfil()
    {
        return $this->perfil;
    }
}