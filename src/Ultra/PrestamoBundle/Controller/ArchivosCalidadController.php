<?php

namespace Ultra\PrestamoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\PrestamoBundle\Entity\Procedimiento;
use Ultra\PrestamoBundle\Entity\Sot;
use Ultra\PrestamoBundle\Form\SolicitudProcType;
use Ultra\PrestamoBundle\Form\SolicitudSotType;
use \BusinessDays\Calculator;

/**
 * ArchivosCalidad controller.
 *
 * @Route("/archivos/calidad")
 */
class ArchivosCalidadController extends Controller
{

    /**
     * Lists all ArchivosCalidad entities.
     *
     * @Route("/", name="archivos_calidad")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuario=$this->getUser()->getCurricula()->getId();
        //ladybug_dump_die($usuario);
        //$entities = $em->getRepository('PrestamoBundle:Archivo')->findAll();

        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Procedimiento a JOIN a.solicitante c
            WHERE c.id = :id
        ")->setParameters( array('id'=>$usuario));
        $entities = $query->getResult();


        return array(
            'entities' => $entities,
        );
    }

    /**
     * Prestamos por autorizar
     *
     * @Route("/{t}/plist", name="archivos_calidad_plist")
     * @Method("GET")
     * @Template("PrestamoBundle:ArchivosCalidad:plist.html.twig")
     */
    public function plistAction($t)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Archivo a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities = $query->getResult();

        //SOTS-----------------

        $query1 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Sot a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entitiess = $query1->getResult();

        //Procedimientos
        $query2 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Procedimiento a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities3 = $query2->getResult();

        //Prerfiles
        $query3 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaPerfil a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities4 = $query3->getResult();

        //Auditorias
        $query4 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaAuditoria a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities5 = $query4->getResult();

        //Recomendaciones
        $query5 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaRecom a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities6 = $query5->getResult();

        return array(
            'entities' => $entities,
            'entitiess' => $entitiess,
            'entities3' => $entities3,
            'entities4' => $entities4,
            'entities5' => $entities5,
            'entities6' => $entities6,
            't'=> $t
        );
    }

    /**
     * Creates a new Archivo entity.
     *
     * @Route("/", name="archivos_calidad_create")
     * @Method("POST")
     * @Template("PrestamoBundle:ArchivosCalidad:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Procedimiento();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $today=new \DateTime();

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        $usuario = $this->getUser()->getUsername();

            $entity->setEstado(1);
            $entity->setFechaSolicitud($today);
            $entity->setSolicitante($this->getUser()->getCurricula());


        //Consulta si la curricula no esta prestada
        $em = $this->getDoctrine()->getManager();
        $proc=$entity->getProcedimiento();

        $query = $em->createQuery("
            SELECT c,a FROM PrestamoBundle:Procedimiento a JOIN a.procedimiento c
            WHERE c.id = :id and a.estado=4
        ")->setParameters( array('id'=>$proc));
        $result = $query->getResult();

        if (empty ($result))
        {

            //--------------------------------------

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'success',
                    'La solicitud fue enviada'
                );

                $message = \Swift_Message::newInstance()
                    ->setSubject('Autorizaciones Pendientes')
                    ->setFrom(array('sad.alert@ultraingenieria.com.mx' => 'Sistema de Administración de Documentos'))
                    ->setTo('dgalindo@ultraingenieria.com.mx')
                    ->setBody(
                        $this->renderView(
                            'PrestamoBundle:Emails:notificar_prestamo.html.twig',
                            array('name' => '')
                        ),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);

                return $this->redirect($this->generateUrl('archivos_calidad_show', array('id' => $entity->getId())));
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El expediente que necesita se encuentra prestado.'
            );
            return $this->redirect($this->generateUrl('archivos_calidad'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Archivo entity.
     *
     * @param ArchivosCalidad $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Procedimiento $entity)
    {
        $form = $this->createForm(new SolicitudProcType(), $entity, array(
            'action' => $this->generateUrl('archivos_calidad_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new ArchivosCalidad entity.
     *
     * @Route("/new", name="archivos_calidad_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Procedimiento();


        $form   = $this->createCreateForm($entity);


        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Archivo entity.
     *
     * @Route("/{id}", name="archivos_calidad_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:Procedimiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la información.');
        }

        return array(
            'entity'      => $entity,
        );
    }


    /**
     * Edits an existing Archivo entity.
     *
     * @Route("/{id}/{t}/update", name="archivos_calidad_update")
     */
    public function updateAction($id,$t)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:Procedimiento')->find($id);

        //Actualizo los datos

        //$day=new \DateTime();
        $today=new \DateTime();

        //$day->modify("+15 days");

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        //----------------------


        $entity->setEstado($t);

        //Si es autorización cambia la fecha
        if ($t==2 or $t==4) {   $entity->setFechaRetorno($returnday);   $entity->setAutoriza($this->getUser()->getCurricula()); }
        //Si es devolución cambia la fecha real
        if ($t==6) {   $entity->setFechaRetornoReal($today);   }

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el dato');
        }

        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success',
            'Los datos fueron actualizados'
        );


        return $this->redirect($this->generateUrl('archivos_calidad_plist', array('t' => $t)));

    }

    /**
     * Deletes a Archivo entity.
     *
     * @Route("/{id}", name="archivos_calidad_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PrestamoBundle:Procedimiento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Archivo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('archivos_calidad'));
    }

    /**
     * Creates a form to delete a Archivo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('archivo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
