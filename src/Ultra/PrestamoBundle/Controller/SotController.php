<?php

namespace Ultra\PrestamoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\PrestamoBundle\Entity\Sot;
use Ultra\PrestamoBundle\Entity\Curricula;
use Ultra\PrestamoBundle\Form\SolicitudSotType;

use \BusinessDays\Calculator;

/**
 * Sot controller.
 *
 * @Route("/sot")
 */
class SotController extends Controller
{

    /**
     * Lists all Sot entities.
     *
     * @Route("/", name="sot_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuario=$this->getUser()->getCurricula()->getId();


        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Sot a JOIN a.solicitante c
            WHERE c.id = :id
        ")->setParameters( array('id'=>$usuario));
        $entities = $query->getResult();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Prestamos por autorizar.
     *
     * @Route("/{t}/plist", name="sot_plist")
     * @Method("GET")
     * @Template("PrestamoBundle:ArchivosCalidad:plist.html.twig")
     */
    public function plistAction($t)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Archivo a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities = $query->getResult();

        //SOTS-----------------

        $query1 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Sot a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entitiess = $query1->getResult();

        //Procedimientos
        $query2 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Procedimiento a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities3 = $query2->getResult();

        //Perfiles
        $query3 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaPerfil a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities4 = $query3->getResult();

        //Auditorias
        $query4 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaAuditoria a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities5 = $query4->getResult();

        //Recomendaciones
        $query5 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaRecom a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities6 = $query5->getResult();

        return array(
            'entities' => $entities,
            'entitiess' => $entitiess,
            'entities3' => $entities3,
            'entities4' => $entities4,
            'entities5' => $entities5,
            'entities6' => $entities6,
            't'=> $t
        );
    }

    /**
     * Creates a new Sot entity.
     *
     * @Route("/", name="sot_create")
     * @Method("POST")
     * @Template("PrestamoBundle:Sot:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Sot();

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $today=new \DateTime();

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        /**
         * @var $usuario \Ultra\UsuarioBundle\Entity\UltraUser
         */
        $usuario = $this->getUser();

        if(null === $usuario->getCurricula()){
            throw $this->createNotFoundException('Este usuario no tiene una Curricula relacionada');
        }

            $entity->setEstado(1);
            $entity->setFechaSolicitud($today);
            $entity->setSolicitante($usuario->getCurricula());


        //Consulta si la curricula no esta prestada
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $sot = $entity->getOt()->getId();

            $query = $em->createQuery(
                "
                SELECT c,a FROM PrestamoBundle:Sot a JOIN a.ot c
                WHERE c.id = :id and a.estado=4
            "
            )->setParameters(array('id' => $sot));
            $result = $query->getResult();
        }

        if (empty ($result))
        {

            //--------------------------------------

            if ($form->isValid()) {
                //$form->get('submit')->isClicked();
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'La solicitud fue enviada'
                );

                $message = \Swift_Message::newInstance()
                    ->setSubject('Autorizaciones Pendientes')
                    ->setFrom(array('sad.alert@ultraingenieria.com.mx' => 'Sistema de Administración de Documentos'))
                    ->setTo('dgalindo@ultraingenieria.com.mx')
                    ->setBody(
                        $this->renderView(
                            'PrestamoBundle:Emails:notificar_prestamo.html.twig',
                            array('name' => '')
                        ),
                        'text/html'
                    )
                ;

                $this->get('mailer')->send($message);

                return $this->redirect($this->generateUrl('sot_show', array('id' => $entity->getId())));
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El expediente que necesita se encuentra prestado.'
            );
            return $this->redirect($this->generateUrl('sot_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sot entity.
     *
     * @param Sot $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sot $entity)
    {
        $form = $this->createForm(new SolicitudSotType(), $entity, array(
            'action' => $this->generateUrl('sot_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Sot entity.
     *
     * @Route("/new", name="sot_new")
     * @Method({"GET","POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Sot();


        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);


        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sot entity.
     *
     * @Route("/{id}", name="sot_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:Sot')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar la información.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     * Edits an existing Sot entity.
     *
     * @Route("/{id}/{t}/update", name="sot_update")
     */
    public function updateAction($id,$t)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:Sot')->find($id);


        //Actualizo los datos

        //$day=new \DateTime();
        $today=new \DateTime();

        //$day->modify("+15 days");

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        //----------------------


        $entity->setEstado($t);

        //Si es autorización cambia la fecha
        if ($t==2 or $t==4) {   $entity->setFechaRetorno($returnday);   $entity->setAutoriza($this->getUser()->getCurricula()); }
        //Si es devolución cambia la fecha real
        if ($t==6) {   $entity->setFechaRetornoReal($today);   }

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el dato');
        }

        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success',
            'Los datos fueron actualizados'
        );


        return $this->redirect($this->generateUrl('sot_plist', array('t' => $t)));

    }

    /**
     * Deletes a Sot entity.
     *
     * @Route("/{id}", name="sot_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PrestamoBundle:Sot')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sot entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('sot_index'));
    }

    /**
     * Creates a form to delete a Sot entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sot_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
