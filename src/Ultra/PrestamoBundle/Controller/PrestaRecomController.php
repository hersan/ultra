<?php

namespace Ultra\PrestamoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\PrestamoBundle\Entity\PrestaRecom;
use Ultra\PrestamoBundle\Form\PrestaRecomType;
use \BusinessDays\Calculator;

/**
 * PrestaRecom controller.
 *
 * @Route("/presta/recom")
 */
class PrestaRecomController extends Controller
{

    /**
     * Lists all PrestaRecom entities.
     *
     * @Route("/", name="presta_recom")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuario=$this->getUser()->getCurricula()->getId();

        //ladybug_dump_die($usuario);
        //$entities = $em->getRepository('PrestamoBundle:Archivo')->findAll();

        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaRecom a JOIN a.solicitante c
            WHERE c.id = :id
        ")->setParameters( array('id'=>$usuario));
        $entities = $query->getResult();


        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new PrestaRecom entity.
     *
     * @Route("/", name="presta_recom_create")
     * @Method("POST")
     * @Template("PrestamoBundle:PrestaRecom:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new PrestaRecom();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $today=new \DateTime();

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        $usuario = $this->getUser()->getUsername();

        $entity->setEstado(2);
        $entity->setFechaSolicitud($today);
        $entity->setSolicitante($this->getUser()->getCurricula());


        //Consulta si la curricula no esta prestada
        $em = $this->getDoctrine()->getManager();
        $recomendacion=$entity->getRecomendacion();

        $query = $em->createQuery("
            SELECT c,a FROM PrestamoBundle:PrestaRecom a JOIN a.recomendacion c
            WHERE c.id = :id and a.estado=4
        ")->setParameters( array('id'=>$recomendacion));
        $result = $query->getResult();

        if (empty ($result))
        {

            //--------------------------------------

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                    'success',
                    'La solicitud fue enviada'
                );

                return $this->redirect($this->generateUrl('presta_recom_show', array('id' => $entity->getId())));
            }
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El expediente que necesita se encuentra prestado.'
            );
            return $this->redirect($this->generateUrl('presta_recom'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a PrestaRecom entity.
     *
     * @param PrestaRecom $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(PrestaRecom $entity)
    {
        $form = $this->createForm(new PrestaRecomType(), $entity, array(
            'action' => $this->generateUrl('presta_recom_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new PrestaRecom entity.
     *
     * @Route("/new", name="presta_recom_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new PrestaRecom();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a PrestaRecom entity.
     *
     * @Route("/{id}", name="presta_recom_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:PrestaRecom')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PrestaRecom entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }

    /**
     * Prestamos por autorizar
     *
     * @Route("/{t}/plist", name="presta_recom_plist")
     * @Method("GET")
     * @Template("PrestamoBundle:ArchivosCalidad:plist.html.twig")
     */
    public function plistAction($t)
    {
        $em = $this->getDoctrine()->getManager();


        $query = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Archivo a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities = $query->getResult();

        //SOTS-----------------

        $query1 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Sot a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entitiess = $query1->getResult();

        //Procedimientos
        $query2 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:Procedimiento a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities3 = $query2->getResult();

        //Perfiles
        $query3 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaPerfil a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities4 = $query3->getResult();

        //Auditorias
        $query4 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaAuditoria a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities5 = $query4->getResult();

        //Recomendaciones
        $query5 = $em->createQuery("
            SELECT a,c FROM PrestamoBundle:PrestaRecom a JOIN a.solicitante c
            WHERE a.estado = :e
        ")->setParameters( array('e'=>$t));
        $entities6 = $query5->getResult();

        return array(
            'entities' => $entities,
            'entitiess' => $entitiess,
            'entities3' => $entities3,
            'entities4' => $entities4,
            'entities5' => $entities5,
            'entities6' => $entities6,
            't'=> $t
        );
    }

    /**
     * Edits an existing PrestaAuditoria entity.
     *
     * @Route("/{id}/{t}/update", name="presta_recom_update")
     */
    public function updateAction($id,$t)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PrestamoBundle:PrestaRecom')->find($id);


        //$day=new \DateTime();
        $today=new \DateTime();

        //$day->modify("+15 days");

        //calcula 15 días hábiles
        $freeWeekDays = [
            Calculator::SATURDAY,
            Calculator::SUNDAY
        ];

        $day = new Calculator();
        $day->setStartDate(new \DateTime());
        $day->setFreeWeekDays($freeWeekDays); // repeat every week

        $day->addBusinessDays(15);             // add X working days

        $returnday = $day->getDate();            // \DateTime

        //----------------------


        $entity->setEstado($t);

        //Si es autorización cambia la fecha
        if ($t==2 or $t==4) {   $entity->setFechaRetorno($returnday);   $entity->setAutoriza($this->getUser()->getCurricula()); }
        //Si es devolución cambia la fecha real
        if ($t==6) {   $entity->setFechaRetornoReal($today);   }


        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el dato.');
        }


        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'success',
            'Los datos fueron actualizados'
        );


        return $this->redirect($this->generateUrl('presta_recom_plist', array('t' => $t)));
    }

}
