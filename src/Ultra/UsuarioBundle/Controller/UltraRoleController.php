<?php

namespace Ultra\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\UsuarioBundle\Entity\UltraRole;
use Ultra\UsuarioBundle\Form\UltraRoleType;
use Symfony\Component\HttpFoundation\Response;

/**
 * UltraRole controller.
 *
 * @Route("/roles")
 */
class UltraRoleController extends Controller
{

    /**
     * Lists all UltraRole entities.
     *
     * @Route("/", name="roles")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UsuarioBundle:UltraRole')->findAll();

      //   $paginator  = $this->get('knp_paginator');
      //  $pagination = $paginator->paginate($entities,$this->get('request')->query->get('page', 1),10);

        return array(
            'entities' => $entities
        );

    }

    /**
     * Creates a new User entity.
     *
     * @Route("/rolespdf", name="roles_pdf")
     * @Method("GET")
     * @Template()
     */
    public function rolespdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UsuarioBundle:UltraRole')->findAll();

        $html = $this->renderView('UsuarioBundle:UltraRole:rolespdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Áreas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listaderoles.pdf"',
            )
        );

        return $response;

    }



    /**
     * Creates a new UltraRole entity.
     *
     * @Route("/", name="roles_create")
     * @Method("POST")
     * @Template("UsuarioBundle:UltraRole:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new UltraRole();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El rol fue agregado con éxito con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('roles_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El rol no pudo ser agregado'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a UltraRole entity.
    *
    * @param UltraRole $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(UltraRole $entity)
    {
        $form = $this->createForm(new UltraRoleType(), $entity, array(
            'action' => $this->generateUrl('roles_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array('label' => 'Guardar', 'attr' => array('class' => 'btn btn-success pull-left',
            'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new UltraRole entity.
     *
     * @Route("/new", name="roles_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UltraRole();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a UltraRole entity.
     *
     * @Route("/{id}", name="roles_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:UltraRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraRole entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UltraRole entity.
     *
     * @Route("/{id}/edit", name="roles_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:UltraRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraRole entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a UltraRole entity.
    *
    * @param UltraRole $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UltraRole $entity)
    {
        $form = $this->createForm(new UltraRoleType(), $entity, array(
            'action' => $this->generateUrl('roles_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array('label' => 'Actualizar', 'attr' => array('class' => 'btn btn-success',
            'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit',));

        return $form;
    }
    /**
     * Edits an existing UltraRole entity.
     *
     * @Route("/{id}", name="roles_update")
     * @Method("PUT")
     * @Template("UsuarioBundle:UltraRole:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:UltraRole')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraRole entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos del rol han sido actualizados'
            );
            return $this->redirect($this->generateUrl('roles_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'Los datos del rol no pudieron actualizarse.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a UltraRole entity.
     *
     * @Route("/{id}", name="roles_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UsuarioBundle:UltraRole')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UltraRole entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'El rol fue eliminado con éxito.'
            );
            return $this->redirect($this->generateUrl('roles'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El rol no pudo ser eliminado.'
            );
        }

        return $this->redirect($this->generateUrl('roles'));
    }

    /**
     * Creates a form to delete a UltraRole entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('roles_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array('label' => 'Borrar',  'attr' => array('class' => 'btn btn-warning',
                'title'=>'Borrar datos'),
                'glyphicon' => 'glyphicon glyphicon-trash'))
            ->getForm()
        ;
    }
}
