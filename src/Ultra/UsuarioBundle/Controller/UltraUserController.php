<?php

namespace Ultra\UsuarioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\UsuarioBundle\Entity\UltraUser;
use Ultra\UsuarioBundle\Form\UltraUserType;
use Symfony\Component\HttpFoundation\Response;

/**
 * UltraUser controller.
 *
 * @Route("/users")
 */
class UltraUserController extends Controller
{

    /**
     * Lists all UltraUser entities.
     *
     * @Route("/", name="users")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('UsuarioBundle:UltraUser')->findAll();

        //$paginator  = $this->get('knp_paginator');
        //$pagination = $paginator->paginate($entities,$this->get('request')->query->get('page', 1),10);

        return array(
            'entities' => $entities
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/userspdf", name="users_pdf")
     * @Method("GET")
     * @Template()
     */
    public function userspdfAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('UsuarioBundle:UltraUser')->findAll();

        $html = $this->renderView('UsuarioBundle:UltraUser:userspdf.html.twig',
            array(
                'entities' => $entities,
            ));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title'=> 'Áreas',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="listadeusuarios.pdf"',
            )
        );

        return $response;

    }

    /**
     * Creates a new UltraUser entity.
     *
     * @Route("/", name="users_create")
     * @Method("POST")
     * @Template("UsuarioBundle:UltraUser:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new UltraUser();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
        $encodePassword = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());

        if ($form->isValid()) {
            $entity->setPassword($encodePassword);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El usuario se agregó con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('users_show', array('id' => $entity->getId())));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El usuario no se agregó correctamente'
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a UltraUser entity.
    *
    * @param UltraUser $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(UltraUser $entity)
    {
        $form = $this->createForm(new UltraUserType(), $entity, array(
            'action' => $this->generateUrl('users_create'),
            'method' => 'POST',
            'validation_groups' => array('creation')
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success pull-left',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'

            ));

        return $form;
    }

    /**
     * Displays a form to create a new UltraUser entity.
     *
     * @Route("/new", name="users_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UltraUser();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a UltraUser entity.
     *
     * @Route("/{id}", name="users_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:UltraUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraUser entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing UltraUser entity.
     *
     * @Route("/{id}/edit", name="users_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('UsuarioBundle:UltraUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraUser entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a UltraUser entity.
    *
    * @param UltraUser $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(UltraUser $entity)
    {
        $form = $this->createForm(new UltraUserType(), $entity, array(
            'action' => $this->generateUrl('users_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'validation_groups' => array('editing'),
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));

        return $form;
    }

    /**
     * Edits an existing UltraUser entity.
     *
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/{id}", name="users_update")
     * @Method("PUT")
     * @Template("UsuarioBundle:UltraUser:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var $entity \Ultra\UsuarioBundle\Entity\UltraUser
         */
        $entity = $em->getRepository('UsuarioBundle:UltraUser')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UltraUser entity.');
        }

        $currentPassword = $entity->getPassword();

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if($entity->getPassword() === null){
                $entity->setPassword($currentPassword);
            }else{
                $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
                $encodePassword = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
                $entity->setPassword($encodePassword);
            }
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Se modificaron los datos del usuario de la siguiente manera:'
            );

            return $this->redirect($this->generateUrl('users_show', array('id' => $id)));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'Los datos del usuario no pudieron modificarse.'
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a UltraUser entity.
     *
     * @Route("/{id}", name="users_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('UsuarioBundle:UltraUser')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UltraUser entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'El usuario fue eliminado.'
            );
            return $this->redirect($this->generateUrl('users'));
        }
        else
        {
            $this->get('session')->getFlashBag()->add(
                'danger',
                'El usuario no pudo ser eliminado'
            );
        }

        return $this->redirect($this->generateUrl('users'));
    }

    /**
     * Creates a form to delete a UltraUser entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
