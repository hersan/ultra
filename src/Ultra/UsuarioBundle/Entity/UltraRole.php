<?php

namespace Ultra\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

//use Ultra\UsuarioBundle\Entity\UltraUser;

/**
 * UltraRole
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\UsuarioBundle\Entity\UltraRoleRepository")
 */
class UltraRole implements RoleInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="role", type="string", length=50, unique=true)
     */
    private $role;

    /**
     * @var string
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="interface", type="string", length=20)
     */
    private $interface;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="Ultra\UsuarioBundle\Entity\UltraUser", mappedBy="roles")
     */
    private $users;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UltraRole
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return UltraRole
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }
    
    /**
     * Add users
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraUser $users
     * @return UltraRole
     */
    public function addUser(UltraUser $users)
    {
        $this->users->add($users);
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraUser $users
     */
    public function removeUser(UltraUser $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function __toString(){
        return $this->getName();
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return \serialize(array(
                $this->id,
                $this->name,
                $this->role,
                $this->interface
            ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name,
            $this->role,
            $this->interface
            ) = \unserialize($serialized);
    }

    /**
     * Set interface
     *
     * @param \String $interface
     * @return UltraRole
     */
    public function setInterface($interface)
    {
        $this->interface = $interface;
    
        return $this;
    }

    /**
     * Get interface
     *
     * @return \String 
     */
    public function getInterface()
    {
        return $this->interface;
    }
}