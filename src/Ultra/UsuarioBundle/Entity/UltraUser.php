<?php

namespace Ultra\UsuarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * UltraUser Contiene los usuario del sistema
 *
 * @author Herminio Heredia Santos <herminio.heredia@hotmail.com>
 * @version 1.0
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\UsuarioBundle\Entity\UltraUserRepository")
 * @UniqueEntity(fields={"username"}, groups={"creation","editing"})
 *
 */
class UltraUser implements AdvancedUserInterface, \Serializable
{
    /**
     * @var integer Identifica de forma unica al usuario
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string Representa el nombre de usuario
     *
     * @Assert\NotBlank(message="Este campo no puede estar vacío", groups={"creation","editing"})
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="username", type="string", length=40, unique=true)
     */
    private $username;

    /**
     * @var string contiene la contraseña del usuario
     *
     * @Assert\NotBlank(message="Este campo no puede estar vacío",groups={"creation"})
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="password", type="string", length=128)
     */
    private $password;

    /**
     * @var string semilla para la generación de la contraseña
     *
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @Assert\Type(type="string", message="El valor {{ value }} no es de tipo {{ type }}.")
     * @ORM\Column(name="salt", type="string", length=32)
     */
    private $salt;

    /**
     * @var boolean determina si la cuenta esta habilitada para acceder a la aplicación
     *
     * @Assert\NotBlank(message="Este campo no puede estar vacío")
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    private $isEnabled;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="\Ultra\UsuarioBundle\Entity\UltraRole", inversedBy="users")
     */
    private $roles;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="\Ultra\ControlDocumentoBundle\Entity\Curricula", inversedBy="user")
     */
    private $curricula;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return UltraUser
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return UltraUser
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return UltraUser
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return Boolean true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return Boolean true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return Boolean true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return Boolean true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
       return $this->roles->toArray();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {

    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isEnabled = true;
        $this->roles = new ArrayCollection();
        $this->salt = md5(uniqid('ultra.dsad',true));
    }
    
    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     * @return UltraUser
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    
        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean 
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Add roles
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraRole $roles
     * @return UltraUser
     */
    public function addRole(UltraRole $roles)
    {
        $this->roles->add($roles);
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Ultra\UsuarioBundle\Entity\UltraRole $roles
     */
    public function removeRole(UltraRole $roles)
    {
        $this->roles->removeElement($roles);
    }

    public function __toString(){
        return $this->getUsername();
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return \serialize(array(
               $this->id,
               $this->username,
               $this->password,
               $this->salt,
            ));
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list(
                $this->id,
                $this->username,
                $this->password,
                $this->salt,
            ) = \unserialize($serialized);
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return UltraUser
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;
    
        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCurricula()
    {
        return $this->curricula;
    }

    /**
     * Determina si el usuario cuenta con un Rol
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role){

        $checkRole = function($role)
        {
            return function($key, UltraRole $roles) use ($role){
                return $roles->getRole() == $role;
            };
        };

        return $this->roles->exists($checkRole($role));
    }
}