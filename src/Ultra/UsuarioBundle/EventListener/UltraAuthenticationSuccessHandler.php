<?php
/**
 * Proyect: Ultra.
 * Archivo: UltraAuthenticationSuccessHandler.php.
 * @author: Herminio Heredia <herminio.heredia@hotmail.com>
 */

namespace Ultra\UsuarioBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Ultra\UsuarioBundle\Entity\UltraRole;

/**
 * Class UltraAuthenticationSuccessHandler
 * @package Ultra\UsuarioBundle\EventListener
 */
class UltraAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var \Symfony\Component\Routing\Router Routes
     */
    protected $router;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext Security Context
     */
    protected $securityContext;

    /**
     * Constructor
     *
     * @param Router $router
     * @param SecurityContext $securityContext
     * @param EntityManager $entityManager
     */
    public function __construct(Router $router, SecurityContext $securityContext, EntityManager $entityManager)
    {
        $this->router = $router;
        $this->entityManager = $entityManager;
        $this->securityContext = $securityContext;
    }


    /**
     * This is called when an interactive authentication attempt succeeds. This
     * is called by authentication listeners inheriting from
     * AbstractAuthenticationListener.
     *
     * @param Request $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {

        foreach($this->getAllRoles() as $role)
        {
            if($this->securityContext->isGranted($role->getRole()) && $this->checkUserRole($token, $role)){
                return $response = new RedirectResponse($this->router->generate(
                    sprintf('%s_index', strtolower($role->getInterface()))
                ));
            }
        }

        $this->logger->info('User "{user}" has been authenticated successfully', array(
            'user' => $token->getUser()->getUsername(),
        ));

        return new RedirectResponse($this->router->generate('adm_index'));
    }

    /**
     * @return array|\Ultra\UsuarioBundle\Entity\UltraRole[]
     */
    private function getAllRoles()
    {
        return $this->entityManager->getRepository('UsuarioBundle:UltraRole')->findAll();
    }

    /**
     * @param LoggerInterface $loggerInterface
     */
    public function setLoggerService(LoggerInterface $loggerInterface)
    {
        $this->logger = $loggerInterface;
    }


    /**
     * @param TokenInterface $token
     * @param UltraRole      $role
     *
     * @return mixed
     */
    public function checkUserRole(TokenInterface $token, UltraRole $role)
    {
        return $token->getUser()->hasRole($role->getRole());
    }
}