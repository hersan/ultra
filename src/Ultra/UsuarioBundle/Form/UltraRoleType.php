<?php

namespace Ultra\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UltraRoleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text',
                array(
                    'label' => 'Nombre',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Nombre del rol',
                        'autocomplete'=>'on',
                        'maxlength'=>'50',
                        'title'=>'Escribe el nombre del rol',
                        'autofocus'=>true),
                    'required' => false
                     )
                  )

            ->add('role','text',
                array(
                    'label' => 'Rol',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Rol',
                        'autocomplete'=>'on',
                        'maxlength'=>'50',
                        'title'=>'Escribe el tipo de rol'),
                    'required' => false
                     )
            )
            ->add('interface','text',
                array(
                    'label' => 'Interfaz',
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Interfaz correspondiente al rol',
                        'autocomplete'=>'on',
                        'maxlength'=>'50',
                        'title'=>'Escribe el nombre de la interface'),
                    'required' => false
                     )
                  )
        //    ->add('users')
            ->add('users', 'entity', array(
                    'empty_value' => 'Seleccionar',
                    'class' => 'UsuarioBundle:UltraUser',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title'=>'Selecciona usuario'
                    ),
                    'label' => 'Usuarios',
                    'multiple' => true
                ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\UsuarioBundle\Entity\UltraRole'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_usuariobundle_ultrarole';
    }
}
