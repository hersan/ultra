<?php

namespace Ultra\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UltraUserType extends AbstractType
{
     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username','text', array(
                    'attr' => array('class' => 'form-control  input-sm',
                        'placeholder'=> 'Usuario',
                        'autocomplete'=>'on',
                        'maxlength'=>'20',
                        'title'=>'Escribe el nombre de usuario',
                        'autofocus'=>true),
                    'label' => 'Usuario',
                    'required' => false
                ))
            ->add('password','password', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Contraseña',
                        'autocomplete'=>'on',
                        'maxlength'=>'15',
                        'title'=>'Escribe la contraseña de usuario'),
                    'label' => 'Contraseña',
                    'required' => false,
                ))
           /* ->add('salt', 'text', array(
                    'attr' => array('class' => 'form-control input-sm',
                        'placeholder'=> 'Salt'),
                    'label' => 'Salt',
                    'required'=> false
                ))*/
            ->add('isEnabled', 'choice', array(
                    'attr' => array('class' => 'form-control selectpicker',
                        'title'=>'¿Habilitado?'),
                    'choices'=>array('1'=>'Si','0'=>'No'),
                    'label' => '¿Habilitado?',
                    'required' => false
                ))
            ->add('roles', 'entity', array(
                    'empty_value' => 'Selecciona el rol',
                    'empty_data' => null,
                    'class' => 'UsuarioBundle:UltraRole',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title' => 'Selecciona el rol'
                    ),
                    'label' => 'Rol',
                    'multiple' => true,
                    'required' => false,

                ))
            ->add('curricula', 'entity', array(
                    'empty_value' => 'Seleccionar curricula',
                    'empty_data' => null,
                    'class' => 'ControlDocumentoBundle:Curricula',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                        'title'=>'Selecciona curricula'
                    ),
                    'label' => 'Currícula',
                    'required' => false,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\UsuarioBundle\Entity\UltraUser'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_usuariobundle_ultrauser';
    }
}
