<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 13/04/15
 * Time: 06:46 PM
 */

namespace Ultra\RHBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Ultra\RHBundle\Form\DataTransformer\CheckBoxToDouble;

class UltraCheckBoxType extends AbstractType{

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_checkbox';
    }

    public function getParent()
    {
        return 'form';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new CheckBoxToDouble();
        $builder->addModelTransformer($transformer);
    }
}