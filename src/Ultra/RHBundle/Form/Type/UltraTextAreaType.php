<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 6/05/15
 * Time: 10:48 AM
 */

namespace Ultra\RHBundle\Form\Type;


use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Form\DataTransformer\ArrayCollectionToText;

class UltraTextAreaType extends AbstractType{

    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new ArrayCollectionToText($this->objectManager);
        $builder
            ->add('options','entity',array(
                'class' => 'RHBundle:Section',
                'multiple' => true,
            ))
            ->add('text','textarea',array(
                'label'=> 'Agregar nuevo'
            ))
            ->addModelTransformer($transformer)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected issue does not exist',
            'error_bubbling'  => false,
            'compound'        => true,
            'required'        => false,
            'empty_data'      => ' ',
            'empty_value'     => '',
        ));
    }

    public function getParent()
    {
        return 'form';
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'ultra_text_area';
    }
}