<?php

namespace Ultra\RHBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Entity\RHEventCalendar;

class RHEventCalendarType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Titulo',
                'attr' => array('class' => 'form-control')
            ))
            ->add('evaluationCriteria')
            ->add('instructor')
            ->add('begin', 'date', array(
                'input' => 'datetime',
                'label' => 'Inicia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('end', 'date', array(
                'input' => 'datetime',
                'label' => 'Termina',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('comments', 'textarea', array(
                'label' => 'Comentarios',
                'attr' => array('class' => 'form-control')
            ))
            ->add('agenda', 'entity', array(
                'class' => 'Ultra\RHBundle\Entity\RHAgenda',
                'label' => 'Agendar en',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
            ))
            ->add('discipline', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Disciplina',
                'label' => 'Disciplina',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
            ))
            ->add('status', 'entity', array(
                'class' => 'Ultra\CalidadBundle\Entity\EventStatus',
                'label' => 'Estatus',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('es')
                        ->where('es.id = 8 or es.id = 9');
                },
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'preSetDataEvent'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\RHEventCalendar',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_rheventcalendar';
    }

    public function preSetDataEvent(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($data instanceof RHEventCalendar) {
            $this->addFields($form, $data);
        }
    }

    public function addFields(FormInterface $form, RHEventCalendar $eventCalendar)
    {
        $form->add('evaluationCriteria', 'choice', array(
            'label' => 'Criterio',
            'empty_value' => 'Ninguno',
            'empty_data' => null,
            'attr' => array('class' => 'form-control selectpicker'),
            'choices' => $eventCalendar->getCriteria(),
        ))
            ->add('instructor', 'choice', array(
                'label' => 'Instructor',
                'empty_value' => 'Ninguno',
                'empty_data' => null,
                'attr' => array('class' => 'form-control selectpicker'),
                'choices' => $eventCalendar->getTrainer()
            ))
        ;
    }
}
