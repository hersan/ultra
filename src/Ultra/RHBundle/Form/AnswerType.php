<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Entity\Answer;

class AnswerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('score')
            ->add('text')
            ->add('section')
            ->add('question')
            //->add('categorizationResponse')
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
    }

    public function onPreSetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $entity = $event->getData();
        if($entity instanceof Answer)
        {
            $this->addElement($form, $entity);
        }

    }

    public function addElement(FormInterface $form, Answer $answer)
    {
        $array = $answer->getQuestion()->getAnswers()->toArray();

        $form->add('score','choice',array(
            'choices' => $answer->getQuestion()->getAnswers(),
        ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Answer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_answer';
    }
}
