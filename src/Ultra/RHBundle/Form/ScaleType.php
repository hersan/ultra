<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ScaleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text', array(
                'label' => 'Nombre',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('value',
                  'text',
                  array(
                      'label' => 'Valor',
                      'attr' => array(
                          'class' => 'form-control'
                      ),
                      'div_column' => 'col-sm-2'
                  ))
            /*->add('metaQuestion',
                  'entity',
                  array(
                      'class' => 'Ultra\RHBundle\Entity\MetaQuestion',
                      'label' => 'Selecciona un Pregunta',
                      'empty_value' => 'Ninguna...',
                      'empty_data' => null,
                      'attr' => array(
                          'class' => 'form-control selectpicker',
                          'data-live-search' => true,
                      ),
                      'div_column' => 'col-sm-2'
                  ))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Scale'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_scale';
    }
}
