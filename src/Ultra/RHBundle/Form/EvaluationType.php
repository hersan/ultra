<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Entity\Answer;
use Ultra\RHBundle\Entity\Categorization;
use Ultra\RHBundle\Entity\Evaluation;

class EvaluationType extends AbstractType
{

    private $categorization;

    public function __construct(Categorization $categorization = null)
    {
        $this->categorization = $categorization;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            //->add('created')
            //->add('updated')
            ->add(
                'responses',
                'collection',
                array(
                    'required' => false,
                    'type' => new AnswerType(),
                    'label' => null,
                    'allow_add' => true,
                    //'by_reference' => false,
                    //'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=> false,
                ))
        ;

        //$builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
    }

    public function onPreSetDataEvent(FormEvent $event)
    {
        $form = $event->getForm();
        $entity = $event->getData();

        if($entity instanceof Evaluation)
        {
            foreach($this->categorization->getQuestions() as $question)
            {
                $answer = new Answer();
                $answer->setQuestion($question);
                $answer->setSection($question->getSection());
                //$answer->setEvaluation($entity);
                $answer->setScore(0.0);
                $answer->setText(null);
                $entity->addResponse($answer);
            }

        }

        $event->setData($entity);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Evaluation'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_evaluation';
    }
}
