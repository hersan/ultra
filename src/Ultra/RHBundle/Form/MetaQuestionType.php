<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Entity\Configuration;
use Ultra\RHBundle\Entity\MetaQuestion;

class MetaQuestionType extends AbstractType
{
    private $configuration;

    public function __construct(Configuration $configuration = null)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',
                'text',
                array(
                    'label' => 'Nombre',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'div_column' => 'col-sm-4'
                ))
            ->add('label','text', array(
                'label' => 'Etiqueta',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('type','text', array(
                'label' => 'Tipo',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('section','entity',array(
                'class' => 'Ultra\RHBundle\Entity\Section',
                'label' => 'Sección',
                'empty_value' => 'Seleccione..',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker'
                ),
                //'choices' => $this->configuration->getSections(),
                'div_column' => 'col-sm-2'
            ))
            //->add('configuration')
        ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\MetaQuestion',
            'type_form' => 'edit'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_metaquestion';
    }

    public function onPreSetDataEvent(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        if ($entity instanceof MetaQuestion) {
            $this->addSection($form, $entity);
            $this->addScale($form, $entity);
        }
    }

    public function addSection(FormInterface $form, MetaQuestion $entity)
    {
        $form->add('section','entity',array(
            'class' => 'Ultra\RHBundle\Entity\Section',
            'label' => 'Sección',
            'empty_value' => 'Seleccione..',
            'empty_data' => null,
            'attr' => array(
                'class' => 'form-control selectpicker'
            ),
            'choices' => $this->getConfiguration($entity)->getSections(),
            'div_column' => 'col-sm-2'
        ));
    }

    private function getConfiguration(MetaQuestion $entity)
    {
        if (null != $this->configuration) {
            return $this->configuration;
        }
        return $entity->getConfiguration();
    }

    public function addScale(FormInterface $form, MetaQuestion $entity)
    {
        $type = $form->getConfig()->getOption('type_form');

        if ($type == 'edit') {

            $form->add('scale','collection',array(
                'required' => false,
                'type' => new ScaleType(),
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'error_bubbling' => false,
                'cascade_validation'=>false,
            ));

        }
    }
}
