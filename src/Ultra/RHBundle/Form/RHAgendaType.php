<?php

namespace Ultra\RHBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RHAgendaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('revision', 'number', array(
                'label' => 'Revision',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('name', 'text', array(
                'label' => 'Titulo',
                'attr' => array('class' => 'form-control')
            ))
            ->add('begin', 'date', array(
                'input' => 'datetime',
                'label' => 'Inicia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('end', 'date', array(
                'input' => 'datetime',
                'label' => 'Termina',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            //->add('created')
            ->add('comments','textarea', array(
                'label' => 'Registro de revisiones',
                'attr' => array('class' => 'form-control')
            ))
            ->add('createdBy', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Creado por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 19')
                        ;
                },
            ))
            ->add('approvedBy', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Aprobado por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 4')
                        ;
                },
            ))
            ->add('revisedBy', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Revisado por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 6')
                        ;
                },
            ))
            ->add('approvalBy', 'entity', array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Vo. Bo. por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'multiple' => true,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 12')
                        ;
                },
            ))
            ->add('file','file', array(
                'required' => false,
            ));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\RHAgenda',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_rhagenda';
    }
}
