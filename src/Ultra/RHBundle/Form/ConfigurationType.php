<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\RHBundle\Entity\Configuration;

class ConfigurationType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('sections','entity', array(
                'class' => 'Ultra\RHBundle\Entity\Section',
                'multiple' => true,
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Configuration'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_configuration';
    }

    public function onPreSetDataEvent(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        if ($entity instanceof Configuration && $entity->getId() != null) {
            $form->add(
                'metaQuestions',
                'collection',
                array(
                    'required' => false,
                    'type' => new MetaQuestionType($entity),
                    'label' => null,
                    'allow_add' => true,
                    //'by_reference' => false,
                    'allow_delete' => true,
                    'error_bubbling' => false,
                    'cascade_validation'=> false,
                    ));
        }
    }
}
