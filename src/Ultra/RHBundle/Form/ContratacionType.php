<?php

namespace Ultra\RHBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContratacionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('curricula','entity', array(
                    'label' => 'Nombre',
                    'class' => 'ControlDocumentoBundle:Curricula',
                    'empty_data' => null,
                    'empty_value' => 'Selecciona un nombre',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    ),
                    'query_builder' => function(EntityRepository $er) use ($options){
                        if($options['type_form']  == 'editar' )
                        {
                            return $er->createQueryBuilder('c')->leftJoin('c.contratos','con');
                        }
                        else
                        {
                            return $er->createQueryBuilder('c')->leftJoin('c.contratos','con')->where('con.id IS NULL');
                        }

                    },
                ))
            ->add('puesto','text', array(
                    'label' => 'Puesto',
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Introduce el Puesto'
                    )
                ))
            ->add('inicio','date',array(
                    'input' => 'datetime',
                    'label' => 'Inicia',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'format' => 'yyyy-MM-dd',
                ))
            ->add('vigencia','date', array(
                    'input' => 'datetime',
                    'label' => 'Termina',
                    'widget' => 'single_text',
                    'attr' => array('class'=>'form-control'),
                    'format' => 'yyyy-MM-dd',
                ))
            ->add('regimen','choice',array(
                    'label' => 'Regimen Fiscal',
                    'empty_data' => null,
                    'empty_value' => 'Selecciona un regimen',
                    'choices' => array('Honorarios'=> 'Honorarios', 'Ordinario'=>'Ordinario'),
                    'attr' => array(
                        'class' => 'form-control selectpicker'
                    )
                ))
            ->add('disciplina','entity', array(
                    'label' => 'Disciplina Asignada',
                    'class' => 'ControlDocumentoBundle:Disciplina',
                    'empty_data' => null,
                    'empty_value' => 'Selecciona una disciplina',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    ),
                ))
            ->add('categoria','entity',array(
                    'label' => 'Categoria',
                    'class' => 'ControlDocumentoBundle:Perfil',
                    'empty_data' => null,
                    'empty_value' => 'Selecciona una categoria',
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    ),
                    'query_builder' => function(EntityRepository $repository){
                        return $repository->createQueryBuilder('cat')->join('cat.estado','e')->where('e.id = 1');
                    }
                ))
            ->add('estado','choice',array(
                    'label' => 'Seleccione el estatus',
                    'empty_data' => null,
                    'empty_value' => 'Estado del contrato',
                    'choices' => array('Vigente'=> 'Vigente', 'Cancelado'=>'Cancelado'),
                    'attr' => array(
                        'class' => 'form-control selectpicker'
                    )
                ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA,array($this,'preSetData'));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Contratacion',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_contratacion';
    }

    public function preSetData(FormEvent $event)
    {
        $entity = $event->getData();
        $form = $event->getForm();

        $this->addElements($form);
    }

    public function addElements(FormInterface $formInterface){

        $type = $formInterface->getConfig()->getOption('type_form');

        if($type == 'editar')
        {
            $formInterface->add('baja','date',array(
                                    'input' => 'datetime',
                                    'label' => 'Fecha de Baja',
                                    'widget' => 'single_text',
                                    'attr' => array('class'=>'form-control'),
                                    'format' => 'yyyy-MM-dd',
                            ))
                            ->add('motivo','textarea',array(
                                    'label' => 'Motivo de la baja',
                                    'attr' => array(
                                        'class' => 'form-control'
                                    )
                            ));
        }

    }
}
