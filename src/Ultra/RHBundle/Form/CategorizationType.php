<?php

namespace Ultra\RHBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategorizationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text',array(
                'label' => 'Nombre',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('description','textarea',array(
                'label' => 'Descripción',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('sections','entity', array(
                'class'       => 'RHBundle:Section',
                'label'       => 'Secciones',
                'multiple'    => true,
                'empty_data'  => null,
                'empty_value' => 'Selecciona..',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\RHBundle\Entity\Categorization'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_rhbundle_categorization';
    }
}
