<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/06/15
 * Time: 06:26 PM
 */

namespace Ultra\RHBundle\Model;


use Ultra\RHBundle\Entity\Answer;
use Ultra\RHBundle\Entity\Categorization;

class EvaluationBuilder {

    /**
     * @var \Ultra\RHBundle\Entity\Evaluation
     */
    private $evaluation;

    public function __construct()
    {
        $this->createEvaluation();
    }

    /**
     * @return EvaluationBuilder
     */
    private function createEvaluation()
    {
        $this->evaluation = new \Ultra\RHBundle\Entity\Evaluation();

        return $this;
    }

    /**
     * @param Categorization $categorization
     * @return EvaluationBuilder
     */
    public function setCategorization(Categorization $categorization)
    {
        $this->evaluation->setCategorization($categorization);

        return $this;
    }

    /**
     * Get product evaluation
     *
     * @return \Ultra\RHBundle\Entity\Evaluation
     */
    public function getEvaluation()
    {
        $this->addAnswers();
        return $this->evaluation;
    }

    /**
     * @return EvaluationBuilder
     */
    public function addAnswers()
    {
        $categorization = $this->evaluation->getCategorization();

        foreach($categorization->getQuestions() as $question)
        {
            $answer = new Answer();
            $answer->setText(null);
            $answer->setScore(0.0);
            $answer->setSection($question->getSection());
            $question->addAnswer($answer);
            $this->evaluation->addResponse($answer);
        }

        return $this;
    }

}