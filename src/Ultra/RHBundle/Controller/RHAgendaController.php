<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\RHAgenda;
use Ultra\RHBundle\Form\RHAgendaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * RHAgenda controller.
 *
 * @Route("/rh/agenda")
 */
class RHAgendaController extends Controller
{

    /**
     * Lists all RHAgenda entities.
     *
     * @Route("/", name="rh_agenda")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:RHAgenda')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new RHAgenda entity.
     *
     * @Route("/", name="rh_agenda_create")
     * @Method("POST")
     * @Template("RHBundle:RHAgenda:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new RHAgenda();
        $entity->setCreated(new \DateTime());
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La capacitación se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('rh_agenda_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a RHAgenda entity.
     *
     * @param RHAgenda $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RHAgenda $entity)
    {
        $form = $this->createForm(new RHAgendaType(), $entity, array(
            'action' => $this->generateUrl('rh_agenda_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new RHAgenda entity.
     *
     * @Route("/new", name="rh_agenda_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new RHAgenda();
        $entity->setCreated(new \DateTime());
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a RHAgenda entity.
     *
     * @Route("/{id}", name="rh_agenda_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHAgenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar el programa anual.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a RHAgenda entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rh_agenda_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing RHAgenda entity.
     *
     * @Route("/{id}/edit", name="rh_agenda_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHAgenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la agenda.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a RHAgenda entity.
    *
    * @param RHAgenda $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RHAgenda $entity)
    {
        $form = $this->createForm(new RHAgendaType(), $entity, array(
            'action' => $this->generateUrl('rh_agenda_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing RHAgenda entity.
     *
     * @Route("/{id}", name="rh_agenda_update")
     * @Method("PUT")
     * @Template("RHBundle:RHAgenda:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHAgenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RHAgenda entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('rh_agenda_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a RHAgenda entity.
     *
     * @Route("/{id}", name="rh_agenda_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:RHAgenda')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RHAgenda entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('rh_agenda'));
    }

    /**
     * Lists all Agenda events.
     *
     * @param Request $request
     * @param $id
     *
     * @return array
     *
     * @Route("/program/{id}", name="rh_program")
     * @Method("GET")
     * @Template()
     */
    public function programAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var \Ultra\RHBundle\Entity\RHAgenda $entity
         */
        $entity = $em->getRepository('RHBundle:RHAgenda')->find($id);


        if (!$entity){
            $this->createNotFoundException('No se encuentra el Programa especificado');
        }

        return array(
            'entity' => $entity
        );

    }

    //pdf del programa

    /**
     * Lists all Agenda events.
     *
     * @param Request $request
     * @param $id
     *
     * @return array
     *
     * @Route("/rh/programpdf/{id}", name="rh_agenda_programpdf")
     * @Method("GET")
     * @Template("RHBundle:RHAgenda:rhprogrampdf.html.twig")
     */
    public function rhprogrampdfAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHAgenda:RHAgenda')->find($id);

//        $query = $em->createQuery(
//            "SELECT p  FROM RHBundle:RHAgenda:RHAgenda p
//             JOIN p.events ag
//              ORDER BY ag.begin DESC
//            ");
//
//        $entity = $query->getResult();

//ladybug_dump_die($entity);

        if (!$entity){
            $this->createNotFoundException('No se encuentra el Programa especificado');
        }
        else
        {

            $html = $this->renderView('RHBundle:RHAgenda:rhprogrampdf.html.twig',
                array(
                    'entities' => $entity));

            $response = new Response (
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                    array('lowquality' => false,
                        'print-media-type' => true,
                        'encoding' => 'utf-8',
                        'page-size' => 'Letter',
                        'outline-depth' => 8,
                        'orientation' => 'portrait',
                        'title'=> 'Programa Anual',
                        'user-style-sheet'=> 'css/bootstrap.css',
                        'header-right'=>'Pag. [page] de [toPage]',
                        'header-font-size'=>7,
                    )),
                200,
                array(
                    'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                    'Content-Disposition'   => 'attachment; filename="programapdf.pdf"',
                )
            );

            return $response;


        }}
}
