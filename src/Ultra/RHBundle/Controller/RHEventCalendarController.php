<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\RHEventCalendar;
use Ultra\RHBundle\Form\RHEventCalendarType;

/**
 * RHEventCalendar controller.
 *
 * @Route("/rh/event")
 */
class RHEventCalendarController extends Controller
{

    /**
     * Lists all RHEventCalendar entities.
     *
     * @Route("/", name="rh_event")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:RHEventCalendar')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new RHEventCalendar entity.
     *
     * @Route("/", name="rh_event_create")
     * @Method("POST")
     * @Template("RHBundle:RHEventCalendar:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new RHEventCalendar();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La capacitación se agregó con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('rh_event_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a RHEventCalendar entity.
     *
     * @param RHEventCalendar $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RHEventCalendar $entity)
    {
        $form = $this->createForm(new RHEventCalendarType(), $entity, array(
            'action' => $this->generateUrl('rh_event_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new RHEventCalendar entity.
     *
     * @Route("/new", name="rh_event_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new RHEventCalendar();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a RHEventCalendar entity.
     *
     * @Route("/{id}", name="rh_event_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHEventCalendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RHEventCalendar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a RHEventCalendar entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('rh_event_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing RHEventCalendar entity.
     *
     * @Route("/{id}/edit", name="rh_event_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHEventCalendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RHEventCalendar entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a RHEventCalendar entity.
    *
    * @param RHEventCalendar $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RHEventCalendar $entity)
    {
        $form = $this->createForm(new RHEventCalendarType(), $entity, array(
            'action' => $this->generateUrl('rh_event_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }

    /**
     * Edits an existing RHEventCalendar entity.
     *
     * @Route("/{id}", name="rh_event_update")
     * @Method("PUT")
     * @Template("RHBundle:RHEventCalendar:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:RHEventCalendar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RHEventCalendar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos fueron modificados con éxito.'
            );
            return $this->redirect($this->generateUrl('rh_event_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a RHEventCalendar entity.
     *
     * @Route("/{id}", name="rh_event_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:RHEventCalendar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RHEventCalendar entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('rh_event'));
    }
}
