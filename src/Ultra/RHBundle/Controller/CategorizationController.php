<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\Categorization;
use Ultra\RHBundle\Form\CategorizationType;

/**
 * Categorization controller.
 *
 * @Route("/categorization")
 */
class CategorizationController extends Controller
{

    /**
     * Lists all Categorization entities.
     *
     * @Route("/", name="categorization")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:Categorization')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Categorization entity.
     *
     * @Route("/", name="categorization_create")
     * @Method("POST")
     * @Template("RHBundle:Categorization:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Categorization();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('categorization_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Categorization entity.
     *
     * @param Categorization $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categorization $entity)
    {
        $form = $this->createForm(new CategorizationType(), $entity, array(
            'action' => $this->generateUrl('categorization_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create',
            'attr' => array(
                'class' => 'form-control btn btn-default'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Categorization entity.
     *
     * @Route("/new", name="categorization_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Categorization();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Categorization entity.
     *
     * @Route("/{id}", name="categorization_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Categorization')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorization entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Categorization entity.
     *
     * @Route("/{id}/edit", name="categorization_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Categorization')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorization entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Categorization entity.
    *
    * @param Categorization $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Categorization $entity)
    {
        $form = $this->createForm(new CategorizationType(), $entity, array(
            'action' => $this->generateUrl('categorization_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Editar',
            'attr' => array(
                'class' => 'btn btn-default'
            )
        ));

        return $form;
    }
    /**
     * Edits an existing Categorization entity.
     *
     * @Route("/{id}", name="categorization_update")
     * @Method("PUT")
     * @Template("RHBundle:Categorization:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Categorization')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorization entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('categorization_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Categorization entity.
     *
     * @Route("/{id}", name="categorization_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:Categorization')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Categorization entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('categorization'));
    }

    /**
     * Creates a form to delete a Categorization entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorization_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Borrar',
                'attr' => array(
                    'class' => 'btn btn-warning'
                )
            ))
            ->getForm()
        ;
    }
}
