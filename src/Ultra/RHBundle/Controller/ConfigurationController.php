<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\Configuration;
use Ultra\RHBundle\Entity\MetaQuestion;
use Ultra\RHBundle\Form\ConfigurationType;

/**
 * Configuration controller.
 *
 * @Route("/configuracion")
 */
class ConfigurationController extends Controller
{

    /**
     * Lists all Configuration entities.
     *
     * @Route("/", name="configuracion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:Configuration')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Configuration entity.
     *
     * @Route("/", name="configuracion_create")
     * @Method("POST")
     * @Template("RHBundle:Configuration:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Configuration();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('configuracion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Configuration entity.
     *
     * @param Configuration $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Configuration $entity)
    {
        $form = $this->createForm(new ConfigurationType(), $entity, array(
            'action' => $this->generateUrl('configuracion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Configuration entity.
     *
     * @Route("/new", name="configuracion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Configuration();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Configuration entity.
     *
     * @Route("/{id}", name="configuracion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Configuration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Configuration entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a Configuration entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('configuracion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing Configuration entity.
     *
     * @Route("/{id}/edit", name="configuracion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Configuration')->find($id);

        if (!$entity->hasMetaQuestions()) {

            $entity->addMetaQuestion(new MetaQuestion());

        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Configuration entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Configuration entity.
    *
    * @param Configuration $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Configuration $entity)
    {
        $form = $this->createForm(new ConfigurationType(), $entity, array(
            'action' => $this->generateUrl('configuracion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Configuration entity.
     *
     * @Route("/{id}", name="configuracion_update")
     * @Method("PUT")
     * @Template("RHBundle:Configuration:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Configuration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Configuration entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('configuracion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Configuration entity.
     *
     * @Route("/{id}", name="configuracion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:Configuration')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Configuration entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('configuracion'));
    }
}
