<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\MetaQuestion;
use Ultra\RHBundle\Entity\Scale;
use Ultra\RHBundle\Form\MetaQuestionType;

/**
 * MetaQuestion controller.
 *
 * @Route("/meta/datos")
 */
class MetaQuestionController extends Controller
{

    /**
     * Lists all MetaQuestion entities.
     *
     * @Route("/", name="meta_datos")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:MetaQuestion')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Lists all MetaQuestion entities.
     *
     * @Route("/list/{id}", name="meta_datos_list")
     * @Method("GET")
     * @Template("RHBundle:MetaQuestion:index.html.twig")
     */
    public function listByConfigurationAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $configuration = $em->getRepository('RHBundle:Configuration')->find($id);

        return array(
            'entities' => $configuration->getMetaQuestions()
        );
    }

    /**
     * Creates a new MetaQuestion entity.
     *
     * @Route("/", name="meta_datos_create")
     * @Method("POST")
     * @Template("RHBundle:MetaQuestion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MetaQuestion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('meta_datos_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a MetaQuestion entity.
     *
     * @param MetaQuestion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MetaQuestion $entity)
    {
        $form = $this->createForm(new MetaQuestionType(), $entity, array(
            'action' => $this->generateUrl('meta_datos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new MetaQuestion entity.
     *
     * @Route("/new", name="meta_datos_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new MetaQuestion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a MetaQuestion entity.
     *
     * @Route("/{id}", name="meta_datos_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:MetaQuestion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MetaQuestion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a MetaQuestion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('meta_datos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing MetaQuestion entity.
     *
     * @Route("/{id}/edit", name="meta_datos_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:MetaQuestion')->find($id);

        if (!$entity->hasScale()) {
            $entity->addScale(new Scale());
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MetaQuestion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a MetaQuestion entity.
    *
    * @param MetaQuestion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(MetaQuestion $entity)
    {
        $form = $this->createForm(new MetaQuestionType(), $entity, array(
            'action' => $this->generateUrl('meta_datos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing MetaQuestion entity.
     *
     * @Route("/{id}", name="meta_datos_update")
     * @Method("PUT")
     * @Template("RHBundle:MetaQuestion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:MetaQuestion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MetaQuestion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('meta_datos_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a MetaQuestion entity.
     *
     * @Route("/{id}", name="meta_datos_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:MetaQuestion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MetaQuestion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('meta_datos'));
    }
}
