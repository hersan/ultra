<?php

namespace Ultra\RHBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\RHBundle\Entity\Categorization;
use Ultra\RHBundle\Entity\Evaluation;
use Ultra\RHBundle\Form\EvaluationType;
use Ultra\RHBundle\Model\EvaluationBuilder;

/**
 * Evaluation controller.
 *
 * @Route("/evaluation")
 */
class EvaluationController extends Controller
{

    /**
     * Lists all Evaluation entities.
     *
     * @Route("/", name="evaluation")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RHBundle:Evaluation')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Evaluation entity.
     *
     * @param Request $request
     * @param $categorize
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/create/{categorize}", name="evaluation_create")
     * @Method("POST")
     * @Template("RHBundle:Evaluation:new.html.twig")
     */
    public function createAction(Request $request, $categorize)
    {
        $entity = new Evaluation();

        $em = $this->getDoctrine()->getManager();
        $categorization = $em->getRepository('RHBundle:Categorization')->find($categorize);

        if(!$categorization)
        {
            throw $this->createNotFoundException('No existe la categorización');
        }

        $form = $this->createCreateForm($entity, $categorization);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('evaluation_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Evaluation entity.
     *
     * @param Evaluation $entity The entity
     * @param Categorization $categorization
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Evaluation $entity, Categorization $categorization)
    {
        $form = $this->createForm(new EvaluationType($categorization), $entity, array(
            'action' => $this->generateUrl('evaluation_create', array('categorize' => $categorization->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Evaluation entity.
     *
     * @Route("/new/{categorize}", name="evaluation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($categorize)
    {
        //$entity = new Evaluation();

        $em = $this->getDoctrine()->getManager();

        $categorization = $em->getRepository('RHBundle:Categorization')->find($categorize);

        if(!$categorization)
        {
            throw $this->createNotFoundException('No existe la categorización');
        }

        $builder = new EvaluationBuilder();
        $builder->setCategorization($categorization);
        //$builder->addAnswers();
        $entity = $builder->getEvalution();

        $form   = $this->createCreateForm($entity, $categorization);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Evaluation entity.
     *
     * @Route("/{id}", name="evaluation_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Evaluation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evaluation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to delete a Evaluation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('evaluation_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Displays a form to edit an existing Evaluation entity.
     *
     * @Route("/{id}/edit", name="evaluation_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Evaluation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evaluation entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Evaluation entity.
    *
    * @param Evaluation $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Evaluation $entity)
    {
        $form = $this->createForm(new EvaluationType(), $entity, array(
            'action' => $this->generateUrl('evaluation_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Evaluation entity.
     *
     * @Route("/{id}", name="evaluation_update")
     * @Method("PUT")
     * @Template("RHBundle:Evaluation:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RHBundle:Evaluation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Evaluation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('evaluation_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Evaluation entity.
     *
     * @Route("/{id}", name="evaluation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RHBundle:Evaluation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Evaluation entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('evaluation'));
    }
}
