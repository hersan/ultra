<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\AnswerRepository")
 */
class Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="float")
     */
    private $score;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", nullable=true)
     */
    private $text;

    /**
     * @var \Ultra\RHBundle\Entity\Section
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Section")
     */
    private $section;

    /**
     * @var \Ultra\RHBundle\Entity\Evaluation $evaluation
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Evaluation", inversedBy="responses")
     */
    private $evaluation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set score
     *
     * @param float $score
     * @return Answer
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Answer
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get section
     *
     * @return \Ultra\RHBundle\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set section
     *
     * @param \Ultra\RHBundle\Entity\Section $section
     * @return Answer
     */
    public function setSection(\Ultra\RHBundle\Entity\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get evaluation
     *
     * @return \Ultra\RHBundle\Entity\Evaluation
     */
    public function getEvaluation()
    {
        return $this->evaluation;
    }

    /**
     * Set evaluation
     *
     * @param \Ultra\RHBundle\Entity\Evaluation $evaluation
     * @return Answer
     */
    public function setEvaluation(\Ultra\RHBundle\Entity\Evaluation $evaluation = null)
    {
        $this->evaluation = $evaluation;

        return $this;
    }
}