<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evaluation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\EvaluationRepository")
 */
class Evaluation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="date")
     */
    private $updated;

    /**
     * @var \Ultra\RHBundle\Entity\Answer
     *
     * @ORM\OneToMany(targetEntity="Ultra\RHBundle\Entity\Answer", mappedBy="evaluation", cascade={"persist"})
     */
    private $responses;

    /**
     * @var \Ultra\RHBundle\Entity\Categorization
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Categorization")
     */
    private $categorization;

    private $comments;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Evaluation
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Evaluation
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Evaluation
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add responses
     *
     * @param \Ultra\RHBundle\Entity\Answer $responses
     * @return Evaluation
     */
    public function addResponse(\Ultra\RHBundle\Entity\Answer $responses)
    {
        $this->responses[] = $responses;
    
        return $this;
    }

    /**
     * Remove responses
     *
     * @param \Ultra\RHBundle\Entity\Answer $responses
     */
    public function removeResponse(\Ultra\RHBundle\Entity\Answer $responses)
    {
        $this->responses->removeElement($responses);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Get Categorization
     *
     * @return \Ultra\RHBundle\Entity\Categorization
     */
    public function getCategorization()
    {
        return $this->categorization;
    }

    /**
     * Set categorization
     *
     * @param mixed $categorization
     */
    public function setCategorization($categorization = null)
    {
        $this->categorization = $categorization;
    }

    public function hasResponses()
    {
        return $this->getResponses()->isEmpty() ? false : true;
    }

}