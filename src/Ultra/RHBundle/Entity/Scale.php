<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Scale
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Scale
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", scale=1, precision=4)
     */
    private $value;

    /**
     * @var \Ultra\RHBundle\Entity\MetaQuestion
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\MetaQuestion", inversedBy="scale")
     */
    private $metaQuestion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Scale
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Scale
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get metaData
     *
     * @return \Ultra\RHBundle\Entity\MetaQuestion
     */
    public function getMetaQuestion()
    {
        return $this->metaQuestion;
    }

    /**
     * Set metaData
     *
     * @param \Ultra\RHBundle\Entity\MetaQuestion $metaQuestion
     * @return Scale
     */
    public function setMetaQuestion(\Ultra\RHBundle\Entity\MetaQuestion $metaQuestion = null)
    {
        $this->metaQuestion = $metaQuestion;

        return $this;
    }
}