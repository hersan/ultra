<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contratacion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\ContratacionRepository")
 */
class Contratacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="puesto", type="string", length=255)
     */
    private $puesto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date")
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vigencia", type="date")
     */
    private $vigencia;

    /**
     * @var string
     *
     * @ORM\Column(name="regimen", type="string", length=255)
     */
    private $regimen;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="baja", type="date", nullable=true)
     */
    private $baja;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo", type="string", length=255, nullable=true)
     */
    private $motivo;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     */
    private $estado;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Disciplina
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina")
     */
    private $disciplina;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Perfil
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Perfil")
     */
    private $categoria;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula", inversedBy="contratos")
     */
    private $curricula;


    public function __construct()
    {
        $this->estado = true;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set puesto
     *
     * @param string $puesto
     * @return Contratacion
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;
    
        return $this;
    }

    /**
     * Get puesto
     *
     * @return string 
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     * @return Contratacion
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set vigencia
     *
     * @param \DateTime $vigencia
     * @return Contratacion
     */
    public function setVigencia($vigencia)
    {
        $this->vigencia = $vigencia;
    
        return $this;
    }

    /**
     * Get vigencia
     *
     * @return \DateTime 
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }

    /**
     * Set regimen
     *
     * @param string $regimen
     * @return Contratacion
     */
    public function setRegimen($regimen)
    {
        $this->regimen = $regimen;
    
        return $this;
    }

    /**
     * Get regimen
     *
     * @return string 
     */
    public function getRegimen()
    {
        return $this->regimen;
    }

    /**
     * Set baja
     *
     * @param \DateTime $baja
     * @return Contratacion
     */
    public function setBaja($baja)
    {
        $this->baja = $baja;
    
        return $this;
    }

    /**
     * Get baja
     *
     * @return \DateTime 
     */
    public function getBaja()
    {
        return $this->baja;
    }

    /**
     * Set motivo
     *
     * @param string $motivo
     * @return Contratacion
     */
    public function setMotivo($motivo)
    {
        $this->motivo = $motivo;
    
        return $this;
    }

    /**
     * Get motivo
     *
     * @return string 
     */
    public function getMotivo()
    {
        return $this->motivo;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Contratacion
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set disciplina
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplina
     * @return Contratacion
     */
    public function setDisciplina(\Ultra\ControlDocumentoBundle\Entity\Disciplina $disciplina = null)
    {
        $this->disciplina = $disciplina;
    
        return $this;
    }

    /**
     * Get disciplina
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Disciplina 
     */
    public function getDisciplina()
    {
        return $this->disciplina;
    }

    /**
     * Set categoria
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Perfil $categoria
     * @return Contratacion
     */
    public function setCategoria(\Ultra\ControlDocumentoBundle\Entity\Perfil $categoria = null)
    {
        $this->categoria = $categoria;
    
        return $this;
    }

    /**
     * Get categoria
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Perfil 
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set curricula
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $curricula
     * @return Contratacion
     */
    public function setCurricula(\Ultra\ControlDocumentoBundle\Entity\Curricula $curricula = null)
    {
        $this->curricula = $curricula;
    
        return $this;
    }

    /**
     * Get curricula
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCurricula()
    {
        return $this->curricula;
    }
}