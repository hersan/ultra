<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Configuration
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\ConfigurationRepository")
 */
class Configuration
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var \Ultra\RHBundle\Entity\Section
     *
     * @ORM\ManyToMany(targetEntity="Ultra\RHBundle\Entity\Section", inversedBy="configuration")
     */
    private $sections;

    /**
     * @var \Ultra\RHBundle\Entity\MetaQuestion
     *
     * @ORM\OneToMany(targetEntity="Ultra\RHBundle\Entity\MetaQuestion", mappedBy="configuration", cascade={"persist"})
     */
    private $metaQuestions;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->metaQuestions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Configuration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Configuration
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add sections
     *
     * @param \Ultra\RHBundle\Entity\Section $sections
     * @return Configuration
     */
    public function addSection(\Ultra\RHBundle\Entity\Section $sections)
    {
        $this->sections[] = $sections;
    
        return $this;
    }

    /**
     * Remove sections
     *
     * @param \Ultra\RHBundle\Entity\Section $sections
     */
    public function removeSection(\Ultra\RHBundle\Entity\Section $sections)
    {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Add meta question
     *
     * @param \Ultra\RHBundle\Entity\MetaQuestion $metaQuestion
     * @return Configuration
     */
    public function addMetaQuestion(\Ultra\RHBundle\Entity\MetaQuestion $metaQuestion)
    {
        $metaQuestion->setConfiguration($this);

        $this->metaQuestions[] = $metaQuestion;
    
        return $this;
    }

    /**
     * Remove meta Questions
     *
     * @param \Ultra\RHBundle\Entity\MetaQuestion $metaQuestion
     */
    public function removeMetaQuestion(\Ultra\RHBundle\Entity\MetaQuestion $metaQuestion)
    {
        $this->metaQuestions->removeElement($metaQuestion);
    }

    public function hasMetaQuestions()
    {
        return !$this->getMetaQuestions()->isEmpty();
    }

    /**
     * Get meta Questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMetaQuestions()
    {
        return $this->metaQuestions;
    }
}