<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ultra\ControlDocumentoBundle\Model\RH\RHAgendaUploadableTrait;

/**
 * RHAgenda
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\RHAgendaRepository")
 */
class RHAgenda
{
    use RHAgendaUploadableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="revision", type="smallint")
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="date")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date")
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255)
     */
    private $comments;

    /**
     * Establish who created the Agenda (RH)
     *
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $createdBy;

    /**
     * Establish who approved the Agenda (general manager)
     *
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $approvedBy;

    /**
     * Establish who revised the Agenda (quality department)
     *
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $revisedBy;

    /**
     * Establish who approval the agenda (Project Managers)
     *
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula", inversedBy="agenda")
     */
    private $approvalBy;

    /**
     * @var \Ultra\RHBundle\Entity\RHEventCalendar
     *
     * @ORM\OneToMany(targetEntity="Ultra\RHBundle\Entity\RHEventCalendar", mappedBy="agenda", cascade={"persist"})
     */
    private $events;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->approvalBy = new \Doctrine\Common\Collections\ArrayCollection();
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get revision
     *
     * @return integer 
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * Set revision
     *
     * @param integer $revision
     * @return RHAgenda
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * Set discipline
     *
     * @param string $discipline
     * @return RHAgenda
     */
    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;

        return $this;
    }

    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * Set begin
     *
     * @param \DateTime $begin
     * @return RHAgenda
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return RHAgenda
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return RHAgenda
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return RHAgenda
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    
    /**
     * Set createdBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $createdBy
     * @return RHAgenda
     */
    public function setCreatedBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set approvedBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $approvedBy
     * @return RHAgenda
     */
    public function setApprovedBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get revisedBy
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getRevisedBy()
    {
        return $this->revisedBy;
    }

    /**
     * Set revisedBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $revisedBy
     * @return RHAgenda
     */
    public function setRevisedBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $revisedBy = null)
    {
        $this->revisedBy = $revisedBy;

        return $this;
    }

    /**
     * Add approvalBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $approvalBy
     * @return RHAgenda
     */
    public function addApprovalBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $approvalBy)
    {
        $this->approvalBy[] = $approvalBy;

        return $this;
    }

    /**
     * Remove approvalBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $approvalBy
     */
    public function removeApprovalBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $approvalBy)
    {
        $this->approvalBy->removeElement($approvalBy);
    }

    /**
     * Get approvalBy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApprovalBy()
    {
        return $this->approvalBy;
    }

    /**
     * Add events
     *
     * @param \Ultra\RHBundle\Entity\RHEventCalendar $events
     * @return RHAgenda
     */
    public function addEvent(\Ultra\RHBundle\Entity\RHEventCalendar $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \Ultra\RHBundle\Entity\RHEventCalendar $events
     */
    public function removeEvent(\Ultra\RHBundle\Entity\RHEventCalendar $events)
    {
        $this->events->removeElement($events);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RHAgenda
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}