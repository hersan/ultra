<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorization
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\CategorizationRepository")
 */
class Categorization
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var \Ultra\RHBundle\Entity\Section
     *
     * @ORM\ManyToMany(targetEntity="Ultra\RHBundle\Entity\Section", inversedBy="categorization")
     */
    private $sections;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Categorization
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add sections
     *
     * @param \Ultra\RHBundle\Entity\Section $sections
     * @return Categorization
     */
    public function addSection(\Ultra\RHBundle\Entity\Section $sections)
    {
        $this->sections[] = $sections;

        return $this;
    }

    /**
     * Remove sections
     *
     * @param \Ultra\RHBundle\Entity\Section $sections
     */
    public function removeSection(\Ultra\RHBundle\Entity\Section $sections)
    {
        $this->sections->removeElement($sections);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSections()
    {
        return $this->sections;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Categorization
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}