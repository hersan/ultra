<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\CommentRepository")
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255)
     */
    private $note;

    /**
     * @var \Ultra\RHBundle\Entity\Section
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Categorization", inversedBy="comments")
     */
    private $categorization;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Comment
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set categorization
     *
     * @param \Ultra\RHBundle\Entity\Categorization $categorization
     * @return Comment
     */
    public function setCategorization(\Ultra\RHBundle\Entity\Categorization $categorization = null)
    {
        $this->categorization = $categorization;
    
        return $this;
    }

    /**
     * Get categorization
     *
     * @return \Ultra\RHBundle\Entity\Categorization 
     */
    public function getCategorization()
    {
        return $this->categorization;
    }
}