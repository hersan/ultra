<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RHEventCalendar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\RHEventCalendarRepository")
 */
class RHEventCalendar
{

    private $criteria = array(
        'EO' => 'Examen Oral',
        'EE' => 'Examen Escrito',
        'T' => 'Trabajo',
        'PY' => 'Proyecto',
        'PPT' => 'Presentación',
        'RCL' => 'Resumen de Compresión de Lectura'
    );

    private $trainer = array(
        'SDA' => 'Supervisor de Area',
        'CI' => 'Capacitador Interno',
        'CE' => 'Capacitador Externo',
        'FC' => 'Facilitador del Curso',
        'ASD' => 'A Ser Determinado'
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="evaluationCriteria", type="string", length=255)
     */
    private $evaluationCriteria;

    /**
     * @var string
     *
     * @ORM\Column(name="instructor", type="string", length=255)
     */
    private $instructor;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="date")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date")
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255)
     */
    private $comments;

    /**
     * @var \Ultra\RHBundle\Entity\RHAgenda
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\RHAgenda", inversedBy="events")
     */
    private $agenda;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Disciplina
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Disciplina")
     */
    private $discipline;

    /**
     * @var \Ultra\CalidadBundle\Entity\EventStatus
     *
     * @ORM\ManyToOne(targetEntity="Ultra\CalidadBundle\Entity\EventStatus")
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RHEventCalendar
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get evaluationCriteria
     *
     * @return string
     */
    public function getEvaluationCriteria()
    {
        return $this->evaluationCriteria;
    }

    /**
     * Set evaluationCriteria
     *
     * @param string $evaluationCriteria
     * @return RHEventCalendar
     */
    public function setEvaluationCriteria($evaluationCriteria)
    {
        $this->evaluationCriteria = $evaluationCriteria;

        return $this;
    }

    /**
     * Get instructor
     *
     * @return string
     */
    public function getInstructor()
    {
        return $this->instructor;
    }

    /**
     * Set instructor
     *
     * @param string $instructor
     * @return RHEventCalendar
     */
    public function setInstructor($instructor)
    {
        $this->instructor = $instructor;

        return $this;
    }

    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * Set begin
     *
     * @param \DateTime $begin
     * @return RHEventCalendar
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return RHEventCalendar
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return RHEventCalendar
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return array
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @return array
     */
    public function getTrainer()
    {
        return $this->trainer;
    }

    /**
     * Get agenda
     *
     * @return \Ultra\RHBundle\Entity\RHAgenda
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * Set agenda
     *
     * @param \Ultra\RHBundle\Entity\RHAgenda $agenda
     * @return RHEventCalendar
     */
    public function setAgenda(\Ultra\RHBundle\Entity\RHAgenda $agenda = null)
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * Get discipline
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Disciplina
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }

    /**
     * Set discipline
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Disciplina $discipline
     * @return RHEventCalendar
     */
    public function setDiscipline(\Ultra\ControlDocumentoBundle\Entity\Disciplina $discipline = null)
    {
        $this->discipline = $discipline;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Ultra\CalidadBundle\Entity\EventStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param \Ultra\CalidadBundle\Entity\EventStatus $status
     * @return RHEventCalendar
     */
    public function setStatus(\Ultra\CalidadBundle\Entity\EventStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }
}