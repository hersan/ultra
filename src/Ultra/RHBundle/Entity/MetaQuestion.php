<?php

namespace Ultra\RHBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MetaQuestion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\RHBundle\Entity\MetaQuestionRepository")
 */
class MetaQuestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var \Ultra\RHBundle\Entity\Scale
     *
     * @ORM\OneToMany(targetEntity="Ultra\RHBundle\Entity\Scale", mappedBy="metaQuestion", cascade={"persist"})
     */
    private $scale;

    /**
     * @var \Ultra\RHBundle\Entity\Section
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Section")
     */
    private $section;

    /**
     * @var \Ultra\RHBundle\Entity\Configuration
     *
     * @ORM\ManyToOne(targetEntity="Ultra\RHBundle\Entity\Configuration", inversedBy="metaQuestions")
     */
    private $configuration;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->scale = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return MetaQuestion
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return MetaQuestion
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return \Ultra\RHBundle\Entity\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set section
     *
     * @param \Ultra\RHBundle\Entity\Section $section
     * @return MetaQuestion
     */
    public function setSection(\Ultra\RHBundle\Entity\Section $section = null)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get configuration
     *
     * @return \Ultra\RHBundle\Entity\Configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Set configuration
     *
     * @param \Ultra\RHBundle\Entity\Configuration $configuration
     * @return MetaQuestion
     */
    public function setConfiguration(\Ultra\RHBundle\Entity\Configuration $configuration = null)
    {
        $this->configuration = $configuration;
    
        return $this;
    }

    /**
     * Add a scale
     *
     * @param \Ultra\RHBundle\Entity\Scale $scale
     * @return MetaQuestion
     */
    public function addScale(\Ultra\RHBundle\Entity\Scale $scale)
    {
        $this->scale[] = $scale;

        return $this;
    }

    /**
     * Remove a scale
     *
     * @param \Ultra\RHBundle\Entity\Scale $scale
     */
    public function removeScale(\Ultra\RHBundle\Entity\Scale $scale)
    {
        $this->scale->removeElement($scale);
    }
    
    /**
     * Representation like string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return MetaQuestion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Return true if has scale
     *
     * @return bool
     */
    public function hasScale()
    {
        return !$this->getScale()->isEmpty();
    }

    /**
     * Get scale Collection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScale()
    {
        return $this->scale;
    }
}