<?php

namespace Ultra\LibrosBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ultra\LibrosBundle\Entity\Libros;
use Ultra\LibrosBundle\Form\LibrosType;

/**
 * Libros controller.
 *
 * @Route("/libros")
 */
class LibrosController extends Controller {

    /**
     * Lista todos los Libros.
     *
     * @Route("/", name="libros")
     * @Method("GET")
     * @Template("LibrosBundle:Libros:index.html.twig")
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('LibrosBundle:Libros')->findAll();
        $entities = new ArrayCollection($entities);

        if ($this->getUser()->hasRole('ROLE_SUPER_ADMIN') || $this->getUser()->hasRole('ROLE_CONTROL_DOC')) {
            return array(
                'entities' => $entities,
            );
        } else {
            $entities = $entities->filter(function($books) {
                return $books->getVisible() == 0;
            });
            return array(
                'entities' => $entities,
            );
        }
    }

    /**
     * Creates a newLibros entity.
     *
     * @Route("/librospdf", name="libros_pdf")
     * @Method("GET")
     * @Template()
     */
    public function librospdfAction() {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('LibrosBundle:Libros')->findAll();

        $html = $this->renderView('LibrosBundle:Libros:librospdf.html.twig', array(
            'entities' => $entities,
        ));

        $response = new Response(
                $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Portrait',
                    'title' => 'Áreas',
                    'user-style-sheet' => 'css/bootstrap.css',
                    'header-right' => 'Pag. [page] de [toPage]',
                    'header-font-size' => 7,
                )), 200, array(
            'Content-Type' => '/home/aloyo/public_html/Ultra/web/pdf',
            'Content-Disposition' => 'attachment; filename="listadelibros.pdf"',
                )
        );

        return $response;
    }

    /**
     * Creates a new Libros entity.
     *
     * @Route("/", name="libros_create")
     * @Method("POST")
     * @Template("LibrosBundle:Libros:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Libros();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('libros_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Libros entity.
     *
     * @param Libros $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Libros $entity) {
        $form = $this->createForm(new LibrosType(), $entity, array(
            'action' => $this->generateUrl('libros_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Guardar registro'));


        $form->add('submit', 'usubmit', array(
            'label' => 'Guardar',
            'attr' => array('class' => 'btn btn-success pull-left',
                'title' => 'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new Libros entity.
     *
     * @Route("/new", name="libros_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Libros();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Libros entity.
     *
     * @Route("/{id}", name="libros_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Libros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Libros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Libros entity.
     *
     * @Route("/{id}/edit", name="libros_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Libros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Libros entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Libros entity.
     *
     * @param Libros $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Libros $entity) {
        $form = $this->createForm(new LibrosType(), $entity, array(
            'action' => $this->generateUrl('libros_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn btn-success pull-left',
                'title' => 'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit',
        ));

        return $form;
    }

    /**
     * Edits an existing Libros entity.
     *
     * @Route("/{id}", name="libros_update")
     * @Method("PUT")
     * @Template("LibrosBundle:Libros:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Libros')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Libros entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('libros_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('libros_show', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Libros entity.
     *
     * @Route("/{id}", name="libros_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LibrosBundle:Libros')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Libros entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('libros'));
    }

    /**
     * Creates a form to delete a Libros entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('libros_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'usubmit', array(
                            'label' => 'Borrar',
                            'attr' => array('class' => 'btn btn-warning',
                                'title' => 'Borrar datos'),
                            'glyphicon' => 'glyphicon glyphicon-trash',
                        ))
                        ->getForm()
        ;
    }

}
