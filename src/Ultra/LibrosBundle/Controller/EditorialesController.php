<?php

namespace Ultra\LibrosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\LibrosBundle\Entity\Editoriales;
use Ultra\LibrosBundle\Form\EditorialesType;

/**
 * Editoriales controller.
 *
 * @Route("/libros/editoriales")
 */
class EditorialesController extends Controller
{

    /**
     * Lists all Editoriales entities.
     *
     * @Route("/", name="libros_editoriales")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LibrosBundle:Editoriales')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Editoriales entity.
     *
     * @Route("/", name="libros_editoriales_create")
     * @Method("POST")
     * @Template("LibrosBundle:Editoriales:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Editoriales();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('libros_editoriales_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Editoriales entity.
    *
    * @param Editoriales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Editoriales $entity)
    {
        $form = $this->createForm(new EditorialesType(), $entity, array(
            'action' => $this->generateUrl('libros_editoriales_create'),
            'method' => 'POST',
        ));

$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success pull-left',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new Editoriales entity.
     *
     * @Route("/new", name="libros_editoriales_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Editoriales();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Editoriales entity.
     *
     * @Route("/{id}", name="libros_editoriales_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Editoriales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editoriales entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Editoriales entity.
     *
     * @Route("/{id}/edit", name="libros_editoriales_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Editoriales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editoriales entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Editoriales entity.
    *
    * @param Editoriales $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Editoriales $entity)
    {
        $form = $this->createForm(new EditorialesType(), $entity, array(
            'action' => $this->generateUrl('libros_editoriales_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success pull-left',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing Editoriales entity.
     *
     * @Route("/{id}", name="libros_editoriales_update")
     * @Method("PUT")
     * @Template("LibrosBundle:Editoriales:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Editoriales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Editoriales entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('libros_editoriales_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('libros_editoriales_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Editoriales entity.
     *
     * @Route("/{id}", name="libros_editoriales_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LibrosBundle:Editoriales')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Editoriales entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('libros_editoriales'));
    }

    /**
     * Creates a form to delete a Editoriales entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libros_editoriales_delete', array('id' => $id)))
            ->setMethod('DELETE')
          ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
