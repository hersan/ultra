<?php

namespace Ultra\LibrosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Ultra\LibrosBundle\Entity\Libros;


class ServirLibroController extends Controller
{
    /**
     * @Route("/{id}/verlibro", name="verlibro")
     * @Template()
     * @ParamConverter("libro", class="LibrosBundle:Libros")
     */
    public function servirAction(Libros $libro)
    {
        if (!file_exists($libro->getAbsolutePath())) {
            throw $this->createNotFoundException('No se encontro el archivo.');
        }
        // prepara la descarga del archivo pdf
        $response = new BinaryFileResponse($libro->getAbsolutePath());
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $libro->getRuta(),
            iconv('UTF-8', 'ASCII//TRANSLIT', $libro->getRuta())
        );
        return $response;
    }


}
