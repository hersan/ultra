<?php

namespace Ultra\LibrosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\LibrosBundle\Entity\Autores;
use Ultra\LibrosBundle\Form\AutoresType;

/**
 * Autores controller.
 *
 * @Route("/libros/autores")
 */
class AutoresController extends Controller
{

    /**
     * Lists all Autores entities.
     *
     * @Route("/", name="libros_autores")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('LibrosBundle:Autores')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Autores entity.
     *
     * @Route("/", name="libros_autores_create")
     * @Method("POST")
     * @Template("LibrosBundle:Autores:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Autores();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('libros_autores_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Autores entity.
    *
    * @param Autores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Autores $entity)
    {
        $form = $this->createForm(new AutoresType(), $entity, array(
            'action' => $this->generateUrl('libros_autores_create'),
            'method' => 'POST',
        ));

$form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success pull-left',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new Autores entity.
     *
     * @Route("/new", name="libros_autores_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Autores();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Autores entity.
     *
     * @Route("/{id}", name="libros_autores_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Autores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Autores entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Autores entity.
     *
     * @Route("/{id}/edit", name="libros_autores_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Autores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Autores entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Autores entity.
    *
    * @param Autores $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Autores $entity)
    {
        $form = $this->createForm(new AutoresType(), $entity, array(
            'action' => $this->generateUrl('libros_autores_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success pull-left',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));
        return $form;
    }
    /**
     * Edits an existing Autores entity.
     *
     * @Route("/{id}", name="libros_autores_update")
     * @Method("PUT")
     * @Template("LibrosBundle:Autores:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LibrosBundle:Autores')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Autores entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('libros_autores_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('libros_autores_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Autores entity.
     *
     * @Route("/{id}", name="libros_autores_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LibrosBundle:Autores')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Autores entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('libros_autores'));
    }

    /**
     * Creates a form to delete a Autores entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libros_autores_delete', array('id' => $id)))
            ->setMethod('DELETE')
                  ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
