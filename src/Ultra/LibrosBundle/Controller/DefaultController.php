<?php

namespace Ultra\LibrosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/libross/{name}")
     * @Template()
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }


     /**
     * @Route("/libross")
     * @Template("LibrosBundle:Default:index.html.twig")
     */
    public function librosAction()
    {
        return array('name' => "Libros");
    }

}
