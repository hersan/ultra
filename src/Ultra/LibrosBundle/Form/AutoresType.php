<?php

namespace Ultra\LibrosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AutoresType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paterno',NULL,array('label'=>"Apellido paterno:", 'attr' => array ('class' => 'form-control input-sm')))
            ->add('materno',NULL,array('label'=>"Apellido materno:", 'required'=>false,'attr' => array ('class' => 'form-control input-sm') ))
        ->add('nombre',Null,array('label'=>"Nombre:", 'attr' => array ('class' => 'form-control input-sm')))
  //          ->add('libros_porautor')
        ;
    }
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\LibrosBundle\Entity\Autores'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_librosbundle_autores';
    }
}
