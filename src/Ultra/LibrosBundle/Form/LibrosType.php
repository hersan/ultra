<?php

namespace Ultra\LibrosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LibrosType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('type', 'choice', array(
                    'choices' => \Ultra\LibrosBundle\Entity\Libros::$types,
                    'empty_value' => 'Selecciona una opcion',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    )
                ))
                ->add('tipoNormativa', 'choice', array(
                    'choices' => \Ultra\LibrosBundle\Entity\Libros::$normativas,
                    'empty_value' => 'Selecciona una opcion',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    )
                ))
                ->add('visible', 'choice', array(
                    'choices' => \Ultra\LibrosBundle\Entity\Libros::$visibilidad,
                    'empty_value' => 'Selecciona una opcion',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true,
                    )
                ))
                ->add('file', 'file', array(
                    'label' => 'Ingresa el archivo (libro):',
                    'required' => false,
                    'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                    'property_path' => 'file',
                ))
                ->add('titulo', 'text', array("label" => "Título:",
                    'attr' => array(
                        'class' => 'form-control input-sm')))
                
                ->add('autores_porlibro', 'entity', array(
                    "label" => "Autor/autores:",
                    'class' => 'LibrosBundle:Autores',
                    'attr' => array(
                        'class' => 'selectpicker',
                        'multiple' => true,
                        'title' => 'Selecciona los autores...',
                        'data-width' => '100%',
                        'data-size' => '7',
                        'data-live-search' => true,
                    ),
                    'multiple' => true,
                    'empty_data' => ' ',
                    'empty_value' => 'Selecciona los autores...'
                ))
                ->add('editorial_desdetablaEditoriales', 'entity', array(
                    "label" => "Editorial:",
                    'class' => 'LibrosBundle:Editoriales',
                    'empty_value' => 'Seleccionar...',
                    'empty_data' => null,
                    'attr' => array(
                        'class' => 'form-control selectpicker',
                        'data-live-search' => true
                    )
                        )
                )

//           ->add('editorialId')
                ->add('edicion', 'text', array("label" => "Año de edición:", 'attr' => array('class' => 'form-control input-sm')))
                ->add('noEdicion', 'text', array("label" => "Edición número:", 'attr' => array('class' => 'form-control input-sm')))
                ->add('isbn', 'text', array("label" => "ISBN:", 'attr' => array('class' => 'form-control input-sm')))
                ->add('totalPaginas', 'text', array("label" => "Total de páginas:", 'attr' => array('class' => 'form-control input-sm')))
//            ->add('ruta')
                ->add('tema', 'text', array("label" => "Tema:", 'attr' => array('class' => 'form-control input-sm')))
//        ->add('editorial_desdetablaEditoriales')
//        ->add('autores_porlibro')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\LibrosBundle\Entity\Libros'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ultra_librosbundle_libros';
    }

}
