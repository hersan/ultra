<?php

namespace Ultra\LibrosBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * LibrosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LibrosRepository extends EntityRepository
{
    public function findLibroByTitulo($titulo){
        $query = $this->getEntityManager()->createQuery(
            "SELECT d FROM LibrosBundle:Libros d
              WHERE d.titulo LIKE :titulo
            ")->setParameters(array(
                'titulo' => '%'.$titulo.'%'
            ));

        try{
            return $query->getResult();
        }catch (\Doctrine\ORM\NoResultException $e){
            return null;
        }
    }
}
