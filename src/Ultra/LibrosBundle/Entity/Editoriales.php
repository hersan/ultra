<?php

namespace Ultra\LibrosBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Editoriales
 *
 * @ORM\Table(name="Libros_Editoriales")
 * @ORM\Entity(repositoryClass="Ultra\LibrosBundle\Entity\EditorialesRepository")
 */
class Editoriales
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Escriba el nombre de la editorial...")
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

     /**
     * @ORM\OneToMany(targetEntity="Libros", mappedBy="editorial_desdetablaEditoriales")
     */
    protected $libros_poreditorial;

    public function __construct()
    {
        $this->libros_poreditorial = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Editoriales
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add libros_poreditorial
     *
     * @param Libros $librosPoreditorial
     * @return Editoriales
     */
    public function addLibrosPoreditorial(Libros $librosPoreditorial)
    {
        $this->libros_poreditorial[] = $librosPoreditorial;
    
        return $this;
    }

    /**
     * Remove libros_poreditorial
     *
     * @param Libros $librosPoreditorial
     */
    public function removeLibrosPoreditorial(Libros $librosPoreditorial)
    {
        $this->libros_poreditorial->removeElement($librosPoreditorial);
    }

    /**
     * Get libros_poreditorial
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLibrosPoreditorial()
    {
        return $this->libros_poreditorial;
    }


    public function __toString()
    {
        return $this->descripcion;
    }
}