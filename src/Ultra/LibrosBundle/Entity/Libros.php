<?php

namespace Ultra\LibrosBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * Libros
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\LibrosBundle\Entity\LibrosRepository")
 * @ORM\HasLifecycleCallbacks
 * @Assert\Callback(methods={"isAuthorsValid"})
 */
class Libros {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el título del libro...")
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var integer
     * @ORM\Column(name="editorial_id", type="integer")
     */
    private $editorialId;
    
    /**
     * @var array
     */
    public static $visibilidad = array(
        1 => 'Visible',
        2 => 'NoVisible',
    );

    /**
     * @var string
     * @ORM\Column(name="visible", type="smallint", nullable=true)
     */
    private $visible;
    
    /**
     * @var string
     * @ORM\Column(name="type", type="smallint", nullable=true)
     */
    private $type;

    /**
     * @var string
     * @ORM\Column(name="tipoNormativa", type="smallint", nullable=true)
     */
    private $tipoNormativa;

    /**
     * @var array
     */
    public static $types = array(
        1 => 'Libros',
        2 => 'Normativas',
    );

    /**
     * @var array
     */
    public static $normativas = array(
        1 => 'Interno',
        2 => 'Externo',
        3 => 'No Normativa',
    );

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el año de edición...")
     * @ORM\Column(name="edicion", type="string", length=5)
     */
    private $edicion;

    /**
     * @var integer
     * @Assert\NotBlank(message="Digite el número de edición...")
     * @ORM\Column(name="NoEdicion", type="integer")
     */
    private $noEdicion;

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el ISBN...")
     * @ORM\Column(name="isbn", type="string", length=30)
     */
    private $isbn;

    /**
     * @var string
     * @Assert\NotBlank(message="Digite el tema...")
     * @ORM\Column(name="tema", type="string", length=255)
     */
    private $tema;

    /**
     * @var integer
     * @Assert\NotBlank(message="Digite el número total de páginas...")
     * @ORM\Column(name="TotalPaginas", type="integer")
     */
    private $totalPaginas;

    /**
     * @var string
     *
     * @ORM\Column(name="ruta", type="string", length=255, nullable=true)
     */
    private $ruta;

    /**
     * @var string
     *
     * @Assert\File(maxSize = "60M", mimeTypes = {
     *   "application/pdf",
     *   "application/x-pdf"
     * })
     */
    private $file;

    /**
     * Ruta anterior de un archivo
     * @var string
     */
    private $path;

    /**
     * @Assert\NotBlank(message="Seleccione la editorial...")
     * @ORM\ManyToOne(targetEntity="Editoriales", inversedBy="libros_poreditorial")
     * @ORM\JoinColumn(name="editorial_id", referencedColumnName="id")
     */
    protected $editorial_desdetablaEditoriales;

    /**
     * @ORM\ManyToMany(targetEntity="Ultra\LibrosBundle\Entity\Autores", inversedBy="libros_porautor")
     * @ORM\JoinTable(name="Libros_LibrosAutores")
     */
    private $autores_porlibro;

    public function __construct() {
        $this->autores_porlibro = new ArrayCollection();
    }

    public function isAuthorsValid(ExecutionContext $context) {
        if ($this->autores_porlibro->count() == 0) {
            $context->addViolation('Seleccione al autor / autores...', array(), null);
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Libros
     */
    public function setTitulo($titulo) {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo() {
        return $this->titulo;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Libros
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    
    
    /**
     * Set tipoNormativa
     *
     * @param string $tipoNormativa
     * @return Libros
     */
    public function setTipoNormativa($tipoNormativa) {
        $this->tipoNormativa = $tipoNormativa;

        return $this;
    }

    /**
     * Get tipoNormativa
     *
     * @return string 
     */
    public function getTipoNormativa()
    {
        return $this->tipoNormativa;
    }

    /**
     * Set edicion
     *
     * @param string $edicion
     * @return Libros
     */
    public function setEdicion($edicion) {
        $this->edicion = $edicion;

        return $this;
    }

    /**
     * Get edicion
     *
     * @return string 
     */
    public function getEdicion() {
        return $this->edicion;
    }

    /**
     * Set visible
     *
     * @param string $visible
     * @return Libros
     */
    public function setVisible($visible) {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return string 
     */
    public function getVisible()
    {
        return $this->visible;
    }
    
    /**
     * Set noEdicion
     *
     * @param integer $noEdicion
     * @return Libros
     */
    public function setNoEdicion($noEdicion) {
        $this->noEdicion = $noEdicion;

        return $this;
    }

    /**
     * Get noEdicion
     *
     * @return integer 
     */
    public function getNoEdicion() {
        return $this->noEdicion;
    }

    /**
     * Set isbn
     *
     * @param string $isbn
     * @return Libros
     */
    public function setIsbn($isbn) {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * Get isbn
     *
     * @return string 
     */
    public function getIsbn() {
        return $this->isbn;
    }

    /**
     * Set tema
     *
     * @param string $tema
     * @return Libros
     */
    public function setTema($tema) {
        $this->tema = $tema;

        return $this;
    }

    /**
     * Get tema
     *
     * @return string 
     */
    public function getTema() {
        return $this->tema;
    }

    /**
     * Set totalPaginas
     *
     * @param integer $totalPaginas
     * @return Libros
     */
    public function setTotalPaginas($totalPaginas) {
        $this->totalPaginas = $totalPaginas;

        return $this;
    }

    /**
     * Get totalPaginas
     *
     * @return integer 
     */
    public function getTotalPaginas() {
        return $this->totalPaginas;
    }

    /**
     * Set editorialId
     *
     * @param integer $editorialId
     * @return Libros
     */
    public function setEditorialId($editorialId) {
        $this->editorialId = $editorialId;

        return $this;
    }

    /**
     * Get editorialId
     *
     * @return integer 
     */
    public function getEditorialId() {
        return $this->editorialId;
    }

    /**
     * Set editorial_desdetablaEditoriales
     *
     * @param \Ultra\LibrosBundle\Entity\Editoriales $editorialDesdetablaEditoriales
     * @return Libros
     */
    public function setEditorialDesdetablaEditoriales(\Ultra\LibrosBundle\Entity\Editoriales $editorialDesdetablaEditoriales = null) {
        $this->editorial_desdetablaEditoriales = $editorialDesdetablaEditoriales;

        return $this;
    }

    /**
     * Get editorial_desdetablaEditoriales
     *
     * @return \Ultra\LibrosBundle\Entity\Editoriales 
     */
    public function getEditorialDesdetablaEditoriales() {
        return $this->editorial_desdetablaEditoriales;
    }

    /**
     * Set ruta
     *
     * @param string $ruta
     * @return Libros
     */
    public function setRuta($ruta) {
        $this->ruta = $ruta;

        return $this;
    }

    /**
     * Get ruta
     *
     * @return string 
     */
    public function getRuta() {
        return $this->ruta;
    }

    /**
     * Add autores_porlibro
     *
     * @param \Ultra\LibrosBundle\Entity\Autores $autoresPorlibro
     * @return Libros
     */
    public function addAutoresPorlibro(\Ultra\LibrosBundle\Entity\Autores $autoresPorlibro) {
        $this->autores_porlibro[] = $autoresPorlibro;

        return $this;
    }

    /**
     * Remove autores_porlibro
     *
     * @param \Ultra\LibrosBundle\Entity\Autores $autoresPorlibro
     */
    public function removeAutoresPorlibro(\Ultra\LibrosBundle\Entity\Autores $autoresPorlibro) {
        $this->autores_porlibro->removeElement($autoresPorlibro);
    }

    /**
     * Get autores_porlibro
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAutoresPorlibro() {
        return $this->autores_porlibro;
    }

    //comienza implementacion para guardar documento

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
            //$this->path = null;
        } else {
            $this->ruta = 'initial';
        }
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }

    public function revisarnombre($archivo_jf = "No hay nombre") {
        // $string = (strlen($string) > 13) ? substr($string,0,10).'...' : $string;


        $archivo_jf = explode('.', $archivo_jf);
        //  $ext = array_pop( $archivo_jf );
        array_pop($archivo_jf);
        $archivo_jf = implode('.', $archivo_jf);
        $archivo_jf = substr($archivo_jf, 0, 70);
        $archivo_jf = trim($archivo_jf, "_- ");

        $archivo_jf = strip_tags($archivo_jf);
        $archivo_jf = preg_replace('/[\r\n\t ]+/', ' ', $archivo_jf);
        $archivo_jf = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $archivo_jf);
        //    $archivo_jf = strtolower($archivo_jf);
        $archivo_jf = html_entity_decode($archivo_jf, ENT_QUOTES, "utf-8");
        $archivo_jf = htmlentities($archivo_jf, ENT_QUOTES, "utf-8");
        $archivo_jf = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $archivo_jf);
        $archivo_jf = str_replace(' ', '_', $archivo_jf);
        //   $archivo_jf = rawurlencode($archivo_jf);
        //   $archivo_jf = str_replace('%', '-', $archivo_jf);
        //    $archivo_jf = strtoupper( $archivo_jf );


        return $archivo_jf;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload() {
        if (null !== $this->getFile()) {

            $archivo_jf = $this->revisarnombre($this->getFile()->getClientOriginalName());
            $this->ruta = $this->id . "__LIBRO__" . $archivo_jf . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate() {
        if (null != $this->getFile()) {

            $archivo_jf = $this->revisarnombre($this->getFile()->getClientOriginalName());

            $this->ruta = $this->id . "__LIBRO__" . $archivo_jf . '.' . $this->getFile()->guessExtension();
        }

        if (null != $this->getTemporaryPath()) {
            $f = new Filesystem();
            $f->copy(
                    $this->getTemporaryPath(), $this->getAbsolutePath(), true);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
                $this->getUploadRootDir(), $this->ruta
        );

        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath() {
//        return null === $this->ruta
//            ? null
//            : $this->getUploadRootDir().'/'.$this->ruta;

        return $this->getUploadRootDir() . '/' . $this->ruta;
    }

    public function getWebPath() {
        return null === $this->ruta ? null : $this->getUploadDir() . '/' . $this->ruta;
    }

    public function getUploadRootDir() {
        return '/var/sad/' . $this->getUploadDir();
    }

    public function setUploadRootDir($dir) {
        $this->rootDir = $dir;
    }

    public function getUploadDir() {
        return 'uploads/libros';
    }

    public function temporaryPath($path) {
        $this->path = $path;
    }

    public function getTemporaryPath() {
        return $this->path;
    }

    /**
     * @return mixed return the visibility or null if not exist
     */
    public function getVisibilidad()
    {
        if (empty($this->getVisible())) {
            return null;
        }
        return self::$visibilidad[$this->getVisible()];
    }

    /**
     * @return mixed return the type or null if not exist
     */
    public function getTypes()
    {
        if (empty($this->getType())) {
            return null;
        }

        return self::$types[$this->getType()];
    }

    /**
     * @return mixed return the normative or null if not exist
     */
    public function getNormativas()
    {
        if (empty($this->tipoNormativa)) {
            return null;
        }

        return self::$normativas[$this->tipoNormativa];
    }


}
