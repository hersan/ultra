<?php

namespace Ultra\LibrosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Autores
 *
 * @ORM\Table(name="Libros_Autores")
 * @ORM\Entity(repositoryClass="Ultra\LibrosBundle\Entity\AutoresRepository")
 */
class Autores
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Escriba el nombre / nombres del autor...")
     * @ORM\Column(name="nombre", type="string", length=80)
     */
    private $nombre;

    /**
     * @var string
     * @Assert\NotBlank(message="Escriba el apellido paterno del autor...")
     * @ORM\Column(name="paterno", type="string", length=80)
     */
    private $paterno;

    /**
     * @var string
     *
     * @ORM\Column(name="materno", type="string", length=80, nullable=TRUE)
     */
    private $materno;

    /**
     * @ORM\ManyToMany(targetEntity="Ultra\LibrosBundle\Entity\Libros", mappedBy="autores_porlibro")
     * @ORM\JoinTable(name="Libros_LibrosAutores")
     */
    protected $libros_porautor;

    public function __construct()
    {
        $this->libros_porautor = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Autores
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set paterno
     *
     * @param string $paterno
     * @return Autores
     */
    public function setPaterno($paterno)
    {
        $this->paterno = $paterno;
    
        return $this;
    }

    /**
     * Get paterno
     *
     * @return string 
     */
    public function getPaterno()
    {
        return $this->paterno;
    }

    /**
     * Set materno
     *
     * @param string $materno
     * @return Autores
     */
    public function setMaterno($materno)
    {
        $this->materno = $materno;
    
        return $this;
    }

    /**
     * Get materno
     *
     * @return string 
     */
    public function getMaterno()
    {
        return $this->materno;
    }

    /**
     * Add libros_porautor
     *
     * @param \Ultra\LibrosBundle\Entity\Libros $librosPorautor
     * @return Autores
     */
    public function addLibrosPorautor(\Ultra\LibrosBundle\Entity\Libros $librosPorautor)
    {
        $this->libros_porautor[] = $librosPorautor;
    
        return $this;
    }

    /**
     * Remove libros_porautor
     *
     * @param \Ultra\LibrosBundle\Entity\Libros $librosPorautor
     */
    public function removeLibrosPorautor(\Ultra\LibrosBundle\Entity\Libros $librosPorautor)
    {
        $this->libros_porautor->removeElement($librosPorautor);
    }

    /**
     * Get libros_porautor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLibrosPorautor()
    {
        return $this->libros_porautor;
    }

    public function __toString()
    {
      // $var_jf=$this->id." - ";
        $var_jf='';

        if ( isset ($this->paterno) ) $var_jf.= $this->paterno;

        if ( isset ($this->materno) ) $var_jf.= " ".$this->materno;

        if ( isset ($this->nombre) )  $var_jf.= ", ".$this->nombre;

        return $var_jf;
    }
}