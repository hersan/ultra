<?php

namespace Ultra\AcquisitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductServiceType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array(
                    'label' => 'Nombre',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('description', 'text', array(
                    'label' => 'Descripcion',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('tipo', 'choice', array(
                'choices' => \Ultra\AcquisitionBundle\Entity\ProductService::$items,
                'empty_value' => 'Selecciona una opcion',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\AcquisitionBundle\Entity\ProductService'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ultra_acquisitionbundle_productservice';
    }

}
