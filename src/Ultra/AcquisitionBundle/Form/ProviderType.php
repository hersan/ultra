<?php

namespace Ultra\AcquisitionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Ultra\AcquisitionBundle\Entity\Provider;

class ProviderType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('productType', 'choice', array(
                'choices' => Provider::$items,
                'label' => 'Tipo de Producto',
                'empty_value' => 'Selecciona una opcion',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('code', 'text', array(
                'label' => 'Código',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
                
            ->add('certificado', 'text', array(
                'label' => 'Certificado',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
                
            ->add('name', 'text', array(
                'label' => 'Nombre',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('companyName', 'text', array(
                'label' => 'Razón Social',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
                
            ->add('fechaSeleccion', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha de Seleccion',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
                
            ->add('contactPerson', 'text', array(
                'label' => 'Contacto',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('telephone', 'text', array(
                'label' => 'Telefono',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('email', 'text', array(
                'label' => 'Correo Electronico',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('address', 'textarea', array(
                'label' => 'Dirección',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('location', 'text', array(
                'label' => 'Localidad',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('evaluation', 'text', array(
                'label' => 'Impacto sobre el servico/Satisfacción al cliente',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('comments', 'textarea', array(
                'label' => 'Justificacion',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
                
                           
            ->add('category')
            ->add('productOrService')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\AcquisitionBundle\Entity\Provider'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_acquisitionbundle_provider';
    }
}
