<?php

namespace Ultra\AcquisitionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\AcquisitionBundle\Entity\ProductService;
use Ultra\AcquisitionBundle\Form\ProductServiceType;

/**
 * ProductService controller.
 *
 * @Route("/product-service")
 */
class ProductServiceController extends Controller {

    /**
     * Lists all ProductService entities.
     *
     * @Route("/", name="product-service")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AcquisitionBundle:ProductService')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new ProductService entity.
     *
     * @Route("/", name="product-service_create")
     * @Method("POST")
     * @Template("AcquisitionBundle:ProductService:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new ProductService();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('product-service'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ProductService entity.
     *
     * @param ProductService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ProductService $entity) {
        $form = $this->createForm(new ProductServiceType(), $entity, array(
            'action' => $this->generateUrl('product-service_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Guardar',
            'attr' => array('class' => 'btn btn-success pull-right',
                'title' => 'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new ProductService entity.
     *
     * @Route("/new", name="product-service_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new ProductService();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a ProductService entity.
     *
     * @Route("/{id}", name="product-service_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:ProductService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ProductService entity.
     *
     * @Route("/{id}/edit", name="product-service_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:ProductService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductService entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a ProductService entity.
     *
     * @param ProductService $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ProductService $entity) {
        $form = $this->createForm(new ProductServiceType(), $entity, array(
            'action' => $this->generateUrl('product-service_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array('class' => 'btn btn-success pull-left',
                'title' => 'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit',
        ));
        return $form;
    }

    /**
     * Edits an existing ProductService entity.
     *
     * @Route("/{id}", name="product-service_update")
     * @Method("PUT")
     * @Template("AcquisitionBundle:ProductService:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:ProductService')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductService entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('product-service_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ProductService entity.
     *
     * @Route("/{id}", name="product-service_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcquisitionBundle:ProductService')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProductService entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('product-service'));
    }

    /**
     * Creates a form to delete a ProductService entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('product-service_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'usubmit', array(
                            'label' => 'Borrar',
                            'attr' => array('class' => 'btn btn-warning',
                                'title' => 'Borrar datos'),
                            'glyphicon' => 'glyphicon glyphicon-trash',
                        ))
                        ->getForm()
        ;
    }

}
