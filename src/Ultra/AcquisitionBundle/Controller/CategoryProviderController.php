<?php

namespace Ultra\AcquisitionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\AcquisitionBundle\Entity\CategoryProvider;
use Ultra\AcquisitionBundle\Form\CategoryProviderType;

/**
 * CategoryProvider controller.
 *
 * @Route("/category/provider")
 */
class CategoryProviderController extends Controller
{

    /**
     * Lists all CategoryProvider entities.
     *
     * @Route("/", name="category_provider")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AcquisitionBundle:CategoryProvider')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new CategoryProvider entity.
     *
     * @Route("/", name="category_provider_create")
     * @Method("POST")
     * @Template("AcquisitionBundle:CategoryProvider:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new CategoryProvider();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('category_provider'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a CategoryProvider entity.
     *
     * @param CategoryProvider $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CategoryProvider $entity)
    {
        $form = $this->createForm(new CategoryProviderType(), $entity, array(
            'action' => $this->generateUrl('category_provider_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Guardar',
               'attr' => array('class' => 'btn btn-success pull-right',
               'title'=>'Guardar datos'),
               'glyphicon' => 'glyphicon glyphicon-floppy-saved'));

        return $form;
    }

    /**
     * Displays a form to create a new CategoryProvider entity.
     *
     * @Route("/new", name="category_provider_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new CategoryProvider();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a CategoryProvider entity.
     *
     * @Route("/{id}", name="category_provider_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:CategoryProvider')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoryProvider entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing CategoryProvider entity.
     *
     * @Route("/{id}/edit", name="category_provider_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:CategoryProvider')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoryProvider entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a CategoryProvider entity.
    *
    * @param CategoryProvider $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CategoryProvider $entity)
    {
        $form = $this->createForm(new CategoryProviderType(), $entity, array(
            'action' => $this->generateUrl('category_provider_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'usubmit', array(
                'label' => 'Actualizar',
                'attr' => array('class' => 'btn btn-success pull-left',
                'title'=>'Actualizar datos'),
                'glyphicon' => 'glyphicon glyphicon-edit',
            ));

        return $form;
    }
    /**
     * Edits an existing CategoryProvider entity.
     *
     * @Route("/{id}", name="category_provider_update")
     * @Method("PUT")
     * @Template("AcquisitionBundle:CategoryProvider:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AcquisitionBundle:CategoryProvider')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CategoryProvider entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('category_provider_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a CategoryProvider entity.
     *
     * @Route("/{id}", name="category_provider_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AcquisitionBundle:CategoryProvider')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CategoryProvider entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('category_provider'));
    }

    /**
     * Creates a form to delete a CategoryProvider entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('category_provider_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'usubmit', array(
                    'label' => 'Borrar',
                    'attr' => array('class' => 'btn btn-warning',
                    'title'=>'Borrar datos'),
                    'glyphicon' => 'glyphicon glyphicon-trash',
                ))
            ->getForm()
        ;
    }
}
