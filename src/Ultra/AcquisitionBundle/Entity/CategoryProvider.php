<?php

namespace Ultra\AcquisitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryProvider
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\AcquisitionBundle\Entity\CategoryProviderRepository")
 */
class CategoryProvider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CategoryProvider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
    /**
     * Get description
     *
     * @return string 
     */
     public function __toString() {
         return $this->getName();
     }


    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
