<?php
namespace Ultra\AcquisitionBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Provider
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\AcquisitionBundle\Entity\ProviderRepository")
 */
class Provider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     */
    public static $items = array(
        'Producto',
        'Servicio',
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="product_type", type="smallint")
     */
    private $productType;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="certificado", type="string", length=255)
     */
    private $certificado;
    
    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255)
     */
    private $companyName;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_seleccion", type="date", nullable=true)
     */
    private $fechaSeleccion;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="string", length=255)
     */
    private $contactPerson;
   
    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="evaluation", type="smallint")
     */
    private $evaluation;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", nullable=true)
     */
    private $comments;

    /**
     * @var \Ultra\AcquisitionBundle\Entity\CategoryProvider $category
     *
     * @ORM\ManyToOne(targetEntity="Ultra\AcquisitionBundle\Entity\CategoryProvider")
     */
    private $category;

    /**
     * @var \Ultra\AcquisitionBundle\Entity\ProductService $productOrService
     *
     * @ORM\ManyToOne(targetEntity="Ultra\AcquisitionBundle\Entity\ProductService")
     */
    private $productOrService;

    /**
     * Provider constructor.
     *
     */
    public function __construct()
    {
        $this->productType = 0;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Provider
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Provider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
   
    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return Provider
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    
    /**
     * Get fechaSeleccion
     *
     * @return \DateTime
     */
    public function getFechaSeleccion()
    {
        return $this->fechaSeleccion;
    }

    /**
     * Set fechaSeleccion
     *
     * @param \DateTime $fechaSeleccion
     * @return Provider
     */
    public function setFechaSeleccion($fechaSeleccion)
    {
        $this->fechaSeleccion = $fechaSeleccion;

        return $this;
    }
    
    
    /**
     * Set certificado
     *
     * @param string $certificado
     * @return Provider
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;

        return $this;
    }
   
    /**
     * Get certificado
     *
     * @return string 
     */
    public function getCertificado()
    {
        return $this->certificado;
    }
    
    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     * @return Provider
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string 
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Provider
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Provider
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Provider
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return Provider
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set evaluation
     *
     * @param integer $evaluation
     * @return Provider
     */
    public function setEvaluation($evaluation)
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Get evaluation
     *
     * @return integer 
     */
    public function getEvaluation()
    {
        return $this->evaluation;
    }

    /**
     * Set productType
     *
     * @param integer $productType
     * @return Provider
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return integer 
     */
    public function getProductType()
    {
        return self::$items[$this->productType];
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Provider
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }
   
    /**
     * Set category
     *
     * @param \Ultra\AcquisitionBundle\Entity\CategoryProvider $category
     * @return Provider
     */
    public function setCategory(\Ultra\AcquisitionBundle\Entity\CategoryProvider $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Ultra\AcquisitionBundle\Entity\CategoryProvider 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set productOrService
     *
     * @param \Ultra\AcquisitionBundle\Entity\ProductService $productOrService
     * @return Provider
     */
    public function setProductOrService(\Ultra\AcquisitionBundle\Entity\ProductService $productOrService = null)
    {
        $this->productOrService = $productOrService;

        return $this;
    }

    /**
     * Get productOrService
     *
     * @return \Ultra\AcquisitionBundle\Entity\ProductService 
     */
    public function getProductOrService()
    {
        return $this->productOrService;
    }
}
