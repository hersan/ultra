<?php

namespace Ultra\AcquisitionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductService
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\AcquisitionBundle\Entity\ProductServiceRepository")
 */
class ProductService
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var array
     */    
    public static $items = [
        1 => 'Producto',
        2 => 'Servicio',
    ];
    
    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="integer")
     */
    private $tipo;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ProductService
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tipo
     *
     * @return ProductService
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return integer 
     */
    public function getTipo()
    {
        return $this->tipo;
    }
    
    /**
     * Set description
     *
     * @param string $description
     * @return ProductService
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }
    
    /**
     * Get tipo
     *
     * @return integer 
     */
    public function geTipo()
    {
        return self::$items[$this->productType];
    }
    
     /**
     * Get description
     *
     * @return string 
     */
     public function __toString() {
         return $this->getName();
     }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
