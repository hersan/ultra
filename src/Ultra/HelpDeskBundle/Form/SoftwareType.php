<?php

namespace Ultra\HelpDeskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SoftwareType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('softwareName')
            ->add('softwareVersion')
            ->add('softwareSize')
            ->add('softwareNeed')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\HelpDeskBundle\Entity\Software'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_helpdeskbundle_software';
    }
}
