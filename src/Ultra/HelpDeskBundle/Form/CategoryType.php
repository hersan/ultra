<?php

namespace Ultra\HelpDeskBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('categoryName','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Nombre de la categoría',
                    'autocomplete'=>'on',
                    'maxlength'=>'45',
                    'title'=>'Ingresa el nombre de la categoría',
                    'autofocus'=>true),
                'label' => 'Nombre de la categoría',
                'required' => false
            ))
            ->add('software', 'entity', array(
                'empty_value' => 'Seleccionar software',
                'class' => 'HelpDeskBundle:Software',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona software'
                ),
                'label' => 'Software',
                'required' => false,
            ))
            ->add('hwCategory', 'entity', array(
                'empty_value' => 'Seleccionar hardware',
                'class' => 'HelpDeskBundle:Hardware',
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                    'title'=>'Selecciona hardware'
                ),
                'label' => 'Hardware',
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\HelpDeskBundle\Entity\Category'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_helpdeskbundle_category';
    }
}
