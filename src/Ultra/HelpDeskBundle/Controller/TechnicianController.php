<?php

namespace Ultra\HelpDeskBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\HelpDeskBundle\Entity\Technician;
use Ultra\HelpDeskBundle\Form\TechnicianType;

/**
 * Technician controller.
 *
 * @Route("/helpdesk/technician")
 */
class TechnicianController extends Controller
{

    /**
     * Lists all Technician entities.
     *
     * @Route("/", name="helpdesk_technician")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HelpDeskBundle:Technician')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Technician entity.
     *
     * @Route("/", name="helpdesk_technician_create")
     * @Method("POST")
     * @Template("HelpDeskBundle:Technician:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Technician();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('helpdesk_technician_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Technician entity.
     *
     * @param Technician $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Technician $entity)
    {
        $form = $this->createForm(new TechnicianType(), $entity, array(
            'action' => $this->generateUrl('helpdesk_technician_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Technician entity.
     *
     * @Route("/new", name="helpdesk_technician_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Technician();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Technician entity.
     *
     * @Route("/{id}", name="helpdesk_technician_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Technician')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Technician entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Technician entity.
     *
     * @Route("/{id}/edit", name="helpdesk_technician_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Technician')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Technician entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Technician entity.
    *
    * @param Technician $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Technician $entity)
    {
        $form = $this->createForm(new TechnicianType(), $entity, array(
            'action' => $this->generateUrl('helpdesk_technician_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Technician entity.
     *
     * @Route("/{id}", name="helpdesk_technician_update")
     * @Method("PUT")
     * @Template("HelpDeskBundle:Technician:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Technician')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Technician entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('helpdesk_technician_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Technician entity.
     *
     * @Route("/{id}", name="helpdesk_technician_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HelpDeskBundle:Technician')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Technician entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('helpdesk_technician'));
    }

    /**
     * Creates a form to delete a Technician entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('helpdesk_technician_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
