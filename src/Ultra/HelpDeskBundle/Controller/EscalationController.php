<?php

namespace Ultra\HelpDeskBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\HelpDeskBundle\Entity\Escalation;
use Ultra\HelpDeskBundle\Form\EscalationType;

/**
 * Escalation controller.
 *
 * @Route("/helpdesk/escalation")
 */
class EscalationController extends Controller
{

    /**
     * Lists all Escalation entities.
     *
     * @Route("/", name="helpdesk_escalation")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HelpDeskBundle:Escalation')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Escalation entity.
     *
     * @Route("/", name="helpdesk_escalation_create")
     * @Method("POST")
     * @Template("HelpDeskBundle:Escalation:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Escalation();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('helpdesk_escalation_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Escalation entity.
     *
     * @param Escalation $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Escalation $entity)
    {
        $form = $this->createForm(new EscalationType(), $entity, array(
            'action' => $this->generateUrl('helpdesk_escalation_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Escalation entity.
     *
     * @Route("/new", name="helpdesk_escalation_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Escalation();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Escalation entity.
     *
     * @Route("/{id}", name="helpdesk_escalation_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Escalation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escalation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Escalation entity.
     *
     * @Route("/{id}/edit", name="helpdesk_escalation_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Escalation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escalation entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Escalation entity.
    *
    * @param Escalation $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Escalation $entity)
    {
        $form = $this->createForm(new EscalationType(), $entity, array(
            'action' => $this->generateUrl('helpdesk_escalation_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Escalation entity.
     *
     * @Route("/{id}", name="helpdesk_escalation_update")
     * @Method("PUT")
     * @Template("HelpDeskBundle:Escalation:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HelpDeskBundle:Escalation')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Escalation entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('helpdesk_escalation_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Escalation entity.
     *
     * @Route("/{id}", name="helpdesk_escalation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HelpDeskBundle:Escalation')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Escalation entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('helpdesk_escalation'));
    }

    /**
     * Creates a form to delete a Escalation entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('helpdesk_escalation_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
