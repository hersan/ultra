<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Technician
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\TechnicianRepository")
 */
class Technician
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     **/
    private $tecnico;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Escalation")
     **/
    private $escalation;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Technician
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set tecnico
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $tecnico
     * @return Technician
     */
    public function setTecnico(\Ultra\ControlDocumentoBundle\Entity\Curricula $tecnico = null)
    {
        $this->tecnico = $tecnico;
    
        return $this;
    }

    /**
     * Get tecnico
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getTecnico()
    {
        return $this->tecnico;
    }

    /**
     * Set escalation
     *
     * @param \Ultra\HelpDeskBundle\Entity\Escalation $escalation
     * @return Technician
     */
    public function setEscalation(\Ultra\HelpDeskBundle\Entity\Escalation $escalation = null)
    {
        $this->escalation = $escalation;
    
        return $this;
    }

    /**
     * Get escalation
     *
     * @return \Ultra\HelpDeskBundle\Entity\Escalation 
     */
    public function getEscalation()
    {
        return $this->escalation;
    }

    public function __toString(){
        return $this->getEscalation();
    }

}