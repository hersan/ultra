<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Software
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\SoftwareRepository")
 */
class Software
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="softwareName", type="string", length=100)
     */
    private $softwareName;

    /**
     * @var string
     *
     * @ORM\Column(name="softwareVersion", type="string", length=100)
     */
    private $softwareVersion;

    /**
     * @var string
     *
     * @ORM\Column(name="softwareSize", type="string", length=100)
     */
    private $softwareSize;

    /**
     * @var string
     *
     * @ORM\Column(name="softwareNeed", type="string", length=100)
     */
    private $softwareNeed;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Category", inversedBy="software")
     **/
    private $category;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set softwareName
     *
     * @param string $softwareName
     * @return Software
     */
    public function setSoftwareName($softwareName)
    {
        $this->softwareName = $softwareName;
    
        return $this;
    }

    /**
     * Get softwareName
     *
     * @return string 
     */
    public function getSoftwareName()
    {
        return $this->softwareName;
    }

    /**
     * Set softwareVersion
     *
     * @param string $softwareVersion
     * @return Software
     */
    public function setSoftwareVersion($softwareVersion)
    {
        $this->softwareVersion = $softwareVersion;
    
        return $this;
    }

    /**
     * Get softwareVersion
     *
     * @return string 
     */
    public function getSoftwareVersion()
    {
        return $this->softwareVersion;
    }

    /**
     * Set softwareSize
     *
     * @param string $softwareSize
     * @return Software
     */
    public function setSoftwareSize($softwareSize)
    {
        $this->softwareSize = $softwareSize;
    
        return $this;
    }

    /**
     * Get softwareSize
     *
     * @return string 
     */
    public function getSoftwareSize()
    {
        return $this->softwareSize;
    }

    /**
     * Set softwareNeed
     *
     * @param string $softwareNeed
     * @return Software
     */
    public function setSoftwareNeed($softwareNeed)
    {
        $this->softwareNeed = $softwareNeed;
    
        return $this;
    }

    /**
     * Get softwareNeed
     *
     * @return string 
     */
    public function getSoftwareNeed()
    {
        return $this->softwareNeed;
    }

    /**
     * Set category
     *
     * @param \Ultra\HelpDeskBundle\Entity\Category $category
     * @return Software
     */
    public function setCategory(\Ultra\HelpDeskBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Ultra\HelpDeskBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function __toString()
    {
       return $this->getSoftwareName();
    }
}