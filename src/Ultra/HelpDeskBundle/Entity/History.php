<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * History
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\HistoryRepository")
 */
class History
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="date")
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastUpdate", type="date")
     */
    private $lastUpdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="solvingDate", type="date")
     */
    private $solvingDate;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Issue", inversedBy="history")
     */
    private $issue;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return History
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     * @return History
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    
        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime 
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set solvingDate
     *
     * @param \DateTime $solvingDate
     * @return History
     */
    public function setSolvingDate($solvingDate)
    {
        $this->solvingDate = $solvingDate;
    
        return $this;
    }

    /**
     * Get solvingDate
     *
     * @return \DateTime 
     */
    public function getSolvingDate()
    {
        return $this->solvingDate;
    }

    /**
     * Set issue
     *
     * @param \Ultra\HelpDeskBundle\Entity\Issue $issue
     * @return History
     */
    public function setIssue(\Ultra\HelpDeskBundle\Entity\Issue $issue = null)
    {
        $this->issue = $issue;
    
        return $this;
    }

    /**
     * Get issue
     *
     * @return \Ultra\HelpDeskBundle\Entity\Issue 
     */
    public function getIssue()
    {
        return $this->issue;
    }
}