<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="categoryName", type="string", length=100)
     */
    private $categoryName;

    /**
     * @ORM\OneToMany(targetEntity="Ultra\HelpDeskBundle\Entity\Issue", mappedBy="category")
     **/
    private $catissues;

    /**
     * @ORM\OneToMany(targetEntity="Ultra\HelpDeskBundle\Entity\Software", mappedBy="category")
     **/
    private $software;

    /**
     * @ORM\OneToMany(targetEntity="Ultra\HelpDeskBundle\Entity\Hardware", mappedBy="category")
     **/
    private $hwCategory;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->catissues = new \Doctrine\Common\Collections\ArrayCollection();
        $this->software = new \Doctrine\Common\Collections\ArrayCollection();
        $this->hwCategory = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     * @return Category
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;
    
        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string 
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Add catissues
     *
     * @param \Ultra\HelpDeskBundle\Entity\Issue $catissues
     * @return Category
     */
    public function addCatissue(\Ultra\HelpDeskBundle\Entity\Issue $catissues)
    {
        $this->catissues[] = $catissues;
    
        return $this;
    }

    /**
     * Remove catissues
     *
     * @param \Ultra\HelpDeskBundle\Entity\Issue $catissues
     */
    public function removeCatissue(\Ultra\HelpDeskBundle\Entity\Issue $catissues)
    {
        $this->catissues->removeElement($catissues);
    }

    /**
     * Get catissues
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCatissues()
    {
        return $this->catissues;
    }

    /**
     * Add software
     *
     * @param \Ultra\HelpDeskBundle\Entity\Software $software
     * @return Category
     */
    public function addSoftware(\Ultra\HelpDeskBundle\Entity\Software $software)
    {
        $this->software[] = $software;
    
        return $this;
    }

    /**
     * Remove software
     *
     * @param \Ultra\HelpDeskBundle\Entity\Software $software
     */
    public function removeSoftware(\Ultra\HelpDeskBundle\Entity\Software $software)
    {
        $this->software->removeElement($software);
    }

    /**
     * Get software
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSoftware()
    {
        return $this->software;
    }

    /**
     * Add hwCategory
     *
     * @param \Ultra\HelpDeskBundle\Entity\Hardware $hwCategory
     * @return Category
     */
    public function addHwCategory(\Ultra\HelpDeskBundle\Entity\Hardware $hwCategory)
    {
        $this->hwCategory[] = $hwCategory;
    
        return $this;
    }

    /**
     * Remove hwCategory
     *
     * @param \Ultra\HelpDeskBundle\Entity\Hardware $hwCategory
     */
    public function removeHwCategory(\Ultra\HelpDeskBundle\Entity\Hardware $hwCategory)
    {
        $this->hwCategory->removeElement($hwCategory);
    }

    /**
     * Get hwCategory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHwCategory()
    {
        return $this->hwCategory;
    }

    /**
     * String representation
     *
     * @return string
     */
    public function __toString(){
        return $this->categoryName;
    }
}