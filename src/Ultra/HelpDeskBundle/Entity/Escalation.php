<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Escalation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\EscalationRepository")
 */
class Escalation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Level", type="string", length=100)
     */
    private $level;

    /**
     * @ORM\OneToMany(targetEntity="Ultra\HelpDeskBundle\Entity\Issue", mappedBy="escalation")
     **/
    private $issues;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param string $level
     * @return Escalation
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return string 
     */
    public function getLevel()
    {
        return $this->level;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isuess = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add isuess
     *
     * @param \Ultra\HelpDeskBundle\Entity\Issue $isuess
     * @return Escalation
     */
    public function addIssues(\Ultra\HelpDeskBundle\Entity\Issue $isuess)
    {
        $this->isuess[] = $isuess;
    
        return $this;
    }

    /**
     * Remove isuess
     *
     * @param \Ultra\HelpDeskBundle\Entity\Issue $isuess
     */
    public function removeIssues(\Ultra\HelpDeskBundle\Entity\Issue $isuess)
    {
        $this->isuess->removeElement($isuess);
    }

    /**
     * Get isuess
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIssues()
    {
        return $this->isuess;
    }


    /**
     * String Representation
     *
     * @return string
     */
    public function __toString(){
        return $this->getLevel();
    }

}