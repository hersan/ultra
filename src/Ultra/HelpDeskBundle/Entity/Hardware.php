<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hardware
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\HardwareRepository")
 */
class Hardware
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="HardwareName", type="string", length=100)
     */
    private $hardwareName;

    /**
     * @var string
     *
     * @ORM\Column(name="HardwareSerial", type="string", length=100)
     */
    private $hardwareSerial;

    /**
     * @var string
     *
     * @ORM\Column(name="HardwareType", type="string", length=100)
     */
    private $hardwareType;

    /**
     * @var string
     *
     * @ORM\Column(name="HardwareNeed", type="string", length=100)
     */
    private $hardwareNeed;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Category", inversedBy="hwCategory")
     **/
    private $category;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hardwareName
     *
     * @param string $hardwareName
     * @return Hardware
     */
    public function setHardwareName($hardwareName)
    {
        $this->hardwareName = $hardwareName;
    
        return $this;
    }

    /**
     * Get hardwareName
     *
     * @return string 
     */
    public function getHardwareName()
    {
        return $this->hardwareName;
    }

    /**
     * Set hardwareSerial
     *
     * @param string $hardwareSerial
     * @return Hardware
     */
    public function setHardwareSerial($hardwareSerial)
    {
        $this->hardwareSerial = $hardwareSerial;
    
        return $this;
    }

    /**
     * Get hardwareSerial
     *
     * @return string 
     */
    public function getHardwareSerial()
    {
        return $this->hardwareSerial;
    }

    /**
     * Set hardwareType
     *
     * @param string $hardwareType
     * @return Hardware
     */
    public function setHardwareType($hardwareType)
    {
        $this->hardwareType = $hardwareType;
    
        return $this;
    }

    /**
     * Get hardwareType
     *
     * @return string 
     */
    public function getHardwareType()
    {
        return $this->hardwareType;
    }

    /**
     * Set hardwareNeed
     *
     * @param string $hardwareNeed
     * @return Hardware
     */
    public function setHardwareNeed($hardwareNeed)
    {
        $this->hardwareNeed = $hardwareNeed;
    
        return $this;
    }

    /**
     * Get hardwareNeed
     *
     * @return string 
     */
    public function getHardwareNeed()
    {
        return $this->hardwareNeed;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add category
     *
     * @param \Ultra\HelpDeskBundle\Entity\Category $category
     * @return Hardware
     */
    public function addCategory(\Ultra\HelpDeskBundle\Entity\Category $category)
    {
        $this->category[] = $category;
    
        return $this;
    }

    /**
     * Remove category
     *
     * @param \Ultra\HelpDeskBundle\Entity\Category $category
     */
    public function removeCategory(\Ultra\HelpDeskBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function __toString()
    {
        return $this->getHardwareName()."-".$this->getHardwareType();
    }
}