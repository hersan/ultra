<?php

namespace Ultra\HelpDeskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Issue
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\HelpDeskBundle\Entity\IssueRepository")
 */
class Issue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Priority", type="string", length=16)
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="issueDate", type="date")
     */
    private $issueDate;

    /**
     * @var
     *
     * @ORM\OneToOne(targetEntity="Ultra\HelpDeskBundle\Entity\History", mappedBy="issue")
     */
    private $history;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Escalation", inversedBy="issues")
     **/
    private $escalation;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\HelpDeskBundle\Entity\Category", inversedBy="catissues")
     **/
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     **/
    private $cliente;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Issue
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    
        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Issue
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Issue
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set issueDate
     *
     * @param \DateTime $issueDate
     * @return Issue
     */
    public function setIssueDate($issueDate)
    {
        $this->issueDate = $issueDate;
    
        return $this;
    }

    /**
     * Get issueDate
     *
     * @return \DateTime 
     */
    public function getIssueDate()
    {
        return $this->issueDate;
    }

    /**
     * Set history
     *
     * @param \Ultra\HelpDeskBundle\Entity\History $history
     * @return Issue
     */
    public function setHistory(\Ultra\HelpDeskBundle\Entity\History $history = null)
    {
        $this->history = $history;
    
        return $this;
    }

    /**
     * Get history
     *
     * @return \Ultra\HelpDeskBundle\Entity\History 
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * Set escalation
     *
     * @param \Ultra\HelpDeskBundle\Entity\Escalation $escalation
     * @return Issue
     */
    public function setEscalation(\Ultra\HelpDeskBundle\Entity\Escalation $escalation = null)
    {
        $this->escalation = $escalation;
    
        return $this;
    }

    /**
     * Get escalation
     *
     * @return \Ultra\HelpDeskBundle\Entity\Escalation 
     */
    public function getEscalation()
    {
        return $this->escalation;
    }

    /**
     * Set category
     *
     * @param \Ultra\HelpDeskBundle\Entity\Category $category
     * @return Issue
     */
    public function setCategory(\Ultra\HelpDeskBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Ultra\HelpDeskBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set cliente
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $cliente
     * @return Issue
     */
    public function setCliente(\Ultra\ControlDocumentoBundle\Entity\Curricula $cliente = null)
    {
        $this->cliente = $cliente;
    
        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula 
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}