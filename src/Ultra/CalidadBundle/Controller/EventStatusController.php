<?php

namespace Ultra\CalidadBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\CalidadBundle\Entity\EventStatus;
use Ultra\CalidadBundle\Form\EventStatusType;

/**
 * EventStatus controller.
 *
 * @Route("/estatus")
 */
class EventStatusController extends Controller
{

    /**
     * Lists all EventStatus entities.
     *
     * @Route("/", name="estatus")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CalidadBundle:EventStatus')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new EventStatus entity.
     *
     * @Route("/", name="estatus_create")
     * @Method("POST")
     * @Template("CalidadBundle:EventStatus:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new EventStatus();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('estatus_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a EventStatus entity.
     *
     * @param EventStatus $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventStatus $entity)
    {
        $form = $this->createForm(new EventStatusType(), $entity, array(
            'action' => $this->generateUrl('estatus_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EventStatus entity.
     *
     * @Route("/new", name="estatus_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new EventStatus();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a EventStatus entity.
     *
     * @Route("/{id}", name="estatus_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:EventStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventStatus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing EventStatus entity.
     *
     * @Route("/{id}/edit", name="estatus_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:EventStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventStatus entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a EventStatus entity.
    *
    * @param EventStatus $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EventStatus $entity)
    {
        $form = $this->createForm(new EventStatusType(), $entity, array(
            'action' => $this->generateUrl('estatus_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EventStatus entity.
     *
     * @Route("/{id}", name="estatus_update")
     * @Method("PUT")
     * @Template("CalidadBundle:EventStatus:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:EventStatus')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventStatus entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('estatus_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a EventStatus entity.
     *
     * @Route("/{id}", name="estatus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CalidadBundle:EventStatus')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EventStatus entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('estatus'));
    }

    /**
     * Creates a form to delete a EventStatus entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('estatus_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
