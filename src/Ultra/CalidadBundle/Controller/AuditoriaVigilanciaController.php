<?php

namespace Ultra\CalidadBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\CalidadBundle\Entity\AuditoriaVigilancia;
use Ultra\CalidadBundle\Form\AuditoriaVigilanciaCalidadType;
use Ultra\CalidadBundle\Form\AuditoriaVigilanciaType;

/**
 * AuditoriaVigilancia controller.
 *
 * @Route("/calidad/auditoria")
 */
class AuditoriaVigilanciaController extends Controller
{

    /**
     * Lists all AuditoriaVigilancia entities.
     *
     * @Route("/{e}", name="calidad_auditoria")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($e)
    {
        //Auditorias
        $v = 'Auditoria';
        $em = $this->getDoctrine()->getManager();

        //Si el usuario es calidad consulta las vigentes
        //Si el usuario es  control de documentos consulta las cerradas
        //Si el usuario es tecnología consulta todas
        $user = $this->getUser();
        $query = null;

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery(
                "SELECT a, e, r
                  FROM CalidadBundle:AuditoriaVigilancia a
                  JOIN a.estado e
                  LEFT JOIN a.recommendations r
                  ");
        }


        if ($user->hasRole("ROLE_JEFE_CALIDAD")) {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery("
            SELECT a, e, r
                  FROM CalidadBundle:AuditoriaVigilancia a
                  JOIN a.estado e
                  LEFT JOIN a.recommendations r
                  WHERE e.id = :est and a.tipo= :v
             ")->setParameters(array('est' => $e, 'v' => $v));
        }

        if ($user->hasRole("ROLE_CONTROL_DOC") || $user->hasRole("ROLE_CAPT")) {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery(
                "SELECT a FROM CalidadBundle:AuditoriaVigilancia a
                  JOIN a.estado e
                  WHERE e.id = :est and a.tipo= :v
                ")->setParameters(array('est' => $e, 'v' => $v));

        }

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
            'estado' => $e
        );
    }

    /**
     * Lists all AuditoriaVigilancia entities.
     *
     * @Route("/vigilancias/{e}", name="calidad_vigilancia")
     * @Method("GET")
     * @Template("CalidadBundle:AuditoriaVigilancia:vigilancias.html.twig")
     */
    public function vigilanciasAction($e)
    {
        //Vigilancias
        $v = 'Vigilancia';

        $em = $this->getDoctrine()->getManager();

        //Si el usuario es calidad consulta las vigentes
        //Si el usuario es  control de documentos consulta las cerradas
        //Si el usuario es tecnología consulta todas
        $permiso = $this->getUser()->getRoles();
        $query = null;

        if ($permiso[0]->getRole() == "ROLE_SUPER_ADMIN") {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery(
                "SELECT a
                  FROM CalidadBundle:AuditoriaVigilancia a
                    JOIN a.estado e
                  WHERE e.id = :est and a.tipo= :v
                  ")->setParameters(array('est' => $e, 'v' => $v));
        }


        if ($permiso[0]->getRole() == "ROLE_JEFE_CALIDAD") {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery("
            SELECT a
                  FROM CalidadBundle:AuditoriaVigilancia a
                    JOIN a.estado e
                  WHERE e.id = :est and a.tipo= :v
             ")->setParameters(array('est' => $e, 'v' => $v));


        }

        if (($permiso[0]->getRole() == "ROLE_CONTROL_DOC") or ($permiso[0]->getRole() == "ROLE_CAPT")) {
            /** @var \Doctrine\ORM\Query $query */
            $query = $em->createQuery(
                "SELECT a FROM CalidadBundle:AuditoriaVigilancia a
                    JOIN a.estado e
                 WHERE e.id = :est and a.tipo= :v
                ")->setParameters(array('est' => $e, 'v' => $v));

        }

        $entities = $query->getResult();

        return array(
            'entities' => $entities,
            'estado' => $e
        );
    }

    /**
     * Creates a new AuditoriaVigilancia entity.
     *
     * @Route("/create", name="calidad_auditoria_create")
     * @Method("POST")
     * @Template("CalidadBundle:AuditoriaVigilancia:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new AuditoriaVigilancia();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La auditoría se creó con éxito con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('calidad_auditoria_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a AuditoriaVigilancia entity.
     *
     * @param AuditoriaVigilancia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(AuditoriaVigilancia $entity)
    {
        $permiso = $this->getUser()->getRoles();
        if ($permiso[0]->getRole() == "ROLE_JEFE_CALIDAD") {
            //ladybug_dump_die($permiso[0]->getRole());
            $form = $this->createForm(new AuditoriaVigilanciaCalidadType(), $entity, array(
                'action' => $this->generateUrl('calidad_auditoria_create'),
                'method' => 'POST',
            ));
        } else {
            $form = $this->createForm(new AuditoriaVigilanciaType(), $entity, array(
                'action' => $this->generateUrl('calidad_auditoria_create'),
                'method' => 'POST',
            ));
        }

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array(
                'class' => 'btn btn-success',
                'title' => 'Guardar datos'
            ),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new AuditoriaVigilancia entity.
     *
     * @Route("/new/auditoria", name="calidad_auditoria_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new AuditoriaVigilancia();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a AuditoriaVigilancia entity.
     *
     * @Route("/{id}/show/", name="calidad_auditoria_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:AuditoriaVigilancia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to edit an existing AuditoriaVigilancia entity.
     *
     * @Route("/{id}/edit", name="calidad_auditoria_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:AuditoriaVigilancia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos.');
        }

        $editForm = $this->createEditForm($entity);


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Creates a form to edit a AuditoriaVigilancia entity.
     *
     * @param AuditoriaVigilancia $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(AuditoriaVigilancia $entity)
    {
        $permiso = $this->getUser()->getRoles();
        if ($permiso[0]->getRole() == "ROLE_JEFE_CALIDAD") {
            $form = $this->createForm(new AuditoriaVigilanciaCalidadType(), $entity, array(
                'action' => $this->generateUrl('calidad_auditoria_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }
        if (($permiso[0]->getRole() == "ROLE_CONTROL_DOC") or ($permiso[0]->getRole() == "ROLE_SUPER_ADMIN") or ($permiso[0]->getRole() == "ROLE_CAPT")) {
            $form = $this->createForm(new AuditoriaVigilanciaType(), $entity, array(
                'action' => $this->generateUrl('calidad_auditoria_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }
        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr' => array(
                'class' => 'btn btn-success',
                'title' => 'Actualizar datos'
            ),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }

    /**
     * Edits an existing AuditoriaVigilancia entity.
     *
     * @Route("/{id}", name="calidad_auditoria_update")
     * @Method("PUT")
     * @Template("CalidadBundle:AuditoriaVigilancia:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var $entity \Ultra\CalidadBundle\Entity\AuditoriaVigilancia */
        $entity = $em->getRepository('CalidadBundle:AuditoriaVigilancia')->find($id);
        $condicion = $entity->getEstado();
        $path = $entity->getAbsolutePath();

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar los datos');
        }


        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $entity->move($path, $condicion);

            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos de la auditoría fueron modificados con éxito.'
            );

            return $this->redirect($this->generateUrl('calidad_auditoria_show', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),

        );
    }


}
