<?php

namespace Ultra\CalidadBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\CalidadBundle\Entity\Activity;
use Ultra\CalidadBundle\Entity\Recomendaciones;
use Ultra\CalidadBundle\Form\RecomendacionesType;
use Ultra\CalidadBundle\Form\RecomendacionesCalidadType;

/**
 * Recomendaciones controller.
 *
 * @Route("/calidad/recomendaciones")
 */
class RecomendacionesController extends Controller
{

    /**
     * Lists all Recomendaciones entities.
     *
     * @Route("/{e}", name="calidad_recomendaciones")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($e)
    {
        $em = $this->getDoctrine()->getManager();

        //Si el usuario es calidad consulta las vigentes
        //Si el usuario es  control de documentos consulta las cerradas
        //Si el usuario es tecnología consulta todas
        $permiso=$this->getUser()->getRoles();

        if ($permiso[0]->getRole()=="ROLE_SUPER_ADMIN" or $permiso[0]->getRole()=="ROLE_JEFE_CALIDAD")
        {
            $query = $em->createQuery("
            SELECT a
                  FROM CalidadBundle:Recomendaciones a
                  JOIN a.estado e
                  WHERE e.id = :est
             ")->setParameters( array('est'=>$e));
        }


        if (($permiso[0]->getRole()=="ROLE_CONTROL_DOC") or ($permiso[0]->getRole()=="ROLE_CAPT"))
        {
            $query = $em->createQuery(
                "SELECT a FROM CalidadBundle:Recomendaciones a
                  JOIN a.estado e
                  WHERE e.id = :est
                ")->setParameters( array('est'=>$e));

        }

        $entities = $query->getResult();


        return array(
            'entities' => $entities,
            'estado' => $e
        );
    }
    /**
     * Creates a new Recomendaciones entity.
     *
     * @Route("/", name="calidad_recomendaciones_create")
     * @Method("POST")
     * @Template("CalidadBundle:Recomendaciones:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Recomendaciones();
        $entity->addActivity(new Activity());

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                'success',
                'La recomendación se creó con éxito con los siguientes datos:'
            );

            return $this->redirect($this->generateUrl('calidad_recomendaciones_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Recomendaciones entity.
     *
     * @param Recomendaciones $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Recomendaciones $entity)
    {
        $permiso=$this->getUser()->getRoles();
        if ($permiso[0]->getRole()=="ROLE_JEFE_CALIDAD")
        {
            $form = $this->createForm(new RecomendacionesCalidadType(), $entity, array(
                'action' => $this->generateUrl('calidad_recomendaciones_create'),
                'method' => 'POST',
            ));
        }
        else
        {
            $form = $this->createForm(new RecomendacionesType(), $entity, array(
                'action' => $this->generateUrl('calidad_recomendaciones_create'),
                'method' => 'POST',
            ));
        }

        $form->add('submit', 'usubmit', array(
            'label' => ' Guardar',
            'attr' => array('class' => 'btn btn-success',
                'title'=>'Guardar datos'),
            'glyphicon' => 'glyphicon glyphicon-floppy-saved'
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Recomendaciones entity.
     *
     * @Route("/recom/new", name="calidad_recomendaciones_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Recomendaciones();
        $entity->addActivity(new Activity());

        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Recomendaciones entity.
     *
     * @Route("/{id}/show", name="calidad_recomendaciones_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:Recomendaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la recomendación.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Recomendaciones entity.
     *
     * @Route("/{id}/edit", name="calidad_recomendaciones_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:Recomendaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Imposible encontrar la recomendación.');
        }

        if (!$entity->hasActivities()) {
            $entity->addActivity(new Activity());
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Recomendaciones entity.
    *
    * @param Recomendaciones $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Recomendaciones $entity)
    {
        $permiso=$this->getUser()->getRoles();

        if ($permiso[0]->getRole()=="ROLE_JEFE_CALIDAD")
        {
            $form = $this->createForm(new RecomendacionesCalidadType(), $entity, array(
                'action' => $this->generateUrl('calidad_recomendaciones_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }
        else
        {
            $form = $this->createForm(new RecomendacionesType(), $entity, array(
                'action' => $this->generateUrl('calidad_recomendaciones_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            ));
        }

        $form->add('submit', 'usubmit', array(
            'label' => 'Actualizar',
            'attr'=>array('class'=>'btn btn-success',
                'title'=>'Actualizar datos'),
            'glyphicon' => 'glyphicon glyphicon-edit'
        ));

        return $form;
    }
    /**
     * Edits an existing Recomendaciones entity.
     *
     * @Route("/{id}", name="calidad_recomendaciones_update")
     * @Method("PUT")
     * @Template("CalidadBundle:Recomendaciones:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var Recomendaciones $entity
         */
        $entity = $em->getRepository('CalidadBundle:Recomendaciones')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Recomendaciones entity.');
        }

        $condicion = $entity->getEstado();
        $path = $entity->getAbsolutePath();

        /** @var \Ultra\CalidadBundle\Entity\Activity[] $currentActivities */
        $currentActivities = $entity->makeCurrentActivities();

        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $entity->move($path, $condicion);

            foreach($currentActivities as $activity)
            {
                if(false === $entity->contains($activity)){
                    $activity->setRecommendation(null);
                    $em->remove($activity);
                }
            }

            $em->persist($entity);

            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'La recomendación se modificó con éxito.'
            );

            return $this->redirect($this->generateUrl('calidad_recomendaciones_show', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),

        );
    }
    /**
     * Deletes a Recomendaciones entity.
     *
     * @Route("/{id}", name="calidad_recomendaciones_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CalidadBundle:Recomendaciones')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Recomendaciones entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('calidad_recomendaciones'));
    }

    /**
     * Creates a form to delete a Recomendaciones entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('calidad_recomendaciones_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
