<?php

namespace Ultra\CalidadBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Ultra\CalidadBundle\Entity\Agenda;
use Ultra\CalidadBundle\Form\AgendaType;
use Symfony\Component\HttpFoundation\Response;


/**
 * Agenda controller.
 *
 * @Route("/agenda")
 */
class AgendaController extends Controller
{

    /**
     * Lists all Agenda entities.
     *
     * @Route("/", name="agenda")
     * @Method("GET")
     * @Template("CalidadBundle:Agenda:index.html.twig")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine();

        $entities = $em->getRepository('CalidadBundle:Agenda')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Agenda entity.
     *
     * @Route("/", name="agenda_create")
     * @Method("POST")
     * @Template("CalidadBundle:Agenda:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Agenda();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos se agregaron con los siguientes datos:'
            );
            return $this->redirect($this->generateUrl('agenda_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Agenda entity.
     *
     * @param Agenda $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Agenda $entity)
    {
        $form = $this->createForm(new AgendaType(), $entity, array(
            'action' => $this->generateUrl('agenda_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Agenda entity.
     *
     * @Route("/new", name="agenda_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Agenda();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Agenda entity.
     *
     * @Route("/{id}", name="agenda_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar el programa.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Agenda entity.
     *
     * @Route("/{id}/edit", name="agenda_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se puede encontrar el programa.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Agenda entity.
    *
    * @param Agenda $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Agenda $entity)
    {
        $form = $this->createForm(new AgendaType(), $entity, array(
            'action' => $this->generateUrl('agenda_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Actualizar'));

        return $form;
    }
    /**
     * Edits an existing Agenda entity.
     *
     * @Route("/{id}", name="agenda_update")
     * @Method("PUT")
     * @Template("CalidadBundle:Agenda:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('No se te puede encontrar el programa.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $this->get('session')->getFlashBag()->add(
                'success',
                'Los datos fueron modificados con éxito.'
            );
            return $this->redirect($this->generateUrl('agenda_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Agenda entity.
     *
     * @Route("/{id}", name="agenda_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Imposible encontrar el programa.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('agenda'));
    }

    /**
     * Creates a form to delete a Agenda entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('agenda_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }


    /**
     * Lists all Agenda events.
     *
     * @param Request $request
     * @param $id
     *
     * @return array
     *
     * @Route("/program/{id}", name="agenda_program")
     * @Method("GET")
     * @Template()
     */
    public function programAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /**
         * @var \Ultra\CalidadBundle\Entity\Agenda $entity
         */
        $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

        if (!$entity){
            $this->createNotFoundException('No se encuentra el Programa especificado');
        }

        return array(
            'entity' => $entity
        );

    }


    //pdf del programa

    /**
     * Lists all Agenda events.
     *
     * @param Request $request
     * @param $id
     *
     * @return array
     *
     * @Route("/programpdf/{id}", name="agenda_programpdf")
     * @Method("GET")
     * @Template("CalidadBundle:Agenda:programpdf.html.twig")
     */
    public function programpdfAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('CalidadBundle:Agenda')->find($id);

        if (!$entity){
            $this->createNotFoundException('No se encuentra el Programa especificado');
        }
        else
        {

        $html = $this->renderView('CalidadBundle:Agenda:programpdf.html.twig',
            array(
                'entities' => $entity));

        $response = new Response (
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html,
                array('lowquality' => false,
                    'print-media-type' => true,
                    'encoding' => 'utf-8',
                    'page-size' => 'Letter',
                    'outline-depth' => 8,
                    'orientation' => 'Landscape',
                    'title'=> 'Programa Anual',
                    'user-style-sheet'=> 'css/bootstrap.css',
                    'header-right'=>'Pag. [page] de [toPage]',
                    'header-font-size'=>7,
                )),
            200,
            array(
                'Content-Type'          => '/home/aloyo/public_html/Ultra/web/pdf',
                'Content-Disposition'   => 'attachment; filename="programapdf.pdf"',
            )
        );

        return $response;


    }}
}
