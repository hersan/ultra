<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 4/03/15
 * Time: 03:43 PM
 */

namespace Ultra\CalidadBundle\EventListener;


use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Ultra\CalidadBundle\Entity\AuditoriaVigilancia;

class AuditoriaVigilanciaListener {

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach($uow->getScheduledEntityUpdates() as $entity)
        {
            if(!$entity instanceof AuditoriaVigilancia)
            {
                continue;
            }

            /*$change = $uow->getEntityChangeSet($entity);
            $original = $uow->getOriginalEntityData($entity);

            $hash = $entity->getHash();

            $uow->computeChangeSets();*/

        }
    }

    public function postFlush(PostFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if(!$entity instanceof AuditoriaVigilancia)
            {
                continue;
            }

            /*$change = $uow->getEntityChangeSet($entity);
            $original = $uow->getOriginalEntityData($entity);

            $hash = $entity->getHash();

            $uow->computeChangeSets();*/
        }

    }

}