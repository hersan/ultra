<?php
/**
 * Created by PhpStorm.
 * User: hheredia
 * Date: 2/09/15
 * Time: 11:24 AM
 */

namespace Ultra\CalidadBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ReminderForInitiatingVigilanceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('ultra:vigilance:reminder')
            ->getDescription('Send email when a vigilance is come to start');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        /**
         * @var \Doctrine\ORM\Query $query
         */
        $query = $em->createQuery("
            SELECT ec FROM CalidadBundle:EventCalendar ec
              JOIN ec.status s
              JOIN ec.type t
            WHERE CURRENT_DATE() = DATE_SUB(ec.begin,7, 'day')
              AND s.name = 'PROGRAMADA'
              AND t.id = 10
        ");

        $recommendations = $query->getResult();

        if (count($recommendations))
        {
            $message = \Swift_Message::newInstance()
                ->setSubject('Acciones Pendientes')
                ->setFrom(array('sad.alert@ultraingenieria.com.mx' => 'Sistema de Administración de Documentos'))
                ->setTo('jcontreras@ultraingenieria.com.mx')
                ->setBody(
                    $this->getContainer()->get('templating')->render(
                        'CalidadBundle:Email:reminder_vigilance.html.twig',
                        array('name' => '')
                    ),
                    'text/html'
                )
            ;

            $this->getContainer()->get('mailer')->send($message);
        }


    }
}