<?php
/**
 * Proyect: Ultra.
 * Archivo: PendingActivitiesDueCommand.php.
 * @author: Herminio Heredia <herminio.heredia@hotmail.com>
 */

namespace Ultra\CalidadBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class PendingActivitiesDueCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('ultra:activity:reminder')
            ->getDescription('Send email when recommendation activities reach their limit time');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var \Doctrine\ORM\Query $query */
        $query = $em->createQuery("
            SELECT r, ac FROM CalidadBundle:Recomendaciones r
            JOIN r.activities ac
            WHERE CURRENT_DATE() = DATE_SUB(ac.limitDate,2, 'day') AND ac.complete = FALSE
        ");

        /**
         * @var \Ultra\CalidadBundle\Entity\Recomendaciones $recommendation
         */
        $recommendations = $query->getResult();

        if (count($recommendations))
        {
            $message = \Swift_Message::newInstance()
                ->setSubject('Acciones Pendientes')
                ->setFrom(array('sad.alert@ultraingenieria.com.mx' => 'Sistema de Administración de Documentos'))
                ->setTo('jcontreras@ultraingenieria.com.mx')
                //->addBcc('')
                //->addBcc('')
                ->setBody(
                    $this->getContainer()->get('templating')->render(
                        'CalidadBundle:Email:notificar.html.twig',
                        array('recommendations' => $recommendations)
                    ),
                    'text/html'
                )
            ;

            $this->getContainer()->get('mailer')->send($message);
        }


    }
}