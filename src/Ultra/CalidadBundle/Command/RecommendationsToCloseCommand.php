<?php

namespace Ultra\CalidadBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RecommendationsToCloseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('ultra:recommendation:close')
            ->getDescription('Send email when recommendations turn close');
    }

    protected function execute(InputInterface $inputInterface, OutputInterface $outputInterface)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var \Doctrine\ORM\Query $query */
        $query = $em->createQuery("
            SELECT r FROM CalidadBundle:Recomendaciones r
            WHERE CURRENT_DATE() = DATE_SUB(r.fechaCierre,1, 'day')
        ");

        $recommendations = $query->getResult();

        if (count($recommendations))
        {
            $message = \Swift_Message::newInstance()
                ->setSubject('Acciones Pendientes')
                ->setFrom(array('sad.alert@ultraingenieria.com.mx' => 'Sistema de Administración de Documentos'))
                ->setTo('jcontreras@ultraingenieria.com.mx')
                ->addBcc('eacosta@ultraingenieria.com.mx')
                ->setBody(
                    $this->getContainer()->get('templating')->render(
                        'CalidadBundle:Email:notificar.html.twig',
                        array('name' => '')
                    ),
                    'text/html'
                )
            ;

            $this->getContainer()->get('mailer')->send($message);
        }


    }
}