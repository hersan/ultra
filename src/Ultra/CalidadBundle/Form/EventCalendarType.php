<?php

namespace Ultra\CalidadBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventCalendarType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agenda','entity',array(
                'class' => 'CalidadBundle:Agenda',
                'label' => 'Selecciona un programa',
                'empty_value' => 'Ninguna ...',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('status','entity',array(
                'class' => 'CalidadBundle:EventStatus',
                'label' => 'Selecciona un estado',
                'empty_value' => 'Ninguno ...',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                )
            ))
            ->add('code','text', array(
                'label' => 'No.',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('auditor','entity', array(
                'label' => 'Auditor líder',
                'class' => 'ControlDocumentoBundle:Curricula',
                'empty_value' => 'Selecciona uno..',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 6')
                        ;
                },
            ))
            ->add('audit','textarea', array(
                'label' => 'Auditoria / Vigilancia',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('document','textarea', array(
                'label' => 'Documentos Aplicados',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('begin','date', array(
                'input' => 'datetime',
                'label' => 'Inicia',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('end','date', array(
                'input' => 'datetime',
                'label' => 'Termina',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('type','entity',array(
                'class' => 'ControlDocumentoBundle:TipoDocumento',
                'label' => 'Tipo',
                'empty_value' => 'Seleccione uno',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker'
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->where('d.id = 9')->orWhere('d.id = 10')
                        ;
                },
            ))
            ->add('comments','textarea', array(
                'label' => 'Comentarios a la revisión',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\CalidadBundle\Entity\EventCalendar',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_event_calendar';
    }
}
