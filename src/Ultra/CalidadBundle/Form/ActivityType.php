<?php

namespace Ultra\CalidadBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActivityType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Actividad',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'div_column' => 'col-sm-6'
            ))
            ->add('complete', 'checkbox', array(
                'label' => '¿Completada?',
                'value' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
                'div_column' => 'col-sm-2'
            ))
            ->add('limitDate', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha límite',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control datepicker'),
                'format' => 'yyyy-MM-dd',
                'div_column' => 'col-sm-2'
            ))
            //->add('recommendation')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\CalidadBundle\Entity\Activity',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_calidadbundle_activity';
    }
}
