<?php

namespace Ultra\CalidadBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Ultra\CalidadBundle\Entity\AuditoriaVigilancia;

class AuditoriaVigilanciaCalidadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clave','text',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Clave',
                    'autocomplete'=>'on',
                    'maxlength'=>'50',
                    'title'=>'Ingresa la clave',
                    'autofocus'=>true),
                'label' => 'Clave',
            ))
            ->add('area','choice', array(
                'label' => 'Área',
                'empty_value' => 'Selecciona un área',
                'empty_data' => '',
                'multiple' => true,
                'choices' => array(
                    'Control de Documentos y Registros' =>'Control de Documentos y Registros',
                    'Facturación, Cobranza y Compras.' => 'Facturación, Cobranza y Compras.',
                    'Servicios/Transportes.' => 'Servicios/Transportes.',
                    'Recursos Humanos.' => 'Recursos Humanos.',
                    'Rh- Nómina' => 'Rh- Nómina',
                    'Rh- Entrenamiento y Calificación de personal' => 'Rh- Entrenamiento y Calificación de personal',
                    'Licitaciones.' => 'Licitaciones.',
                    'Administración.' => 'Administración.',
                    'Control de Proyectos.' => 'Control de Proyectos.',
                    'Jefatura de Proyectos.' => 'Jefatura de Proyectos.',
                    'Garantía de Calidad.' => 'Garantía de Calidad.',
                    'Infraestructura General' =>'Infraestructura General',
                    'Sistemas Computacionales.' => 'Sistemas Computacionales.',
                    'Ingeniería. Proyecto 3681 (CFE)' =>'Ingeniería. Proyecto 3681 (CFE)',
                    'Ingeniería. Proyecto 3681A (CFE)' =>'Ingeniería. Proyecto 3681A (CFE)',
                    'Ingeniería. Proyecto 3673 (CFE)' =>'Ingeniería. Proyecto 3673 (CFE)',
                    'Ingeniería. Proyecto 3666 (PEMEX)' =>'Ingeniería. Proyecto 3666 (PEMEX)',
                    'Ingeniería. Proyecto 3649 (CFE)' =>'Ingeniería. Proyecto 3649 (CFE)',
                    'Ingeniería. Proyecto 3697 (CFE)' =>'Ingeniería. Proyecto 3697 (CFE)',
                    'Ingeniería. Mecánica.' => 'Ingeniería.Mecánica.',
                    'Ingeniería. Tuberias.' => 'Ingeniería Tuberias.',
                    'Ingeniería. Análisis de Ingeniería.' => 'Ingeniería.Análisis de Ingeniería.',
                    'Ingeniería. HVAC/FP.' => 'Ingeniería. HVAC/FP.',
                    'Ingeniería. IPC.' => 'Ingeniería. IPC.',
                    'Ingeniería. Coordinación Técnica.' => 'Ingeniería. Coordinación Técnica.',
                    'Ingeniería. Proyecto 3699 (PEMEX)' => 'Ingeniería. Proyecto 3699 (PEMEX)',
                    'Ingeniería. Proyecto 3678' => 'Ingeniería. Proyecto 3678',
                    'ISO 9001-2008. Ejecución y Medición'=>'ISO 9001-2008. Ejecución y Medición',
                    'SSPA' => 'SSPA',
                    'MEGC'=>'MEGC',
                    'Certificación' => 'Certificación',
                    'Todas' => 'Todas'
                ),
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                ),
                'error_bubbling' => false,
            ))
            ->add('descripcion','textarea',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Descripción',
                    'autocomplete'=>'on',
                    'maxlength'=>'255',
                    'title'=>'Ingresa la descripción'
                ),
                'label' => 'Descripción',
            ))
            ->add('tipo','choice',array(
                'label' => 'Tipo',
                'empty_value' => '-- Selecciona --',
                'empty_data' => '',
                'attr' => array('class' => 'form-control selectpicker input-sm'),
                'choices' => array(
                    'Vigilancia' => 'Vigilancia',
                    'Auditoría' => 'Auditoría'
                ),
            ))
            ->add('estado','entity',array(
                'class' => 'ControlDocumentoBundle:Condicion',
                'label' => 'Estado',
                'empty_data' => null,
                'empty_value' => 'Selecione el estado',
                'attr' => array('class' => 'form-control selectpicker input-sm'),
            ))
            ->add('fechaApertura', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha de apertura',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'format' => 'yyyy-MM-dd',
            ))
            ->add('fechaCierre', 'date', array(
                'input' => 'datetime',
                'label' => 'Fecha de cierre',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'format' => 'yyyy-MM-dd',
            ))
            ->add('file', 'file', array(
                'label' => 'Archivo',
            ))

            ->add('originators','entity', array(
                'label' => 'Originador(es)',
                'class' => 'ControlDocumentoBundle:Curricula',
                'empty_value' => 'Selecciona...',
                'empty_data' => null,
                'multiple' => true,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'data-live-search' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->leftJoin('c.user', 'User')
                            ->leftJoin('User.roles', 'Roles')
                            ->where('Roles.id = 6')
                            ;
                    },
            ))

            ->add('externalOriginators','textarea',array(
                'attr' => array('class' => 'form-control input-sm',
                    'placeholder'=> 'Originador externo',
                    'autocomplete'=>'on',
                    'maxlength'=>'255',
                    'title'=>'Originador externo'
                ),
                'label' => 'Originador Externo (Cuando aplique)',
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, array($this, 'onPreSetDataEvent'));
    }

    public function onPreSetDataEvent(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($data instanceof AuditoriaVigilancia) {
            $this->addElement($data, $form);
        }
    }

    public function addElement(AuditoriaVigilancia $vigilancia, FormInterface $form)
    {
        $form->add('type', 'choice', array(
            'label' => 'Tipo',
            'empty_value' => '-- Ninguno --',
            'empty_data' => null,
            'attr' => array(
                'class' => 'form-control selectpicker'
            ),
            'choices' => $vigilancia->getLocations()
        ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\CalidadBundle\Entity\AuditoriaVigilancia',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_calidadbundle_auditoriavigilanciacalidad';
    }
}
