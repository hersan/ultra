<?php

namespace Ultra\CalidadBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgendaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('revision','number', array(
                'label' => 'Revisión',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('name','text',array(
                'label' => 'Nombre del Programa',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))

            ->add('projects','textarea',array(
                'label' => 'Proyectos',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('created','date',array(
                'input' => 'datetime',
                'label' => 'Fecha de creación',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('begin','date',array(
                'input' => 'datetime',
                'label' => 'Inicia periodo',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))
            ->add('end','date',array(
                'input' => 'datetime',
                'label' => 'Termina periodo',
                'widget' => 'single_text',
                'attr' => array('class'=>'form-control'),
                'required' => false,
                'format' => 'yyyy-MM-dd',
            ))

            ->add('comments','textarea',array(
                'label' => 'Registro de revisiones',
                'attr' => array(
                    'class' => 'form-control',
                )
            ))
            ->add('createdBy','entity',array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Creado por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 6')
                        ;
                },
            ))
            ->add('approvedBy','entity',array(
                'class' => 'Ultra\ControlDocumentoBundle\Entity\Curricula',
                'label' => 'Aprobado por',
                'empty_value' => '-- Ninguno --',
                'empty_data' => null,
                'attr' => array(
                    'class' => 'form-control selectpicker',
                    'search-data-live' => true,
                ),
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->leftJoin('c.user', 'User')
                        ->leftJoin('User.roles', 'Roles')
                        ->where('Roles.id = 6')
                        ;
                },
            ))
            ->add('file', 'file', array(
                'label' => 'Agregar Archivo'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ultra\CalidadBundle\Entity\Agenda',
            'required' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ultra_calidadbundle_agenda';
    }
}
