<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventCalendar
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\EventCalendarRepository")
 */
class EventCalendar
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="audit", type="string", length=255)
     */
    private $audit;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $auditor;

    /**
     * @var string
     *
     * @ORM\Column(name="document", type="string", length=255)
     */
    private $document;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\TipoDocumento
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\TipoDocumento")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="date")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date")
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", nullable=true)
     */
    private $comments;

    /**
     * @var \Ultra\CalidadBundle\Entity\Agenda
     *
     * @ORM\ManyToOne(targetEntity="Ultra\CalidadBundle\Entity\Agenda", inversedBy="events")
     */
    private $agenda;

    /**
     * @var \Ultra\CalidadBundle\Entity\EventStatus
     *
     * @ORM\ManyToOne(targetEntity="Ultra\CalidadBundle\Entity\EventStatus")
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return EventCalendar
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get audit
     *
     * @return string
     */
    public function getAudit()
    {
        return $this->audit;
    }

    /**
     * Set audit
     *
     * @param string $audit
     * @return EventCalendar
     */
    public function setAudit($audit)
    {
        $this->audit = $audit;

        return $this;
    }

    /**
     * Get auditor
     *
     * @return string
     */
    public function getAuditor()
    {
        return $this->auditor;
    }

    /**
     * Set auditor
     *
     * @param string $auditor
     * @return EventCalendar
     */
    public function setAuditor($auditor)
    {
        $this->auditor = $auditor;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set document
     *
     * @param string $document
     * @return EventCalendar
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * Set begin
     *
     * @param \DateTime $begin
     * @return EventCalendar
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return EventCalendar
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\TipoDocumento
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\TipoDocumento $type
     * @return EventCalendar
     */
    public function setType(\Ultra\ControlDocumentoBundle\Entity\TipoDocumento $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return EventCalendar
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get agenda
     *
     * @return \Ultra\CalidadBundle\Entity\Agenda
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * Set agenda
     *
     * @param \Ultra\CalidadBundle\Entity\Agenda $agenda
     * @return EventCalendar
     */
    public function setAgenda(\Ultra\CalidadBundle\Entity\Agenda $agenda = null)
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Ultra\CalidadBundle\Entity\EventStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param \Ultra\CalidadBundle\Entity\EventStatus $status
     * @return EventCalendar
     */
    public function setStatus(\Ultra\CalidadBundle\Entity\EventStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }
}