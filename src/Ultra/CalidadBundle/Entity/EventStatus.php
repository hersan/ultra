<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventStatus
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\EventStatusRepository")
 */
class EventStatus
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get string representation
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return EventStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}