<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Ultra\ControlDocumentoBundle\Entity\Condicion;

/**
 * AuditoriaVigilancia
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\AuditoriaVigilanciaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AuditoriaVigilancia
{
    private $locations = array(
        0 => 'Interna',
        1 => 'Externa',
     );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="clave", type="string", length=50)
     */
    private $clave;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="area", type="array")
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="tipo", type="string", length=50)
     */
    private $tipo;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Condicion
     *
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Condicion")
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaTransmision", type="date", nullable=true)
     */
    private $fechaTransmision;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaApertura", type="date", nullable=true)
     */
    private $fechaApertura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCierre", type="date"), nullable=true
     */
    private $fechaCierre;

    /**
     * @var integer
     *
     * @Assert\Type(type="numeric")
     * @ORM\Column(name="conteo", type="integer", nullable=true)
     */
    private $conteo;

    /**
     * @var string
     *
     * @ORM\Column(name="localizacion", type="string", length=20, nullable=true)
     */
    private $localizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=9, nullable=true)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $creado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $actualizado;

    /**
     * @var
     *
     * @Assert\NotNull()
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalOriginators;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="object")
     * @ORM\ManyToMany(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula", inversedBy="auditorias")
     */
    private $originators;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\CalidadBundle\Entity\Recomendaciones", mappedBy="referencia", cascade={"persist"})
     */
    private $recommendations;

    private $file;

    private $path;

    private $temp;

    public function __construct()
    {
        $this->originators = new \Doctrine\Common\Collections\ArrayCollection();
        $this->recommendations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return AuditoriaVigilancia
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set area
     *
     * @param integer $area
     * @return AuditoriaVigilancia
     */
    public function setArea ($area)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return integer 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return AuditoriaVigilancia
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return AuditoriaVigilancia
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set estado
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Condicion $estado
     * @return AuditoriaVigilancia
     */
    public function setEstado(\Ultra\ControlDocumentoBundle\Entity\Condicion $estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Condicion
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaTransmision
     *
     * @param \DateTime $fechaTransmision
     * @return AuditoriaVigilancia
     */
    public function setFechaTransmision($fechaTransmision)
    {
        $this->fechaTransmision = $fechaTransmision;
    
        return $this;
    }


    /**
     * Get fechaTransmision
     *
     * @return \DateTime
     */
    public function getFechaTransmision()
    {
        return $this->fechaTransmision;
    }

    /**
     * Get fechaApertura
     *
     * @return \DateTime
     */
    public function getFechaApertura()
    {
        return $this->fechaApertura;
    }

    /**
     * Set fechaApertura
     *
     * @param \DateTime $fechaApertura
     * @return AuditoriaVigilancia
     */
    public function setFechaApertura($fechaApertura)
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    /**
     * Get fechaCierre
     *
     * @return \DateTime
     */
    public function getFechaCierre()
    {
        return $this->fechaCierre;
    }

    /**
     * Set fechaCierre
     *
     * @param \DateTime $fechaCierre
     * @return AuditoriaVigilancia
     */
    public function setFechaCierre($fechaCierre)
    {
        $this->fechaCierre = $fechaCierre;

        return $this;
    }

    /**
     * Set conteo
     *
     * @param integer $conteo
     * @return AuditoriaVigilancia
     */
    public function setConteo($conteo)
    {
        $this->conteo = $conteo;
    
        return $this;
    }

    /**
     * Get conteo
     *
     * @return integer 
     */
    public function getConteo()
    {
        return $this->conteo;
    }

    /**
     * Set localizacion
     *
     * @param string $localizacion
     * @return AuditoriaVigilancia
     */
    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;
    
        return $this;
    }

    /**
     * Get localizacion
     *
     * @return string 
     */
    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return AuditoriaVigilancia
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    
        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return AuditoriaVigilancia
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set originador
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $originador
     * @return AuditoriaVigilancia
     */
    public function addOriginator(\Ultra\ControlDocumentoBundle\Entity\Curricula $originador)
    {
        $this->originators[] = $originador;

        return $this;
    }

    /**
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $originador
     * @return AuditoriaVigilancia
     */
    public function removeOriginator(\Ultra\ControlDocumentoBundle\Entity\Curricula $originador)
    {
        $this->originators->removeElement($originador);

        return $this;
    }

    /**
     * Get originador
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getOriginators()
    {
        return $this->originators;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (is_file($this->getAbsolutePath())) {
            $this->temp = $this->getAbsolutePath();
            //$this->path = null;
        } else {
            $this->pdf = 'initial';
        }
    }

    /**
     * Get file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            //[clave]-r[revision].pdf
            $name = sprintf(
                '%s.%s',
                $this->getClave(),
                $this->getFile()->guessExtension()
            );
            $this->setPdf($name);
            $this->setHash();
        }
        $this->creado = new \DateTime();

    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {
            //[clave]-r[revision].pdf
            $name = sprintf(
                '%s.%s',
                $this->getClave(),
                $this->getFile()->guessExtension()
            );

            $this->setPdf($name);
            $this->setHash();
            $this->actualizado = new \DateTime();
        }

        if(null != $this->getTemporaryPath()){
            $f = new Filesystem();
            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),true);
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {

        if(null == $this->getFile())
        {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->temp = null;
        $this->file = null;
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadRootDir().'/'.$this->pdf;
    }

    public function getUploadRootDir()
    {
        return '/var/sad/'.$this->getUploadDir().'/'.$this->estado;
    }

    public function getUploadDir()
    {
        return 'uploads/calidad/'.$this->tipo;
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    public function getTemporaryPath(){
        return $this->path;
    }

    public function setTemp($path)
    {
        $this->temp = $path;
    }

    public function getTemp(){
        return $this->temp;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return AuditoriaVigilancia
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * Set actualizado
     *
     * @param \DateTime $actualizado
     * @return AuditoriaVigilancia
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;

        return $this;
    }

    /**
     * Get actualizado
     *
     * @return \DateTime
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    public function move($path, Condicion $estado)
    {
        if($this->getEstado()->getId() !== $estado->getId())
        {
            $fs = new Filesystem();

            if($fs->exists($path))
            {
                try{
                    $fs->copy($path, $this->getAbsolutePath(), true);
                    //$fs->remove($path);
                }
                catch (FileNotFoundException $e){
                    return $e->getTrace();
                }
                catch (IOException $e){
                    return $e->getTrace();
                }
            }
            $fs = null;
        }

    }

    public function __toString()
    {
        return $this->getClave();
    }

    /**
     * Set hash
     * @return AuditoriaVigilancia
     * @internal param string $hash
     */
    public function setHash()
    {
        $fileName = $this->getFile()->getRealPath();
        $this->hash = sha1_file($fileName);
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Add recomendation
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recomendation
     * @return AuditoriaVigilancia
     */
    public function addRecomendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recomendation)
    {
        $this->recommendations[] = $recomendation;
    
        return $this;
    }

    /**
     * Remove recomendation
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recomendation
     */
    public function removeRecomendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recomendation)
    {
        $this->recommendations->removeElement($recomendation);
    }

    /**
     * Get recomendation
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecomendation()
    {
        return $this->recommendations;
    }

    public function hasPending()
    {
        $collection = $this->recommendations->filter(function(Recomendaciones $entity){
            return $entity->getEstado()->getId() == 1;
        });

        if ($collection->isEmpty()) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return AuditoriaVigilancia
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }



    /**
     * Add recommendations
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recommendations
     * @return AuditoriaVigilancia
     */
    public function addRecommendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recommendations)
    {
        $this->recommendations[] = $recommendations;
    
        return $this;
    }

    /**
     * Remove recommendations
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recommendations
     */
    public function removeRecommendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recommendations)
    {
        $this->recommendations->removeElement($recommendations);
    }

    /**
     * Get recommendations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * Set externalOriginators
     *
     * @param string $externalOriginators
     * @return AuditoriaVigilancia
     */
    public function setExternalOriginators($externalOriginators)
    {
        $this->externalOriginators = $externalOriginators;

        return $this;
    }

    /**
     * Get externalOriginators
     *
     * @return string 
     */
    public function getExternalOriginators()
    {
        return $this->externalOriginators;
    }
}
