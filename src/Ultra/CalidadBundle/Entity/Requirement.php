<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requirement
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\RequirementRepository")
 */
class Requirement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="Ultra\CalidadBundle\Entity\Recomendaciones", mappedBy="requirements")
     */
    private $recommendations;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Requirement
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Requirement
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recommendations = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add recommendations
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recommendations
     * @return Requirement
     */
    public function addRecommendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recommendations)
    {
        $this->recommendations[] = $recommendations;
    
        return $this;
    }

    /**
     * Remove recommendations
     *
     * @param \Ultra\CalidadBundle\Entity\Recomendaciones $recommendations
     */
    public function removeRecommendation(\Ultra\CalidadBundle\Entity\Recomendaciones $recommendations)
    {
        $this->recommendations->removeElement($recommendations);
    }

    /**
     * Get recommendations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecommendations()
    {
        return $this->recommendations;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}