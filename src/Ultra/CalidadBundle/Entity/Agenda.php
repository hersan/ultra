<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ultra\ControlDocumentoBundle\Model\Calidad\CalidadUploadableTrait;

/**
 * Agenda
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\AgendaRepository")
 */
class Agenda
{
    use CalidadUploadableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="revision", type="smallint")
     */
    private $revision;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="projects", type="string")
     */
    private $projects;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="date")
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date")
     */
    private $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string")
     */
    private $comments;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $createdBy;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Curricula
     *
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Curricula")
     */
    private $approvedBy;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\CalidadBundle\Entity\EventCalendar", mappedBy="agenda", cascade={"persist"})
     */
    private $events;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * Set begin
     *
     * @param \DateTime $begin
     * @return Agenda
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Agenda
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Add events
     *
     * @param \Ultra\CalidadBundle\Entity\EventCalendar $events
     * @return Agenda
     */
    public function addEvent(\Ultra\CalidadBundle\Entity\EventCalendar $events)
    {
        $this->events[] = $events;

        return $this;
    }

    /**
     * Remove events
     *
     * @param \Ultra\CalidadBundle\Entity\EventCalendar $events
     */
    public function removeEvent(\Ultra\CalidadBundle\Entity\EventCalendar $events)
    {
        $this->events->removeElement($events);
    }
    
    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Get representational string of Agenda
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return sprintf('%s - rev %d', $this->name, $this->revision);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Agenda
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get projects
     *
     * @return string
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set projects
     *
     * @param string $projects
     * @return Agenda
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Agenda
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Agenda
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get revision
     *
     * @return integer
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * Set revision
     *
     * @param integer $revision
     * @return Agenda
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $createdBy
     * @return Agenda
     */
    public function setCreatedBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Curricula
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set approvedBy
     *
     * @param \Ultra\ControlDocumentoBundle\Entity\Curricula $approvedBy
     * @return Agenda
     */
    public function setApprovedBy(\Ultra\ControlDocumentoBundle\Entity\Curricula $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }
}