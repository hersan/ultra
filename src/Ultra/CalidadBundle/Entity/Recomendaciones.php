<?php

namespace Ultra\CalidadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Ultra\ControlDocumentoBundle\Entity\Condicion;

/**
 * Recomendaciones
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Ultra\CalidadBundle\Entity\RecomendacionesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Recomendaciones
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="clave", type="string", length=255)
     */
    private $clave;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @ORM\Column(name="area", type="array")
     */
    private $area;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaTransmision", type="date", nullable=true)
     */
    private $fechaTransmision;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaApertura", type="date", nullable=true)
     */
    private $fechaApertura;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaCierre", type="date", nullable=true)
     */
    private $fechaCierre;

    /**
     * @var integer
     *
     * Assert\NotBlank()
     * Assert\Type(type="numeric")
     * @ORM\Column(name="conteo", type="integer", nullable=true)
     */
    private $conteo;

    /**
     * @var string
     *
     * Assert\NotBlank()
     * @ORM\Column(name="localizacion", type="string", length=20, nullable=true)
     */
    private $localizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=255, nullable=true)
     */
    private $codigo;

    /**
     * @var \Ultra\ControlDocumentoBundle\Entity\Condicion $estado
     *
     * Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Ultra\ControlDocumentoBundle\Entity\Condicion")
     */
    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", length=255, nullable=true)
     */
    private $pdf;

    /**
     * @var
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $hash;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Ultra\CalidadBundle\Entity\AuditoriaVigilancia", inversedBy="recommendations")
     */
    private $referencia;

    /**
     * @var
     *
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @ORM\ManyToMany(targetEntity="Ultra\CalidadBundle\Entity\Requirement", inversedBy="recommendations")
     */
    private $requirements;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Ultra\CalidadBundle\Entity\Activity", mappedBy="recommendation", cascade={"persist"})
     */
    private $activities;

    /**
     * @var
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $creado;

    /**
     * @var
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $actualizado;

    /**
     * @var
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $actionPlan;

    private $file;

    private $path;

    private $temp;

    /**
     * @var
     */
    private $currentActivities;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clave
     *
     * @param string $clave
     * @return Recomendaciones
     */
    public function setClave($clave)
    {
        $this->clave = $clave;
    
        return $this;
    }

    /**
     * Get clave
     *
     * @return string 
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Set area
     *
     * @param integer $area
     * @return Recomendaciones
     */
    public function setArea($area)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return integer 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Recomendaciones
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaTransmision
     *
     * @param \DateTime $fechaTransmision
     * @return Recomendaciones
     */
    public function setFechaTransmision($fechaTransmision)
    {
        $this->fechaTransmision = $fechaTransmision;
    
        return $this;
    }

    /**
     * Get fechaTransmision
     *
     * @return \DateTime 
     */
    public function getFechaTransmision()
    {
        return $this->fechaTransmision;
    }

    /**
     * Get fechaApertura
     *
     * @return \DateTime
     */
    public function getFechaApertura()
    {
        return $this->fechaApertura;
    }

    /**
     * Set fechaApertura
     *
     * @param \DateTime $fechaApertura
     * @return AuditoriaVigilancia
     */
    public function setFechaApertura($fechaApertura)
    {
        $this->fechaApertura = $fechaApertura;

        return $this;
    }

    /**
     * Get fechaCierre
     *
     * @return \DateTime
     */
    public function getFechaCierre()
    {
        return $this->fechaCierre;
    }

    /**
     * Set fechaCierre
     *
     * @param \DateTime $fechaCierre
     * @return AuditoriaVigilancia
     */
    public function setFechaCierre($fechaCierre)
    {
        $this->fechaCierre = $fechaCierre;

        return $this;
    }

    /**
     * Set conteo
     *
     * @param integer $conteo
     * @return Recomendaciones
     */
    public function setConteo($conteo)
    {
        $this->conteo = $conteo;
    
        return $this;
    }

    /**
     * Get conteo
     *
     * @return integer 
     */
    public function getConteo()
    {
        return $this->conteo;
    }

    /**
     * Set localizacion
     *
     * @param string $localizacion
     * @return Recomendaciones
     */
    public function setLocalizacion($localizacion)
    {
        $this->localizacion = $localizacion;
    
        return $this;
    }

    /**
     * Get localizacion
     *
     * @return string 
     */
    public function getLocalizacion()
    {
        return $this->localizacion;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Recomendaciones
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    
        return $this;
    }

    /**
     * Get codigo
     *
     * @return string 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     * @return Recomendaciones
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    
        return $this;
    }

    /**
     * Get pdf
     *
     * @return string 
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set referencia
     *
     * @param \Ultra\CalidadBundle\Entity\AuditoriaVigilancia $referencia
     * @return Recomendaciones
     */
    public function setReferencia(\Ultra\CalidadBundle\Entity\AuditoriaVigilancia $referencia = null)
    {
        $this->referencia = $referencia;

        return $this;
    }

    /**
     * Get referencia
     *
     * @return \Ultra\CalidadBundle\Entity\AuditoriaVigilancia
     */
    public function getReferencia()
    {
        return $this->referencia;
    }

    /**
     * Set estado
     *
     * @param string|Condicion $estado
     * @return Recomendaciones
     */
    public function setEstado(\Ultra\ControlDocumentoBundle\Entity\Condicion $estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return \Ultra\ControlDocumentoBundle\Entity\Condicion
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set file
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return UploadedFile
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        if (is_file($this->getAbsolutePath())) {
            $this->temp = $this->getAbsolutePath();
            //$this->path = null;
        } else {
            $this->pdf = 'initial';
        }
    }

    /**
     * Get file
     *
     * @return \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     *
     * @ORM\PrePersist()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            //[clave]-r[revision].pdf
            $name = sprintf(
                '%s.%s',
                $this->getClave(),
                $this->getFile()->guessExtension()
            );

            $this->setPdf($name);
            $this->setHash();


        }
        $this->creado = new \DateTime();

    }

    /**
     *
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if (null != $this->getFile()) {
            //[clave]-r[revision].pdf
            $name = sprintf(
                '%s.%s',
                $this->getClave(),
                $this->getFile()->guessExtension()
            );

            $this->setPdf($name);
            $this->setHash();
            $this->actualizado = new \DateTime();
        }

        if(null != $this->getTemporaryPath()){
            $f = new Filesystem();

            $f->copy(
                $this->getTemporaryPath(),
                $this->getAbsolutePath(),true
            );
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {

        if(null == $this->getFile())
        {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->pdf
        );

        $this->temp = null;
        $this->file = null;
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->pdf
            ? null
            : $this->getUploadRootDir().'/'.$this->pdf;
    }

    public function getUploadRootDir()
    {
        return '/var/sad/'.$this->getUploadDir().'/'.$this->getEstado()->getDescripcion();
    }

    public function getUploadDir()
    {
        return 'uploads/calidad/Recomendacion';
    }

    public function temporaryPath($path){
        $this->path = $path;
    }

    public function getTemporaryPath(){
        return $this->path;
    }

    public function setTemp($path)
    {
        $this->temp = $path;
    }

    public function getTemp(){
        return $this->temp;
    }

    /**
     * Set creado
     *
     * @param \DateTime $creado
     * @return Recomendaciones
     */
    public function setCreado($creado)
    {
        $this->creado = $creado;

        return $this;
    }

    /**
     * Get creado
     *
     * @return \DateTime
     */
    public function getCreado()
    {
        return $this->creado;
    }

    /**
     * Set actualizado
     *
     * @param \DateTime $actualizado
     * @return Recomendaciones
     */
    public function setActualizado($actualizado)
    {
        $this->actualizado = $actualizado;

        return $this;
    }

    /**
     * Get actualizado
     *
     * @return \DateTime
     */
    public function getActualizado()
    {
        return $this->actualizado;
    }

    /**
     * @param $path
     * @param Condicion $estado
     * @return Recomendaciones
     */
    public function move($path, Condicion $estado)
    {
        if($this->isChange($estado))
        {
            $fs = new Filesystem();

            if($fs->exists($path))
            {
                try{
                    $fs->copy($path, $this->getAbsolutePath(), true);
                    //$fs->remove($path);
                }
                catch (FileNotFoundException $e){
                    return $e->getTrace();
                }
                catch (IOException $e){
                    return $e->getTrace();
                }
            }
            $fs = null;
        }

        return $this;
    }

    /**
     * Return true if the condition state change
     *
     * @param Condicion $state
     *
     * @return bool
     */
    private function isChange(Condicion $state)
    {
        return $this->getEstado()->getId() !== $state->getId();
    }

    /**
     * Set hash
     * @return Recomendaciones
     * @internal param string $hash
     */
    public function setHash()
    {
        $fileName = $this->getFile()->getRealPath();
        $this->hash = sha1_file($fileName);
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Get string representation
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getClave();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->requirements = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->currentActivities = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add requirements
     *
     * @param \Ultra\CalidadBundle\Entity\Requirement $requirements
     * @return Recomendaciones
     */
    public function addRequirement(\Ultra\CalidadBundle\Entity\Requirement $requirements)
    {
        $this->requirements[] = $requirements;
    
        return $this;
    }

    /**
     * Remove requirements
     *
     * @param \Ultra\CalidadBundle\Entity\Requirement $requirements
     */
    public function removeRequirement(\Ultra\CalidadBundle\Entity\Requirement $requirements)
    {
        $this->requirements->removeElement($requirements);
    }

    /**
     * Get requirements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * Set actionPlan
     *
     * @param string $actionPlan
     * @return Recomendaciones
     */
    public function setActionPlan($actionPlan)
    {
        $this->actionPlan = $actionPlan;
    
        return $this;
    }

    /**
     * Get actionPlan
     *
     * @return string 
     */
    public function getActionPlan()
    {
        return $this->actionPlan;
    }

    /**
     * Add activities
     *
     * @param \Ultra\CalidadBundle\Entity\Activity $activities
     * @return Recomendaciones
     */
    public function addActivity(\Ultra\CalidadBundle\Entity\Activity $activities)
    {
        $activities->setRecommendation($this);

        $this->activities[] = $activities;
    
        return $this;
    }

    /**
     * Remove activities
     *
     * @param \Ultra\CalidadBundle\Entity\Activity $activities
     */
    public function removeActivity(\Ultra\CalidadBundle\Entity\Activity $activities)
    {
        $this->activities->removeElement($activities);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Check if has Activities
     *
     * @return bool true if has activities false otherwise
     */
    public function hasActivities()
    {
        return !$this->getActivities()->isEmpty();
    }

    public function contains(Activity $activity)
    {
        return (false === $this->activities->contains($activity)) ? false : true;
    }

    public function makeCurrentActivities()
    {
        if ($this->hasActivities()) {

            foreach ($this->getActivities() as $activity) {
                $this->currentActivities[] = $activity;
            }

            return $this->currentActivities;
        }

        return array();
    }
}